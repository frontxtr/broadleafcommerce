-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 01, 2018 at 09:02 AM
-- Server version: 5.7.20-enterprise-commercial-advanced-log
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `broadleaf`
--

-- --------------------------------------------------------

--
-- Table structure for table `blc_additional_offer_info`
--

CREATE TABLE `blc_additional_offer_info` (
  `BLC_ORDER_ORDER_ID` bigint(20) NOT NULL,
  `OFFER_INFO_ID` bigint(20) NOT NULL,
  `OFFER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_address`
--

CREATE TABLE `blc_address` (
  `ADDRESS_ID` bigint(20) NOT NULL,
  `ADDRESS_LINE1` varchar(255) NOT NULL,
  `ADDRESS_LINE2` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE3` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) NOT NULL,
  `COMPANY_NAME` varchar(255) DEFAULT NULL,
  `COUNTY` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `FAX` varchar(255) DEFAULT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `FULL_NAME` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` tinyint(1) DEFAULT NULL,
  `IS_BUSINESS` tinyint(1) DEFAULT NULL,
  `IS_DEFAULT` tinyint(1) DEFAULT NULL,
  `IS_MAILING` tinyint(1) DEFAULT NULL,
  `IS_STREET` tinyint(1) DEFAULT NULL,
  `ISO_COUNTRY_SUB` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `POSTAL_CODE` varchar(255) DEFAULT NULL,
  `PRIMARY_PHONE` varchar(255) DEFAULT NULL,
  `SECONDARY_PHONE` varchar(255) DEFAULT NULL,
  `STANDARDIZED` tinyint(1) DEFAULT NULL,
  `SUB_STATE_PROV_REG` varchar(255) DEFAULT NULL,
  `TOKENIZED_ADDRESS` varchar(255) DEFAULT NULL,
  `VERIFICATION_LEVEL` varchar(255) DEFAULT NULL,
  `ZIP_FOUR` varchar(255) DEFAULT NULL,
  `COUNTRY` varchar(255) DEFAULT NULL,
  `ISO_COUNTRY_ALPHA2` varchar(255) DEFAULT NULL,
  `PHONE_FAX_ID` bigint(20) DEFAULT NULL,
  `PHONE_PRIMARY_ID` bigint(20) DEFAULT NULL,
  `PHONE_SECONDARY_ID` bigint(20) DEFAULT NULL,
  `STATE_PROV_REGION` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_address`
--

INSERT INTO `blc_address` (`ADDRESS_ID`, `ADDRESS_LINE1`, `ADDRESS_LINE2`, `ADDRESS_LINE3`, `CITY`, `COMPANY_NAME`, `COUNTY`, `EMAIL_ADDRESS`, `FAX`, `FIRST_NAME`, `FULL_NAME`, `IS_ACTIVE`, `IS_BUSINESS`, `IS_DEFAULT`, `IS_MAILING`, `IS_STREET`, `ISO_COUNTRY_SUB`, `LAST_NAME`, `POSTAL_CODE`, `PRIMARY_PHONE`, `SECONDARY_PHONE`, `STANDARDIZED`, `SUB_STATE_PROV_REG`, `TOKENIZED_ADDRESS`, `VERIFICATION_LEVEL`, `ZIP_FOUR`, `COUNTRY`, `ISO_COUNTRY_ALPHA2`, `PHONE_FAX_ID`, `PHONE_PRIMARY_ID`, `PHONE_SECONDARY_ID`, `STATE_PROV_REGION`) VALUES
(1, 'fdsfsdf', 'dsfsdfsdfsdf', NULL, 'ffsdfdsf', NULL, NULL, NULL, NULL, 'Can', NULL, 1, 0, 0, 0, 0, 'US-AL', 'Bayraktar', '345435', NULL, NULL, 0, 'AL', NULL, NULL, NULL, NULL, 'US', NULL, 1, NULL, NULL),
(2, 'fdsfsdf', 'dsfsdfsdfsdf', NULL, 'ffsdfdsf', NULL, NULL, NULL, NULL, 'Can', NULL, 1, 0, 0, 0, 0, 'US-AL', 'Bayraktar', '345435', NULL, NULL, 0, 'AL', NULL, NULL, NULL, NULL, 'US', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_module`
--

CREATE TABLE `blc_admin_module` (
  `ADMIN_MODULE_ID` bigint(20) NOT NULL,
  `DISPLAY_ORDER` int(11) DEFAULT NULL,
  `ICON` varchar(255) DEFAULT NULL,
  `MODULE_KEY` varchar(255) NOT NULL,
  `NAME` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_admin_module`
--

INSERT INTO `blc_admin_module` (`ADMIN_MODULE_ID`, `DISPLAY_ORDER`, `ICON`, `MODULE_KEY`, `NAME`) VALUES
(-9, 250, 'blc-icon-inventory', 'BLCInventory', 'Inventory'),
(-8, 150, 'fa fa-usd', 'BLCPricing', 'Pricing'),
(-7, 500, 'blc-icon-site-updates', 'BLCWorkflow', 'Site Updates'),
(-6, 400, 'blc-icon-design', 'BLCDesign', 'Design'),
(-5, 700, 'blc-icon-settings', 'BLCModuleConfiguration', 'Settings'),
(-4, 600, 'blc-icon-security', 'BLCOpenAdmin', 'Security'),
(-3, 550, 'blc-icon-customer-care', 'BLCCustomerCare', 'Customer Care'),
(-2, 200, 'blc-icon-content', 'BLCContentManagement', 'Content'),
(-1, 100, 'blc-icon-catalog', 'BLCMerchandising', 'Catalog');

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_password_token`
--

CREATE TABLE `blc_admin_password_token` (
  `PASSWORD_TOKEN` varchar(255) NOT NULL,
  `ADMIN_USER_ID` bigint(20) NOT NULL,
  `CREATE_DATE` datetime NOT NULL,
  `TOKEN_USED_DATE` datetime DEFAULT NULL,
  `TOKEN_USED_FLAG` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_permission`
--

CREATE TABLE `blc_admin_permission` (
  `ADMIN_PERMISSION_ID` bigint(20) NOT NULL,
  `DESCRIPTION` varchar(255) NOT NULL,
  `IS_FRIENDLY` tinyint(1) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  `PERMISSION_TYPE` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_admin_permission`
--

INSERT INTO `blc_admin_permission` (`ADMIN_PERMISSION_ID`, `DESCRIPTION`, `IS_FRIENDLY`, `NAME`, `PERMISSION_TYPE`) VALUES
(-27029, 'All Field', 1, 'PERMISSION_ALL_FIELD', 'ALL'),
(-27028, 'Read Field', 1, 'PERMISSION_READ_FIELD', 'READ'),
(-27027, 'Delete Field', 1, 'PERMISSION_DELETE_FIELD', 'DELETE'),
(-27026, 'Update Field', 1, 'PERMISSION_UPDATE_FIELD', 'UPDATE'),
(-27025, 'Create Field', 1, 'PERMISSION_CREATE_FIELD', 'CREATE'),
(-27024, 'All SearchFacet', NULL, 'PERMISSION_ALL_SEARCHFACET', 'ALL'),
(-27023, 'Read SearchFacet', NULL, 'PERMISSION_READ_SEARCHFACET', 'READ'),
(-27022, 'Delete SearchFacet', 1, 'PERMISSION_DELETE_SEARCHFACET', 'DELETE'),
(-27021, 'Update SearchFacet', 1, 'PERMISSION_UPDATE_SEARCHFACET', 'UPDATE'),
(-27020, 'Create SearchFacet', 1, 'PERMISSION_CREATE_SEARCHFACET', 'CREATE'),
(-27019, 'All SearchRedirect', NULL, 'PERMISSION_ALL_SEARCHREDIRECT', 'ALL'),
(-27018, 'Read SearchRedirect', NULL, 'PERMISSION_READ_SEARCHREDIRECT', 'READ'),
(-27017, 'Delete SearchRedirect', NULL, 'PERMISSION_DELETE_SEARCHREDIRECT', 'DELETE'),
(-27016, 'Update SearchRedirect', NULL, 'PERMISSION_UPDATE_SEARCHREDIRECT', 'UPDATE'),
(-27015, 'Create SearchRedirect', NULL, 'PERMISSION_CREATE_SEARCHREDIRECT', 'CREATE'),
(-27014, 'Delete Fulfillment Group', 1, 'PERMISSION_DELETE_FULFILLMENT_GROUP', 'DELETE'),
(-27013, 'Update Fulfillment Group', 1, 'PERMISSION_UPDATE_FULFILLMENT_GROUP', 'UPDATE'),
(-27012, 'Create Fulfillment Group', 1, 'PERMISSION_CREATE_FULFILLMENT_GROUP', 'CREATE'),
(-27011, 'Delete Order Item', 1, 'PERMISSION_DELETE_ORDER_ITEM', 'DELETE'),
(-27010, 'Update Order Item', 1, 'PERMISSION_UPDATE_ORDER_ITEM', 'UPDATE'),
(-27009, 'Create Order Item', 1, 'PERMISSION_CREATE_ORDER_ITEM', 'CREATE'),
(-27008, 'All Structured Content Type', 1, 'PERMISSION_ALL_STRUCTURED_CONTENT_TYPE', 'ALL'),
(-27007, 'Read Structured Content Type', 1, 'PERMISSION_READ_STRUCTURED_CONTENT_TYPE', 'READ'),
(-27006, 'Delete Structured Content Type', 1, 'PERMISSION_DELETE_STRUCTURED_CONTENT_TYPE', 'DELETE'),
(-27005, 'Update Structured Content Type', NULL, 'PERMISSION_UPDATE_STRUCTURED_CONTENT_TYPE', 'UPDATE'),
(-27004, 'Create Structured Content Type', 1, 'PERMISSION_CREATE_STRUCTURED_CONTENT_TYPE', 'CREATE'),
(-27003, 'Maintain Menus', 1, 'PERMISSION_MENU', 'ALL'),
(-27002, 'View Menus', 1, 'PERMISSION_MENU', 'READ'),
(-27001, 'All Menu', 1, 'PERMISSION_ALL_MENU', 'ALL'),
(-27000, 'Read Menu', 1, 'PERMISSION_READ_MENU', 'READ'),
(-200, 'Read Page Template', 1, 'PERMISSION_READ_PAGE_TEMPLATE', 'ALL'),
(-186, 'Maintain Order', 1, 'PERMISSION_ALL_ORDER', 'ALL'),
(-185, 'View Order', 1, 'PERMISSION_READ_ORDER', 'READ'),
(-181, 'Maintain Promotion Messages', 1, 'PERMISSION_PROMOTION_MESSAGE', 'ALL'),
(-180, 'View Promotion Messages', 1, 'PERMISSION_PROMOTION_MESSAGE', 'READ'),
(-161, 'Maintain Field Definitions', 1, 'PERMISSION_FLDDEF_ALL', 'ALL'),
(-160, 'View Field Definitions', 1, 'PERMISSION_FLDDEF_VIEW', 'READ'),
(-151, 'Maintain Permissions', 1, 'PERMISSION_PERM_ALL', 'ALL'),
(-150, 'View Permissions', 1, 'PERMISSION_PERM_VIEW', 'READ'),
(-141, 'Maintain Roles', 1, 'PERMISSION_ROLE_ALL', 'ALL'),
(-140, 'View Roles', 1, 'PERMISSION_ROLE_VIEW', 'READ'),
(-135, 'Maintain Sku', 1, 'PERMISSION_SKU', 'ALL'),
(-134, 'View Sku', 1, 'PERMISSION_SKU', 'READ'),
(-131, 'Maintain Translations', 1, 'PERMISSION_TRANSLATION', 'ALL'),
(-130, 'View Translations', 1, 'PERMISSION_TRANSLATION', 'READ'),
(-129, 'Maintain Enumeration', 1, 'PERMISSION_ENUMERATION', 'ALL'),
(-128, 'View Enumeration', 1, 'PERMISSION_ENUMERATION', 'READ'),
(-127, 'Maintain Module Configurations', 1, 'PERMISSION_MODULECONFIGURATION', 'ALL'),
(-126, 'View Module Configurations', 1, 'PERMISSION_MODULECONFIGURATION', 'READ'),
(-125, 'Maintain Search Redirect', 1, 'PERMISSION_SEARCHREDIRECT', 'ALL'),
(-124, 'View Search Redirect', 1, 'PERMISSION_SEARCHREDIRECT', 'READ'),
(-121, 'Maintain Users', 1, 'PERMISSION_USER', 'ALL'),
(-120, 'View Users', 1, 'PERMISSION_USER', 'READ'),
(-119, 'Maintain Customers', 1, 'PERMISSION_CUSTOMER', 'ALL'),
(-118, 'View Customers', 1, 'PERMISSION_CUSTOMER', 'READ'),
(-115, 'Maintain URL Redirects', 1, 'PERMISSION_URLREDIRECT', 'ALL'),
(-114, 'View URL Redirects', 1, 'PERMISSION_URLREDIRECT', 'READ'),
(-113, 'All System Property', 1, 'PERMISSION_ALL_SYSTEM_PROPERTY', 'ALL'),
(-112, 'Read System Property', 1, 'PERMISSION_READ_SYSTEM_PROPERTY', 'READ'),
(-111, 'Maintain Assets', 1, 'PERMISSION_ASSET', 'ALL'),
(-110, 'View Assets', 1, 'PERMISSION_ASSET', 'READ'),
(-109, 'Maintain Pages', 1, 'PERMISSION_PAGE', 'ALL'),
(-108, 'View Pages', 1, 'PERMISSION_PAGE', 'READ'),
(-107, 'Maintain Offers', 1, 'PERMISSION_OFFER', 'ALL'),
(-106, 'View Offers', 1, 'PERMISSION_OFFER', 'READ'),
(-105, 'Maintain Product Options', 1, 'PERMISSION_PRODUCTOPTIONS', 'ALL'),
(-104, 'View Product Options', 1, 'PERMISSION_PRODUCTOPTIONS', 'READ'),
(-103, 'Maintain Products', 1, 'PERMISSION_PRODUCT', 'ALL'),
(-102, 'View Products', 1, 'PERMISSION_PRODUCT', 'READ'),
(-101, 'Maintain Categories', 1, 'PERMISSION_CATEGORY', 'ALL'),
(-100, 'View Categories', 1, 'PERMISSION_CATEGORY', 'READ'),
(-93, 'All Configuration', 1, 'PERMISSION_ALL_MODULECONFIGURATION', 'ALL'),
(-92, 'Read Configuration', 1, 'PERMISSION_READ_MODULECONFIGURATION', 'READ'),
(-91, 'Delete Configuration', 1, 'PERMISSION_DELETE_MODULECONFIGURATION', 'DELETE'),
(-90, 'Update Configuration', 1, 'PERMISSION_UPDATE_MODULECONFIGURATION', 'UPDATE'),
(-89, 'Create Configuration', 1, 'PERMISSION_CREATE_MODULECONFIGURATION', 'CREATE'),
(-61, 'All Structured Content', 1, 'PERMISSION_ALL_STRUCTURED_CONTENT', 'ALL'),
(-60, 'Read Structured Content', 1, 'PERMISSION_READ_STRUCTURED_CONTENT', 'ALL'),
(-59, 'Delete Structured Content', 1, 'PERMISSION_DELETE_STRUCTURED_CONTENT', 'DELETE'),
(-58, 'Update Structured Content', 1, 'PERMISSION_UPDATE_STRUCTURED_CONTENT', 'UPDATE'),
(-57, 'Create Structured Content', 1, 'PERMISSION_CREATE_STRUCTURED_CONTENT', 'CREATE'),
(-55, 'All ISO COUNTRIES', 1, 'PERMISSION_ALL_ISO_COUNTRIES', 'ALL'),
(-54, 'Read ISO Countries', 1, 'PERMISSION_READ_ISO_COUNTRIES', 'READ'),
(-53, 'All Catalog Permissions', 1, 'PERMISSION_ALL_CATALOG_PERMS', 'ALL'),
(-52, 'Read Catalog Permissions', 1, 'PERMISSION_READ_CATALOG_PERMS', 'READ'),
(-51, 'All Field Definitions', 1, 'PERMISSION_ALL_FIELD_DEFS', 'ALL'),
(-50, 'Read Field Definitions', 1, 'PERMISSION_READ_FIELD_DEFS', 'READ'),
(-49, 'All Admin Permissions', 1, 'PERMISSION_ALL_ADMIN_PERMS', 'ALL'),
(-48, 'Read Admin Permissions', 1, 'PERMISSION_READ_ADMIN_PERMS', 'READ'),
(-47, 'All Admin Roles', 1, 'PERMISSION_ALL_ADMIN_ROLES', 'ALL'),
(-46, 'Read Admin Roles', 1, 'PERMISSION_READ_ADMIN_ROLES', 'READ'),
(-45, 'All System Property', 1, 'PERMISSION_ALL_SYSTEM_PROPERTY', 'ALL'),
(-44, 'Read System Property', 1, 'PERMISSION_READ_SYSTEM_PROPERTY', 'READ'),
(-43, 'All Site Map Gen Configuration', 1, 'PERMISSION_ALL_SITE_MAP_GEN_CONFIG', 'ALL'),
(-42, 'Read Site Map Gen Configuration', 1, 'PERMISSION_READ_SITE_MAP_GEN_CONFIG', 'READ'),
(-41, 'All Translation', 1, 'PERMISSION_ALL_TRANSLATION', 'ALL'),
(-40, 'Read Translation', 1, 'PERMISSION_READ_TRANSLATION', 'READ'),
(-39, 'All Enumeration', 1, 'PERMISSION_ALL_ENUMERATION', 'ALL'),
(-38, 'Read Enumeration', 1, 'PERMISSION_READ_ENUMERATION', 'READ'),
(-37, 'All Configuration', 1, 'PERMISSION_ALL_MODULECONFIGURATION', 'ALL'),
(-36, 'Read Configuration', 1, 'PERMISSION_READ_MODULECONFIGURATION', 'READ'),
(-35, 'All Currency', 1, 'PERMISSION_ALL_CURRENCY', 'ALL'),
(-34, 'Read Currency', 1, 'PERMISSION_READ_CURRENCY', 'READ'),
(-33, 'All SearchFacet', 1, 'PERMISSION_ALL_SEARCHFACET', 'ALL'),
(-32, 'Read SearchFacet', 1, 'PERMISSION_READ_SEARCHFACET', 'READ'),
(-31, 'All SearchRedirect', 1, 'PERMISSION_ALL_SEARCHREDIRECT', 'ALL'),
(-30, 'Read SearchRedirect', 1, 'PERMISSION_READ_SEARCHREDIRECT', 'READ'),
(-29, 'All URLHandler', 1, 'PERMISSION_ALL_URLHANDLER', 'ALL'),
(-28, 'Read URLHandler', 1, 'PERMISSION_READ_URLHANDLER', 'READ'),
(-27, 'All Admin User', 1, 'PERMISSION_ALL_ADMIN_USER', 'ALL'),
(-26, 'Read Admin User', 1, 'PERMISSION_READ_ADMIN_USER', 'READ'),
(-23, 'All Asset', 1, 'PERMISSION_ALL_ASSET', 'ALL'),
(-22, 'Read Asset', 1, 'PERMISSION_READ_ASSET', 'READ'),
(-21, 'All Page', 1, 'PERMISSION_ALL_PAGE', 'ALL'),
(-20, 'Read Page', 1, 'PERMISSION_READ_PAGE', 'READ'),
(-19, 'All Customer', 1, 'PERMISSION_ALL_CUSTOMER', 'ALL'),
(-18, 'Read Customer', 1, 'PERMISSION_READ_CUSTOMER', 'READ'),
(-17, 'All Order Item', 1, 'PERMISSION_ALL_ORDER_ITEM', 'ALL'),
(-16, 'Read Order Item', 1, 'PERMISSION_READ_ORDER_ITEM', 'READ'),
(-15, 'All Fulfillment Group', 1, 'PERMISSION_ALL_FULFILLMENT_GROUP', 'ALL'),
(-14, 'Read Fulfillment Group', 1, 'PERMISSION_READ_FULFILLMENT_GROUP', 'READ'),
(-13, 'All Order', 1, 'PERMISSION_ALL_ORDER', 'ALL'),
(-12, 'Read Order', 1, 'PERMISSION_READ_ORDER', 'READ'),
(-11, 'All Promotion', 1, 'PERMISSION_ALL_PROMOTION', 'ALL'),
(-10, 'Read Promotion', 1, 'PERMISSION_READ_PROMOTION', 'READ'),
(-9, 'All Sku', 1, 'PERMISSION_ALL_SKU', 'ALL'),
(-8, 'Read Sku', 1, 'PERMISSION_READ_SKU', 'READ'),
(-7, 'All Product Option', 1, 'PERMISSION_ALL_PRODUCT_OPTION', 'ALL'),
(-6, 'Read Product Option', 1, 'PERMISSION_READ_PRODUCT_OPTION', 'READ'),
(-5, 'All Product', 1, 'PERMISSION_ALL_PRODUCT', 'ALL'),
(-4, 'Read Product', 1, 'PERMISSION_READ_PRODUCT', 'READ'),
(-3, 'All Category', 1, 'PERMISSION_ALL_CATEGORY', 'ALL'),
(-2, 'Read Category', 1, 'PERMISSION_READ_CATEGORY', 'READ'),
(-1, 'Default Permission', 1, 'PERMISSION_OTHER_DEFAULT', 'OTHER');

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_permission_entity`
--

CREATE TABLE `blc_admin_permission_entity` (
  `ADMIN_PERMISSION_ENTITY_ID` bigint(20) NOT NULL,
  `CEILING_ENTITY` varchar(255) NOT NULL,
  `ADMIN_PERMISSION_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_admin_permission_entity`
--

INSERT INTO `blc_admin_permission_entity` (`ADMIN_PERMISSION_ENTITY_ID`, `CEILING_ENTITY`, `ADMIN_PERMISSION_ID`) VALUES
(-27088, 'org.broadleafcommerce.core.catalog.domain.SkuBundleItemImpl', -9),
(-27087, 'org.broadleafcommerce.core.catalog.domain.SkuBundleItemImpl', -20),
(-27086, 'org.broadleafcommerce.core.catalog.domain.SkuBundleItemImpl', -19),
(-27085, 'org.broadleafcommerce.core.catalog.domain.SkuBundleItemImpl', -18),
(-27084, 'org.broadleafcommerce.core.catalog.domain.SkuBundleItemImpl', -17),
(-27083, 'org.broadleafcommerce.core.search.domain.Field', -27029),
(-27082, 'org.broadleafcommerce.core.search.domain.Field', -27028),
(-27081, 'org.broadleafcommerce.core.search.domain.Field', -27027),
(-27080, 'org.broadleafcommerce.core.search.domain.Field', -27026),
(-27079, 'org.broadleafcommerce.core.search.domain.Field', -27025),
(-27078, 'org.broadleafcommerce.core.search.domain.CategoryExcludedSearchFacet', -27024),
(-27077, 'org.broadleafcommerce.core.search.domain.CategoryExcludedSearchFacet', -27023),
(-27076, 'org.broadleafcommerce.core.search.domain.CategoryExcludedSearchFacet', -27022),
(-27075, 'org.broadleafcommerce.core.search.domain.CategoryExcludedSearchFacet', -27021),
(-27074, 'org.broadleafcommerce.core.search.domain.CategoryExcludedSearchFacet', -27020),
(-27073, 'org.broadleafcommerce.core.search.domain.SearchFacetRange', -27024),
(-27072, 'org.broadleafcommerce.core.search.domain.SearchFacetRange', -27023),
(-27071, 'org.broadleafcommerce.core.search.domain.SearchFacetRange', -27022),
(-27070, 'org.broadleafcommerce.core.search.domain.SearchFacetRange', -27021),
(-27069, 'org.broadleafcommerce.core.search.domain.SearchFacetRange', -27020),
(-27068, 'org.broadleafcommerce.core.search.domain.CategorySearchFacet', -27024),
(-27067, 'org.broadleafcommerce.core.search.domain.CategorySearchFacet', -27023),
(-27066, 'org.broadleafcommerce.core.search.domain.CategorySearchFacet', -27022),
(-27065, 'org.broadleafcommerce.core.search.domain.CategorySearchFacet', -27021),
(-27064, 'org.broadleafcommerce.core.search.domain.CategorySearchFacet', -27020),
(-27063, 'org.broadleafcommerce.core.search.domain.Field', -27024),
(-27062, 'org.broadleafcommerce.core.search.domain.Field', -27023),
(-27061, 'org.broadleafcommerce.core.search.domain.Field', -27022),
(-27060, 'org.broadleafcommerce.core.search.domain.Field', -27021),
(-27059, 'org.broadleafcommerce.core.search.domain.Field', -27020),
(-27058, 'org.broadleafcommerce.core.search.domain.SearchFacet', -27024),
(-27057, 'org.broadleafcommerce.core.search.domain.SearchFacet', -27023),
(-27056, 'org.broadleafcommerce.core.search.domain.SearchFacet', -27022),
(-27055, 'org.broadleafcommerce.core.search.domain.SearchFacet', -27021),
(-27054, 'org.broadleafcommerce.core.search.domain.SearchFacet', -27020),
(-27053, 'org.broadleafcommerce.core.search.redirect.domain.SearchRedirect', -27019),
(-27052, 'org.broadleafcommerce.core.search.redirect.domain.SearchRedirect', -27018),
(-27051, 'org.broadleafcommerce.core.search.redirect.domain.SearchRedirect', -27017),
(-27050, 'org.broadleafcommerce.core.search.redirect.domain.SearchRedirect', -27016),
(-27049, 'org.broadleafcommerce.core.search.redirect.domain.SearchRedirect', -27015),
(-27048, 'org.broadleafcommerce.core.order.domain.FulfillmentGroupItemImpl', -27014),
(-27047, 'org.broadleafcommerce.core.order.domain.FulfillmentGroupItemImpl', -27013),
(-27046, 'org.broadleafcommerce.core.order.domain.FulfillmentGroupItemImpl', -27012),
(-27045, 'org.broadleafcommerce.core.order.domain.FulfillmentGroupFeeImpl', -27014),
(-27044, 'org.broadleafcommerce.core.order.domain.FulfillmentGroupFeeImpl', -27013),
(-27043, 'org.broadleafcommerce.core.order.domain.FulfillmentGroupFeeImpl', -27012),
(-27042, 'org.broadleafcommerce.core.offer.domain.FulfillmentGroupAdjustment', -27014),
(-27041, 'org.broadleafcommerce.core.offer.domain.FulfillmentGroupAdjustment', -27013),
(-27040, 'org.broadleafcommerce.core.offer.domain.FulfillmentGroupAdjustment', -27012),
(-27039, 'org.broadleafcommerce.core.order.domain.FulfillmentGroup', -27014),
(-27038, 'org.broadleafcommerce.core.order.domain.FulfillmentGroup', -27013),
(-27037, 'org.broadleafcommerce.core.order.domain.FulfillmentGroup', -27012),
(-27036, 'org.broadleafcommerce.core.order.domain.BundleOrderItemFeePriceImpl', -27011),
(-27035, 'org.broadleafcommerce.core.order.domain.BundleOrderItemFeePriceImpl', -27010),
(-27034, 'org.broadleafcommerce.core.order.domain.BundleOrderItemFeePriceImpl', -27009),
(-27033, 'org.broadleafcommerce.core.order.domain.OrderItemPriceDetailImpl', -27011),
(-27032, 'org.broadleafcommerce.core.order.domain.OrderItemPriceDetailImpl', -27010),
(-27031, 'org.broadleafcommerce.core.order.domain.OrderItemPriceDetailImpl', -27009),
(-27030, 'org.broadleafcommerce.core.offer.domain.OrderItemPriceDetailAdjustmentImpl', -27011),
(-27029, 'org.broadleafcommerce.core.offer.domain.OrderItemPriceDetailAdjustmentImpl', -27010),
(-27028, 'org.broadleafcommerce.core.offer.domain.OrderItemPriceDetailAdjustmentImpl', -27009),
(-27027, 'org.broadleafcommerce.core.offer.domain.OrderItemAdjustment', -27011),
(-27026, 'org.broadleafcommerce.core.offer.domain.OrderItemAdjustment', -27010),
(-27025, 'org.broadleafcommerce.core.offer.domain.OrderItemAdjustment', -27009),
(-27024, 'org.broadleafcommerce.core.order.domain.DiscreteOrderItemFeePrice', -27011),
(-27023, 'org.broadleafcommerce.core.order.domain.DiscreteOrderItemFeePrice', -27010),
(-27022, 'org.broadleafcommerce.core.order.domain.DiscreteOrderItemFeePrice', -27009),
(-27021, 'org.broadleafcommerce.core.order.domain.OrderItem', -27011),
(-27020, 'org.broadleafcommerce.core.order.domain.OrderItem', -27010),
(-27019, 'org.broadleafcommerce.core.order.domain.OrderItem', -27009),
(-27018, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', -27008),
(-27017, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', -27007),
(-27016, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', -27006),
(-27015, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', -27005),
(-27014, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', -27004),
(-27013, 'org.broadleafcommerce.cms.structure.domain.StructuredContentTypeImpl', -27008),
(-27012, 'org.broadleafcommerce.cms.structure.domain.StructuredContentTypeImpl', -27007),
(-27011, 'org.broadleafcommerce.cms.structure.domain.StructuredContentTypeImpl', -27006),
(-27010, 'org.broadleafcommerce.cms.structure.domain.StructuredContentTypeImpl', -27005),
(-27009, 'org.broadleafcommerce.cms.structure.domain.StructuredContentTypeImpl', -27004),
(-27008, 'org.broadleafcommerce.cms.structure.domain.StructuredContentTypeImpl', -57),
(-27007, 'org.broadleafcommerce.cms.structure.domain.StructuredContentTypeImpl', -61),
(-27006, 'org.broadleafcommerce.cms.structure.domain.StructuredContentTypeImpl', -60),
(-27005, 'org.broadleafcommerce.cms.structure.domain.StructuredContentTypeImpl', -59),
(-27004, 'org.broadleafcommerce.cms.structure.domain.StructuredContentTypeImpl', -58),
(-27003, 'org.broadleafcommerce.menu.domain.MenuItem', -27001),
(-27002, 'org.broadleafcommerce.menu.domain.MenuItem', -27000),
(-27001, 'org.broadleafcommerce.menu.domain.Menu', -27001),
(-27000, 'org.broadleafcommerce.menu.domain.Menu', -27000),
(-1011, 'org.broadleafcommerce.common.i18n.domain.ISOCountry', -55),
(-1010, 'org.broadleafcommerce.common.i18n.domain.ISOCountry', -54),
(-1003, 'org.broadleafcommerce.common.site.domain.Site', -53),
(-1002, 'org.broadleafcommerce.common.site.domain.Site', -52),
(-1001, 'org.broadleafcommerce.common.site.domain.Catalog', -53),
(-1000, 'org.broadleafcommerce.common.site.domain.Catalog', -52),
(-991, 'org.broadleafcommerce.cms.field.domain.FieldDefinition', -51),
(-990, 'org.broadleafcommerce.cms.field.domain.FieldDefinition', -50),
(-983, 'org.broadleafcommerce.openadmin.server.security.domain.AdminPermissionQualifiedEntity', -48),
(-982, 'org.broadleafcommerce.openadmin.server.security.domain.AdminPermissionQualifiedEntity', -49),
(-981, 'org.broadleafcommerce.openadmin.server.security.domain.AdminPermission', -48),
(-980, 'org.broadleafcommerce.openadmin.server.security.domain.AdminPermission', -49),
(-971, 'org.broadleafcommerce.openadmin.server.security.domain.AdminRole', -47),
(-970, 'org.broadleafcommerce.openadmin.server.security.domain.AdminRole', -46),
(-964, 'org.broadleafcommerce.common.config.domain.SystemProperty', -113),
(-963, 'org.broadleafcommerce.common.config.domain.SystemProperty', -112),
(-962, 'org.broadleafcommerce.common.config.domain.SystemProperty', -111),
(-961, 'org.broadleafcommerce.common.config.domain.SystemProperty', -45),
(-960, 'org.broadleafcommerce.common.config.domain.SystemProperty', -44),
(-953, 'org.broadleafcommerce.common.sitemap.domain.SiteMapURLEntry', -43),
(-952, 'org.broadleafcommerce.common.sitemap.domain.SiteMapURLEntry', -42),
(-951, 'org.broadleafcommerce.common.sitemap.domain.SiteMapGeneratorConfiguration', -43),
(-950, 'org.broadleafcommerce.common.sitemap.domain.SiteMapGeneratorConfiguration', -42),
(-911, 'org.broadleafcommerce.common.i18n.domain.Translation', -41),
(-910, 'org.broadleafcommerce.common.i18n.domain.Translation', -40),
(-903, 'org.broadleafcommerce.common.enumeration.domain.DataDrivenEnumerationValue', -39),
(-902, 'org.broadleafcommerce.common.enumeration.domain.DataDrivenEnumerationValue', -38),
(-901, 'org.broadleafcommerce.common.enumeration.domain.DataDrivenEnumeration', -39),
(-900, 'org.broadleafcommerce.common.enumeration.domain.DataDrivenEnumeration', -38),
(-884, 'org.broadleafcommerce.common.config.domain.ModuleConfiguration', -93),
(-883, 'org.broadleafcommerce.common.config.domain.ModuleConfiguration', -92),
(-882, 'org.broadleafcommerce.common.config.domain.ModuleConfiguration', -91),
(-881, 'org.broadleafcommerce.common.config.domain.ModuleConfiguration', -37),
(-880, 'org.broadleafcommerce.common.config.domain.ModuleConfiguration', -36),
(-851, 'org.broadleafcommerce.common.currency.domain.BroadleafCurrency', -35),
(-850, 'org.broadleafcommerce.common.currency.domain.BroadleafCurrency', -34),
(-813, 'org.broadleafcommerce.core.search.domain.IndexFieldType', -33),
(-812, 'org.broadleafcommerce.core.search.domain.IndexFieldType', -32),
(-811, 'org.broadleafcommerce.core.search.domain.IndexField', -33),
(-810, 'org.broadleafcommerce.core.search.domain.IndexField', -32),
(-809, 'org.broadleafcommerce.core.search.domain.CategoryExcludedSearchFacet', -33),
(-808, 'org.broadleafcommerce.core.search.domain.CategoryExcludedSearchFacet', -32),
(-807, 'org.broadleafcommerce.core.search.domain.SearchFacetRange', -33),
(-806, 'org.broadleafcommerce.core.search.domain.SearchFacetRange', -32),
(-805, 'org.broadleafcommerce.core.search.domain.CategorySearchFacet', -33),
(-804, 'org.broadleafcommerce.core.search.domain.CategorySearchFacet', -32),
(-803, 'org.broadleafcommerce.core.search.domain.Field', -33),
(-802, 'org.broadleafcommerce.core.search.domain.Field', -32),
(-801, 'org.broadleafcommerce.core.search.domain.SearchFacet', -33),
(-800, 'org.broadleafcommerce.core.search.domain.SearchFacet', -32),
(-781, 'org.broadleafcommerce.core.search.redirect.domain.SearchRedirect', -31),
(-780, 'org.broadleafcommerce.core.search.redirect.domain.SearchRedirect', -30),
(-753, 'org.broadleafcommerce.common.locale.domain.Locale', -29),
(-752, 'org.broadleafcommerce.common.locale.domain.Locale', -28),
(-751, 'org.broadleafcommerce.cms.url.domain.URLHandler', -29),
(-750, 'org.broadleafcommerce.cms.url.domain.URLHandler', -28),
(-721, 'org.broadleafcommerce.openadmin.server.security.domain.AdminUser', -27),
(-720, 'org.broadleafcommerce.openadmin.server.security.domain.AdminUser', -26),
(-674, 'org.broadleafcommerce.common.locale.domain.Locale', -61),
(-673, 'org.broadleafcommerce.common.locale.domain.Locale', -60),
(-672, 'org.broadleafcommerce.common.locale.domain.Locale', -59),
(-671, 'org.broadleafcommerce.common.locale.domain.Locale', -58),
(-670, 'org.broadleafcommerce.common.locale.domain.Locale', -57),
(-669, 'org.broadleafcommerce.cms.structure.domain.StructuredContentFieldTemplate', -61),
(-668, 'org.broadleafcommerce.cms.structure.domain.StructuredContentFieldTemplate', -60),
(-667, 'org.broadleafcommerce.cms.structure.domain.StructuredContentFieldTemplate', -59),
(-666, 'org.broadleafcommerce.cms.structure.domain.StructuredContentFieldTemplate', -58),
(-665, 'org.broadleafcommerce.cms.structure.domain.StructuredContentFieldTemplate', -57),
(-664, 'org.broadleafcommerce.cms.structure.domain.StructuredContentItemCriteria', -61),
(-663, 'org.broadleafcommerce.cms.structure.domain.StructuredContentItemCriteria', -60),
(-662, 'org.broadleafcommerce.cms.structure.domain.StructuredContentItemCriteria', -59),
(-661, 'org.broadleafcommerce.cms.structure.domain.StructuredContentItemCriteria', -58),
(-660, 'org.broadleafcommerce.cms.structure.domain.StructuredContentItemCriteria', -57),
(-659, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', -60),
(-658, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', NULL),
(-657, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', -59),
(-656, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', -58),
(-655, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', -57),
(-654, 'org.broadleafcommerce.cms.structure.domain.StructuredContent', -61),
(-653, 'org.broadleafcommerce.cms.structure.domain.StructuredContent', NULL),
(-652, 'org.broadleafcommerce.cms.structure.domain.StructuredContent', -59),
(-651, 'org.broadleafcommerce.cms.structure.domain.StructuredContent', -58),
(-650, 'org.broadleafcommerce.cms.structure.domain.StructuredContent', -57),
(-603, 'org.broadleafcommerce.cms.file.domain.StaticAssetFolder', -23),
(-602, 'org.broadleafcommerce.cms.file.domain.StaticAssetFolder', -22),
(-601, 'org.broadleafcommerce.cms.file.domain.StaticAsset', -23),
(-600, 'org.broadleafcommerce.cms.file.domain.StaticAsset', -22),
(-557, 'org.broadleafcommerce.common.locale.domain.Locale', -21),
(-556, 'org.broadleafcommerce.common.locale.domain.Locale', -20),
(-555, 'org.broadleafcommerce.cms.page.domain.PageItemCriteria', -21),
(-554, 'org.broadleafcommerce.cms.page.domain.PageItemCriteria', -20),
(-552, 'org.broadleafcommerce.cms.page.domain.PageTemplate', -200),
(-551, 'org.broadleafcommerce.cms.page.domain.Page', -21),
(-550, 'org.broadleafcommerce.cms.page.domain.Page', -20),
(-514, 'org.broadleafcommerce.core.catalog.domain.CrossSaleProductImpl', -19),
(-513, 'org.broadleafcommerce.core.catalog.domain.CrossSaleProductImpl', -18),
(-512, 'org.broadleafcommerce.profile.core.domain.CustomerPhone', -19),
(-511, 'org.broadleafcommerce.profile.core.domain.CustomerPhone', -18),
(-510, 'org.broadleafcommerce.profile.core.domain.CustomerPayment', -19),
(-509, 'org.broadleafcommerce.profile.core.domain.CustomerPayment', -18),
(-508, 'org.broadleafcommerce.profile.core.domain.CustomerAddress', -19),
(-507, 'org.broadleafcommerce.profile.core.domain.CustomerAddress', -18),
(-506, 'org.broadleafcommerce.profile.core.domain.CustomerAttribute', -19),
(-504, 'org.broadleafcommerce.profile.core.domain.CustomerAttribute', -18),
(-503, 'org.broadleafcommerce.profile.core.domain.ChallengeQuestion', -19),
(-502, 'org.broadleafcommerce.profile.core.domain.ChallengeQuestion', -18),
(-501, 'org.broadleafcommerce.profile.core.domain.Customer', -19),
(-500, 'org.broadleafcommerce.profile.core.domain.Customer', -18),
(-461, 'org.broadleafcommerce.core.order.domain.BundleOrderItemFeePriceImpl', -17),
(-460, 'org.broadleafcommerce.core.order.domain.BundleOrderItemFeePriceImpl', -16),
(-459, 'org.broadleafcommerce.core.order.domain.OrderItemPriceDetailImpl', -17),
(-458, 'org.broadleafcommerce.core.order.domain.OrderItemPriceDetailImpl', -16),
(-457, 'org.broadleafcommerce.core.offer.domain.OrderItemPriceDetailAdjustmentImpl', -17),
(-456, 'org.broadleafcommerce.core.offer.domain.OrderItemPriceDetailAdjustmentImpl', -16),
(-455, 'org.broadleafcommerce.core.offer.domain.OrderItemAdjustment', -17),
(-454, 'org.broadleafcommerce.core.offer.domain.OrderItemAdjustment', -16),
(-453, 'org.broadleafcommerce.core.order.domain.DiscreteOrderItemFeePrice', -17),
(-452, 'org.broadleafcommerce.core.order.domain.DiscreteOrderItemFeePrice', -16),
(-451, 'org.broadleafcommerce.core.order.domain.OrderItem', -17),
(-450, 'org.broadleafcommerce.core.order.domain.OrderItem', -16),
(-407, 'org.broadleafcommerce.core.order.domain.FulfillmentGroupItemImpl', -15),
(-406, 'org.broadleafcommerce.core.order.domain.FulfillmentGroupItemImpl', -14),
(-405, 'org.broadleafcommerce.core.order.domain.FulfillmentGroupFeeImpl', -15),
(-404, 'org.broadleafcommerce.core.order.domain.FulfillmentGroupFeeImpl', -14),
(-403, 'org.broadleafcommerce.core.offer.domain.FulfillmentGroupAdjustment', -15),
(-402, 'org.broadleafcommerce.core.offer.domain.FulfillmentGroupAdjustment', -14),
(-401, 'org.broadleafcommerce.core.order.domain.FulfillmentGroup', -15),
(-400, 'org.broadleafcommerce.core.order.domain.FulfillmentGroup', -14),
(-370, 'org.broadleafcommerce.core.payment.domain.PaymentTransactionImpl', -13),
(-369, 'org.broadleafcommerce.core.payment.domain.PaymentTransactionImpl', -12),
(-368, 'org.broadleafcommerce.profile.core.domain.State', -13),
(-367, 'org.broadleafcommerce.profile.core.domain.State', -12),
(-366, 'org.broadleafcommerce.profile.core.domain.Country', -13),
(-365, 'org.broadleafcommerce.profile.core.domain.Country', -12),
(-361, 'org.broadleafcommerce.core.payment.domain.OrderPayment', -13),
(-360, 'org.broadleafcommerce.core.payment.domain.OrderPayment', -12),
(-356, 'org.broadleafcommerce.core.offer.domain.OrderAdjustment', -13),
(-355, 'org.broadleafcommerce.core.offer.domain.OrderAdjustment', -12),
(-351, 'org.broadleafcommerce.core.order.domain.Order', -13),
(-350, 'org.broadleafcommerce.core.order.domain.Order', -12),
(-307, 'org.broadleafcommerce.core.offer.domain.OfferTier', -11),
(-306, 'org.broadleafcommerce.core.offer.domain.OfferTier', -10),
(-305, 'org.broadleafcommerce.core.offer.domain.OfferCode', -11),
(-304, 'org.broadleafcommerce.core.offer.domain.OfferCode', -10),
(-303, 'org.broadleafcommerce.core.offer.domain.OfferItemCriteria', -11),
(-302, 'org.broadleafcommerce.core.offer.domain.OfferItemCriteria', -10),
(-301, 'org.broadleafcommerce.core.offer.domain.Offer', -11),
(-300, 'org.broadleafcommerce.core.offer.domain.Offer', -10),
(-251, 'org.broadleafcommerce.core.catalog.domain.Sku', -9),
(-250, 'org.broadleafcommerce.core.catalog.domain.Sku', -8),
(-207, 'org.broadleafcommerce.core.catalog.domain.SkuProductOptionValueXref', -9),
(-206, 'org.broadleafcommerce.core.catalog.domain.SkuProductOptionValueXref', -6),
(-205, 'org.broadleafcommerce.core.catalog.domain.ProductOptionXref', -7),
(-204, 'org.broadleafcommerce.core.catalog.domain.ProductOptionXref', -6),
(-203, 'org.broadleafcommerce.core.catalog.domain.ProductOptionValue', -7),
(-202, 'org.broadleafcommerce.core.catalog.domain.ProductOptionValue', -6),
(-201, 'org.broadleafcommerce.core.catalog.domain.ProductOption', -7),
(-200, 'org.broadleafcommerce.core.catalog.domain.ProductOption', -6),
(-183, 'org.broadleafcommerce.core.offer.domain.AdvancedOfferPromotionMessageXref', -107),
(-182, 'org.broadleafcommerce.core.offer.domain.AdvancedOfferPromotionMessageXref', -106),
(-181, 'org.broadleafcommerce.core.promotionMessage.domain.PromotionMessage', -181),
(-180, 'org.broadleafcommerce.core.promotionMessage.domain.PromotionMessage', -180),
(-107, 'org.broadleafcommerce.core.catalog.domain.SkuBundleItemImpl', -5),
(-106, 'org.broadleafcommerce.core.catalog.domain.SkuBundleItemImpl', -4),
(-105, 'org.broadleafcommerce.core.catalog.domain.UpSaleProductImpl', -5),
(-104, 'org.broadleafcommerce.core.catalog.domain.UpSaleProductImpl', -4),
(-103, 'org.broadleafcommerce.core.catalog.domain.ProductAttribute', -5),
(-102, 'org.broadleafcommerce.core.catalog.domain.ProductAttribute', -4),
(-101, 'org.broadleafcommerce.core.catalog.domain.Product', -5),
(-100, 'org.broadleafcommerce.core.catalog.domain.Product', -4),
(-14, 'org.broadleafcommerce.core.catalog.domain.UpSaleProductImpl', -3),
(-13, 'org.broadleafcommerce.core.catalog.domain.UpSaleProductImpl', -2),
(-12, 'org.broadleafcommerce.core.catalog.domain.CrossSaleProductImpl', -3),
(-11, 'org.broadleafcommerce.core.catalog.domain.CrossSaleProductImpl', -2),
(-10, 'org.broadleafcommerce.core.catalog.domain.FeaturedProductImpl', -3),
(-9, 'org.broadleafcommerce.core.catalog.domain.FeaturedProductImpl', -2),
(-8, 'org.broadleafcommerce.core.catalog.domain.CategoryXrefImpl', -3),
(-7, 'org.broadleafcommerce.core.catalog.domain.CategoryXrefImpl', -2),
(-6, 'org.broadleafcommerce.core.catalog.domain.CategoryProductXrefImpl', -3),
(-5, 'org.broadleafcommerce.core.catalog.domain.CategoryProductXrefImpl', -2),
(-4, 'org.broadleafcommerce.core.catalog.domain.CategoryAttribute', -3),
(-3, 'org.broadleafcommerce.core.catalog.domain.CategoryAttribute', -2),
(-2, 'org.broadleafcommerce.core.catalog.domain.Category', -3),
(-1, 'org.broadleafcommerce.core.catalog.domain.Category', -2);

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_permission_xref`
--

CREATE TABLE `blc_admin_permission_xref` (
  `CHILD_PERMISSION_ID` bigint(20) NOT NULL,
  `ADMIN_PERMISSION_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_admin_permission_xref`
--

INSERT INTO `blc_admin_permission_xref` (`CHILD_PERMISSION_ID`, `ADMIN_PERMISSION_ID`) VALUES
(-2, -100),
(-4, -100),
(-32, -100),
(-53, -100),
(-3, -101),
(-4, -101),
(-32, -101),
(-53, -101),
(-4, -102),
(-6, -102),
(-8, -102),
(-34, -102),
(-53, -101),
(-5, -103),
(-6, -103),
(-9, -103),
(-34, -103),
(-53, -103),
(-6, -104),
(-32, -104),
(-53, -104),
(-7, -105),
(-32, -105),
(-53, -105),
(-10, -106),
(-53, -106),
(-11, -107),
(-53, -106),
(-20, -108),
(-200, -108),
(-21, -109),
(-200, -109),
(-22, -110),
(-23, -111),
(-28, -114),
(-29, -115),
(-18, -118),
(-19, -119),
(-26, -120),
(-46, -120),
(-48, -120),
(-27, -121),
(-46, -120),
(-48, -120),
(-36, -126),
(-37, -127),
(-40, -130),
(-41, -131),
(-46, -140),
(-48, -140),
(-47, -141),
(-48, -141),
(-48, -150),
(-49, -151),
(-50, -160),
(-51, -161),
(-180, -106),
(-181, -107),
(-27000, -27002),
(-27001, -27003),
(-38, -128),
(-39, -129),
(-8, -134),
(-9, -135),
(-30, -124),
(-31, -125),
(-12, -185),
(-14, -185),
(-15, -185),
(-16, -185),
(-17, -185),
(-12, -186),
(-14, -186),
(-15, -186),
(-16, -186),
(-17, -186),
(-27009, -186),
(-27011, -186),
(-27010, -186),
(-27013, -186),
(-13, -185),
(-13, -186),
(-13, -27013);

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_role`
--

CREATE TABLE `blc_admin_role` (
  `ADMIN_ROLE_ID` bigint(20) NOT NULL,
  `DESCRIPTION` varchar(255) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_admin_role`
--

INSERT INTO `blc_admin_role` (`ADMIN_ROLE_ID`, `DESCRIPTION`, `NAME`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(-7, 'CMS Designer', 'ROLE_CONTENT_DESIGNER', NULL, NULL, NULL, NULL),
(-6, 'CMS Approver', 'ROLE_CONTENT_APPROVER', NULL, NULL, NULL, NULL),
(-5, 'CMS Editor', 'ROLE_CONTENT_EDITOR', NULL, NULL, NULL, NULL),
(-4, 'CSR', 'ROLE_CUSTOMER_SERVICE_REP', NULL, NULL, NULL, NULL),
(-3, 'Promotion Manager', 'ROLE_PROMOTION_MANAGER', NULL, NULL, NULL, NULL),
(-2, 'Merchandiser', 'ROLE_MERCHANDISE_MANAGER', NULL, NULL, NULL, NULL),
(-1, 'Admin Master Access', 'ROLE_ADMIN', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_role_permission_xref`
--

CREATE TABLE `blc_admin_role_permission_xref` (
  `ADMIN_ROLE_ID` bigint(20) NOT NULL,
  `ADMIN_PERMISSION_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_admin_role_permission_xref`
--

INSERT INTO `blc_admin_role_permission_xref` (`ADMIN_ROLE_ID`, `ADMIN_PERMISSION_ID`) VALUES
(-1, -27029),
(-1, -27028),
(-1, -27027),
(-1, -27026),
(-1, -27025),
(-1, -27022),
(-1, -27021),
(-1, -27020),
(-1, -27013),
(-1, -27012),
(-1, -27011),
(-1, -27010),
(-1, -27009),
(-1, -27008),
(-1, -27007),
(-1, -27006),
(-1, -27004),
(-5, -27003),
(-1, -27003),
(-1, -27002),
(-1, -27001),
(-1, -27000),
(-1, -200),
(-1, -186),
(-1, -185),
(-1, -181),
(-1, -180),
(-5, -161),
(-1, -161),
(-1, -160),
(-1, -151),
(-1, -150),
(-1, -141),
(-1, -140),
(-1, -135),
(-1, -134),
(-5, -131),
(-3, -131),
(-2, -131),
(-1, -131),
(-1, -130),
(-1, -129),
(-1, -128),
(-1, -127),
(-1, -126),
(-1, -125),
(-1, -124),
(-1, -121),
(-4, -119),
(-1, -119),
(-1, -118),
(-1, -115),
(-1, -114),
(-1, -113),
(-1, -112),
(-6, -111),
(-5, -111),
(-2, -111),
(-1, -111),
(-1, -110),
(-6, -109),
(-5, -109),
(-1, -109),
(-1, -108),
(-3, -107),
(-1, -107),
(-1, -106),
(-2, -105),
(-1, -105),
(-1, -104),
(-2, -103),
(-1, -103),
(-1, -102),
(-2, -101),
(-1, -101),
(-1, -100),
(-1, -93),
(-1, -92),
(-1, -91),
(-1, -90),
(-1, -89),
(-1, -61),
(-1, -60),
(-1, -59),
(-1, -57),
(-1, -55),
(-1, -54),
(-1, -53),
(-1, -52),
(-1, -51),
(-1, -50),
(-1, -49),
(-1, -48),
(-1, -47),
(-1, -46),
(-1, -45),
(-1, -44),
(-1, -43),
(-1, -42),
(-1, -41),
(-1, -40),
(-1, -39),
(-1, -38),
(-1, -37),
(-1, -35),
(-1, -34),
(-1, -33),
(-1, -32),
(-1, -31),
(-1, -30),
(-1, -29),
(-1, -28),
(-1, -27),
(-1, -26),
(-1, -23),
(-1, -22),
(-1, -21),
(-1, -20),
(-1, -19),
(-1, -18),
(-1, -17),
(-1, -16),
(-1, -15),
(-1, -14),
(-1, -13),
(-1, -12),
(-1, -11),
(-1, -10),
(-1, -9),
(-1, -8),
(-1, -7),
(-1, -6),
(-1, -5),
(-1, -4),
(-1, -3),
(-1, -2),
(-1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_section`
--

CREATE TABLE `blc_admin_section` (
  `ADMIN_SECTION_ID` bigint(20) NOT NULL,
  `CEILING_ENTITY` varchar(255) DEFAULT NULL,
  `DISPLAY_CONTROLLER` varchar(255) DEFAULT NULL,
  `DISPLAY_ORDER` int(11) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  `SECTION_KEY` varchar(255) NOT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `USE_DEFAULT_HANDLER` tinyint(1) DEFAULT NULL,
  `ADMIN_MODULE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_admin_section`
--

INSERT INTO `blc_admin_section` (`ADMIN_SECTION_ID`, `CEILING_ENTITY`, `DISPLAY_CONTROLLER`, `DISPLAY_ORDER`, `NAME`, `SECTION_KEY`, `URL`, `USE_DEFAULT_HANDLER`, `ADMIN_MODULE_ID`) VALUES
(-27000, 'org.broadleafcommerce.menu.domain.Menu', NULL, 3000, 'Menus', 'Menus', '/menu', 1, -2),
(-21, 'org.broadleafcommerce.core.search.domain.Field', NULL, 1000, 'Field', 'Field', '/Field', NULL, -5),
(-20, 'org.broadleafcommerce.core.search.domain.SearchFacet', NULL, 1000, 'Search Facet', 'Search Facet', '/search-facet', NULL, -5),
(-19, 'org.broadleafcommerce.core.search.redirect.domain.SearchRedirect', NULL, 1000, 'Search Redirect', 'Search Redirect', '/search-redirect', NULL, -5),
(-18, 'org.broadleafcommerce.cms.structure.domain.StructuredContentType', NULL, 2000, 'Structured Content Types', 'StructuredContentTypes', '/structured-content-type', NULL, -2),
(-17, 'org.broadleafcommerce.openadmin.server.security.domain.AdminPermission', NULL, 11000, 'Permission Management', 'PermissionManagement', '/permission-management', NULL, -5),
(-16, 'org.broadleafcommerce.common.config.domain.SystemProperty', NULL, 2000, 'System Properties', 'SystemProperties', '/system-properties', NULL, -5),
(-15, 'org.broadleafcommerce.common.config.domain.SystemProperty', NULL, 3000, 'System Property Management', 'SystemPropertyManagement', '/system-properties-management', NULL, -5),
(-14, 'org.broadleafcommerce.common.enumeration.domain.DataDrivenEnumeration', NULL, 2000, 'Enumerations', 'Enumerations', '/enumerations', NULL, -5),
(-13, 'org.broadleafcommerce.common.config.domain.ModuleConfiguration', NULL, 10000, 'Configuration Management', 'ConfigurationManagement', '/configuration-management', NULL, -5),
(-12, 'org.broadleafcommerce.openadmin.server.security.domain.AdminRole', NULL, 3000, 'Role Management', 'RoleManagement', '/role-management', NULL, -5),
(-11, 'org.broadleafcommerce.openadmin.server.security.domain.AdminUser', NULL, 2000, 'User Management', 'UserManagement', '/user-management', NULL, -5),
(-10, 'org.broadleafcommerce.profile.core.domain.Customer', NULL, 1000, 'Customer', 'Customer', '/customer', NULL, -3),
(-9, 'org.broadleafcommerce.core.order.domain.Order', NULL, 2000, 'Order', 'Order', '/order', NULL, -3),
(-8, 'org.broadleafcommerce.cms.url.domain.URLHandler', NULL, 7000, 'Redirect URL', 'RedirectURL', '/redirect-url', NULL, -2),
(-7, 'org.broadleafcommerce.cms.structure.domain.StructuredContent', NULL, 2000, 'Structured Content', 'StructuredContent', '/structured-content', NULL, -2),
(-6, 'org.broadleafcommerce.cms.file.domain.StaticAsset', NULL, 4000, 'Assets', 'Assets', '/assets', NULL, -2),
(-5, 'org.broadleafcommerce.cms.page.domain.Page', NULL, 2000, 'Pages', 'Pages', '/pages', NULL, -2),
(-4, 'org.broadleafcommerce.core.offer.domain.Offer', NULL, 1000, 'Offer', 'Offer', '/offer', NULL, -8),
(-3, 'org.broadleafcommerce.core.catalog.domain.ProductOption', NULL, 5000, 'Product Options', 'ProductOptions', '/product-options', NULL, -1),
(-2, 'org.broadleafcommerce.core.catalog.domain.Product', NULL, 3000, 'Product', 'Product', '/product', NULL, -1),
(-1, 'org.broadleafcommerce.core.catalog.domain.Category', NULL, 2000, 'Category', 'Category', '/category', NULL, -1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_sec_perm_xref`
--

CREATE TABLE `blc_admin_sec_perm_xref` (
  `ADMIN_SECTION_ID` bigint(20) NOT NULL,
  `ADMIN_PERMISSION_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_admin_sec_perm_xref`
--

INSERT INTO `blc_admin_sec_perm_xref` (`ADMIN_SECTION_ID`, `ADMIN_PERMISSION_ID`) VALUES
(-1, -100),
(-1, -101),
(-2, -102),
(-2, -103),
(-3, -104),
(-3, -105),
(-4, -106),
(-4, -107),
(-5, -108),
(-5, -109),
(-6, -110),
(-6, -111),
(-8, -114),
(-8, -115),
(-10, -118),
(-10, -119),
(-11, -120),
(-11, -121),
(-12, -140),
(-12, -141),
(-13, -126),
(-13, -127),
(-17, -150),
(-17, -151),
(-27000, -27002),
(-27000, -27003),
(-9, -12),
(-9, -13),
(-9, -185),
(-9, -186),
(-13, -89),
(-13, -90),
(-13, -91),
(-13, -92),
(-14, -128),
(-14, -129),
(-15, -109),
(-15, -110),
(-15, -111),
(-15, -110),
(-15, -111),
(-15, -112),
(-7, -57),
(-7, -58),
(-7, -59),
(-7, -60),
(-5, -47),
(-5, -48),
(-5, -49),
(-5, -50),
(-18, -27004),
(-18, -27005),
(-18, -27006),
(-18, -27007),
(-18, -27008),
(-19, -27015),
(-19, -27016),
(-19, -27017),
(-19, -27018),
(-19, -27019),
(-20, -27020),
(-20, -27021),
(-20, -27022),
(-20, -27023),
(-20, -27024),
(-21, -27025),
(-21, -27026),
(-21, -27027),
(-21, -27028),
(-21, -27029);

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_user`
--

CREATE TABLE `blc_admin_user` (
  `ADMIN_USER_ID` bigint(20) NOT NULL,
  `ACTIVE_STATUS_FLAG` tinyint(1) DEFAULT NULL,
  `EMAIL` varchar(255) NOT NULL,
  `LOGIN` varchar(255) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `PHONE_NUMBER` varchar(255) DEFAULT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_admin_user`
--

INSERT INTO `blc_admin_user` (`ADMIN_USER_ID`, `ACTIVE_STATUS_FLAG`, `EMAIL`, `LOGIN`, `NAME`, `PASSWORD`, `PHONE_NUMBER`, `ARCHIVED`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(-1, 1, 'admin@yourdomain.com', 'admin', 'Administrator', 'admin', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_user_addtl_fields`
--

CREATE TABLE `blc_admin_user_addtl_fields` (
  `ATTRIBUTE_ID` bigint(20) NOT NULL,
  `FIELD_NAME` varchar(255) NOT NULL,
  `FIELD_VALUE` varchar(255) DEFAULT NULL,
  `ADMIN_USER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_user_permission_xref`
--

CREATE TABLE `blc_admin_user_permission_xref` (
  `ADMIN_USER_ID` bigint(20) NOT NULL,
  `ADMIN_PERMISSION_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_user_role_xref`
--

CREATE TABLE `blc_admin_user_role_xref` (
  `ADMIN_USER_ID` bigint(20) NOT NULL,
  `ADMIN_ROLE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_admin_user_role_xref`
--

INSERT INTO `blc_admin_user_role_xref` (`ADMIN_USER_ID`, `ADMIN_ROLE_ID`) VALUES
(-1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_admin_user_sandbox`
--

CREATE TABLE `blc_admin_user_sandbox` (
  `SANDBOX_ID` bigint(20) DEFAULT NULL,
  `ADMIN_USER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_asset_desc_map`
--

CREATE TABLE `blc_asset_desc_map` (
  `STATIC_ASSET_ID` bigint(20) NOT NULL,
  `STATIC_ASSET_DESC_ID` bigint(20) NOT NULL,
  `MAP_KEY` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_bank_account_payment`
--

CREATE TABLE `blc_bank_account_payment` (
  `PAYMENT_ID` bigint(20) NOT NULL,
  `ACCOUNT_NUMBER` varchar(255) NOT NULL,
  `REFERENCE_NUMBER` varchar(255) NOT NULL,
  `ROUTING_NUMBER` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_bundle_order_item`
--

CREATE TABLE `blc_bundle_order_item` (
  `BASE_RETAIL_PRICE` decimal(19,5) DEFAULT NULL,
  `BASE_SALE_PRICE` decimal(19,5) DEFAULT NULL,
  `ORDER_ITEM_ID` bigint(20) NOT NULL,
  `PRODUCT_BUNDLE_ID` bigint(20) DEFAULT NULL,
  `SKU_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_bund_item_fee_price`
--

CREATE TABLE `blc_bund_item_fee_price` (
  `BUND_ITEM_FEE_PRICE_ID` bigint(20) NOT NULL,
  `AMOUNT` decimal(19,5) DEFAULT NULL,
  `IS_TAXABLE` tinyint(1) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REPORTING_CODE` varchar(255) DEFAULT NULL,
  `BUND_ORDER_ITEM_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_candidate_fg_offer`
--

CREATE TABLE `blc_candidate_fg_offer` (
  `CANDIDATE_FG_OFFER_ID` bigint(20) NOT NULL,
  `DISCOUNTED_PRICE` decimal(19,5) DEFAULT NULL,
  `FULFILLMENT_GROUP_ID` bigint(20) DEFAULT NULL,
  `OFFER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_candidate_item_offer`
--

CREATE TABLE `blc_candidate_item_offer` (
  `CANDIDATE_ITEM_OFFER_ID` bigint(20) NOT NULL,
  `DISCOUNTED_PRICE` decimal(19,5) DEFAULT NULL,
  `OFFER_ID` bigint(20) NOT NULL,
  `ORDER_ITEM_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_candidate_order_offer`
--

CREATE TABLE `blc_candidate_order_offer` (
  `CANDIDATE_ORDER_OFFER_ID` bigint(20) NOT NULL,
  `DISCOUNTED_PRICE` decimal(19,5) DEFAULT NULL,
  `OFFER_ID` bigint(20) NOT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_catalog`
--

CREATE TABLE `blc_catalog` (
  `CATALOG_ID` bigint(20) NOT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_category`
--

CREATE TABLE `blc_category` (
  `CATEGORY_ID` bigint(20) NOT NULL,
  `ACTIVE_END_DATE` datetime DEFAULT NULL,
  `ACTIVE_START_DATE` datetime DEFAULT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `DISPLAY_TEMPLATE` varchar(255) DEFAULT NULL,
  `EXTERNAL_ID` varchar(255) DEFAULT NULL,
  `FULFILLMENT_TYPE` varchar(255) DEFAULT NULL,
  `INVENTORY_TYPE` varchar(255) DEFAULT NULL,
  `LONG_DESCRIPTION` longtext,
  `META_DESC` varchar(255) DEFAULT NULL,
  `META_TITLE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  `OVERRIDE_GENERATED_URL` tinyint(1) DEFAULT NULL,
  `PRODUCT_DESC_PATTERN_OVERRIDE` varchar(255) DEFAULT NULL,
  `PRODUCT_TITLE_PATTERN_OVERRIDE` varchar(255) DEFAULT NULL,
  `ROOT_DISPLAY_ORDER` decimal(10,6) DEFAULT NULL,
  `TAX_CODE` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `URL_KEY` varchar(255) DEFAULT NULL,
  `DEFAULT_PARENT_CATEGORY_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_category`
--

INSERT INTO `blc_category` (`CATEGORY_ID`, `ACTIVE_END_DATE`, `ACTIVE_START_DATE`, `ARCHIVED`, `DESCRIPTION`, `DISPLAY_TEMPLATE`, `EXTERNAL_ID`, `FULFILLMENT_TYPE`, `INVENTORY_TYPE`, `LONG_DESCRIPTION`, `META_DESC`, `META_TITLE`, `NAME`, `OVERRIDE_GENERATED_URL`, `PRODUCT_DESC_PATTERN_OVERRIDE`, `PRODUCT_TITLE_PATTERN_OVERRIDE`, `ROOT_DISPLAY_ORDER`, `TAX_CODE`, `URL`, `URL_KEY`, `DEFAULT_PARENT_CATEGORY_ID`) VALUES
(2001, NULL, '2017-09-16 16:40:00', NULL, 'Home', 'layout/homepage', NULL, NULL, 'ALWAYS_AVAILABLE', NULL, NULL, NULL, 'Home', NULL, NULL, NULL, '-5.000000', NULL, '/', NULL, NULL),
(2002, NULL, '2017-09-16 16:40:54', 'Y', 'Hot Sauces', NULL, NULL, NULL, 'CHECK_QUANTITY', '<p>Çeşitli Oyuncak seçenekleri burada yer alacak\r\n</p>', NULL, NULL, 'Oyuncak Grubuh', NULL, NULL, NULL, '-4.000000', NULL, '/oyuncaklar', NULL, NULL),
(2003, NULL, '2017-09-16 16:40:00', 'Y', 'Merchandise', NULL, NULL, NULL, 'ALWAYS_AVAILABLE', '<p>Sadece Kız çocuklarına yönelik oyuncaklar burada yer alacak\r\n</p>', NULL, NULL, 'Kız Çocukları İçin Oyuncaklar', 1, NULL, NULL, '-3.000000', NULL, '/kiz-cocuklari-icin-oyuncaklar', NULL, NULL),
(2004, NULL, '2017-09-16 16:40:54', 'Y', 'Clearance', NULL, NULL, NULL, NULL, '<p>Erkek çocukları için oyuncaklar burada yer alacaklar</p>', NULL, NULL, 'Erkek Çocukları İçin Oyuncaklar', NULL, NULL, NULL, '-2.000000', NULL, '/erkek-cocuklari-icin-oyuncaklar', NULL, NULL),
(2007, NULL, '2017-09-16 16:40:54', 'Y', 'Mens', NULL, NULL, NULL, NULL, '<p>Gerizekalı çocuğunuzun zekasını arttırmaya yönelik oyuncaklar</p>', NULL, NULL, 'IQ Oyuncaklar', 0, NULL, NULL, NULL, NULL, '/iq-oyuncaklar', NULL, NULL),
(2008, NULL, '2017-09-16 16:40:54', 'Y', 'Womens', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Womens', 0, NULL, NULL, NULL, NULL, '/womens', NULL, NULL),
(10000, NULL, '2017-09-25 13:10:58', 'Y', NULL, NULL, NULL, NULL, NULL, '<h2>Lorem Ipsum Nedir?</h2>\r\n<p><strong>Lorem Ipsum</strong>, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500\'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. Beşyüz yıl boyunca varlığını sürdürmekle kalmamış, aynı zamanda pek değişmeden elektronik dizgiye de sıçramıştır. 1960\'larda Lorem Ipsum pasajları da içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü yayıncılık yazılımları ile popüler olmuştur.\r\n</p>', 'Çok sevimlidir', 'Kız Çocukları', 'Deneme Kategorisi', 0, 'inanma hıammına', 'Desem de', NULL, NULL, '/deneme-kategorisi', NULL, NULL),
(10050, NULL, '2018-02-12 14:41:04', 'N', NULL, NULL, NULL, NULL, NULL, '<p>Ekomera Şık Erkek Giyim Modelleri ile bu sezon bütün gözler üstünüzde</p>', NULL, NULL, 'Erkek Giyim', 0, NULL, NULL, NULL, NULL, '/erkek-giyim', NULL, NULL),
(10051, NULL, '2018-02-12 14:42:17', 'N', NULL, NULL, NULL, NULL, NULL, '<p>Öne çıkan erkek giyim modelleri ile tarzınızı belirleyin\r\n</p>', NULL, NULL, 'Erkek Öne Çıkanlar', 0, NULL, NULL, NULL, NULL, '/erkek-one-cikanlar', NULL, NULL),
(10052, NULL, '2018-02-12 14:43:02', 'N', NULL, NULL, NULL, NULL, NULL, '<p>Erkek giyim trendleri, casual, smart - casual ve business fit trendleri ile ilgili aradığınız herşey ekomera fashion\'da ! </p>', NULL, NULL, 'Erkek Trendler', 0, NULL, NULL, NULL, NULL, '/erkek-trendler', NULL, NULL),
(10053, NULL, '2018-02-12 14:44:00', 'N', NULL, NULL, NULL, NULL, NULL, '<p>Şık erkek pantolon modelleri, her modaya ve bütçeye uygun şık ve elit pantolonlar</p>', NULL, NULL, 'Erkek Pantolon', 0, NULL, NULL, NULL, NULL, '/erkek-pantolon', NULL, NULL),
(10054, NULL, '2018-02-12 14:44:40', 'N', NULL, NULL, NULL, NULL, NULL, '<p>Şık ve kaliteli erkek kazak modelleri ile bu sene kızların gözü üstünüzde !</p>', NULL, NULL, 'Erkek Kazak', 0, NULL, NULL, NULL, NULL, '/erkek-kazak', NULL, NULL),
(10055, NULL, '2018-02-12 14:45:18', 'N', NULL, NULL, NULL, NULL, NULL, '<p>Şık ve elit kadın giyim modelleri sadece ekomera fashion\'da !\r\n</p>', NULL, NULL, 'Kadın Giyim', 0, NULL, NULL, NULL, NULL, '/kadin-giyim', NULL, NULL),
(10056, NULL, '2018-02-12 14:48:05', 'N', NULL, NULL, NULL, NULL, NULL, '<p>Şık ve elit kadın kazak modelleri\r\n</p>', NULL, NULL, 'Kadın Kazak', 0, NULL, NULL, NULL, NULL, '/kadin-kazak', NULL, NULL),
(10057, NULL, '2018-02-12 14:48:42', 'N', NULL, NULL, NULL, NULL, NULL, '<p>Kadın Pantolon modelleri, en göz alıcı tasarımlar ile ekomera fashion\'da\r\n</p>', NULL, NULL, 'Kadın Pantolon', 0, NULL, NULL, NULL, NULL, '/kadin-pantolon', NULL, NULL),
(10058, NULL, '2018-02-12 14:56:22', 'N', NULL, NULL, NULL, NULL, NULL, '<p>En şık ve bıcırık çocuk giyim modelleri ekomera fashion\'da\r\n</p>', NULL, NULL, 'Çocuk Giyim', 0, NULL, NULL, NULL, NULL, '/cocuk-giyim', NULL, NULL),
(10059, NULL, '2018-02-12 14:57:12', 'N', NULL, NULL, NULL, NULL, NULL, '<p>En güzel çocuk penye modelleri ekomera fashion\'da\r\n</p>', NULL, NULL, 'Çocuk Penye', 0, NULL, NULL, NULL, NULL, '/cocuk-penye', NULL, NULL),
(10060, NULL, '2018-02-12 15:40:09', 'N', NULL, NULL, NULL, NULL, NULL, '<p style=\"margin-left: 20px;\">Erkek casual giyim\r\n</p>', NULL, NULL, 'Casual', 0, NULL, NULL, NULL, NULL, '/erkek-casual', NULL, NULL),
(10061, NULL, '2018-02-12 15:41:00', 'N', NULL, NULL, NULL, NULL, NULL, '<p>Smart casual erkek giyimm</p>', NULL, NULL, 'Erkek Smart Casual', 0, NULL, NULL, NULL, NULL, '/erkek-smart-casual', NULL, NULL),
(10100, NULL, '2018-02-15 19:25:06', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Deneme Kategori', 0, NULL, NULL, NULL, NULL, '/deneme-kategori', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_category_attribute`
--

CREATE TABLE `blc_category_attribute` (
  `CATEGORY_ATTRIBUTE_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_category_media_map`
--

CREATE TABLE `blc_category_media_map` (
  `CATEGORY_MEDIA_ID` bigint(20) NOT NULL,
  `MAP_KEY` varchar(255) NOT NULL,
  `BLC_CATEGORY_CATEGORY_ID` bigint(20) NOT NULL,
  `MEDIA_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_category_media_map`
--

INSERT INTO `blc_category_media_map` (`CATEGORY_MEDIA_ID`, `MAP_KEY`, `BLC_CATEGORY_CATEGORY_ID`, `MEDIA_ID`) VALUES
(101, 'primary', 10100, 100100),
(151, 'primary', 10050, 100150),
(152, 'primary', 10055, 100151),
(153, 'primary', 10058, 100152);

-- --------------------------------------------------------

--
-- Table structure for table `blc_category_product_xref`
--

CREATE TABLE `blc_category_product_xref` (
  `CATEGORY_PRODUCT_ID` bigint(20) NOT NULL,
  `DEFAULT_REFERENCE` tinyint(1) DEFAULT NULL,
  `DISPLAY_ORDER` decimal(10,6) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_category_product_xref`
--

INSERT INTO `blc_category_product_xref` (`CATEGORY_PRODUCT_ID`, `DEFAULT_REFERENCE`, `DISPLAY_ORDER`, `CATEGORY_ID`, `PRODUCT_ID`) VALUES
(20, NULL, '1.000000', 2001, 3),
(21, NULL, '2.000000', 2001, 6),
(22, NULL, '3.000000', 2001, 9),
(23, NULL, '4.000000', 2001, 12),
(1056, NULL, NULL, 10053, 13),
(1057, NULL, NULL, 10053, 2),
(1058, NULL, NULL, 10053, 8),
(1059, NULL, NULL, 10053, 7),
(1150, 1, NULL, 2001, 1),
(1151, 1, NULL, 2001, 2);

-- --------------------------------------------------------

--
-- Table structure for table `blc_category_xref`
--

CREATE TABLE `blc_category_xref` (
  `CATEGORY_XREF_ID` bigint(20) NOT NULL,
  `DEFAULT_REFERENCE` tinyint(1) DEFAULT NULL,
  `DISPLAY_ORDER` decimal(10,6) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) NOT NULL,
  `SUB_CATEGORY_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_category_xref`
--

INSERT INTO `blc_category_xref` (`CATEGORY_XREF_ID`, `DEFAULT_REFERENCE`, `DISPLAY_ORDER`, `CATEGORY_ID`, `SUB_CATEGORY_ID`) VALUES
(1050, 1, NULL, 10050, 10053),
(1051, 1, NULL, 10050, 10054),
(1052, 1, NULL, 10055, 10056),
(1053, 1, NULL, 10055, 10057),
(1054, 1, NULL, 10058, 10059),
(1055, 1, NULL, 10052, 10061),
(1056, 1, NULL, 10052, 10060),
(1100, NULL, '0.000000', 10100, 10050);

-- --------------------------------------------------------

--
-- Table structure for table `blc_cat_search_facet_excl_xref`
--

CREATE TABLE `blc_cat_search_facet_excl_xref` (
  `CAT_EXCL_SEARCH_FACET_ID` bigint(20) NOT NULL,
  `SEQUENCE` decimal(19,2) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `SEARCH_FACET_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_cat_search_facet_xref`
--

CREATE TABLE `blc_cat_search_facet_xref` (
  `CATEGORY_SEARCH_FACET_ID` bigint(20) NOT NULL,
  `SEQUENCE` decimal(19,2) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `SEARCH_FACET_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_cat_site_map_gen_cfg`
--

CREATE TABLE `blc_cat_site_map_gen_cfg` (
  `ENDING_DEPTH` int(11) NOT NULL,
  `STARTING_DEPTH` int(11) NOT NULL,
  `GEN_CONFIG_ID` bigint(20) NOT NULL,
  `ROOT_CATEGORY_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_cat_site_map_gen_cfg`
--

INSERT INTO `blc_cat_site_map_gen_cfg` (`ENDING_DEPTH`, `STARTING_DEPTH`, `GEN_CONFIG_ID`, `ROOT_CATEGORY_ID`) VALUES
(0, 0, -7, 2004),
(0, 0, -6, 2003),
(0, 0, -5, 2002),
(0, 0, -4, 2001);

-- --------------------------------------------------------

--
-- Table structure for table `blc_challenge_question`
--

CREATE TABLE `blc_challenge_question` (
  `QUESTION_ID` bigint(20) NOT NULL,
  `QUESTION` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_challenge_question`
--

INSERT INTO `blc_challenge_question` (`QUESTION_ID`, `QUESTION`) VALUES
(1, 'What is your favorite sports team?'),
(2, 'What was your high school name?'),
(3, 'What was your childhood nickname?'),
(4, 'What street did you live on in third grade?'),
(5, 'What is your oldest sibling\'s middle name?'),
(6, 'What school did you attend for sixth grade?'),
(7, 'Where does your nearest sibling live?'),
(8, 'What is your youngest brother\'s birthday?'),
(9, 'In what city or town was your first job?');

-- --------------------------------------------------------

--
-- Table structure for table `blc_cms_menu`
--

CREATE TABLE `blc_cms_menu` (
  `MENU_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_cms_menu`
--

INSERT INTO `blc_cms_menu` (`MENU_ID`, `NAME`) VALUES
(1, 'Header Nav'),
(1000, 'Home Page Nav'),
(1001, 'Home Page Nav Sub Menu'),
(1050, 'Header Main Menu'),
(1051, 'Erkek Sub Menu'),
(1052, 'Erkek Giyim Sub Menu'),
(1053, 'Erkek Trendler Sub Menu');

-- --------------------------------------------------------

--
-- Table structure for table `blc_cms_menu_item`
--

CREATE TABLE `blc_cms_menu_item` (
  `MENU_ITEM_ID` bigint(20) NOT NULL,
  `ACTION_URL` varchar(255) DEFAULT NULL,
  `ALT_TEXT` varchar(255) DEFAULT NULL,
  `CUSTOM_HTML` longtext,
  `LABEL` varchar(255) DEFAULT NULL,
  `SEQUENCE` decimal(10,6) DEFAULT NULL,
  `MENU_ITEM_TYPE` varchar(255) DEFAULT NULL,
  `MEDIA_ID` bigint(20) DEFAULT NULL,
  `LINKED_MENU_ID` bigint(20) DEFAULT NULL,
  `LINKED_PAGE_ID` bigint(20) DEFAULT NULL,
  `PARENT_MENU_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_cms_menu_item`
--

INSERT INTO `blc_cms_menu_item` (`MENU_ITEM_ID`, `ACTION_URL`, `ALT_TEXT`, `CUSTOM_HTML`, `LABEL`, `SEQUENCE`, `MENU_ITEM_TYPE`, `MEDIA_ID`, `LINKED_MENU_ID`, `LINKED_PAGE_ID`, `PARENT_MENU_ID`) VALUES
(1, '/', NULL, NULL, 'Home', '1.000000', 'CATEGORY', NULL, NULL, NULL, 1),
(2, '/hot-sauces', NULL, NULL, 'Hot Sauces', '2.000000', 'CATEGORY', NULL, NULL, NULL, 1),
(3, '/merchandise', NULL, NULL, 'Merchandise', '3.000000', 'CATEGORY', NULL, NULL, NULL, 1),
(4, '/clearance', NULL, NULL, 'Clearance', '4.000000', 'CATEGORY', NULL, NULL, NULL, 1),
(5, '/gift-cards', NULL, NULL, 'Gift Cards', '5.000000', 'CATEGORY', NULL, NULL, NULL, 1),
(1003, '/#', NULL, NULL, 'Ana Sayfa', '1.000000', 'LINK', 100003, NULL, NULL, 1000),
(1010, '/deneme', 'deneme', NULL, 'Deneme', '10.000000', 'LINK', 100010, NULL, NULL, 1001),
(1011, '/oyuncaklar', NULL, NULL, 'Oyuncak Grubu', '2.000000', 'SUBMENU', 100011, 1001, NULL, 1000),
(1012, '/asd', NULL, NULL, 'asd', '12.000000', 'LINK', 100012, NULL, NULL, 1001),
(1013, '/kiz-cocuklari-icin-oyuncaklar', NULL, NULL, 'Kız Çocuklar İçin Oyuncaklar', '3.000000', 'LINK', 100013, NULL, NULL, 1000),
(1014, '/erkek-cocuklari-icin-oyuncaklar', NULL, NULL, 'Erkek Çocuklar İçin Oyuncaklar', '11.000000', 'LINK', 100014, NULL, NULL, 1000),
(1015, '/iq-oyuncaklar', NULL, NULL, 'IQ Oyuncaklar', '12.000000', 'LINK', 100015, NULL, NULL, 1000),
(1016, '/', NULL, NULL, 'Hakkımızda', '13.000000', 'LINK', 100016, NULL, NULL, 1000),
(1017, '/', NULL, NULL, 'İletişim', '14.000000', 'LINK', 100017, NULL, NULL, 1000),
(1050, '/', NULL, NULL, 'Ana Sayfa', '15.000000', 'LINK', 100053, NULL, NULL, 1050),
(1051, NULL, NULL, NULL, 'ERKEK', '16.000000', 'SUBMENU', 100054, 1051, NULL, 1050),
(1052, NULL, NULL, NULL, 'Erkek Giyim', '17.000000', 'SUBMENU', 100055, 1052, NULL, 1051),
(1053, NULL, NULL, NULL, 'Trendler', '18.000000', 'SUBMENU', 100056, 1053, NULL, 1051),
(1054, NULL, NULL, NULL, 'Öne Çıkanlar', '19.000000', 'SUBMENU', 100057, NULL, NULL, 1051),
(1055, NULL, NULL, NULL, 'KADIN', '20.000000', 'SUBMENU', 100058, NULL, NULL, 1050),
(1056, '/', NULL, NULL, 'Fırsat Ürünleri', '21.000000', 'LINK', 100059, NULL, NULL, 1050),
(1057, '/hakkimizda', NULL, NULL, 'Hakkımızda', '22.000000', 'LINK', 100060, NULL, NULL, 1050),
(1058, '/', NULL, NULL, 'İletişim Bilgileri', '23.000000', 'LINK', 100061, NULL, NULL, 1050),
(1059, '/erkek-pantolon', NULL, NULL, 'Pantolon', '24.000000', 'CATEGORY', 100062, NULL, NULL, 1052),
(1060, '/erkek-kazak', NULL, NULL, 'Kazak', '25.000000', 'CATEGORY', 100063, NULL, NULL, 1052),
(1061, '/erkek-casual', NULL, NULL, 'Casual', '26.000000', 'LINK', 100064, NULL, NULL, 1053),
(1062, '/erkek-smart-casual', NULL, NULL, 'Smart Casual', '27.000000', 'LINK', 100065, NULL, NULL, 1053),
(1063, '/erkek-business-fit', NULL, NULL, 'Business Fit', '28.000000', 'LINK', 100066, NULL, NULL, 1053);

-- --------------------------------------------------------

--
-- Table structure for table `blc_code_types`
--

CREATE TABLE `blc_code_types` (
  `CODE_ID` bigint(20) NOT NULL,
  `CODE_TYPE` varchar(255) NOT NULL,
  `CODE_DESC` varchar(255) DEFAULT NULL,
  `CODE_KEY` varchar(255) NOT NULL,
  `MODIFIABLE` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_country`
--

CREATE TABLE `blc_country` (
  `ABBREVIATION` varchar(255) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_country`
--

INSERT INTO `blc_country` (`ABBREVIATION`, `NAME`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
('CA', 'Canada', NULL, NULL, NULL, NULL),
('ES', 'Spain', NULL, NULL, NULL, NULL),
('FR', 'France', NULL, NULL, NULL, NULL),
('GB', 'United Kingdom', NULL, NULL, NULL, NULL),
('MX', 'Mexico', NULL, NULL, NULL, NULL),
('US', 'United States', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_country_sub`
--

CREATE TABLE `blc_country_sub` (
  `ABBREVIATION` varchar(255) NOT NULL,
  `ALT_ABBREVIATION` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  `COUNTRY_SUB_CAT` bigint(20) DEFAULT NULL,
  `COUNTRY` varchar(255) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_country_sub`
--

INSERT INTO `blc_country_sub` (`ABBREVIATION`, `ALT_ABBREVIATION`, `NAME`, `COUNTRY_SUB_CAT`, `COUNTRY`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
('CA-AB', 'AB', 'ALBERTA', 63, 'CA', NULL, NULL, NULL, NULL),
('CA-BC', 'BC', 'BRITISH COLUMBIA', 63, 'CA', NULL, NULL, NULL, NULL),
('CA-MB', 'MB', 'MANITOBA', 63, 'CA', NULL, NULL, NULL, NULL),
('CA-NB', 'NB', 'NEW BRUNSWICK', 63, 'CA', NULL, NULL, NULL, NULL),
('CA-NL', 'NL', 'NEWFOUNDLAND', 63, 'CA', NULL, NULL, NULL, NULL),
('CA-NS', 'NS', 'NOVA SCOTIA', 63, 'CA', NULL, NULL, NULL, NULL),
('CA-NT', 'NT', 'NORTHWEST TERRITORIES', 81, 'CA', NULL, NULL, NULL, NULL),
('CA-NU', 'NU', 'NUNAVUT', 81, 'CA', NULL, NULL, NULL, NULL),
('CA-ON', 'ON', 'ONTARIO', 63, 'CA', NULL, NULL, NULL, NULL),
('CA-PE', 'PE', 'PRINCE EDWARD ISLAND', 63, 'CA', NULL, NULL, NULL, NULL),
('CA-QC', 'QC', 'QUEBEC', 63, 'CA', NULL, NULL, NULL, NULL),
('CA-SK', 'SK', 'SASKATCHEWAN', 63, 'CA', NULL, NULL, NULL, NULL),
('CA-YT', 'YT', 'YUKON', 81, 'CA', NULL, NULL, NULL, NULL),
('ES-A', 'A', 'Alicante', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-AB', 'AB', 'Albacete', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-AL', 'AL', 'Almería', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-AV', 'AV', 'Ávila', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-B', 'B', 'Barcelona', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-BA', 'BA', 'Badajoz', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-BI', 'BI', 'Vizcaya', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-BU', 'BU', 'Burgos', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-C', 'C', 'A Coruña', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-CA', 'CA', 'Cádiz', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-CC', 'CC', 'Cáceres', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-CO', 'CO', 'Córdoba', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-CR', 'CR', 'Ciudad Real', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-CS', 'CS', 'Castellón', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-CU', 'CU', 'Cuenca', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-GC', 'GC', 'Las Palmas', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-GI', 'GI', 'Girona', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-GR', 'GR', 'Granada', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-GU', 'GU', 'Guadalajara', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-H', 'H', 'Huelva', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-HU', 'HU', 'Huesca', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-J', 'J', 'Jaén', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-L', 'L', 'Lleida', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-LE', 'LE', 'León', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-LO', 'LO', 'La Rioja', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-LU', 'LU', 'Lugo', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-M', 'M', 'Madrid', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-MA', 'MA', 'Málaga', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-MU', 'MU', 'Murcia', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-NA', 'NA', 'Navarra', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-O', 'O', 'Asturias', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-OR', 'OR', 'Ourense', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-P', 'P', 'Palencia', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-PM', 'PM', 'Balears', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-PO', 'PO', 'Pontevedra', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-S', 'S', 'Cantabria', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-SA', 'SA', 'Salamanca', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-SE', 'SE', 'Sevilla', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-SG', 'SG', 'Segovia', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-SO', 'SO', 'Soria', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-SS', 'SS', 'Guipúzcoa', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-T', 'T', 'Tarragona', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-TE', 'TE', 'Teruel', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-TF', 'TF', 'Santa Cruz de Tenerife', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-TO', 'TO', 'Toledo', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-V', 'V', 'Valencia', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-VA', 'VA', 'Valladolid', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-VI', 'VI', 'Álava', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-Z', 'Z', 'Zaragoza', 63, 'ES', NULL, NULL, NULL, NULL),
('ES-ZA', 'ZA', 'Zamora', 63, 'ES', NULL, NULL, NULL, NULL),
('MX-AGU', 'AGU', 'AGUASCALIENTES', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-BCN', 'BCN', 'BAJA CALIFORNIA', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-BCS', 'BCS', 'BAJA CALIFORNIA SUR', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-CAM', 'CAM', 'CAMPECHE', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-CHH', 'CHH', 'CHIHUAHUA', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-CHP', 'CHP', 'CHIAPAS', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-COA', 'COA', 'COAHUILA', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-COL', 'COL', 'COLIMA', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-DIF', 'DIF', 'DISTRITO FEDERAL', 39, 'MX', NULL, NULL, NULL, NULL),
('MX-DUR', 'DUR', 'DURANGO', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-GRO', 'GRO', 'GUERRERO', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-GUA', 'GUA', 'GUANAJUATO', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-HID', 'HID', 'HIDALGO', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-JAL', 'JAL', 'JALISCO', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-MEX', 'MEX', 'MÉXICO', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-MIC', 'MIC', 'MICHOACÁN', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-MOR', 'MOR', 'MORELOS', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-NAY', 'NAY', 'NAYARIT', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-NLE', 'NLE', 'NUEVO LEÓN', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-OAX', 'OAX', 'OAXACA', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-PUE', 'PUE', 'PUEBLA', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-QUE', 'QUE', 'QUERÉTARO', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-ROO', 'ROO', 'QUINTANA ROO', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-SIN', 'SIN', 'SINALOA', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-SLP', 'SLP', 'SAN LUIS POTOSÍ', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-SON', 'SON', 'SONORA', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-TAB', 'TAB', 'TABASCO', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-TAM', 'TAM', 'TAMAULIPAS', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-TLA', 'TLA', 'TLAXCALA', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-VER', 'VER', 'VERACRUZ', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-YUC', 'YUC', 'YUCATÁN', 79, 'MX', NULL, NULL, NULL, NULL),
('MX-ZAC', 'ZAC', 'ZACATECAS', 79, 'MX', NULL, NULL, NULL, NULL),
('US-AK', 'AK', 'ALASKA', 79, 'US', NULL, NULL, NULL, NULL),
('US-AL', 'AL', 'ALABAMA', 79, 'US', NULL, NULL, NULL, NULL),
('US-AR', 'AR', 'ARKANSAS', 79, 'US', NULL, NULL, NULL, NULL),
('US-AS', 'AS', 'AMERICAN SAMOA', 57, 'US', NULL, NULL, NULL, NULL),
('US-AZ', 'AZ', 'ARIZONA', 79, 'US', NULL, NULL, NULL, NULL),
('US-CA', 'CA', 'CALIFORNIA', 79, 'US', NULL, NULL, NULL, NULL),
('US-CO', 'CO', 'COLORADO', 79, 'US', NULL, NULL, NULL, NULL),
('US-CT', 'CT', 'CONNECTICUT', 79, 'US', NULL, NULL, NULL, NULL),
('US-DC', 'DC', 'DISTRICT OF COLUMBIA', 31, 'US', NULL, NULL, NULL, NULL),
('US-DE', 'DE', 'DELAWARE', 79, 'US', NULL, NULL, NULL, NULL),
('US-FL', 'FL', 'FLORIDA', 79, 'US', NULL, NULL, NULL, NULL),
('US-GA', 'GA', 'GEORGIA', 79, 'US', NULL, NULL, NULL, NULL),
('US-GU', 'GU', 'GUAM', 57, 'US', NULL, NULL, NULL, NULL),
('US-HI', 'HI', 'HAWAII', 79, 'US', NULL, NULL, NULL, NULL),
('US-IA', 'IA', 'IOWA', 79, 'US', NULL, NULL, NULL, NULL),
('US-ID', 'ID', 'IDAHO', 79, 'US', NULL, NULL, NULL, NULL),
('US-IL', 'IL', 'ILLINOIS', 79, 'US', NULL, NULL, NULL, NULL),
('US-IN', 'IN', 'INDIANA', 79, 'US', NULL, NULL, NULL, NULL),
('US-KS', 'KS', 'KANSAS', 79, 'US', NULL, NULL, NULL, NULL),
('US-KY', 'KY', 'KENTUCKY', 79, 'US', NULL, NULL, NULL, NULL),
('US-LA', 'LA', 'LOUISIANA', 79, 'US', NULL, NULL, NULL, NULL),
('US-MA', 'MA', 'MASSACHUSETTS', 79, 'US', NULL, NULL, NULL, NULL),
('US-MD', 'MD', 'MARYLAND', 79, 'US', NULL, NULL, NULL, NULL),
('US-ME', 'ME', 'MAINE', 79, 'US', NULL, NULL, NULL, NULL),
('US-MI', 'MI', 'MICHIGAN', 79, 'US', NULL, NULL, NULL, NULL),
('US-MN', 'MN', 'MINNESOTA', 79, 'US', NULL, NULL, NULL, NULL),
('US-MO', 'MO', 'MISSOURI', 79, 'US', NULL, NULL, NULL, NULL),
('US-MP', 'MP', 'NORTHERN MARIANA ISLANDS', 57, 'US', NULL, NULL, NULL, NULL),
('US-MS', 'MS', 'MISSISSIPPI', 79, 'US', NULL, NULL, NULL, NULL),
('US-MT', 'MT', 'MONTANA', 79, 'US', NULL, NULL, NULL, NULL),
('US-NC', 'NC', 'NORTH CAROLINA', 79, 'US', NULL, NULL, NULL, NULL),
('US-ND', 'ND', 'NORTH DAKOTA', 79, 'US', NULL, NULL, NULL, NULL),
('US-NE', 'NE', 'NEBRASKA', 79, 'US', NULL, NULL, NULL, NULL),
('US-NH', 'NH', 'NEW HAMPSHIRE', 79, 'US', NULL, NULL, NULL, NULL),
('US-NJ', 'NJ', 'NEW JERSEY', 79, 'US', NULL, NULL, NULL, NULL),
('US-NM', 'NM', 'NEW MEXICO', 79, 'US', NULL, NULL, NULL, NULL),
('US-NV', 'NV', 'NEVADA', 79, 'US', NULL, NULL, NULL, NULL),
('US-NY', 'NY', 'NEW YORK', 79, 'US', NULL, NULL, NULL, NULL),
('US-OH', 'OH', 'OHIO', 79, 'US', NULL, NULL, NULL, NULL),
('US-OK', 'OK', 'OKLAHOMA', 79, 'US', NULL, NULL, NULL, NULL),
('US-OR', 'OR', 'OREGON', 79, 'US', NULL, NULL, NULL, NULL),
('US-PA', 'PA', 'PENNSYLVANIA', 79, 'US', NULL, NULL, NULL, NULL),
('US-PR', 'PR', 'PUERTO RICO', 57, 'US', NULL, NULL, NULL, NULL),
('US-RI', 'RI', 'RHODE ISLAND', 79, 'US', NULL, NULL, NULL, NULL),
('US-SC', 'SC', 'SOUTH CAROLINA', 79, 'US', NULL, NULL, NULL, NULL),
('US-SD', 'SD', 'SOUTH DAKOTA', 79, 'US', NULL, NULL, NULL, NULL),
('US-TN', 'TN', 'TENNESSEE', 79, 'US', NULL, NULL, NULL, NULL),
('US-TX', 'TX', 'TEXAS', 79, 'US', NULL, NULL, NULL, NULL),
('US-UM', 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 57, 'US', NULL, NULL, NULL, NULL),
('US-UT', 'UT', 'UTAH', 79, 'US', NULL, NULL, NULL, NULL),
('US-VA', 'VA', 'VIRGINIA', 79, 'US', NULL, NULL, NULL, NULL),
('US-VI', 'VI', 'VIRGIN ISLANDS', 57, 'US', NULL, NULL, NULL, NULL),
('US-VT', 'VT', 'VERMONT', 79, 'US', NULL, NULL, NULL, NULL),
('US-WA', 'WA', 'WASHINGTON', 79, 'US', NULL, NULL, NULL, NULL),
('US-WI', 'WI', 'WISCONSIN', 79, 'US', NULL, NULL, NULL, NULL),
('US-WV', 'WV', 'WEST VIRGINIA', 79, 'US', NULL, NULL, NULL, NULL),
('US-WY', 'WY', 'WYOMING', 79, 'US', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_country_sub_cat`
--

CREATE TABLE `blc_country_sub_cat` (
  `COUNTRY_SUB_CAT_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_country_sub_cat`
--

INSERT INTO `blc_country_sub_cat` (`COUNTRY_SUB_CAT_ID`, `NAME`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(1, 'Administration', NULL, NULL, NULL, NULL),
(2, 'Administrative Atoll', NULL, NULL, NULL, NULL),
(3, 'Administrative Region', NULL, NULL, NULL, NULL),
(4, 'Area', NULL, NULL, NULL, NULL),
(5, 'Autonomous City', NULL, NULL, NULL, NULL),
(6, 'Autonomous Community', NULL, NULL, NULL, NULL),
(7, 'Autonomous District', NULL, NULL, NULL, NULL),
(8, 'Autonomous Municipality', NULL, NULL, NULL, NULL),
(9, 'Autonomous Province', NULL, NULL, NULL, NULL),
(10, 'Autonomous Region', NULL, NULL, NULL, NULL),
(11, 'Autonomous Republic', NULL, NULL, NULL, NULL),
(12, 'Autonomous Sector', NULL, NULL, NULL, NULL),
(13, 'Autonomous Territorial Unit', NULL, NULL, NULL, NULL),
(14, 'Borough', NULL, NULL, NULL, NULL),
(15, 'Canton', NULL, NULL, NULL, NULL),
(16, 'Capital', NULL, NULL, NULL, NULL),
(17, 'Capital City', NULL, NULL, NULL, NULL),
(18, 'Capital District', NULL, NULL, NULL, NULL),
(19, 'Capital Metropolitan City', NULL, NULL, NULL, NULL),
(20, 'City', NULL, NULL, NULL, NULL),
(21, 'City of County Right', NULL, NULL, NULL, NULL),
(22, 'Commune', NULL, NULL, NULL, NULL),
(23, 'Constitutional Province', NULL, NULL, NULL, NULL),
(24, 'Corporation', NULL, NULL, NULL, NULL),
(25, 'Council Area', NULL, NULL, NULL, NULL),
(26, 'Country', NULL, NULL, NULL, NULL),
(27, 'County', NULL, NULL, NULL, NULL),
(28, 'Department', NULL, NULL, NULL, NULL),
(29, 'Dependency', NULL, NULL, NULL, NULL),
(30, 'Development Region', NULL, NULL, NULL, NULL),
(31, 'District', NULL, NULL, NULL, NULL),
(32, 'District With Special Status', NULL, NULL, NULL, NULL),
(33, 'Division', NULL, NULL, NULL, NULL),
(34, 'Economic Prefecture', NULL, NULL, NULL, NULL),
(35, 'Emirate', NULL, NULL, NULL, NULL),
(36, 'Entity', NULL, NULL, NULL, NULL),
(37, 'Federal Capital Territory', NULL, NULL, NULL, NULL),
(38, 'Federal Dependency', NULL, NULL, NULL, NULL),
(39, 'Federal District', NULL, NULL, NULL, NULL),
(40, 'Federal Land', NULL, NULL, NULL, NULL),
(41, 'Federal Territory', NULL, NULL, NULL, NULL),
(42, 'Federated States', NULL, NULL, NULL, NULL),
(43, 'Geographical Entity', NULL, NULL, NULL, NULL),
(44, 'Geographical Regions', NULL, NULL, NULL, NULL),
(45, 'Governorate', NULL, NULL, NULL, NULL),
(46, 'Indigenous Region', NULL, NULL, NULL, NULL),
(47, 'Island', NULL, NULL, NULL, NULL),
(48, 'Island Council', NULL, NULL, NULL, NULL),
(49, 'Local Council', NULL, NULL, NULL, NULL),
(50, 'Metropolitan Administration', NULL, NULL, NULL, NULL),
(51, 'Metropolitan City', NULL, NULL, NULL, NULL),
(52, 'Metropolitan Department', NULL, NULL, NULL, NULL),
(53, 'Metropolitan Region', NULL, NULL, NULL, NULL),
(54, 'Municipality', NULL, NULL, NULL, NULL),
(55, 'Oblast', NULL, NULL, NULL, NULL),
(56, 'Outlying Area', NULL, NULL, NULL, NULL),
(57, 'Outlying Territory', NULL, NULL, NULL, NULL),
(58, 'Overseas Region', NULL, NULL, NULL, NULL),
(59, 'Overseas Territorial Collectivity', NULL, NULL, NULL, NULL),
(60, 'Parish', NULL, NULL, NULL, NULL),
(61, 'Popularate', NULL, NULL, NULL, NULL),
(62, 'Prefecture', NULL, NULL, NULL, NULL),
(63, 'Province', NULL, NULL, NULL, NULL),
(64, 'Quarter', NULL, NULL, NULL, NULL),
(65, 'Rayon', NULL, NULL, NULL, NULL),
(66, 'Region', NULL, NULL, NULL, NULL),
(67, 'Regional Council', NULL, NULL, NULL, NULL),
(68, 'Republic', NULL, NULL, NULL, NULL),
(69, 'Republican City', NULL, NULL, NULL, NULL),
(70, 'Self-governed Part', NULL, NULL, NULL, NULL),
(71, 'Special Administrative City', NULL, NULL, NULL, NULL),
(72, 'Special Administrative Region', NULL, NULL, NULL, NULL),
(73, 'Special City', NULL, NULL, NULL, NULL),
(74, 'Special District', NULL, NULL, NULL, NULL),
(75, 'Special Island Authority', NULL, NULL, NULL, NULL),
(76, 'Special Municipality', NULL, NULL, NULL, NULL),
(77, 'Special Region', NULL, NULL, NULL, NULL),
(78, 'Special Zone', NULL, NULL, NULL, NULL),
(79, 'State', NULL, NULL, NULL, NULL),
(80, 'Territorial Unit', NULL, NULL, NULL, NULL),
(81, 'Territory', NULL, NULL, NULL, NULL),
(82, 'Town Council', NULL, NULL, NULL, NULL),
(83, 'Union Territory', NULL, NULL, NULL, NULL),
(84, 'Unitary Authority', NULL, NULL, NULL, NULL),
(85, 'Urban Community', NULL, NULL, NULL, NULL),
(86, 'Zone', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_credit_card_payment`
--

CREATE TABLE `blc_credit_card_payment` (
  `PAYMENT_ID` bigint(20) NOT NULL,
  `EXPIRATION_MONTH` int(11) NOT NULL,
  `EXPIRATION_YEAR` int(11) NOT NULL,
  `NAME_ON_CARD` varchar(255) NOT NULL,
  `PAN` varchar(255) NOT NULL,
  `REFERENCE_NUMBER` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_currency`
--

CREATE TABLE `blc_currency` (
  `CURRENCY_CODE` varchar(255) NOT NULL,
  `DEFAULT_FLAG` tinyint(1) DEFAULT NULL,
  `FRIENDLY_NAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_currency`
--

INSERT INTO `blc_currency` (`CURRENCY_CODE`, `DEFAULT_FLAG`, `FRIENDLY_NAME`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
('EUR', 0, 'EURO Dollar', NULL, NULL, NULL, NULL),
('GBP', 0, 'GB Pound', NULL, NULL, NULL, NULL),
('MXN', 0, 'Mexican Peso', NULL, NULL, NULL, NULL),
('USD', 1, 'US Dollar', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_customer`
--

CREATE TABLE `blc_customer` (
  `CUSTOMER_ID` bigint(20) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  `CHALLENGE_ANSWER` varchar(255) DEFAULT NULL,
  `DEACTIVATED` tinyint(1) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `IS_TAX_EXEMPT` tinyint(1) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `PASSWORD_CHANGE_REQUIRED` tinyint(1) DEFAULT NULL,
  `IS_PREVIEW` tinyint(1) DEFAULT NULL,
  `RECEIVE_EMAIL` tinyint(1) DEFAULT NULL,
  `IS_REGISTERED` tinyint(1) DEFAULT NULL,
  `TAX_EXEMPTION_CODE` varchar(255) DEFAULT NULL,
  `USER_NAME` varchar(255) DEFAULT NULL,
  `CHALLENGE_QUESTION_ID` bigint(20) DEFAULT NULL,
  `LOCALE_CODE` varchar(255) DEFAULT NULL,
  `ARCHIVED` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_customer`
--

INSERT INTO `blc_customer` (`CUSTOMER_ID`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`, `CHALLENGE_ANSWER`, `DEACTIVATED`, `EMAIL_ADDRESS`, `FIRST_NAME`, `IS_TAX_EXEMPT`, `LAST_NAME`, `PASSWORD`, `PASSWORD_CHANGE_REQUIRED`, `IS_PREVIEW`, `RECEIVE_EMAIL`, `IS_REGISTERED`, `TAX_EXEMPTION_CODE`, `USER_NAME`, `CHALLENGE_QUESTION_ID`, `LOCALE_CODE`, `ARCHIVED`) VALUES
(400, 400, '2017-09-19 17:36:52', '2017-09-19 17:36:52', 400, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1, 0, NULL, '400', NULL, NULL, NULL),
(500, 500, '2017-09-19 17:52:04', '2017-09-19 17:52:04', 500, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1, 0, NULL, '500', NULL, NULL, NULL),
(600, 600, '2017-09-19 17:57:10', '2017-09-19 17:57:10', 600, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1, 0, NULL, '600', NULL, NULL, NULL),
(700, 700, '2017-09-19 18:02:00', '2017-09-19 18:02:00', 700, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1, 0, NULL, '700', NULL, NULL, NULL),
(800, 800, '2017-09-19 18:05:52', '2017-09-19 18:05:52', 800, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1, 0, NULL, '800', NULL, NULL, NULL),
(900, 900, '2017-09-19 18:27:08', '2017-09-19 18:27:08', 900, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1, 0, NULL, '900', NULL, NULL, NULL),
(1000, 1000, '2017-09-20 19:11:41', '2017-09-20 19:11:41', 1000, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1, 0, NULL, '1000', NULL, NULL, NULL),
(3001, 3001, '2017-10-03 12:27:23', '2017-10-03 12:27:23', 3001, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1, 0, NULL, '3001', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_customer_address`
--

CREATE TABLE `blc_customer_address` (
  `CUSTOMER_ADDRESS_ID` bigint(20) NOT NULL,
  `ADDRESS_NAME` varchar(255) DEFAULT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `ADDRESS_ID` bigint(20) NOT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_customer_attribute`
--

CREATE TABLE `blc_customer_attribute` (
  `CUSTOMER_ATTR_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_customer_offer_xref`
--

CREATE TABLE `blc_customer_offer_xref` (
  `CUSTOMER_OFFER_ID` bigint(20) NOT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL,
  `OFFER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_customer_password_token`
--

CREATE TABLE `blc_customer_password_token` (
  `PASSWORD_TOKEN` varchar(255) NOT NULL,
  `CREATE_DATE` datetime NOT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL,
  `TOKEN_USED_DATE` datetime DEFAULT NULL,
  `TOKEN_USED_FLAG` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_customer_payment`
--

CREATE TABLE `blc_customer_payment` (
  `CUSTOMER_PAYMENT_ID` bigint(20) NOT NULL,
  `IS_DEFAULT` tinyint(1) DEFAULT NULL,
  `GATEWAY_TYPE` varchar(255) DEFAULT NULL,
  `PAYMENT_TOKEN` varchar(255) DEFAULT NULL,
  `PAYMENT_TYPE` varchar(255) DEFAULT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_customer_payment_fields`
--

CREATE TABLE `blc_customer_payment_fields` (
  `CUSTOMER_PAYMENT_ID` bigint(20) NOT NULL,
  `FIELD_VALUE` longtext,
  `FIELD_NAME` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_customer_phone`
--

CREATE TABLE `blc_customer_phone` (
  `CUSTOMER_PHONE_ID` bigint(20) NOT NULL,
  `PHONE_NAME` varchar(255) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL,
  `PHONE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_customer_role`
--

CREATE TABLE `blc_customer_role` (
  `CUSTOMER_ROLE_ID` bigint(20) NOT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_cust_site_map_gen_cfg`
--

CREATE TABLE `blc_cust_site_map_gen_cfg` (
  `GEN_CONFIG_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_cust_site_map_gen_cfg`
--

INSERT INTO `blc_cust_site_map_gen_cfg` (`GEN_CONFIG_ID`) VALUES
(-1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_data_drvn_enum`
--

CREATE TABLE `blc_data_drvn_enum` (
  `ENUM_ID` bigint(20) NOT NULL,
  `ENUM_KEY` varchar(255) DEFAULT NULL,
  `MODIFIABLE` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_data_drvn_enum_val`
--

CREATE TABLE `blc_data_drvn_enum_val` (
  `ENUM_VAL_ID` bigint(20) NOT NULL,
  `DISPLAY` varchar(255) DEFAULT NULL,
  `HIDDEN` tinyint(1) DEFAULT NULL,
  `ENUM_KEY` varchar(255) DEFAULT NULL,
  `ENUM_TYPE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_discrete_order_item`
--

CREATE TABLE `blc_discrete_order_item` (
  `BASE_RETAIL_PRICE` decimal(19,5) DEFAULT NULL,
  `BASE_SALE_PRICE` decimal(19,5) DEFAULT NULL,
  `ORDER_ITEM_ID` bigint(20) NOT NULL,
  `BUNDLE_ORDER_ITEM_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `SKU_ID` bigint(20) NOT NULL,
  `SKU_BUNDLE_ITEM_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_discrete_order_item`
--

INSERT INTO `blc_discrete_order_item` (`BASE_RETAIL_PRICE`, `BASE_SALE_PRICE`, `ORDER_ITEM_ID`, `BUNDLE_ORDER_ITEM_ID`, `PRODUCT_ID`, `SKU_ID`, `SKU_BUNDLE_ITEM_ID`) VALUES
('5.99000', '4.49000', 6, NULL, 18, 18, NULL),
('3.99000', '2.99000', 7, NULL, 11, 11, NULL),
('12.99000', '10.99000', 51, NULL, 10, 10, NULL),
('4.99000', '3.99000', 52, NULL, 8, 8, NULL),
('4.99000', '4.99000', 101, NULL, 3, 3, NULL),
('12.99000', '12.99000', 102, NULL, 9, 9, NULL),
('3.99000', '2.99000', 103, NULL, 11, 11, NULL),
('4.99000', '4.99000', 104, NULL, 12, 12, NULL),
('4.99000', '4.99000', 151, NULL, 3, 3, NULL),
('12.99000', '12.99000', 152, NULL, 9, 9, NULL),
('4.99000', '3.99000', 153, NULL, 8, 8, NULL),
('11.99000', '9.99000', 154, NULL, 7, 7, NULL),
('4.99000', '4.99000', 201, NULL, 3, 3, NULL),
('4.99000', '4.99000', 202, NULL, 12, 12, NULL),
('4.99000', '4.99000', 268, NULL, 3, 3, NULL),
('6.99000', '6.99000', 269, NULL, 6, 6, NULL),
('6.99000', '6.99000', 270, NULL, 5, 5, NULL),
('6.99000', '6.99000', 271, NULL, 4, 4, NULL),
('11.99000', '9.99000', 301, NULL, 7, 7, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_disc_item_fee_price`
--

CREATE TABLE `blc_disc_item_fee_price` (
  `DISC_ITEM_FEE_PRICE_ID` bigint(20) NOT NULL,
  `AMOUNT` decimal(19,5) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REPORTING_CODE` varchar(255) DEFAULT NULL,
  `ORDER_ITEM_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_dyn_discrete_order_item`
--

CREATE TABLE `blc_dyn_discrete_order_item` (
  `ORDER_ITEM_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_email_tracking`
--

CREATE TABLE `blc_email_tracking` (
  `EMAIL_TRACKING_ID` bigint(20) NOT NULL,
  `DATE_SENT` datetime DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_email_tracking_clicks`
--

CREATE TABLE `blc_email_tracking_clicks` (
  `CLICK_ID` bigint(20) NOT NULL,
  `CUSTOMER_ID` varchar(255) DEFAULT NULL,
  `DATE_CLICKED` datetime NOT NULL,
  `DESTINATION_URI` varchar(255) DEFAULT NULL,
  `QUERY_STRING` varchar(255) DEFAULT NULL,
  `EMAIL_TRACKING_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_email_tracking_opens`
--

CREATE TABLE `blc_email_tracking_opens` (
  `OPEN_ID` bigint(20) NOT NULL,
  `DATE_OPENED` datetime DEFAULT NULL,
  `USER_AGENT` varchar(255) DEFAULT NULL,
  `EMAIL_TRACKING_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_fg_adjustment`
--

CREATE TABLE `blc_fg_adjustment` (
  `FG_ADJUSTMENT_ID` bigint(20) NOT NULL,
  `ADJUSTMENT_REASON` varchar(255) NOT NULL,
  `ADJUSTMENT_VALUE` decimal(19,5) NOT NULL,
  `FULFILLMENT_GROUP_ID` bigint(20) DEFAULT NULL,
  `OFFER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_fg_fee_tax_xref`
--

CREATE TABLE `blc_fg_fee_tax_xref` (
  `FULFILLMENT_GROUP_FEE_ID` bigint(20) NOT NULL,
  `TAX_DETAIL_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_fg_fg_tax_xref`
--

CREATE TABLE `blc_fg_fg_tax_xref` (
  `FULFILLMENT_GROUP_ID` bigint(20) NOT NULL,
  `TAX_DETAIL_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_fg_item_tax_xref`
--

CREATE TABLE `blc_fg_item_tax_xref` (
  `FULFILLMENT_GROUP_ITEM_ID` bigint(20) NOT NULL,
  `TAX_DETAIL_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_field`
--

CREATE TABLE `blc_field` (
  `FIELD_ID` bigint(20) NOT NULL,
  `ABBREVIATION` varchar(255) DEFAULT NULL,
  `ENTITY_TYPE` varchar(255) NOT NULL,
  `FRIENDLY_NAME` varchar(255) DEFAULT NULL,
  `OVERRIDE_GENERATED_PROP_NAME` tinyint(1) DEFAULT NULL,
  `PROPERTY_NAME` varchar(255) NOT NULL,
  `TRANSLATABLE` tinyint(1) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_field`
--

INSERT INTO `blc_field` (`FIELD_ID`, `ABBREVIATION`, `ENTITY_TYPE`, `FRIENDLY_NAME`, `OVERRIDE_GENERATED_PROP_NAME`, `PROPERTY_NAME`, `TRANSLATABLE`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(1, 'mfg', 'PRODUCT', 'Manufacturer', NULL, 'manufacturer', NULL, NULL, NULL, NULL, NULL),
(2, 'heatRange', 'PRODUCT', 'Heat Range', NULL, 'productAttributes(heatRange).value', NULL, NULL, NULL, NULL, NULL),
(3, 'price', 'PRODUCT', 'Price', NULL, 'defaultSku.price', NULL, NULL, NULL, NULL, NULL),
(4, 'name', 'PRODUCT', 'Product Name', NULL, 'defaultSku.name', 1, NULL, NULL, NULL, NULL),
(5, 'model', 'PRODUCT', 'Model', NULL, 'model', NULL, NULL, NULL, NULL, NULL),
(6, 'desc', 'PRODUCT', 'Description', NULL, 'defaultSku.description', 1, NULL, NULL, NULL, NULL),
(7, 'ldesc', 'PRODUCT', 'Long Description', NULL, 'defaultSku.longDescription', 1, NULL, NULL, NULL, NULL),
(8, 'color', 'PRODUCT', 'Color', NULL, 'productOptionValuesMap(COLOR)', NULL, NULL, NULL, NULL, NULL),
(9, 'margin', 'PRODUCT', 'Margin', NULL, 'margin', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_fld_def`
--

CREATE TABLE `blc_fld_def` (
  `FLD_DEF_ID` bigint(20) NOT NULL,
  `ALLOW_MULTIPLES` tinyint(1) DEFAULT NULL,
  `COLUMN_WIDTH` varchar(255) DEFAULT NULL,
  `FLD_ORDER` int(11) DEFAULT NULL,
  `FLD_TYPE` varchar(255) DEFAULT NULL,
  `FRIENDLY_NAME` varchar(255) DEFAULT NULL,
  `HELP_TEXT` varchar(255) DEFAULT NULL,
  `HIDDEN_FLAG` tinyint(1) DEFAULT NULL,
  `HINT` varchar(255) DEFAULT NULL,
  `MAX_LENGTH` int(11) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REQUIRED_FLAG` tinyint(1) DEFAULT NULL,
  `SECURITY_LEVEL` varchar(255) DEFAULT NULL,
  `TEXT_AREA_FLAG` tinyint(1) DEFAULT NULL,
  `TOOLTIP` varchar(255) DEFAULT NULL,
  `VLDTN_ERROR_MSSG_KEY` varchar(255) DEFAULT NULL,
  `VLDTN_REGEX` varchar(255) DEFAULT NULL,
  `ENUM_ID` bigint(20) DEFAULT NULL,
  `FLD_GROUP_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_fld_def`
--

INSERT INTO `blc_fld_def` (`FLD_DEF_ID`, `ALLOW_MULTIPLES`, `COLUMN_WIDTH`, `FLD_ORDER`, `FLD_TYPE`, `FRIENDLY_NAME`, `HELP_TEXT`, `HIDDEN_FLAG`, `HINT`, `MAX_LENGTH`, `NAME`, `REQUIRED_FLAG`, `SECURITY_LEVEL`, `TEXT_AREA_FLAG`, `TOOLTIP`, `VLDTN_ERROR_MSSG_KEY`, `VLDTN_REGEX`, `ENUM_ID`, `FLD_GROUP_ID`) VALUES
(2, 0, '*', 1, 'HTML', 'Body', NULL, 0, NULL, NULL, 'body', NULL, NULL, 0, NULL, NULL, NULL, NULL, 1),
(3, 0, '*', 0, 'STRING', 'Title', NULL, 0, NULL, NULL, 'title', NULL, NULL, 0, NULL, NULL, NULL, NULL, 1),
(7, 0, '*', 0, 'ASSET_LOOKUP', 'Image URL', NULL, 0, NULL, 150, 'imageUrl', NULL, NULL, 0, NULL, NULL, NULL, NULL, 4),
(8, 0, '*', 1, 'STRING', 'Target URL', NULL, 0, NULL, 150, 'targetUrl', NULL, NULL, 0, NULL, NULL, NULL, NULL, 4),
(9, 0, '*', 0, 'STRING', 'Message Text', NULL, 0, NULL, 150, 'messageText', NULL, NULL, 0, NULL, NULL, NULL, NULL, 6),
(10, 0, '*', 0, 'HTML', 'HTML Content', NULL, 0, NULL, NULL, 'htmlContent', NULL, NULL, 0, NULL, NULL, NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `blc_fld_enum`
--

CREATE TABLE `blc_fld_enum` (
  `FLD_ENUM_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_fld_enum_item`
--

CREATE TABLE `blc_fld_enum_item` (
  `FLD_ENUM_ITEM_ID` bigint(20) NOT NULL,
  `FLD_ORDER` int(11) DEFAULT NULL,
  `FRIENDLY_NAME` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `FLD_ENUM_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_fld_group`
--

CREATE TABLE `blc_fld_group` (
  `FLD_GROUP_ID` bigint(20) NOT NULL,
  `INIT_COLLAPSED_FLAG` tinyint(1) DEFAULT NULL,
  `IS_MASTER_FIELD_GROUP` tinyint(1) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_fld_group`
--

INSERT INTO `blc_fld_group` (`FLD_GROUP_ID`, `INIT_COLLAPSED_FLAG`, `IS_MASTER_FIELD_GROUP`, `NAME`) VALUES
(1, 0, NULL, 'Content'),
(4, 0, NULL, 'Ad Fields'),
(5, 0, NULL, 'HTML Fields'),
(6, 0, NULL, 'Message Fields');

-- --------------------------------------------------------

--
-- Table structure for table `blc_fulfillment_group`
--

CREATE TABLE `blc_fulfillment_group` (
  `FULFILLMENT_GROUP_ID` bigint(20) NOT NULL,
  `DELIVERY_INSTRUCTION` varchar(255) DEFAULT NULL,
  `PRICE` decimal(19,5) DEFAULT NULL,
  `SHIPPING_PRICE_TAXABLE` tinyint(1) DEFAULT NULL,
  `MERCHANDISE_TOTAL` decimal(19,5) DEFAULT NULL,
  `METHOD` varchar(255) DEFAULT NULL,
  `IS_PRIMARY` tinyint(1) DEFAULT NULL,
  `REFERENCE_NUMBER` varchar(255) DEFAULT NULL,
  `RETAIL_PRICE` decimal(19,5) DEFAULT NULL,
  `SALE_PRICE` decimal(19,5) DEFAULT NULL,
  `FULFILLMENT_GROUP_SEQUNCE` int(11) DEFAULT NULL,
  `SERVICE` varchar(255) DEFAULT NULL,
  `SHIPPING_OVERRIDE` tinyint(1) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TOTAL` decimal(19,5) DEFAULT NULL,
  `TOTAL_FEE_TAX` decimal(19,5) DEFAULT NULL,
  `TOTAL_FG_TAX` decimal(19,5) DEFAULT NULL,
  `TOTAL_ITEM_TAX` decimal(19,5) DEFAULT NULL,
  `TOTAL_TAX` decimal(19,5) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL,
  `FULFILLMENT_OPTION_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) NOT NULL,
  `PERSONAL_MESSAGE_ID` bigint(20) DEFAULT NULL,
  `PHONE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_fulfillment_group`
--

INSERT INTO `blc_fulfillment_group` (`FULFILLMENT_GROUP_ID`, `DELIVERY_INSTRUCTION`, `PRICE`, `SHIPPING_PRICE_TAXABLE`, `MERCHANDISE_TOTAL`, `METHOD`, `IS_PRIMARY`, `REFERENCE_NUMBER`, `RETAIL_PRICE`, `SALE_PRICE`, `FULFILLMENT_GROUP_SEQUNCE`, `SERVICE`, `SHIPPING_OVERRIDE`, `STATUS`, `TOTAL`, `TOTAL_FEE_TAX`, `TOTAL_FG_TAX`, `TOTAL_ITEM_TAX`, `TOTAL_TAX`, `TYPE`, `ADDRESS_ID`, `FULFILLMENT_OPTION_ID`, `ORDER_ID`, `PERSONAL_MESSAGE_ID`, `PHONE_ID`) VALUES
(4, NULL, '0.00000', 0, '17.95000', NULL, 0, NULL, '0.00000', '0.00000', NULL, NULL, NULL, NULL, '17.95000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, NULL, NULL, 1, NULL, NULL),
(51, NULL, '0.00000', 0, '14.98000', NULL, 0, NULL, '0.00000', '0.00000', NULL, NULL, NULL, NULL, '14.98000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, NULL, NULL, 51, NULL, NULL),
(101, NULL, '0.00000', 0, '102.77000', NULL, 0, NULL, '0.00000', '0.00000', NULL, NULL, NULL, NULL, '102.77000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, NULL, NULL, 101, NULL, NULL),
(151, NULL, '0.00000', 0, '166.78000', NULL, 0, NULL, '0.00000', '0.00000', NULL, NULL, NULL, NULL, '166.78000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, NULL, NULL, 151, NULL, NULL),
(201, NULL, '0.00000', 0, '109.78000', NULL, 0, NULL, '0.00000', '0.00000', NULL, NULL, NULL, NULL, '109.78000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, NULL, NULL, 201, NULL, NULL),
(255, NULL, '5.00000', 0, '203.68000', NULL, 0, NULL, '5.00000', '5.00000', NULL, NULL, NULL, NULL, '208.68000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, 2, 1, 251, 1, NULL),
(301, NULL, '0.00000', 0, '27.97000', NULL, 0, NULL, '0.00000', '0.00000', NULL, NULL, NULL, NULL, '27.97000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, NULL, NULL, 301, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_fulfillment_group_fee`
--

CREATE TABLE `blc_fulfillment_group_fee` (
  `FULFILLMENT_GROUP_FEE_ID` bigint(20) NOT NULL,
  `AMOUNT` decimal(19,5) DEFAULT NULL,
  `FEE_TAXABLE_FLAG` tinyint(1) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REPORTING_CODE` varchar(255) DEFAULT NULL,
  `TOTAL_FEE_TAX` decimal(19,5) DEFAULT NULL,
  `FULFILLMENT_GROUP_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_fulfillment_group_item`
--

CREATE TABLE `blc_fulfillment_group_item` (
  `FULFILLMENT_GROUP_ITEM_ID` bigint(20) NOT NULL,
  `PRORATED_ORDER_ADJ` decimal(19,2) DEFAULT NULL,
  `QUANTITY` int(11) NOT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TOTAL_ITEM_AMOUNT` decimal(19,5) DEFAULT NULL,
  `TOTAL_ITEM_TAXABLE_AMOUNT` decimal(19,5) DEFAULT NULL,
  `TOTAL_ITEM_TAX` decimal(19,5) DEFAULT NULL,
  `FULFILLMENT_GROUP_ID` bigint(20) NOT NULL,
  `ORDER_ITEM_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_fulfillment_group_item`
--

INSERT INTO `blc_fulfillment_group_item` (`FULFILLMENT_GROUP_ITEM_ID`, `PRORATED_ORDER_ADJ`, `QUANTITY`, `STATUS`, `TOTAL_ITEM_AMOUNT`, `TOTAL_ITEM_TAXABLE_AMOUNT`, `TOTAL_ITEM_TAX`, `FULFILLMENT_GROUP_ID`, `ORDER_ITEM_ID`) VALUES
(6, '0.00', 2, NULL, '8.98000', '8.98000', '0.00000', 4, 6),
(7, '0.00', 3, NULL, '8.97000', '8.97000', '0.00000', 4, 7),
(51, '5.49', 1, NULL, '10.99000', '5.50000', '0.00000', 51, 51),
(52, '2.00', 1, NULL, '3.99000', '1.99000', '0.00000', 51, 52),
(101, '0.00', 2, NULL, '9.98000', '9.98000', '0.00000', 101, 101),
(102, '0.00', 1, NULL, '12.99000', '12.99000', '0.00000', 101, 102),
(103, '0.00', 10, NULL, '29.90000', '29.90000', '0.00000', 101, 103),
(104, '0.00', 10, NULL, '49.90000', '49.90000', '0.00000', 101, 104),
(151, '0.00', 10, NULL, '49.90000', '49.90000', '0.00000', 151, 151),
(152, '0.00', 1, NULL, '12.99000', '12.99000', '0.00000', 151, 152),
(153, '0.00', 1, NULL, '3.99000', '3.99000', '0.00000', 151, 153),
(154, '0.00', 10, NULL, '99.90000', '99.90000', '0.00000', 151, 154),
(201, '0.00', 2, NULL, '9.98000', '9.98000', '0.00000', 201, 201),
(202, '0.00', 20, NULL, '99.80000', '99.80000', '0.00000', 201, 202),
(268, '12.24', 10, NULL, '49.90000', '37.66000', '0.00000', 255, 268),
(269, '34.32', 20, NULL, '139.80000', '105.48000', '0.00000', 255, 269),
(270, '1.72', 1, NULL, '6.99000', '5.27000', '0.00000', 255, 270),
(271, '1.72', 1, NULL, '6.99000', '5.27000', '0.00000', 255, 271),
(301, '0.00', 3, NULL, '27.97000', '27.97000', '0.00000', 301, 301);

-- --------------------------------------------------------

--
-- Table structure for table `blc_fulfillment_option`
--

CREATE TABLE `blc_fulfillment_option` (
  `FULFILLMENT_OPTION_ID` bigint(20) NOT NULL,
  `FULFILLMENT_TYPE` varchar(255) NOT NULL,
  `LONG_DESCRIPTION` longtext,
  `NAME` varchar(255) DEFAULT NULL,
  `TAX_CODE` varchar(255) DEFAULT NULL,
  `TAXABLE` tinyint(1) DEFAULT NULL,
  `USE_FLAT_RATES` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_fulfillment_option`
--

INSERT INTO `blc_fulfillment_option` (`FULFILLMENT_OPTION_ID`, `FULFILLMENT_TYPE`, `LONG_DESCRIPTION`, `NAME`, `TAX_CODE`, `TAXABLE`, `USE_FLAT_RATES`) VALUES
(1, 'PHYSICAL_SHIP', '5 - 7 Days', 'Standard', NULL, NULL, 0),
(2, 'PHYSICAL_SHIP', '3 - 5 Days', 'Priority', NULL, NULL, 0),
(3, 'PHYSICAL_SHIP', '1 - 2 Days', 'Express', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `blc_fulfillment_option_fixed`
--

CREATE TABLE `blc_fulfillment_option_fixed` (
  `PRICE` decimal(19,5) NOT NULL,
  `FULFILLMENT_OPTION_ID` bigint(20) NOT NULL,
  `CURRENCY_CODE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_fulfillment_option_fixed`
--

INSERT INTO `blc_fulfillment_option_fixed` (`PRICE`, `FULFILLMENT_OPTION_ID`, `CURRENCY_CODE`) VALUES
('5.00000', 1, NULL),
('10.00000', 2, NULL),
('20.00000', 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_fulfillment_opt_banded_prc`
--

CREATE TABLE `blc_fulfillment_opt_banded_prc` (
  `FULFILLMENT_OPTION_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_fulfillment_opt_banded_wgt`
--

CREATE TABLE `blc_fulfillment_opt_banded_wgt` (
  `FULFILLMENT_OPTION_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_fulfillment_price_band`
--

CREATE TABLE `blc_fulfillment_price_band` (
  `FULFILLMENT_PRICE_BAND_ID` bigint(20) NOT NULL,
  `RESULT_AMOUNT` decimal(19,5) NOT NULL,
  `RESULT_AMOUNT_TYPE` varchar(255) NOT NULL,
  `RETAIL_PRICE_MINIMUM_AMOUNT` decimal(19,5) NOT NULL,
  `FULFILLMENT_OPTION_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_fulfillment_weight_band`
--

CREATE TABLE `blc_fulfillment_weight_band` (
  `FULFILLMENT_WEIGHT_BAND_ID` bigint(20) NOT NULL,
  `RESULT_AMOUNT` decimal(19,5) NOT NULL,
  `RESULT_AMOUNT_TYPE` varchar(255) NOT NULL,
  `MINIMUM_WEIGHT` decimal(19,5) DEFAULT NULL,
  `WEIGHT_UNIT_OF_MEASURE` varchar(255) DEFAULT NULL,
  `FULFILLMENT_OPTION_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_giftwrap_order_item`
--

CREATE TABLE `blc_giftwrap_order_item` (
  `ORDER_ITEM_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_gift_card_payment`
--

CREATE TABLE `blc_gift_card_payment` (
  `PAYMENT_ID` bigint(20) NOT NULL,
  `PAN` varchar(255) NOT NULL,
  `PIN` varchar(255) DEFAULT NULL,
  `REFERENCE_NUMBER` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_id_generation`
--

CREATE TABLE `blc_id_generation` (
  `ID_TYPE` varchar(255) NOT NULL,
  `BATCH_SIZE` bigint(20) NOT NULL,
  `BATCH_START` bigint(20) NOT NULL,
  `ID_MIN` bigint(20) DEFAULT NULL,
  `ID_MAX` bigint(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_id_generation`
--

INSERT INTO `blc_id_generation` (`ID_TYPE`, `BATCH_SIZE`, `BATCH_START`, `ID_MIN`, `ID_MAX`, `version`) VALUES
('org.broadleafcommerce.profile.core.domain.Customer', 100, 9000, NULL, NULL, 90);

-- --------------------------------------------------------

--
-- Table structure for table `blc_img_static_asset`
--

CREATE TABLE `blc_img_static_asset` (
  `HEIGHT` int(11) DEFAULT NULL,
  `WIDTH` int(11) DEFAULT NULL,
  `STATIC_ASSET_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_img_static_asset`
--

INSERT INTO `blc_img_static_asset` (`HEIGHT`, `WIDTH`, `STATIC_ASSET_ID`) VALUES
(296, 1920, 100000),
(500, 500, 100001),
(500, 500, 100002),
(500, 500, 100003),
(493, 740, 100004),
(500, 500, 100005),
(296, 1920, 100006),
(445, 270, 100056),
(550, 715, 100057),
(250, 335, 100058),
(250, 335, 100059),
(250, 335, 100060),
(734, 1920, 100100),
(734, 1920, 100101),
(734, 1920, 100102),
(800, 800, 100151),
(800, 800, 100152),
(1200, 1920, 100200),
(1200, 1920, 100201),
(800, 800, 100250),
(800, 800, 100251),
(800, 800, 100252);

-- --------------------------------------------------------

--
-- Table structure for table `blc_index_field`
--

CREATE TABLE `blc_index_field` (
  `INDEX_FIELD_ID` bigint(20) NOT NULL,
  `SEARCHABLE` tinyint(1) DEFAULT NULL,
  `FIELD_ID` bigint(20) NOT NULL,
  `ARCHIVED` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_index_field`
--

INSERT INTO `blc_index_field` (`INDEX_FIELD_ID`, `SEARCHABLE`, `FIELD_ID`, `ARCHIVED`) VALUES
(1, 1, 1, NULL),
(2, 0, 2, NULL),
(3, 0, 3, NULL),
(4, 1, 4, NULL),
(5, 1, 5, NULL),
(6, 1, 6, NULL),
(7, 1, 7, NULL),
(8, 0, 8, NULL),
(9, 0, 9, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_index_field_type`
--

CREATE TABLE `blc_index_field_type` (
  `INDEX_FIELD_TYPE_ID` bigint(20) NOT NULL,
  `FIELD_TYPE` varchar(255) DEFAULT NULL,
  `INDEX_FIELD_ID` bigint(20) NOT NULL,
  `ARCHIVED` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_index_field_type`
--

INSERT INTO `blc_index_field_type` (`INDEX_FIELD_TYPE_ID`, `FIELD_TYPE`, `INDEX_FIELD_ID`, `ARCHIVED`) VALUES
(1, 't', 1, NULL),
(2, 's', 1, NULL),
(3, 'i', 2, NULL),
(4, 'p', 3, NULL),
(5, 't', 4, NULL),
(6, 't', 5, NULL),
(7, 't', 6, NULL),
(8, 't', 7, NULL),
(9, 'ss', 8, NULL),
(10, 'p', 9, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_iso_country`
--

CREATE TABLE `blc_iso_country` (
  `ALPHA_2` varchar(255) NOT NULL,
  `ALPHA_3` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NUMERIC_CODE` int(11) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_iso_country`
--

INSERT INTO `blc_iso_country` (`ALPHA_2`, `ALPHA_3`, `NAME`, `NUMERIC_CODE`, `STATUS`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
('AA', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('AB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('AC', 'ASC', 'Ascension Island', -1, 'EXCEPTIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('AD', 'AND', 'Andorra', 20, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AE', 'ARE', 'United Arab Emirates', 784, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AF', 'AFG', 'Afghanistan', 4, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AG', 'ATG', 'Antigua and Barbuda', 28, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('AI', 'AIA', 'Anguilla', 660, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('AK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('AL', 'ALB', 'Albania', 8, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AM', 'ARM', 'Armenia', 51, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AN', 'ANHH', 'Netherlands Antilles', 530, 'TRANSITIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('AO', 'AGO', 'Angola', 24, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AP', NULL, NULL, -1, 'NOT_USED', NULL, NULL, NULL, NULL),
('AQ', 'ATA', 'Antarctica', 10, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AR', 'ARG', 'Argentina', 32, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AS', 'ASM', 'American Samoa', 16, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AT', 'AUT', 'Austria', 40, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AU', 'AUS', 'Australia', 36, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('AW', 'ABW', 'Aruba', 533, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AX', 'ALA', 'Åland Islands', 248, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('AY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('AZ', 'AZE', 'Azerbaijan', 31, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BA', 'BIH', 'Bosnia and Herzegovina', 70, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BB', 'BRB', 'Barbados', 52, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('BD', 'BGD', 'Bangladesh', 50, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BE', 'BEL', 'Belgium', 56, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BF', 'BFA', 'Burkina Faso', 854, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BG', 'BGR', 'Bulgaria', 100, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BH', 'BHR', 'Bahrain', 48, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BI', 'BDI', 'Burundi', 108, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BJ', 'BEN', 'Benin', 204, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('BL', 'BLM', 'Saint Barthélemy', 652, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BM', 'BMU', 'Bermuda', 60, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BN', 'BRN', 'Brunei Darussalam', 96, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BO', 'BOL', 'Bolivia, Plurinational State of', 68, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('BQ', 'BES', 'Bonaire, Sint Eustatius and Saba', 535, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BR', 'BRA', 'Brazil', 76, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BS', 'BHS', 'Bahamas', 44, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BT', 'BTN', 'Bhutan', 64, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BU', 'BUMM', 'Burma', 104, 'TRANSITIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('BV', 'BVT', 'Bouvet Island', 74, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BW', 'BWA', 'Botswana', 72, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BX', NULL, NULL, -1, 'NOT_USED', NULL, NULL, NULL, NULL),
('BY', 'BLR', 'Belarus', 112, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('BZ', 'BLZ', 'Belize', 84, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CA', 'CAN', 'Canada', 124, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('CC', 'CCK', 'Cocos (Keeling) Islands', 166, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CD', 'COD', 'Congo, the Democratic Republic of the', 180, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CE', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('CF', 'CAF', 'Central African Republic', 140, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CG', 'COG', 'Congo', 178, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CH', 'CHE', 'Switzerland', 756, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CI', 'CIV', 'Côte d\'Ivoire', 384, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('CK', 'COK', 'Cook Islands', 184, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CL', 'CHL', 'Chile', 152, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CM', 'CMR', 'Cameroon', 120, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CN', 'CHN', 'China', 156, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CO', 'COL', 'Colombia', 170, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CP', 'CPT', 'Clipperton Island', -1, 'EXCEPTIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('CQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('CR', 'CRI', 'Costa Rica', 188, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CS', 'CSXX', 'Serbia and Montenegro', 891, 'TRANSITIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('CT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('CU', 'CUB', 'Cuba', 192, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CV', 'CPV', 'Cape Verde', 132, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CW', 'CUW', 'Curaçao', 531, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CX', 'CXR', 'Christmas Island', 162, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CY', 'CYP', 'Cyprus', 196, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('CZ', 'CZE', 'Czech Republic', 203, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('DA', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DE', 'DEU', 'Germany', 276, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('DF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DG', 'DGA', 'Diego Garcia', -1, 'EXCEPTIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('DH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DJ', 'DJI', 'Djibouti', 262, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('DK', 'DNK', 'Denmark', 208, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('DL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DM', 'DMA', 'Dominica', 212, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('DN', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DO', 'DOM', 'Dominican Republic', 214, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('DP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DR', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('DY', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('DZ', 'DZA', 'Algeria', 12, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('EA', 'null', 'Ceuta, Melilla', -1, 'EXCEPTIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('EB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('EC', 'ECU', 'Ecuador', 218, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ED', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('EE', 'EST', 'Estonia', 233, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('EF', NULL, NULL, -1, 'NOT_USED', NULL, NULL, NULL, NULL),
('EG', 'EGY', 'Egypt', 818, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('EH', 'ESH', 'Western Sahara', 732, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('EI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('EJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('EK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('EL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('EM', NULL, NULL, -1, 'NOT_USED', NULL, NULL, NULL, NULL),
('EN', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('EO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('EP', NULL, NULL, -1, 'NOT_USED', NULL, NULL, NULL, NULL),
('EQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ER', 'ERI', 'Eritrea', 232, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ES', 'ESP', 'Spain', 724, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ET', 'ETH', 'Ethiopia', 231, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('EU', 'null', 'European Union', -1, 'EXCEPTIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('EV', NULL, NULL, -1, 'NOT_USED', NULL, NULL, NULL, NULL),
('EW', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('EX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('EY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('EZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FA', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FE', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FG', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FI', 'FIN', 'Finland', 246, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('FJ', 'FJI', 'Fiji', 242, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('FK', 'FLK', 'Falkland Islands (Malvinas)', 238, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('FL', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('FM', 'FSM', 'Micronesia, Federated States of', 583, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('FN', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FO', 'FRO', 'Faroe Islands', 234, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('FP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FR', 'FRA', 'France', 250, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('FS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FX', 'FXX', 'France, Metropolitan', -1, 'EXCEPTIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('FY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('FZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('GA', 'GAB', 'Gabon', 266, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GB', 'GBR', 'United Kingdom', 826, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GC', NULL, NULL, -1, 'NOT_USED', NULL, NULL, NULL, NULL),
('GD', 'GRD', 'Grenada', 308, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GE', 'GEO', 'Georgia', 268, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GF', 'GUF', 'French Guiana', 254, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GG', 'GGY', 'Guernsey', 831, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GH', 'GHA', 'Ghana', 288, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GI', 'GIB', 'Gibraltar', 292, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('GK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('GL', 'GRL', 'Greenland', 304, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GM', 'GMB', 'Gambia', 270, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GN', 'GIN', 'Guinea', 324, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('GP', 'GLP', 'Guadeloupe', 312, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GQ', 'GNQ', 'Equatorial Guinea', 226, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GR', 'GRC', 'Greece', 300, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GS', 'SGS', 'South Georgia and the South Sandwich Islands', 239, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GT', 'GTM', 'Guatemala', 320, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GU', 'GUM', 'Guam', 316, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('GW', 'GNB', 'Guinea-Bissau', 624, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('GY', 'GUY', 'Guyana', 328, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('GZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HA', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HE', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HG', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HK', 'HKG', 'Hong Kong', 344, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('HL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HM', 'HMD', 'Heard Island and McDonald Islands', 334, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('HN', 'HND', 'Honduras', 340, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('HO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HR', 'HRV', 'Croatia', 191, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('HS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HT', 'HTI', 'Haiti', 332, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('HU', 'HUN', 'Hungary', 348, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('HV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('HZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IA', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IB', NULL, NULL, -1, 'NOT_USED', NULL, NULL, NULL, NULL),
('IC', 'null', 'Canary Islands', -1, 'EXCEPTIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('ID', 'IDN', 'Indonesia', 360, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('IE', 'IRL', 'Ireland', 372, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('IF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IG', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('II', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IL', 'ISR', 'Israel', 376, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('IM', 'IMN', 'Isle of Man', 833, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('IN', 'IND', 'India', 356, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('IO', 'IOT', 'British Indian Ocean Territory', 86, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('IP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IQ', 'IRQ', 'Iraq', 368, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('IR', 'IRN', 'Iran, Islamic Republic of', 364, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('IS', 'ISL', 'Iceland', 352, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('IT', 'ITA', 'Italy', 380, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('IU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('IZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JA', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('JB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JE', 'JEY', 'Jersey', 832, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('JF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JG', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JM', 'JAM', 'Jamaica', 388, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('JN', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JO', 'JOR', 'Jordan', 400, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('JP', 'JPN', 'Japan', 392, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('JQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JR', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('JZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KA', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KE', 'KEN', 'Kenya', 404, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('KF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KG', 'KGZ', 'Kyrgyzstan', 417, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('KH', 'KHM', 'Cambodia', 116, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('KI', 'KIR', 'Kiribati', 296, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('KJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KM', 'COM', 'Comoros', 174, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('KN', 'KNA', 'Saint Kitts and Nevis', 659, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('KO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KP', 'PRK', 'Korea, Democratic People\'s Republic of', 408, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('KQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KR', 'KOR', 'Korea, Republic of', 410, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('KS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KW', 'KWT', 'Kuwait', 414, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('KX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('KY', 'CYM', 'Cayman Islands', 136, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('KZ', 'KAZ', 'Kazakhstan', 398, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LA', 'LAO', 'Lao People\'s Democratic Republic', 418, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LB', 'LBN', 'Lebanon', 422, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LC', 'LCA', 'Saint Lucia', 662, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LE', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LF', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('LG', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LI', 'LIE', 'Liechtenstein', 438, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LK', 'LKA', 'Sri Lanka', 144, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LM', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LN', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LR', 'LBR', 'Liberia', 430, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LS', 'LSO', 'Lesotho', 426, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LT', 'LTU', 'Lithuania', 440, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LU', 'LUX', 'Luxembourg', 442, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LV', 'LVA', 'Latvia', 428, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('LY', 'LBY', 'Libya', 434, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('LZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('MA', 'MAR', 'Morocco', 504, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('MC', 'MCO', 'Monaco', 492, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MD', 'MDA', 'Moldova, Republic of', 498, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ME', 'MNE', 'Montenegro', 499, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MF', 'MAF', 'Saint Martin (French part)', 663, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MG', 'MDG', 'Madagascar', 450, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MH', 'MHL', 'Marshall Islands', 584, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('MJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('MK', 'MKD', 'Macedonia, the former Yugoslav Republic of', 807, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ML', 'MLI', 'Mali', 466, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MM', 'MMR', 'Myanmar', 104, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MN', 'MNG', 'Mongolia', 496, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MO', 'MAC', 'Macao', 446, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MP', 'MNP', 'Northern Mariana Islands', 580, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MQ', 'MTQ', 'Martinique', 474, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MR', 'MRT', 'Mauritania', 478, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MS', 'MSR', 'Montserrat', 500, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MT', 'MLT', 'Malta', 470, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MU', 'MUS', 'Mauritius', 480, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MV', 'MDV', 'Maldives', 462, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MW', 'MWI', 'Malawi', 454, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MX', 'MEX', 'Mexico', 484, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MY', 'MYS', 'Malaysia', 458, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('MZ', 'MOZ', 'Mozambique', 508, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NA', 'NAM', 'Namibia', 516, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NC', 'NCL', 'New Caledonia', 540, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ND', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NE', 'NER', 'Niger', 562, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NF', 'NFK', 'Norfolk Island', 574, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NG', 'NGA', 'Nigeria', 566, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NI', 'NIC', 'Nicaragua', 558, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NL', 'NLD', 'Netherlands', 528, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NM', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NN', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NO', 'NOR', 'Norway', 578, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NP', 'NPL', 'Nepal', 524, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NR', 'NRU', 'Nauru', 520, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NT', 'NTHH', 'Neutral Zone', 536, 'TRANSITIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('NU', 'NIU', 'Niue', 570, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('NV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('NZ', 'NZL', 'New Zealand', 554, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('OA', NULL, NULL, -1, 'NOT_USED', NULL, NULL, NULL, NULL),
('OB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OE', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OG', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OM', 'OMN', 'Oman', 512, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ON', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OR', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('OZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PA', 'PAN', 'Panama', 591, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PE', 'PER', 'Peru', 604, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PF', 'PYF', 'French Polynesia', 258, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PG', 'PNG', 'Papua New Guinea', 598, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PH', 'PHL', 'Philippines', 608, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PI', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('PJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PK', 'PAK', 'Pakistan', 586, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PL', 'POL', 'Poland', 616, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PM', 'SPM', 'Saint Pierre and Miquelon', 666, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PN', 'PCN', 'Pitcairn', 612, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PR', 'PRI', 'Puerto Rico', 630, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PS', 'PSE', 'Palestine, State of', 275, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PT', 'PRT', 'Portugal', 620, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PW', 'PLW', 'Palau', 585, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('PY', 'PRY', 'Paraguay', 600, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('PZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QA', 'QAT', 'Qatar', 634, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('QB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QE', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QG', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('QM', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QN', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QO', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QP', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QQ', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QR', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QS', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QT', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QU', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QV', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QW', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QX', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QY', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('QZ', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('RA', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('RB', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('RC', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('RD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RE', 'REU', 'Réunion', 638, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('RF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RG', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RH', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('RI', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('RJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RL', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('RM', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('RN', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('RO', 'ROU', 'Romania', 642, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('RP', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('RQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RR', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RS', 'SRB', 'Serbia', 688, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('RT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RU', 'RUS', 'Russian Federation', 643, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('RV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RW', 'RWA', 'Rwanda', 646, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('RX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('RZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('SA', 'SAU', 'Saudi Arabia', 682, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SB', 'SLB', 'Solomon Islands', 90, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SC', 'SYC', 'Seychelles', 690, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SD', 'SDN', 'Sudan', 729, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SE', 'SWE', 'Sweden', 752, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SF', 'FIN', 'Finland', 246, 'TRANSITIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('SG', 'SGP', 'Singapore', 702, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SH', 'SHN', 'Saint Helena, Ascension and Tristan da Cunha', 654, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SI', 'SVN', 'Slovenia', 705, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SJ', 'SJM', 'Svalbard and Jan Mayen', 744, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SK', 'SVK', 'Slovakia', 703, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SL', 'SLE', 'Sierra Leone', 694, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SM', 'SMR', 'San Marino', 674, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SN', 'SEN', 'Senegal', 686, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SO', 'SOM', 'Somalia', 706, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('SQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('SR', 'SUR', 'Suriname', 740, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SS', 'SSD', 'South Sudan', 728, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ST', 'STP', 'Sao Tome and Principe', 678, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SU', 'SUN', 'USSR', -1, 'EXCEPTIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('SV', 'SLV', 'El Salvador', 222, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('SX', 'SXM', 'Sint Maarten (Dutch part)', 534, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SY', 'SYR', 'Syrian Arab Republic', 760, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('SZ', 'SWZ', 'Swaziland', 748, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TA', 'TAA', 'Tristan da Cunha', -1, 'EXCEPTIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('TB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('TC', 'TCA', 'Turks and Caicos Islands', 796, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TD', 'TCD', 'Chad', 148, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TE', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('TF', 'ATF', 'French Southern Territories', 260, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TG', 'TGO', 'Togo', 768, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TH', 'THA', 'Thailand', 764, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('TJ', 'TJK', 'Tajikistan', 762, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TK', 'TKL', 'Tokelau', 772, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TL', 'TLS', 'Timor-Leste', 626, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TM', 'TKM', 'Turkmenistan', 795, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TN', 'TUN', 'Tunisia', 788, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TO', 'TON', 'Tonga', 776, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TP', 'TPTL', 'East Timor', 0, 'TRANSITIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('TQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('TR', 'TUR', 'Turkey', 792, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('TT', 'TTO', 'Trinidad and Tobago', 780, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('TV', 'TUV', 'Tuvalu', 798, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TW', 'TWN', 'Taiwan, Province of China', 158, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('TX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('TY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('TZ', 'TZA', 'Tanzania, United Republic of', 834, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('UA', 'UKR', 'Ukraine', 804, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('UB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UE', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UG', 'UGA', 'Uganda', 800, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('UH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UK', 'null', 'United Kingdom', -1, 'EXCEPTIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('UL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UM', 'UMI', 'United States Minor Outlying Islands', 581, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('UN', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UR', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('US', 'USA', 'United States', 840, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('UT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('UY', 'URY', 'Uruguay', 858, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('UZ', 'UZB', 'Uzbekistan', 860, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('VA', 'VAT', 'Holy See (Vatican City State)', 336, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('VB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VC', 'VCT', 'Saint Vincent and the Grenadines', 670, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('VD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VE', 'VEN', 'Venezuela, Bolivarian Republic of', 862, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('VF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VG', 'VGB', 'Virgin Islands, British', 92, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('VH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VI', 'VIR', 'Virgin Islands, U.S.', 850, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('VJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VM', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VN', 'VNM', 'Viet Nam', 704, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('VO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VR', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VU', 'VUT', 'Vanuatu', 548, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('VV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('VZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WA', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WE', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WF', 'WLF', 'Wallis and Futuna', 876, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('WG', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('WH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WL', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('WM', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WN', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WO', NULL, NULL, -1, 'NOT_USED', NULL, NULL, NULL, NULL),
('WP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WR', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WS', 'WSM', 'Samoa', 882, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('WT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WV', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('WW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('WZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('XA', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XB', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XC', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XD', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XE', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XF', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XG', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XH', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XI', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XJ', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XK', 'XXK', 'Kosovo, Republic of', -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XL', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XM', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XN', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XO', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XP', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XQ', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XR', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XS', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XT', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XU', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XV', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XW', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XX', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XY', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('XZ', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL),
('YA', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YE', 'YEM', 'Yemen', 887, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('YF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YG', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YM', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YN', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YR', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YT', 'MYT', 'Mayotte', 175, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('YU', 'YUCS', 'Yugoslavia', 890, 'TRANSITIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('YV', NULL, NULL, -1, 'INDETERMINATELY_RESERVED', NULL, NULL, NULL, NULL),
('YW', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('YZ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZA', 'ZAF', 'South Africa', 710, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ZB', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZC', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZD', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZE', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZF', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZG', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZH', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZI', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZJ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZK', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZL', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZM', 'ZMB', 'Zambia', 894, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ZN', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZO', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZP', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZQ', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZR', 'ZRCD', 'Zaire', 0, 'TRANSITIONALLY_RESERVED', NULL, NULL, NULL, NULL),
('ZS', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZT', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZU', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZV', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZW', 'ZWE', 'Zimbabwe', 716, 'OFFICIALLY_ASSIGNED', NULL, NULL, NULL, NULL),
('ZX', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZY', NULL, NULL, -1, 'UNASSIGNED', NULL, NULL, NULL, NULL),
('ZZ', NULL, NULL, -1, 'USER_ASSIGNED', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_item_offer_qualifier`
--

CREATE TABLE `blc_item_offer_qualifier` (
  `ITEM_OFFER_QUALIFIER_ID` bigint(20) NOT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `OFFER_ID` bigint(20) NOT NULL,
  `ORDER_ITEM_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_item_offer_qualifier`
--

INSERT INTO `blc_item_offer_qualifier` (`ITEM_OFFER_QUALIFIER_ID`, `QUANTITY`, `OFFER_ID`, `ORDER_ITEM_ID`) VALUES
(1, 1, 1101, 301);

-- --------------------------------------------------------

--
-- Table structure for table `blc_locale`
--

CREATE TABLE `blc_locale` (
  `LOCALE_CODE` varchar(255) NOT NULL,
  `DEFAULT_FLAG` tinyint(1) DEFAULT NULL,
  `FRIENDLY_NAME` varchar(255) DEFAULT NULL,
  `USE_IN_SEARCH_INDEX` tinyint(1) DEFAULT NULL,
  `CURRENCY_CODE` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_locale`
--

INSERT INTO `blc_locale` (`LOCALE_CODE`, `DEFAULT_FLAG`, `FRIENDLY_NAME`, `USE_IN_SEARCH_INDEX`, `CURRENCY_CODE`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
('en', 0, 'English', NULL, 'USD', NULL, NULL, NULL, NULL),
('en_GB', 0, 'English (United Kingdom)', NULL, 'GBP', NULL, NULL, NULL, NULL),
('en_US', 1, 'English US', NULL, 'USD', NULL, NULL, NULL, NULL),
('es', 0, 'Spanish', NULL, 'EUR', NULL, NULL, NULL, NULL),
('es_ES', 0, 'Spanish (Spain)', NULL, 'EUR', NULL, NULL, NULL, NULL),
('es_MX', 0, 'Spanish (Mexico)', NULL, 'MXN', NULL, NULL, NULL, NULL),
('fr', 0, 'French', NULL, 'EUR', NULL, NULL, NULL, NULL),
('fr_FR', 0, 'French (France)', NULL, 'EUR', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_media`
--

CREATE TABLE `blc_media` (
  `MEDIA_ID` bigint(20) NOT NULL,
  `ALT_TEXT` varchar(255) DEFAULT NULL,
  `TAGS` varchar(255) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `URL` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_media`
--

INSERT INTO `blc_media` (`MEDIA_ID`, `ALT_TEXT`, `TAGS`, `TITLE`, `URL`) VALUES
(101, 'primary', NULL, 'Sudden Death Sauce Bottle', '/cmsstatic/img/sauces/Sudden-Death-Sauce-Bottle.jpg'),
(102, 'alt1', NULL, 'Sudden Death Sauce Close-up', '/cmsstatic/img/sauces/Sudden-Death-Sauce-Close.jpg'),
(201, 'primary', NULL, 'Sweet Death Sauce Bottle', '/cmsstatic/img/sauces/Sweet-Death-Sauce-Bottle.jpg'),
(202, 'alt1', NULL, 'Sweet Death Sauce Close-up', '/cmsstatic/img/sauces/Sweet-Death-Sauce-Close.jpg'),
(203, 'alt2', NULL, 'Sweet Death Sauce Close-up', '/cmsstatic/img/sauces/Sweet-Death-Sauce-Skull.jpg'),
(204, 'alt3', NULL, 'Sweet Death Sauce Close-up', '/cmsstatic/img/sauces/Sweet-Death-Sauce-Tile.jpg'),
(205, 'alt4', NULL, 'Sweet Death Sauce Close-up', '/cmsstatic/img/sauces/Sweet-Death-Sauce-Grass.jpg'),
(206, 'alt5', NULL, 'Sweet Death Sauce Close-up', '/cmsstatic/img/sauces/Sweet-Death-Sauce-Logo.jpg'),
(301, 'primary', NULL, 'Hoppin Hot Sauce Bottle', '/cmsstatic/img/sauces/Hoppin-Hot-Sauce-Bottle.jpg'),
(302, 'alt1', NULL, 'Hoppin Hot Sauce Close-up', '/cmsstatic/img/sauces/Hoppin-Hot-Sauce-Close.jpg'),
(401, 'primary', NULL, 'Day of the Dead Chipotle Hot Sauce Bottle', '/cmsstatic/img/sauces/Day-of-the-Dead-Chipotle-Hot-Sauce-Bottle.jpg'),
(402, 'alt1', NULL, 'Day of the Dead Chipotle Hot Sauce Close-up', '/cmsstatic/img/sauces/Day-of-the-Dead-Chipotle-Hot-Sauce-Close.jpg'),
(501, 'primary', NULL, 'Day of the Dead Habanero Hot Sauce Bottle', '/cmsstatic/img/sauces/Day-of-the-Dead-Habanero-Hot-Sauce-Bottle.jpg'),
(502, 'alt1', NULL, 'Day of the Dead Habanero Hot Sauce Close-up', '/cmsstatic/img/sauces/Day-of-the-Dead-Habanero-Hot-Sauce-Close.jpg'),
(601, 'primary', NULL, 'Day of the Dead Scotch Bonnet Hot Sauce Bottle', '/cmsstatic/img/sauces/Day-of-the-Dead-Scotch-Bonnet-Hot-Sauce-Bottle.jpg'),
(602, 'alt1', NULL, 'Day of the Dead Scotch Bonnet Hot Sauce Close-up', '/cmsstatic/img/sauces/Day-of-the-Dead-Scotch-Bonnet-Hot-Sauce-Close.jpg'),
(701, 'primary', NULL, 'Green Ghost Bottle', '/cmsstatic/img/sauces/Green-Ghost-Bottle.jpg'),
(702, 'alt1', NULL, 'Green Ghost Close-up', '/cmsstatic/img/sauces/Green-Ghost-Close.jpg'),
(801, 'primary', NULL, 'Blazin Saddle XXX Hot Habanero Pepper Sauce Bottle', '/cmsstatic/img/sauces/Blazin-Saddle-XXX-Hot-Habanero-Pepper-Sauce-Bottle.jpg'),
(802, 'alt1', NULL, 'Blazin Saddle XXX Hot Habanero Pepper Sauce Close-up', '/cmsstatic/img/sauces/Blazin-Saddle-XXX-Hot-Habanero-Pepper-Sauce-Close.jpg'),
(901, 'primary', NULL, 'Armageddon The Hot Sauce To End All Bottle', '/cmsstatic/img/sauces/Armageddon-The-Hot-Sauce-To-End-All-Bottle.jpg'),
(902, 'alt1', NULL, 'Armageddon The Hot Sauce To End All Close-up', '/cmsstatic/img/sauces/Armageddon-The-Hot-Sauce-To-End-All-Close.jpg'),
(1001, 'primary', NULL, 'Dr. Chilemeisters Insane Hot Sauce Bottle', '/cmsstatic/img/sauces/Dr.-Chilemeisters-Insane-Hot-Sauce-Bottle.jpg'),
(1002, 'alt1', NULL, 'Dr. Chilemeisters Insane Hot Sauce Close-up', '/cmsstatic/img/sauces/Dr.-Chilemeisters-Insane-Hot-Sauce-Close.jpg'),
(1101, 'primary', NULL, 'Bull Snort Cowboy Cayenne Pepper Hot Sauce Bottle', '/cmsstatic/img/sauces/Bull-Snort-Cowboy-Cayenne-Pepper-Hot-Sauce-Bottle.jpg'),
(1102, 'alt1', NULL, 'Bull Snort Cowboy Cayenne Pepper Hot Sauce Close-up', '/cmsstatic/img/sauces/Bull-Snort-Cowboy-Cayenne-Pepper-Hot-Sauce-Close.jpg'),
(1201, 'primary', NULL, 'Cafe Louisiane Sweet Cajun Blackening Sauce Bottle', '/cmsstatic/img/sauces/Cafe-Louisiane-Sweet-Cajun-Blackening-Sauce-Bottle.jpg'),
(1202, 'alt1', NULL, 'Cafe Louisiane Sweet Cajun Blackening Sauce Close-up', '/cmsstatic/img/sauces/Cafe-Louisiane-Sweet-Cajun-Blackening-Sauce-Close.jpg'),
(1301, 'primary', NULL, 'Bull Snort Smokin Toncils Hot Sauce Bottle', '/cmsstatic/img/sauces/Bull-Snort-Smokin-Toncils-Hot-Sauce-Bottle.jpg'),
(1302, 'alt1', NULL, 'Bull Snort Smokin Toncils Hot Sauce Close-up', '/cmsstatic/img/sauces/Bull-Snort-Smokin-Toncils-Hot-Sauce-Close.jpg'),
(1401, 'primary', NULL, 'Cool Cayenne Pepper Hot Sauce Bottle', '/cmsstatic/img/sauces/Cool-Cayenne-Pepper-Hot-Sauce-Bottle.jpg'),
(1402, 'alt1', NULL, 'Cool Cayenne Pepper Hot Sauce Close-up', '/cmsstatic/img/sauces/Cool-Cayenne-Pepper-Hot-Sauce-Close.jpg'),
(1501, 'primary', NULL, 'Roasted Garlic Hot Sauce Bottle', '/cmsstatic/img/sauces/Roasted-Garlic-Hot-Sauce-Bottle.jpg'),
(1502, 'alt1', NULL, 'Roasted Garlic Hot Sauce Close-up', '/cmsstatic/img/sauces/Roasted-Garlic-Hot-Sauce-Close.jpg'),
(1601, 'primary', NULL, 'Scotch Bonnet Hot Sauce Bottle', '/cmsstatic/img/sauces/Scotch-Bonnet-Hot-Sauce-Bottle.jpg'),
(1602, 'alt1', NULL, 'Scotch Bonnet Hot Sauce Close-up', '/cmsstatic/img/sauces/Scotch-Bonnet-Hot-Sauce-Close.jpg'),
(1701, 'primary', NULL, 'Insanity Sauce Bottle', '/cmsstatic/img/sauces/Insanity-Sauce-Bottle.jpg'),
(1702, 'alt1', NULL, 'Insanity Sauce Close-up', '/cmsstatic/img/sauces/Insanity-Sauce-Close.jpg'),
(1801, 'primary', NULL, 'Hurtin Jalepeno Hot Sauce Bottle', '/cmsstatic/img/sauces/Hurtin-Jalepeno-Hot-Sauce-Bottle.jpg'),
(1802, 'alt1', NULL, 'Hurtin Jalepeno Hot Sauce Close-up', '/cmsstatic/img/sauces/Hurtin-Jalepeno-Hot-Sauce-Close.jpg'),
(1901, 'primary', NULL, 'Roasted Red Pepper and Chipotle Hot Sauce Bottle', '/cmsstatic/img/sauces/Roasted-Red-Pepper-and-Chipotle-Hot-Sauce-Bottle.jpg'),
(1902, 'alt1', NULL, 'Roasted Red Pepper and Chipotle Hot Sauce Close-up', '/cmsstatic/img/sauces/Roasted-Red-Pepper-and-Chipotle-Hot-Sauce-Close.jpg'),
(10001, 'primary', 'Black', 'Hawt Like a Habanero Men\'s Black', '/cmsstatic/img/merch/habanero_mens_black.jpg'),
(10002, 'primary', 'Red', 'Hawt Like a Habanero Men\'s Red', '/cmsstatic/img/merch/habanero_mens_red.jpg'),
(10003, 'primary', 'Silver', 'Hawt Like a Habanero Men\'s Silver', '/cmsstatic/img/merch/habanero_mens_silver.jpg'),
(20001, 'primary', 'Black', 'Hawt Like a Habanero Women\'s Black', '/cmsstatic/img/merch/habanero_womens_black.jpg'),
(20002, 'primary', 'Red', 'Hawt Like a Habanero Women\'s Red', '/cmsstatic/img/merch/habanero_womens_red.jpg'),
(20003, 'primary', 'Silver', 'Hawt Like a Habanero Women\'s Silver', '/cmsstatic/img/merch/habanero_womens_silver.jpg'),
(30001, 'primary', 'Black', 'Heat Clinic Hand-Drawn Men\'s Black', '/cmsstatic/img/merch/heat_clinic_handdrawn_mens_black.jpg'),
(30002, 'primary', 'Red', 'Heat Clinic Hand-Drawn Men\'s Red', '/cmsstatic/img/merch/heat_clinic_handdrawn_mens_red.jpg'),
(30003, 'primary', 'Silver', 'Heat Clinic Hand-Drawn Men\'s Silver', '/cmsstatic/img/merch/heat_clinic_handdrawn_mens_silver.jpg'),
(40001, 'primary', 'Black', 'Heat Clinic Hand-Drawn Women\'s Black', '/cmsstatic/img/merch/heat_clinic_handdrawn_womens_black.jpg'),
(40002, 'primary', 'Red', 'Heat Clinic Hand-Drawn Women\'s Red', '/cmsstatic/img/merch/heat_clinic_handdrawn_womens_red.jpg'),
(40003, 'primary', 'Silver', 'Heat Clinic Hand-Drawn Women\'s Silver', '/cmsstatic/img/merch/heat_clinic_handdrawn_womens_silver.jpg'),
(50001, 'primary', 'Black', 'Heat Clinic Mascot Men\'s Black', '/cmsstatic/img/merch/heat_clinic_mascot_mens_black.jpg'),
(50002, 'primary', 'Red', 'Heat Clinic Mascot Men\'s Red', '/cmsstatic/img/merch/heat_clinic_mascot_mens_red.jpg'),
(50003, 'primary', 'Silver', 'Heat Clinic Mascot Men\'s Silver', '/cmsstatic/img/merch/heat_clinic_mascot_mens_silver.jpg'),
(60001, 'primary', 'Black', 'Heat Clinic Mascot Women\'s Black', '/cmsstatic/img/merch/heat_clinic_mascot_womens_black.jpg'),
(60002, 'primary', 'Red', 'Heat Clinic Mascot Women\'s Red', '/cmsstatic/img/merch/heat_clinic_mascot_womens_red.jpg'),
(60003, 'primary', 'Silver', 'Heat Clinic Mascot Women\'s Silver', '/cmsstatic/img/merch/heat_clinic_mascot_womens_silver.jpg'),
(100000, NULL, NULL, NULL, ' '),
(100001, NULL, NULL, NULL, ' '),
(100002, NULL, NULL, NULL, ' '),
(100003, NULL, NULL, NULL, ' '),
(100004, NULL, NULL, NULL, '/cmsstatic/img/sauces/Sweet-Death-Sauce-Close.jpg'),
(100005, NULL, NULL, NULL, ' '),
(100006, NULL, NULL, NULL, ' '),
(100007, NULL, NULL, NULL, ' '),
(100008, NULL, NULL, NULL, ' '),
(100009, NULL, NULL, NULL, ' '),
(100010, NULL, NULL, NULL, ' '),
(100011, NULL, NULL, NULL, ' '),
(100012, NULL, NULL, NULL, ' '),
(100013, NULL, NULL, NULL, ' '),
(100014, NULL, NULL, NULL, ' '),
(100015, NULL, NULL, NULL, ' '),
(100016, NULL, NULL, NULL, ' '),
(100017, NULL, NULL, NULL, ' '),
(100021, NULL, NULL, NULL, '/cmsstatic/product/kiz-oyuncaklari-canim-bebegim-1-1.jpeg'),
(100022, NULL, NULL, NULL, '/cmsstatic/product/10000/vardem-kutulu-4-asorti-4pcs-mini-evcilik-seti-d_1.jpg'),
(100023, NULL, NULL, NULL, '/cmsstatic/product/10000/63721_3.jpg'),
(100024, NULL, NULL, NULL, '/cmsstatic/product/10000/media_banner.jpg'),
(100053, NULL, NULL, NULL, ' '),
(100054, NULL, NULL, NULL, ' '),
(100055, NULL, NULL, NULL, ' '),
(100056, NULL, NULL, NULL, ' '),
(100057, NULL, NULL, NULL, ' '),
(100058, NULL, NULL, NULL, ' '),
(100059, NULL, NULL, NULL, ' '),
(100060, NULL, NULL, NULL, ' '),
(100061, NULL, NULL, NULL, ' '),
(100062, NULL, NULL, NULL, ' '),
(100063, NULL, NULL, NULL, ' '),
(100064, NULL, NULL, NULL, ' '),
(100065, NULL, NULL, NULL, ' '),
(100066, NULL, NULL, NULL, ' '),
(100100, NULL, NULL, NULL, '/cmsstatic/img/banners/member-special.jpg'),
(100150, NULL, NULL, NULL, '/cmsstatic/category/10050/man-category-banner-asddsa.jpg'),
(100151, NULL, NULL, NULL, '/cmsstatic/category/10055/woman-category-banner-123-1.jpg'),
(100152, NULL, NULL, NULL, '/cmsstatic/category/10058/children-category-banner-23-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `blc_module_configuration`
--

CREATE TABLE `blc_module_configuration` (
  `MODULE_CONFIG_ID` bigint(20) NOT NULL,
  `ACTIVE_END_DATE` datetime DEFAULT NULL,
  `ACTIVE_START_DATE` datetime DEFAULT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  `CONFIG_TYPE` varchar(255) NOT NULL,
  `IS_DEFAULT` tinyint(1) NOT NULL,
  `MODULE_NAME` varchar(255) NOT NULL,
  `MODULE_PRIORITY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_module_configuration`
--

INSERT INTO `blc_module_configuration` (`MODULE_CONFIG_ID`, `ACTIVE_END_DATE`, `ACTIVE_START_DATE`, `ARCHIVED`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`, `CONFIG_TYPE`, `IS_DEFAULT`, `MODULE_NAME`, `MODULE_PRIORITY`) VALUES
(-1, NULL, '2017-09-16 16:40:57', NULL, NULL, NULL, NULL, NULL, 'SITE_MAP', 1, 'SITE_MAP', 100);

-- --------------------------------------------------------

--
-- Table structure for table `blc_offer`
--

CREATE TABLE `blc_offer` (
  `OFFER_ID` bigint(20) NOT NULL,
  `APPLY_TO_CHILD_ITEMS` tinyint(1) DEFAULT NULL,
  `APPLY_TO_SALE_PRICE` tinyint(1) DEFAULT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `AUTOMATICALLY_ADDED` tinyint(1) DEFAULT NULL,
  `COMBINABLE_WITH_OTHER_OFFERS` tinyint(1) DEFAULT NULL,
  `OFFER_DESCRIPTION` varchar(255) DEFAULT NULL,
  `OFFER_DISCOUNT_TYPE` varchar(255) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `MARKETING_MESSASGE` varchar(255) DEFAULT NULL,
  `MAX_USES_PER_CUSTOMER` bigint(20) DEFAULT NULL,
  `MAX_USES` int(11) DEFAULT NULL,
  `OFFER_NAME` varchar(255) NOT NULL,
  `OFFER_ITEM_QUALIFIER_RULE` varchar(255) DEFAULT NULL,
  `OFFER_ITEM_TARGET_RULE` varchar(255) DEFAULT NULL,
  `ORDER_MIN_TOTAL` decimal(19,5) DEFAULT NULL,
  `OFFER_PRIORITY` int(11) DEFAULT NULL,
  `QUALIFYING_ITEM_MIN_TOTAL` decimal(19,5) DEFAULT NULL,
  `REQUIRES_RELATED_TAR_QUAL` tinyint(1) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `TARGET_MIN_TOTAL` decimal(19,5) DEFAULT NULL,
  `TARGET_SYSTEM` varchar(255) DEFAULT NULL,
  `TOTALITARIAN_OFFER` tinyint(1) DEFAULT NULL,
  `OFFER_TYPE` varchar(255) NOT NULL,
  `OFFER_VALUE` decimal(19,5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_offer`
--

INSERT INTO `blc_offer` (`OFFER_ID`, `APPLY_TO_CHILD_ITEMS`, `APPLY_TO_SALE_PRICE`, `ARCHIVED`, `AUTOMATICALLY_ADDED`, `COMBINABLE_WITH_OTHER_OFFERS`, `OFFER_DESCRIPTION`, `OFFER_DISCOUNT_TYPE`, `END_DATE`, `MARKETING_MESSASGE`, `MAX_USES_PER_CUSTOMER`, `MAX_USES`, `OFFER_NAME`, `OFFER_ITEM_QUALIFIER_RULE`, `OFFER_ITEM_TARGET_RULE`, `ORDER_MIN_TOTAL`, `OFFER_PRIORITY`, `QUALIFYING_ITEM_MIN_TOTAL`, `REQUIRES_RELATED_TAR_QUAL`, `START_DATE`, `TARGET_MIN_TOTAL`, `TARGET_SYSTEM`, `TOTALITARIAN_OFFER`, `OFFER_TYPE`, `OFFER_VALUE`) VALUES
(1, 0, 0, NULL, 0, 1, NULL, 'PERCENT_OFF', '2020-01-01 00:00:00', NULL, NULL, 0, 'Shirts Special', 'NONE', 'NONE', NULL, NULL, NULL, NULL, '2017-09-16 00:00:00', NULL, NULL, NULL, 'ORDER_ITEM', '20.00000'),
(1001, 1, 1, 'Y', 1, 1, NULL, 'PERCENT_OFF', '2017-09-30 17:32:00', '%50 indirim kazandınız', 1, 99999, '%50 indirim', 'NONE', 'QUALIFIER_TARGET', '20.00000', NULL, '0.00000', 0, '2017-09-19 17:30:45', '5.00000', NULL, 0, 'ORDER', '50.00000'),
(1002, 1, 1, 'Y', 1, 1, '50% indirim kazansana Brooo', 'PERCENT_OFF', '2017-09-30 17:50:00', '%50 indirim kazandınız', 9, 9999, 'Clearance kategorisinde 2 ürün alana %50 indiirm', 'NONE', 'QUALIFIER_TARGET', '100.00000', NULL, '100.00000', 0, '2017-09-19 17:47:48', '2.00000', NULL, 0, 'ORDER', '50.00000'),
(1003, 0, 1, 'Y', 1, 1, '%50 indirim kazanma şansı', 'PERCENT_OFF', '2017-09-30 18:25:00', 'Helaaaaalllll', 0, NULL, '%50 indirim', 'NONE', 'NONE', '40.00000', NULL, '0.00000', 0, '2017-09-19 18:08:16', '0.00000', NULL, 0, 'ORDER', '25.00000'),
(1050, 1, 1, 'N', 0, 1, NULL, 'AMOUNT_OFF', '2017-09-30 18:45:00', NULL, 1, 1, '50 TL indirim kuponu', 'NONE', 'NONE', '200.00000', NULL, '1.00000', 0, '2017-09-18 18:35:00', '1.00000', NULL, 0, 'ORDER', '50.00000'),
(1101, 0, 1, 'N', 1, 1, 'asdfghjkö cxdfghn', 'PERCENT_OFF', NULL, NULL, 0, NULL, '2. ürüne %20 indirim', 'NONE', 'NONE', '0.00000', NULL, '2.00000', 0, '2017-09-20 19:06:24', '0.00000', NULL, 0, 'ORDER_ITEM', '20.00000'),
(1150, 0, 1, 'N', 1, 1, NULL, 'PERCENT_OFF', NULL, 'Deneme', 0, NULL, 'Hediye', 'NONE', 'NONE', '0.00000', NULL, '0.00000', 0, '2017-10-14 00:37:06', '0.00000', NULL, 0, 'ORDER_ITEM', '100.00000');

-- --------------------------------------------------------

--
-- Table structure for table `blc_offer_audit`
--

CREATE TABLE `blc_offer_audit` (
  `OFFER_AUDIT_ID` bigint(20) NOT NULL,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `OFFER_CODE_ID` bigint(20) DEFAULT NULL,
  `OFFER_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `REDEEMED_DATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_offer_audit`
--

INSERT INTO `blc_offer_audit` (`OFFER_AUDIT_ID`, `CUSTOMER_ID`, `OFFER_CODE_ID`, `OFFER_ID`, `ORDER_ID`, `REDEEMED_DATE`) VALUES
(1, 900, 1000, 1050, 251, '2017-09-19 18:56:03');

-- --------------------------------------------------------

--
-- Table structure for table `blc_offer_code`
--

CREATE TABLE `blc_offer_code` (
  `OFFER_CODE_ID` bigint(20) NOT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `MAX_USES` int(11) DEFAULT NULL,
  `OFFER_CODE` varchar(255) NOT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `USES` int(11) DEFAULT NULL,
  `OFFER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_offer_code`
--

INSERT INTO `blc_offer_code` (`OFFER_CODE_ID`, `ARCHIVED`, `EMAIL_ADDRESS`, `MAX_USES`, `OFFER_CODE`, `END_DATE`, `START_DATE`, `USES`, `OFFER_ID`) VALUES
(1000, 'N', NULL, 10, 'Avansas', '2017-09-30 18:49:00', '2017-09-19 18:49:38', 0, 1050);

-- --------------------------------------------------------

--
-- Table structure for table `blc_offer_info`
--

CREATE TABLE `blc_offer_info` (
  `OFFER_INFO_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_offer_info_fields`
--

CREATE TABLE `blc_offer_info_fields` (
  `OFFER_INFO_FIELDS_ID` bigint(20) NOT NULL,
  `FIELD_VALUE` varchar(255) DEFAULT NULL,
  `FIELD_NAME` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_offer_item_criteria`
--

CREATE TABLE `blc_offer_item_criteria` (
  `OFFER_ITEM_CRITERIA_ID` bigint(20) NOT NULL,
  `ORDER_ITEM_MATCH_RULE` longtext,
  `QUANTITY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_offer_item_criteria`
--

INSERT INTO `blc_offer_item_criteria` (`OFFER_ITEM_CRITERIA_ID`, `ORDER_ITEM_MATCH_RULE`, `QUANTITY`) VALUES
(1, 'MvelHelper.toUpperCase(orderItem.?category.?name)==MvelHelper.toUpperCase(\"merchandise\")', 1),
(1000, 'MvelHelper.toUpperCase(orderItem.?category.?name)==MvelHelper.toUpperCase(\"hot sauce\")', 1),
(1057, 'MvelHelper.toUpperCase(orderItem.?category.?name)==MvelHelper.toUpperCase(\"Hot Sauces\")', 1),
(1100, 'MvelHelper.toUpperCase(orderItem.?name)==MvelHelper.toUpperCase(\"Green Ghost\")', 2),
(1103, 'MvelHelper.toUpperCase(orderItem.?name)==MvelHelper.toUpperCase(\"Green Ghost\")&&orderItem.?quantity>=2', 2),
(1154, 'orderItem.?category.?name==\"Men\"&&orderItem.?quantity>=3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_offer_rule`
--

CREATE TABLE `blc_offer_rule` (
  `OFFER_RULE_ID` bigint(20) NOT NULL,
  `MATCH_RULE` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_offer_rule_map`
--

CREATE TABLE `blc_offer_rule_map` (
  `OFFER_OFFER_RULE_ID` bigint(20) NOT NULL,
  `MAP_KEY` varchar(255) NOT NULL,
  `BLC_OFFER_OFFER_ID` bigint(20) NOT NULL,
  `OFFER_RULE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_order`
--

CREATE TABLE `blc_order` (
  `ORDER_ID` bigint(20) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ORDER_NUMBER` varchar(255) DEFAULT NULL,
  `IS_PREVIEW` tinyint(1) DEFAULT NULL,
  `ORDER_STATUS` varchar(255) DEFAULT NULL,
  `ORDER_SUBTOTAL` decimal(19,5) DEFAULT NULL,
  `SUBMIT_DATE` datetime DEFAULT NULL,
  `TAX_OVERRIDE` tinyint(1) DEFAULT NULL,
  `ORDER_TOTAL` decimal(19,5) DEFAULT NULL,
  `TOTAL_SHIPPING` decimal(19,5) DEFAULT NULL,
  `TOTAL_TAX` decimal(19,5) DEFAULT NULL,
  `CURRENCY_CODE` varchar(255) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL,
  `LOCALE_CODE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_order`
--

INSERT INTO `blc_order` (`ORDER_ID`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`, `EMAIL_ADDRESS`, `NAME`, `ORDER_NUMBER`, `IS_PREVIEW`, `ORDER_STATUS`, `ORDER_SUBTOTAL`, `SUBMIT_DATE`, `TAX_OVERRIDE`, `ORDER_TOTAL`, `TOTAL_SHIPPING`, `TOTAL_TAX`, `CURRENCY_CODE`, `CUSTOMER_ID`, `LOCALE_CODE`) VALUES
(1, 400, '2017-09-19 17:36:52', '2017-09-19 17:49:34', 400, NULL, NULL, NULL, NULL, 'IN_PROCESS', '17.95000', NULL, NULL, '17.95000', '0.00000', '0.00000', 'USD', 400, 'en_US'),
(51, 500, '2017-09-19 17:52:04', '2017-09-19 17:52:11', 500, NULL, NULL, NULL, NULL, 'IN_PROCESS', '14.98000', NULL, NULL, '7.49000', '0.00000', '0.00000', 'USD', 500, 'en_US'),
(101, 600, '2017-09-19 17:57:10', '2017-09-19 17:58:05', 600, NULL, NULL, NULL, NULL, 'IN_PROCESS', '102.77000', NULL, NULL, '102.77000', '0.00000', '0.00000', 'USD', 600, 'en_US'),
(151, 700, '2017-09-19 18:02:00', '2017-09-19 18:02:44', 700, NULL, NULL, NULL, NULL, 'IN_PROCESS', '166.78000', NULL, NULL, '166.78000', '0.00000', '0.00000', 'USD', 700, 'en_US'),
(201, 800, '2017-09-19 18:05:52', '2017-09-19 18:06:45', 800, NULL, NULL, NULL, NULL, 'IN_PROCESS', '109.78000', NULL, NULL, '109.78000', '0.00000', '0.00000', 'USD', 800, 'en_US'),
(251, 900, '2017-09-19 18:27:08', '2017-09-20 18:22:59', -1, 'can.bayraktar@hybline.com.tr', NULL, '20170919185602974251', NULL, 'SUBMITTED', '203.68000', '2017-09-19 18:56:03', NULL, '158.68000', '5.00000', '0.00000', 'USD', 900, 'en_US'),
(301, 1000, '2017-09-20 19:11:41', '2017-09-20 19:13:00', 1000, NULL, NULL, NULL, NULL, 'IN_PROCESS', '27.97000', NULL, NULL, '27.97000', '0.00000', '0.00000', 'USD', 1000, 'en_US'),
(351, 3001, '2017-10-03 12:27:23', '2017-10-03 12:27:23', 3001, NULL, NULL, NULL, NULL, 'IN_PROCESS', '0.00000', NULL, NULL, NULL, '0.00000', NULL, 'USD', 3001, 'en_US');

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_adjustment`
--

CREATE TABLE `blc_order_adjustment` (
  `ORDER_ADJUSTMENT_ID` bigint(20) NOT NULL,
  `ADJUSTMENT_REASON` varchar(255) NOT NULL,
  `ADJUSTMENT_VALUE` decimal(19,5) NOT NULL,
  `OFFER_ID` bigint(20) NOT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_order_adjustment`
--

INSERT INTO `blc_order_adjustment` (`ORDER_ADJUSTMENT_ID`, `ADJUSTMENT_REASON`, `ADJUSTMENT_VALUE`, `OFFER_ID`, `ORDER_ID`) VALUES
(1, 'Clearance kategorisinde 2 ürün alana %50 indiirm', '7.49000', 1002, 51),
(61, '50 TL indirim kuponu', '50.00000', 1050, 251);

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_attribute`
--

CREATE TABLE `blc_order_attribute` (
  `ORDER_ATTRIBUTE_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `ORDER_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_item`
--

CREATE TABLE `blc_order_item` (
  `ORDER_ITEM_ID` bigint(20) NOT NULL,
  `DISCOUNTS_ALLOWED` tinyint(1) DEFAULT NULL,
  `HAS_VALIDATION_ERRORS` tinyint(1) DEFAULT NULL,
  `ITEM_TAXABLE_FLAG` tinyint(1) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ORDER_ITEM_TYPE` varchar(255) DEFAULT NULL,
  `PRICE` decimal(19,5) DEFAULT NULL,
  `QUANTITY` int(11) NOT NULL,
  `RETAIL_PRICE` decimal(19,5) DEFAULT NULL,
  `RETAIL_PRICE_OVERRIDE` tinyint(1) DEFAULT NULL,
  `SALE_PRICE` decimal(19,5) DEFAULT NULL,
  `SALE_PRICE_OVERRIDE` tinyint(1) DEFAULT NULL,
  `TOTAL_TAX` decimal(19,2) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `GIFT_WRAP_ITEM_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `PARENT_ORDER_ITEM_ID` bigint(20) DEFAULT NULL,
  `PERSONAL_MESSAGE_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_order_item`
--

INSERT INTO `blc_order_item` (`ORDER_ITEM_ID`, `DISCOUNTS_ALLOWED`, `HAS_VALIDATION_ERRORS`, `ITEM_TAXABLE_FLAG`, `NAME`, `ORDER_ITEM_TYPE`, `PRICE`, `QUANTITY`, `RETAIL_PRICE`, `RETAIL_PRICE_OVERRIDE`, `SALE_PRICE`, `SALE_PRICE_OVERRIDE`, `TOTAL_TAX`, `CATEGORY_ID`, `GIFT_WRAP_ITEM_ID`, `ORDER_ID`, `PARENT_ORDER_ITEM_ID`, `PERSONAL_MESSAGE_ID`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(6, NULL, NULL, 1, 'Hurtin\' Jalapeno Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '4.49000', 2, '5.99000', NULL, '4.49000', NULL, NULL, 2002, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, 1, 'Bull Snort Cowboy Cayenne Pepper Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '2.99000', 3, '3.99000', NULL, '2.99000', NULL, NULL, 2002, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(51, NULL, NULL, 1, 'Dr. Chilemeister\'s Insane Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '10.99000', 1, '12.99000', NULL, '10.99000', NULL, NULL, 2002, NULL, 51, NULL, NULL, NULL, NULL, NULL, NULL),
(52, NULL, NULL, 1, 'Blazin\' Saddle XXX Hot Habanero Pepper Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '3.99000', 1, '4.99000', NULL, '3.99000', NULL, NULL, 2002, NULL, 51, NULL, NULL, NULL, NULL, NULL, NULL),
(101, NULL, NULL, 1, 'Hoppin\' Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '4.99000', 2, '4.99000', NULL, '4.99000', NULL, NULL, 2002, NULL, 101, NULL, NULL, NULL, NULL, NULL, NULL),
(102, NULL, NULL, 1, 'Armageddon The Hot Sauce To End All', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '12.99000', 1, '12.99000', NULL, '12.99000', NULL, NULL, 2002, NULL, 101, NULL, NULL, NULL, NULL, NULL, NULL),
(103, NULL, NULL, 1, 'Bull Snort Cowboy Cayenne Pepper Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '2.99000', 10, '3.99000', NULL, '2.99000', NULL, NULL, 2002, NULL, 101, NULL, NULL, NULL, NULL, NULL, NULL),
(104, NULL, NULL, 1, 'Cafe Louisiane Sweet Cajun Blackening Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '4.99000', 10, '4.99000', NULL, '4.99000', NULL, NULL, 2002, NULL, 101, NULL, NULL, NULL, NULL, NULL, NULL),
(151, NULL, NULL, 1, 'Hoppin\' Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '4.99000', 10, '4.99000', NULL, '4.99000', NULL, NULL, 2002, NULL, 151, NULL, NULL, NULL, NULL, NULL, NULL),
(152, NULL, NULL, 1, 'Armageddon The Hot Sauce To End All', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '12.99000', 1, '12.99000', NULL, '12.99000', NULL, NULL, 2002, NULL, 151, NULL, NULL, NULL, NULL, NULL, NULL),
(153, NULL, NULL, 1, 'Blazin\' Saddle XXX Hot Habanero Pepper Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '3.99000', 1, '4.99000', NULL, '3.99000', NULL, NULL, 2002, NULL, 151, NULL, NULL, NULL, NULL, NULL, NULL),
(154, NULL, NULL, 1, 'Green Ghost', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '9.99000', 10, '11.99000', NULL, '9.99000', NULL, NULL, 2002, NULL, 151, NULL, NULL, NULL, NULL, NULL, NULL),
(201, NULL, NULL, 1, 'Hoppin\' Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '4.99000', 2, '4.99000', NULL, '4.99000', NULL, NULL, 2002, NULL, 201, NULL, NULL, NULL, NULL, NULL, NULL),
(202, NULL, NULL, 1, 'Cafe Louisiane Sweet Cajun Blackening Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '4.99000', 20, '4.99000', NULL, '4.99000', NULL, NULL, 2002, NULL, 201, NULL, NULL, NULL, NULL, NULL, NULL),
(268, NULL, NULL, 1, 'Hoppin\' Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '4.99000', 10, '4.99000', NULL, '4.99000', NULL, NULL, 2002, NULL, 251, NULL, NULL, NULL, NULL, NULL, NULL),
(269, NULL, NULL, 1, 'Day of the Dead Scotch Bonnet Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '6.99000', 20, '6.99000', NULL, '6.99000', NULL, NULL, 2002, NULL, 251, NULL, NULL, NULL, NULL, NULL, NULL),
(270, NULL, NULL, 1, 'Day of the Dead Habanero Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '6.99000', 1, '6.99000', NULL, '6.99000', NULL, NULL, 2002, NULL, 251, NULL, NULL, NULL, NULL, NULL, NULL),
(271, NULL, NULL, 1, 'Day of the Dead Chipotle Hot Sauce', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '6.99000', 1, '6.99000', NULL, '6.99000', NULL, NULL, 2002, NULL, 251, NULL, NULL, NULL, NULL, NULL, NULL),
(301, NULL, NULL, 1, 'Green Ghost', 'org.broadleafcommerce.core.order.domain.DiscreteOrderItem', '9.32000', 3, '11.99000', NULL, '9.99000', NULL, NULL, 2002, NULL, 301, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_item_add_attr`
--

CREATE TABLE `blc_order_item_add_attr` (
  `ORDER_ITEM_ID` bigint(20) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_item_adjustment`
--

CREATE TABLE `blc_order_item_adjustment` (
  `ORDER_ITEM_ADJUSTMENT_ID` bigint(20) NOT NULL,
  `APPLIED_TO_SALE_PRICE` tinyint(1) DEFAULT NULL,
  `ADJUSTMENT_REASON` varchar(255) NOT NULL,
  `ADJUSTMENT_VALUE` decimal(19,5) NOT NULL,
  `OFFER_ID` bigint(20) NOT NULL,
  `ORDER_ITEM_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_item_attribute`
--

CREATE TABLE `blc_order_item_attribute` (
  `ORDER_ITEM_ATTRIBUTE_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  `ORDER_ITEM_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_item_cart_message`
--

CREATE TABLE `blc_order_item_cart_message` (
  `ORDER_ITEM_ID` bigint(20) NOT NULL,
  `CART_MESSAGE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_item_dtl_adj`
--

CREATE TABLE `blc_order_item_dtl_adj` (
  `ORDER_ITEM_DTL_ADJ_ID` bigint(20) NOT NULL,
  `APPLIED_TO_SALE_PRICE` tinyint(1) DEFAULT NULL,
  `OFFER_NAME` varchar(255) DEFAULT NULL,
  `ADJUSTMENT_REASON` varchar(255) NOT NULL,
  `ADJUSTMENT_VALUE` decimal(19,5) NOT NULL,
  `OFFER_ID` bigint(20) NOT NULL,
  `ORDER_ITEM_PRICE_DTL_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_order_item_dtl_adj`
--

INSERT INTO `blc_order_item_dtl_adj` (`ORDER_ITEM_DTL_ADJ_ID`, `APPLIED_TO_SALE_PRICE`, `OFFER_NAME`, `ADJUSTMENT_REASON`, `ADJUSTMENT_VALUE`, `OFFER_ID`, `ORDER_ITEM_PRICE_DTL_ID`) VALUES
(1, 1, '2. ürüne %20 indirim', '2. ürüne %20 indirim', '2.00000', 1101, 302);

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_item_price_dtl`
--

CREATE TABLE `blc_order_item_price_dtl` (
  `ORDER_ITEM_PRICE_DTL_ID` bigint(20) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `USE_SALE_PRICE` tinyint(1) DEFAULT NULL,
  `ORDER_ITEM_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_order_item_price_dtl`
--

INSERT INTO `blc_order_item_price_dtl` (`ORDER_ITEM_PRICE_DTL_ID`, `QUANTITY`, `USE_SALE_PRICE`, `ORDER_ITEM_ID`) VALUES
(6, 2, 1, 6),
(7, 3, 1, 7),
(51, 1, 1, 51),
(52, 1, 1, 52),
(101, 2, 1, 101),
(102, 1, 1, 102),
(103, 10, 1, 103),
(104, 10, 1, 104),
(151, 10, 1, 151),
(152, 1, 1, 152),
(153, 1, 1, 153),
(154, 10, 1, 154),
(201, 2, 1, 201),
(202, 20, 1, 202),
(268, 10, 1, 268),
(269, 20, 1, 269),
(270, 1, 1, 270),
(271, 1, 1, 271),
(301, 2, 1, 301),
(302, 1, 1, 301);

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_lock`
--

CREATE TABLE `blc_order_lock` (
  `LOCK_KEY` varchar(255) NOT NULL,
  `ORDER_ID` bigint(20) NOT NULL,
  `LAST_UPDATED` bigint(20) DEFAULT NULL,
  `LOCKED` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_order_lock`
--

INSERT INTO `blc_order_lock` (`LOCK_KEY`, `ORDER_ID`, `LAST_UPDATED`, `LOCKED`) VALUES
('07b30494-9311-4bab-9e27-583833d22845', 301, 1505923980142, 'N'),
('36f0af0e-ac85-4f33-aaed-3a7f1a2eb39b', 51, 1505832731178, 'N'),
('6b62e7f8-adfc-42bb-9ded-b9a6a6d3c8f6', 251, 1505836562812, 'N'),
('a2453674-5886-4c31-838c-f63914e264ab', 201, 1505833605312, 'N'),
('b407713e-5cbc-4d94-a654-d6ae46ee9cb4', 1, 1505832573884, 'N'),
('c8a790ec-ad1a-435a-9969-03ecc110b521', 101, 1505833085313, 'N'),
('cdd03c5b-cbdf-4dea-89e2-a269b2d9d167', 151, 1505833363679, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_multiship_option`
--

CREATE TABLE `blc_order_multiship_option` (
  `ORDER_MULTISHIP_OPTION_ID` bigint(20) NOT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL,
  `FULFILLMENT_OPTION_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `ORDER_ITEM_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_offer_code_xref`
--

CREATE TABLE `blc_order_offer_code_xref` (
  `ORDER_ID` bigint(20) NOT NULL,
  `OFFER_CODE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_order_offer_code_xref`
--

INSERT INTO `blc_order_offer_code_xref` (`ORDER_ID`, `OFFER_CODE_ID`) VALUES
(251, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_payment`
--

CREATE TABLE `blc_order_payment` (
  `ORDER_PAYMENT_ID` bigint(20) NOT NULL,
  `AMOUNT` decimal(19,5) DEFAULT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `GATEWAY_TYPE` varchar(255) DEFAULT NULL,
  `REFERENCE_NUMBER` varchar(255) DEFAULT NULL,
  `PAYMENT_TYPE` varchar(255) NOT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_order_payment`
--

INSERT INTO `blc_order_payment` (`ORDER_PAYMENT_ID`, `AMOUNT`, `ARCHIVED`, `GATEWAY_TYPE`, `REFERENCE_NUMBER`, `PAYMENT_TYPE`, `ADDRESS_ID`, `ORDER_ID`) VALUES
(1, '158.68000', 'Y', 'Temporary', NULL, 'CREDIT_CARD', 1, 251),
(2, '158.68000', 'N', 'Passthrough', NULL, 'COD', NULL, 251);

-- --------------------------------------------------------

--
-- Table structure for table `blc_order_payment_transaction`
--

CREATE TABLE `blc_order_payment_transaction` (
  `PAYMENT_TRANSACTION_ID` bigint(20) NOT NULL,
  `TRANSACTION_AMOUNT` decimal(19,2) DEFAULT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `CUSTOMER_IP_ADDRESS` varchar(255) DEFAULT NULL,
  `DATE_RECORDED` datetime DEFAULT NULL,
  `RAW_RESPONSE` longtext,
  `SAVE_TOKEN` tinyint(1) DEFAULT NULL,
  `SUCCESS` tinyint(1) DEFAULT NULL,
  `TRANSACTION_TYPE` varchar(255) DEFAULT NULL,
  `ORDER_PAYMENT` bigint(20) NOT NULL,
  `PARENT_TRANSACTION` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_order_payment_transaction`
--

INSERT INTO `blc_order_payment_transaction` (`PAYMENT_TRANSACTION_ID`, `TRANSACTION_AMOUNT`, `ARCHIVED`, `CUSTOMER_IP_ADDRESS`, `DATE_RECORDED`, `RAW_RESPONSE`, `SAVE_TOKEN`, `SUCCESS`, `TRANSACTION_TYPE`, `ORDER_PAYMENT`, `PARENT_TRANSACTION`) VALUES
(1, '158.68', 'N', NULL, '2017-09-19 18:56:03', 'Passthrough Payment', 0, 1, 'AUTHORIZE_AND_CAPTURE', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_page`
--

CREATE TABLE `blc_page` (
  `PAGE_ID` bigint(20) NOT NULL,
  `ACTIVE_END_DATE` datetime DEFAULT NULL,
  `ACTIVE_START_DATE` datetime DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `EXCLUDE_FROM_SITE_MAP` tinyint(1) DEFAULT NULL,
  `FULL_URL` varchar(255) DEFAULT NULL,
  `META_DESCRIPTION` varchar(255) DEFAULT NULL,
  `META_TITLE` varchar(255) DEFAULT NULL,
  `OFFLINE_FLAG` tinyint(1) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `PAGE_TMPLT_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_page`
--

INSERT INTO `blc_page` (`PAGE_ID`, `ACTIVE_END_DATE`, `ACTIVE_START_DATE`, `DESCRIPTION`, `EXCLUDE_FROM_SITE_MAP`, `FULL_URL`, `META_DESCRIPTION`, `META_TITLE`, `OFFLINE_FLAG`, `PRIORITY`, `PAGE_TMPLT_ID`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(1, NULL, NULL, 'About Us', NULL, '/about_us', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(2, NULL, NULL, 'FAQ', NULL, '/faq', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(3, NULL, NULL, 'New to Hot Sauce', NULL, '/new-to-hot-sauce', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(1000, NULL, NULL, 'İletişim', 0, '/iletisim', NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL),
(1050, NULL, NULL, 'Help', 0, '/', NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_page_attributes`
--

CREATE TABLE `blc_page_attributes` (
  `ATTRIBUTE_ID` bigint(20) NOT NULL,
  `FIELD_NAME` varchar(255) NOT NULL,
  `FIELD_VALUE` varchar(255) DEFAULT NULL,
  `PAGE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_page_fld`
--

CREATE TABLE `blc_page_fld` (
  `PAGE_FLD_ID` bigint(20) NOT NULL,
  `FLD_KEY` varchar(255) DEFAULT NULL,
  `LOB_VALUE` longtext,
  `VALUE` varchar(255) DEFAULT NULL,
  `PAGE_ID` bigint(20) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_page_fld`
--

INSERT INTO `blc_page_fld` (`PAGE_FLD_ID`, `FLD_KEY`, `LOB_VALUE`, `VALUE`, `PAGE_ID`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(1, 'body', NULL, 'test content', 1, NULL, NULL, NULL, NULL),
(2, 'title', NULL, '', 1, NULL, NULL, NULL, NULL),
(3, 'body', NULL, '<h2 style=\"text-align:center;\">This is an example of a content-managed page.</h2><h4 style=\"text-align:center;\"><a href=\"http://www.broadleafcommerce.com/features/content\">Click Here</a> to see more about Content Management in Broadleaf.</h4>', 2, NULL, NULL, NULL, NULL),
(4, 'body', NULL, '<h2 style=\"text-align:center;\">This is an example of a content-managed page.</h2>', 3, NULL, NULL, NULL, NULL),
(1000, 'title', NULL, 'İletişim', 1000, NULL, NULL, NULL, NULL),
(1001, 'body', NULL, '<p>Bizimle irtibata geçmek için aşağıdaki formu kullanabilirsiniz.</p>', 1000, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_page_item_criteria`
--

CREATE TABLE `blc_page_item_criteria` (
  `PAGE_ITEM_CRITERIA_ID` bigint(20) NOT NULL,
  `ORDER_ITEM_MATCH_RULE` longtext,
  `QUANTITY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_page_rule`
--

CREATE TABLE `blc_page_rule` (
  `PAGE_RULE_ID` bigint(20) NOT NULL,
  `MATCH_RULE` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_page_rule_map`
--

CREATE TABLE `blc_page_rule_map` (
  `BLC_PAGE_PAGE_ID` bigint(20) NOT NULL,
  `PAGE_RULE_ID` bigint(20) NOT NULL,
  `MAP_KEY` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_page_tmplt`
--

CREATE TABLE `blc_page_tmplt` (
  `PAGE_TMPLT_ID` bigint(20) NOT NULL,
  `TMPLT_DESCR` varchar(255) DEFAULT NULL,
  `TMPLT_NAME` varchar(255) DEFAULT NULL,
  `TMPLT_PATH` varchar(255) DEFAULT NULL,
  `LOCALE_CODE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_page_tmplt`
--

INSERT INTO `blc_page_tmplt` (`PAGE_TMPLT_ID`, `TMPLT_DESCR`, `TMPLT_NAME`, `TMPLT_PATH`, `LOCALE_CODE`) VALUES
(1, 'This template provides a basic layout with header and footer surrounding the content and title.', 'Basic Template', 'content/default', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_payment_log`
--

CREATE TABLE `blc_payment_log` (
  `PAYMENT_LOG_ID` bigint(20) NOT NULL,
  `AMOUNT_PAID` decimal(19,5) DEFAULT NULL,
  `EXCEPTION_MESSAGE` varchar(255) DEFAULT NULL,
  `LOG_TYPE` varchar(255) NOT NULL,
  `ORDER_PAYMENT_ID` bigint(20) DEFAULT NULL,
  `ORDER_PAYMENT_REF_NUM` varchar(255) DEFAULT NULL,
  `TRANSACTION_SUCCESS` tinyint(1) DEFAULT NULL,
  `TRANSACTION_TIMESTAMP` datetime NOT NULL,
  `TRANSACTION_TYPE` varchar(255) NOT NULL,
  `USER_NAME` varchar(255) NOT NULL,
  `CURRENCY_CODE` varchar(255) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_personal_message`
--

CREATE TABLE `blc_personal_message` (
  `PERSONAL_MESSAGE_ID` bigint(20) NOT NULL,
  `MESSAGE` varchar(255) DEFAULT NULL,
  `MESSAGE_FROM` varchar(255) DEFAULT NULL,
  `MESSAGE_TO` varchar(255) DEFAULT NULL,
  `OCCASION` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_personal_message`
--

INSERT INTO `blc_personal_message` (`PERSONAL_MESSAGE_ID`, `MESSAGE`, `MESSAGE_FROM`, `MESSAGE_TO`, `OCCASION`) VALUES
(1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_pgtmplt_fldgrp_xref`
--

CREATE TABLE `blc_pgtmplt_fldgrp_xref` (
  `PG_TMPLT_FLD_GRP_ID` bigint(20) NOT NULL,
  `GROUP_ORDER` decimal(10,6) DEFAULT NULL,
  `FLD_GROUP_ID` bigint(20) DEFAULT NULL,
  `PAGE_TMPLT_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_pgtmplt_fldgrp_xref`
--

INSERT INTO `blc_pgtmplt_fldgrp_xref` (`PG_TMPLT_FLD_GRP_ID`, `GROUP_ORDER`, `FLD_GROUP_ID`, `PAGE_TMPLT_ID`) VALUES
(-100, '0.000000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_phone`
--

CREATE TABLE `blc_phone` (
  `PHONE_ID` bigint(20) NOT NULL,
  `COUNTRY_CODE` varchar(255) DEFAULT NULL,
  `EXTENSION` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` tinyint(1) DEFAULT NULL,
  `IS_DEFAULT` tinyint(1) DEFAULT NULL,
  `PHONE_NUMBER` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_phone`
--

INSERT INTO `blc_phone` (`PHONE_ID`, `COUNTRY_CODE`, `EXTENSION`, `IS_ACTIVE`, `IS_DEFAULT`, `PHONE_NUMBER`) VALUES
(1, NULL, NULL, 1, 0, '65425845'),
(2, NULL, NULL, 1, 0, '65425845');

-- --------------------------------------------------------

--
-- Table structure for table `blc_product`
--

CREATE TABLE `blc_product` (
  `PRODUCT_ID` bigint(20) NOT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `CAN_SELL_WITHOUT_OPTIONS` tinyint(1) DEFAULT NULL,
  `CANONICAL_URL` varchar(255) DEFAULT NULL,
  `DISPLAY_TEMPLATE` varchar(255) DEFAULT NULL,
  `IS_FEATURED_PRODUCT` tinyint(1) NOT NULL,
  `MANUFACTURE` varchar(255) DEFAULT NULL,
  `META_DESC` varchar(255) DEFAULT NULL,
  `META_TITLE` varchar(255) DEFAULT NULL,
  `MODEL` varchar(255) DEFAULT NULL,
  `OVERRIDE_GENERATED_URL` tinyint(1) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `URL_KEY` varchar(255) DEFAULT NULL,
  `DEFAULT_CATEGORY_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_SKU_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_product`
--

INSERT INTO `blc_product` (`PRODUCT_ID`, `ARCHIVED`, `CAN_SELL_WITHOUT_OPTIONS`, `CANONICAL_URL`, `DISPLAY_TEMPLATE`, `IS_FEATURED_PRODUCT`, `MANUFACTURE`, `META_DESC`, `META_TITLE`, `MODEL`, `OVERRIDE_GENERATED_URL`, `URL`, `URL_KEY`, `DEFAULT_CATEGORY_ID`, `DEFAULT_SKU_ID`) VALUES
(1, NULL, 1, NULL, NULL, 1, 'Blair\'s', NULL, NULL, NULL, 0, '/hot-sauces/sudden_death_sauce', NULL, NULL, 1),
(2, NULL, NULL, NULL, NULL, 0, 'Blair\'s', NULL, NULL, NULL, 0, '/hot-sauces/sweet_death_sauce', NULL, NULL, 2),
(3, NULL, 1, NULL, NULL, 0, 'Salsa Express', NULL, NULL, NULL, 0, '/hot-sauces/hoppin_hot_sauce', NULL, NULL, 3),
(4, NULL, NULL, NULL, NULL, 0, 'Spice Exchange', NULL, NULL, NULL, 0, '/hot-sauces/day_of_the_dead_chipotle_hot_sauce', NULL, NULL, 4),
(5, NULL, NULL, NULL, NULL, 0, 'Spice Exchange', NULL, NULL, NULL, 0, '/hot-sauces/day_of_the_dead_habanero_hot_sauce', NULL, NULL, 5),
(6, NULL, NULL, NULL, NULL, 0, 'Spice Exchange', NULL, NULL, NULL, 0, '/hot-sauces/day_of_the_dead_scotch_bonnet_sauce', NULL, NULL, 6),
(7, NULL, NULL, NULL, NULL, 0, 'Garden Row', NULL, NULL, NULL, 0, '/hot-sauces/green_ghost', NULL, NULL, 7),
(8, NULL, NULL, NULL, NULL, 0, 'D. L. Jardine\'s', NULL, NULL, NULL, 0, '/hot-sauces/blazin_saddle_hot_habanero_pepper_sauce', NULL, NULL, 8),
(9, NULL, NULL, NULL, NULL, 1, 'Figueroa Brothers', NULL, NULL, NULL, 0, '/hot-sauces/armageddon_hot_sauce_to_end_all', NULL, NULL, 9),
(10, NULL, NULL, NULL, NULL, 0, 'Figueroa Brothers', NULL, NULL, NULL, 0, '/hot-sauces/dr_chilemeisters_insane_hot_sauce', NULL, NULL, 10),
(11, NULL, NULL, NULL, NULL, 0, 'Brazos Legends', NULL, NULL, NULL, 0, '/hot-sauces/bull_snort_cowboy_cayenne_pepper_hot_sauce', NULL, NULL, 11),
(12, NULL, NULL, NULL, NULL, 0, 'Garden Row', NULL, NULL, NULL, 0, '/hot-sauces/cafe_louisiane_sweet_cajun_blackening_sauce', NULL, NULL, 12),
(13, NULL, NULL, NULL, NULL, 1, 'Brazos Legends', NULL, NULL, NULL, 0, '/hot-sauces/bull_snort_smokin_toncils_hot_sauce', NULL, NULL, 13),
(14, NULL, NULL, NULL, NULL, 0, 'Dave\'s Gourmet', NULL, NULL, NULL, 0, '/hot-sauces/cool_cayenne_pepper_hot_sauce', NULL, NULL, 14),
(15, NULL, NULL, NULL, NULL, 0, 'Dave\'s Gourmet', NULL, NULL, NULL, 0, '/hot-sauces/roasted_garlic_hot_sauce', NULL, NULL, 15),
(16, NULL, NULL, NULL, NULL, 0, 'Dave\'s Gourmet', NULL, NULL, NULL, 0, '/hot-sauces/scotch_bonnet_hot_sauce', NULL, NULL, 16),
(17, NULL, NULL, NULL, NULL, 0, 'Dave\'s Gourmet', NULL, NULL, NULL, 0, '/hot-sauces/insanity_sauce', NULL, NULL, 17),
(18, NULL, NULL, NULL, NULL, 0, 'Dave\'s Gourmet', NULL, NULL, NULL, 0, '/hot-sauces/hurtin_jalepeno_hot_sauce', NULL, NULL, 18),
(19, NULL, NULL, NULL, NULL, 0, 'Dave\'s Gourmet', NULL, NULL, NULL, 0, '/hot-sauces/roasted_red_pepper_chipotle_hot_sauce', NULL, NULL, 19),
(100, NULL, NULL, NULL, NULL, 0, 'The Heat Clinic', NULL, NULL, NULL, NULL, '/merchandise/hawt_like_a_habanero_mens', NULL, NULL, 100),
(200, NULL, NULL, NULL, NULL, 0, 'The Heat Clinic', NULL, NULL, NULL, NULL, '/merchandise/hawt_like_a_habanero_womens', NULL, NULL, 200),
(300, NULL, NULL, NULL, NULL, 0, 'The Heat Clinic', NULL, NULL, NULL, NULL, '/merchandise/heat_clinic_hand-drawn_mens', NULL, NULL, 300),
(400, NULL, NULL, NULL, NULL, 0, 'The Heat Clinic', NULL, NULL, NULL, NULL, '/merchandise/heat_clinic_hand-drawn_womens', NULL, NULL, 400),
(500, NULL, NULL, NULL, NULL, 0, 'The Heat Clinic', NULL, NULL, NULL, NULL, '/merchandise/heat_clinic_mascot_mens', NULL, NULL, 500),
(600, NULL, NULL, NULL, NULL, 0, 'The Heat Clinic', NULL, NULL, NULL, NULL, '/merchandise/heat_clinic_mascot_womens', NULL, NULL, 600),
(10000, 'N', 0, NULL, NULL, 0, 'VOLVO', NULL, NULL, NULL, 0, '/kiz-cocuklari-icin-oyuncaklar/kiz-bebek', NULL, NULL, 10001);

-- --------------------------------------------------------

--
-- Table structure for table `blc_product_attribute`
--

CREATE TABLE `blc_product_attribute` (
  `PRODUCT_ATTRIBUTE_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_product_attribute`
--

INSERT INTO `blc_product_attribute` (`PRODUCT_ATTRIBUTE_ID`, `NAME`, `VALUE`, `PRODUCT_ID`) VALUES
(1, 'heatRange', '4', 1),
(2, 'heatRange', '1', 2),
(3, 'heatRange', '2', 3),
(4, 'heatRange', '2', 4),
(5, 'heatRange', '4', 5),
(6, 'heatRange', '4', 6),
(7, 'heatRange', '3', 7),
(8, 'heatRange', '4', 8),
(9, 'heatRange', '5', 9),
(10, 'heatRange', '5', 10),
(11, 'heatRange', '2', 11),
(12, 'heatRange', '1', 12),
(13, 'heatRange', '2', 13),
(14, 'heatRange', '2', 14),
(15, 'heatRange', '1', 15),
(16, 'heatRange', '3', 16),
(17, 'heatRange', '5', 17),
(18, 'heatRange', '3', 18),
(19, 'heatRange', '1', 19);

-- --------------------------------------------------------

--
-- Table structure for table `blc_product_bundle`
--

CREATE TABLE `blc_product_bundle` (
  `AUTO_BUNDLE` tinyint(1) DEFAULT NULL,
  `BUNDLE_PROMOTABLE` tinyint(1) DEFAULT NULL,
  `ITEMS_PROMOTABLE` tinyint(1) DEFAULT NULL,
  `PRICING_MODEL` varchar(255) DEFAULT NULL,
  `BUNDLE_PRIORITY` int(11) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_product_cross_sale`
--

CREATE TABLE `blc_product_cross_sale` (
  `CROSS_SALE_PRODUCT_ID` bigint(20) NOT NULL,
  `PROMOTION_MESSAGE` varchar(255) DEFAULT NULL,
  `SEQUENCE` decimal(10,6) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `RELATED_SALE_PRODUCT_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_product_cross_sale`
--

INSERT INTO `blc_product_cross_sale` (`CROSS_SALE_PRODUCT_ID`, `PROMOTION_MESSAGE`, `SEQUENCE`, `CATEGORY_ID`, `PRODUCT_ID`, `RELATED_SALE_PRODUCT_ID`) VALUES
(1, NULL, '0.000000', NULL, 1, 5),
(2, NULL, '1.000000', NULL, 1, 9),
(51, NULL, '0.000000', NULL, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_product_featured`
--

CREATE TABLE `blc_product_featured` (
  `FEATURED_PRODUCT_ID` bigint(20) NOT NULL,
  `PROMOTION_MESSAGE` varchar(255) DEFAULT NULL,
  `SEQUENCE` decimal(10,6) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_product_featured`
--

INSERT INTO `blc_product_featured` (`FEATURED_PRODUCT_ID`, `PROMOTION_MESSAGE`, `SEQUENCE`, `CATEGORY_ID`, `PRODUCT_ID`) VALUES
(1, NULL, '1.000000', 2001, 18),
(2, NULL, '2.000000', 2001, 15),
(3, NULL, '3.000000', 2001, 200),
(4, NULL, '4.000000', 2001, 100),
(1000, NULL, '0.000000', 10053, 3),
(1001, NULL, '1.000000', 10053, 9),
(1002, NULL, '2.000000', 10053, 4),
(1003, NULL, '3.000000', 10053, 10);

-- --------------------------------------------------------

--
-- Table structure for table `blc_product_option`
--

CREATE TABLE `blc_product_option` (
  `PRODUCT_OPTION_ID` bigint(20) NOT NULL,
  `ATTRIBUTE_NAME` varchar(255) DEFAULT NULL,
  `DISPLAY_ORDER` int(11) DEFAULT NULL,
  `ERROR_CODE` varchar(255) DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) DEFAULT NULL,
  `LABEL` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `VALIDATION_STRATEGY_TYPE` varchar(255) DEFAULT NULL,
  `VALIDATION_TYPE` varchar(255) DEFAULT NULL,
  `REQUIRED` tinyint(1) DEFAULT NULL,
  `OPTION_TYPE` varchar(255) DEFAULT NULL,
  `USE_IN_SKU_GENERATION` tinyint(1) DEFAULT NULL,
  `VALIDATION_STRING` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_product_option`
--

INSERT INTO `blc_product_option` (`PRODUCT_OPTION_ID`, `ATTRIBUTE_NAME`, `DISPLAY_ORDER`, `ERROR_CODE`, `ERROR_MESSAGE`, `LABEL`, `NAME`, `VALIDATION_STRATEGY_TYPE`, `VALIDATION_TYPE`, `REQUIRED`, `OPTION_TYPE`, `USE_IN_SKU_GENERATION`, `VALIDATION_STRING`) VALUES
(1, 'COLOR', NULL, NULL, NULL, 'Shirt Color', 'Shirt Color', 'NONE', NULL, 1, 'COLOR', NULL, NULL),
(2, 'SIZE', NULL, NULL, NULL, 'Shirt Size', 'Shirt Size', 'NONE', NULL, 1, 'SIZE', NULL, NULL),
(3, 'NAME', NULL, 'INVALID_NAME', 'Name must be less than 30 characters, with only letters and spaces', 'Personalized Name', 'Personalized Name', 'SUBMIT_ORDER', 'REGEX', 0, 'TEXT', 0, '[a-zA-Z ]{0,30}'),
(1000, 'Renk', NULL, NULL, NULL, 'Renk', 'Renk', 'NONE', NULL, NULL, 'COLOR', 0, NULL),
(1001, 'Ebat', NULL, NULL, NULL, 'Ebat', 'Ebat', 'NONE', NULL, NULL, 'SIZE', 0, NULL),
(1002, 'Yaş Aralığı', NULL, NULL, NULL, 'Yaş Aralığı', 'Yaş Aralığı', 'NONE', NULL, NULL, 'DATE', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_product_option_value`
--

CREATE TABLE `blc_product_option_value` (
  `PRODUCT_OPTION_VALUE_ID` bigint(20) NOT NULL,
  `ATTRIBUTE_VALUE` varchar(255) DEFAULT NULL,
  `DISPLAY_ORDER` bigint(20) DEFAULT NULL,
  `PRICE_ADJUSTMENT` decimal(19,5) DEFAULT NULL,
  `PRODUCT_OPTION_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_product_option_value`
--

INSERT INTO `blc_product_option_value` (`PRODUCT_OPTION_VALUE_ID`, `ATTRIBUTE_VALUE`, `DISPLAY_ORDER`, `PRICE_ADJUSTMENT`, `PRODUCT_OPTION_ID`) VALUES
(1, 'Black', 1, NULL, 1),
(2, 'Red', 2, NULL, 1),
(3, 'Silver', 3, NULL, 1),
(11, 'S', 1, NULL, 2),
(12, 'M', 2, NULL, 2),
(13, 'L', 3, NULL, 2),
(14, 'XL', 4, NULL, 2),
(1000, 'Kırmızı', NULL, NULL, 1000),
(1001, 'Mavi', NULL, NULL, 1000),
(1002, 'Mor', NULL, NULL, 1000),
(1003, 'Kahverengi', NULL, NULL, 1000),
(1004, 'Lacivert', NULL, NULL, 1000),
(1005, 'Eflatun', NULL, NULL, 1000),
(1006, 'Sarı', NULL, NULL, 1000),
(1007, 'Beyaz', NULL, NULL, 1000),
(1008, 'Siyah', NULL, NULL, 1000),
(1009, 'XXS', NULL, NULL, 1001),
(1010, 'XS', NULL, NULL, 1001),
(1011, 'S', NULL, NULL, 1001),
(1012, 'M', NULL, NULL, 1001),
(1013, 'L', NULL, NULL, 1001),
(1014, 'XL', NULL, NULL, 1001),
(1015, 'XXL', NULL, NULL, 1001),
(1016, '0-1 Yaş', NULL, NULL, 1002),
(1017, '0 - 2  Yaş', NULL, NULL, 1002),
(1018, '2 - 4 Yaş', NULL, NULL, 1002),
(1019, '4 - 6 Yaş', NULL, NULL, 1002),
(1020, '6+ Yaş', NULL, NULL, 1002);

-- --------------------------------------------------------

--
-- Table structure for table `blc_product_option_xref`
--

CREATE TABLE `blc_product_option_xref` (
  `PRODUCT_OPTION_XREF_ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) NOT NULL,
  `PRODUCT_OPTION_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_product_option_xref`
--

INSERT INTO `blc_product_option_xref` (`PRODUCT_OPTION_XREF_ID`, `PRODUCT_ID`, `PRODUCT_OPTION_ID`) VALUES
(1, 100, 1),
(2, 200, 1),
(3, 300, 1),
(4, 400, 1),
(5, 500, 1),
(6, 600, 1),
(7, 100, 2),
(8, 200, 2),
(9, 300, 2),
(10, 400, 2),
(11, 500, 2),
(12, 600, 2),
(13, 100, 3),
(1050, 2, 1000),
(1052, 10000, 2);

-- --------------------------------------------------------

--
-- Table structure for table `blc_product_up_sale`
--

CREATE TABLE `blc_product_up_sale` (
  `UP_SALE_PRODUCT_ID` bigint(20) NOT NULL,
  `PROMOTION_MESSAGE` varchar(255) DEFAULT NULL,
  `SEQUENCE` decimal(10,6) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `RELATED_SALE_PRODUCT_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_product_up_sale`
--

INSERT INTO `blc_product_up_sale` (`UP_SALE_PRODUCT_ID`, `PROMOTION_MESSAGE`, `SEQUENCE`, `CATEGORY_ID`, `PRODUCT_ID`, `RELATED_SALE_PRODUCT_ID`) VALUES
(1, NULL, '0.000000', NULL, 1, 1),
(2, NULL, '1.000000', NULL, 1, 9),
(51, NULL, '0.000000', NULL, 2, 7);

-- --------------------------------------------------------

--
-- Table structure for table `blc_promotion_message`
--

CREATE TABLE `blc_promotion_message` (
  `PROMOTION_MESSAGE_ID` bigint(20) NOT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `PROMOTION_MESSASGE` varchar(255) DEFAULT NULL,
  `MESSAGE_PLACEMENT` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PROMOTION_MESSAGE_PRIORITY` int(11) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `LOCALE_CODE` varchar(255) DEFAULT NULL,
  `MEDIA_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_prorated_order_item_adjust`
--

CREATE TABLE `blc_prorated_order_item_adjust` (
  `PRORATED_ORDER_ITEM_ADJUST_ID` bigint(20) NOT NULL,
  `PRORATED_QUANTITY` int(11) NOT NULL,
  `ADJUSTMENT_REASON` varchar(255) NOT NULL,
  `PRORATED_ADJUSTMENT_VALUE` decimal(19,5) NOT NULL,
  `OFFER_ID` bigint(20) NOT NULL,
  `ORDER_ITEM_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_qual_crit_offer_xref`
--

CREATE TABLE `blc_qual_crit_offer_xref` (
  `OFFER_QUAL_CRIT_ID` bigint(20) NOT NULL,
  `OFFER_ID` bigint(20) NOT NULL,
  `OFFER_ITEM_CRITERIA_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_qual_crit_offer_xref`
--

INSERT INTO `blc_qual_crit_offer_xref` (`OFFER_QUAL_CRIT_ID`, `OFFER_ID`, `OFFER_ITEM_CRITERIA_ID`) VALUES
(1, 1, 1000),
(58, 1050, 1057),
(102, 1101, 1103);

-- --------------------------------------------------------

--
-- Table structure for table `blc_qual_crit_page_xref`
--

CREATE TABLE `blc_qual_crit_page_xref` (
  `PAGE_ID` bigint(20) NOT NULL DEFAULT '0',
  `PAGE_ITEM_CRITERIA_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_qual_crit_sc_xref`
--

CREATE TABLE `blc_qual_crit_sc_xref` (
  `SC_ID` bigint(20) NOT NULL,
  `SC_ITEM_CRITERIA_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_rating_detail`
--

CREATE TABLE `blc_rating_detail` (
  `RATING_DETAIL_ID` bigint(20) NOT NULL,
  `RATING` double NOT NULL,
  `RATING_SUBMITTED_DATE` datetime NOT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL,
  `RATING_SUMMARY_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_rating_summary`
--

CREATE TABLE `blc_rating_summary` (
  `RATING_SUMMARY_ID` bigint(20) NOT NULL,
  `AVERAGE_RATING` double NOT NULL,
  `ITEM_ID` varchar(255) NOT NULL,
  `RATING_TYPE` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_review_detail`
--

CREATE TABLE `blc_review_detail` (
  `REVIEW_DETAIL_ID` bigint(20) NOT NULL,
  `HELPFUL_COUNT` int(11) NOT NULL,
  `NOT_HELPFUL_COUNT` int(11) NOT NULL,
  `REVIEW_SUBMITTED_DATE` datetime NOT NULL,
  `REVIEW_STATUS` varchar(255) NOT NULL,
  `REVIEW_TEXT` varchar(255) NOT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL,
  `RATING_DETAIL_ID` bigint(20) DEFAULT NULL,
  `RATING_SUMMARY_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_review_feedback`
--

CREATE TABLE `blc_review_feedback` (
  `REVIEW_FEEDBACK_ID` bigint(20) NOT NULL,
  `IS_HELPFUL` tinyint(1) NOT NULL,
  `CUSTOMER_ID` bigint(20) NOT NULL,
  `REVIEW_DETAIL_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_role`
--

CREATE TABLE `blc_role` (
  `ROLE_ID` bigint(20) NOT NULL,
  `ROLE_NAME` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_role`
--

INSERT INTO `blc_role` (`ROLE_ID`, `ROLE_NAME`) VALUES
(1, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Table structure for table `blc_sandbox`
--

CREATE TABLE `blc_sandbox` (
  `SANDBOX_ID` bigint(20) NOT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `AUTHOR` bigint(20) DEFAULT NULL,
  `COLOR` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `GO_LIVE_DATE` datetime DEFAULT NULL,
  `SANDBOX_NAME` varchar(255) DEFAULT NULL,
  `SANDBOX_TYPE` varchar(255) DEFAULT NULL,
  `PARENT_SANDBOX_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sandbox`
--

INSERT INTO `blc_sandbox` (`SANDBOX_ID`, `ARCHIVED`, `AUTHOR`, `COLOR`, `DESCRIPTION`, `GO_LIVE_DATE`, `SANDBOX_NAME`, `SANDBOX_TYPE`, `PARENT_SANDBOX_ID`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(1, 'N', NULL, '#20C0F0', NULL, NULL, 'Default', 'DEFAULT', NULL, NULL, NULL, NULL, NULL),
(2, 'N', -1, NULL, NULL, NULL, 'Default', 'USER', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_sandbox_mgmt`
--

CREATE TABLE `blc_sandbox_mgmt` (
  `SANDBOX_MGMT_ID` bigint(20) NOT NULL,
  `SANDBOX_ID` bigint(20) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sandbox_mgmt`
--

INSERT INTO `blc_sandbox_mgmt` (`SANDBOX_MGMT_ID`, `SANDBOX_ID`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(1, 1, NULL, NULL, NULL, NULL),
(2, 2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_sc`
--

CREATE TABLE `blc_sc` (
  `SC_ID` bigint(20) NOT NULL,
  `CONTENT_NAME` varchar(255) NOT NULL,
  `OFFLINE_FLAG` tinyint(1) DEFAULT NULL,
  `PRIORITY` int(11) NOT NULL,
  `LOCALE_CODE` varchar(255) NOT NULL,
  `SC_TYPE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sc`
--

INSERT INTO `blc_sc` (`SC_ID`, `CONTENT_NAME`, `OFFLINE_FLAG`, `PRIORITY`, `LOCALE_CODE`, `SC_TYPE_ID`) VALUES
(-140, 'RHS - The Essentials Collection', 0, 5, 'en', 4),
(-130, 'Home Page Featured Products Title', 0, 5, 'en', 3),
(-110, 'Home Page Snippet - Aficionado', 0, 5, 'en', 2),
(-105, 'Member Special - $10 off next order over $50', 0, 3, 'en', 1),
(-104, 'Shirt Special - 20% off all shirts', 0, 1, 'en', 1),
(-103, 'Buy One Get One - Twice the Burn', 0, 2, 'en', 1),
(-102, 'Member Special - $10 off next order over $50', 0, 3, 'en', 1),
(-101, 'Shirt Special - 20% off all shirts', 0, 1, 'en', 1),
(-100, 'Buy One Get One - Twice the Burn', 0, 2, 'en', 1),
(151, 'Promocion - 20% de descuento en todas las camisas', 0, 1, 'es', 1),
(152, 'Promocion - 20% de descuento en todas las camisas', 0, 1, 'fr', 1),
(153, 'Home Page Snippet (es) - Aficionado', 0, 5, 'es', 2),
(154, 'Home Page Snippet (es) - Aficionado', 0, 5, 'fr', 2),
(155, 'Home Page Featured Products Title', 0, 5, 'es', 3),
(156, 'Home Page Featured Products Title', 0, 5, 'fr', 3),
(1052, 'Best Seller Rigth', 0, 1, 'en', 1000),
(1053, 'Category Middle', 0, 1, 'en', 1050),
(1054, 'Category Right', 0, 3, 'en', 1051),
(1100, 'Fashion Banner 1', 0, 1, 'en', 1100),
(1102, 'Fashion Banner 3', 0, 3, 'en', 1102),
(1103, 'Fashion Banner 4', 0, 4, 'en', 1103),
(1104, 'Fashion Banner 22', 0, 2, 'en', 1101),
(1150, 'Slider 1', 0, 1, 'en', 1150),
(1151, 'Slider 2', 0, 2, 'en', 1151),
(1152, 'Slider 3', 0, 3, 'en', 1152),
(1301, 'Slider Banner', 0, 5, 'en', 5);

-- --------------------------------------------------------

--
-- Table structure for table `blc_sc_fld`
--

CREATE TABLE `blc_sc_fld` (
  `SC_FLD_ID` bigint(20) NOT NULL,
  `FLD_KEY` varchar(255) DEFAULT NULL,
  `LOB_VALUE` longtext,
  `VALUE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sc_fld`
--

INSERT INTO `blc_sc_fld` (`SC_FLD_ID`, `FLD_KEY`, `LOB_VALUE`, `VALUE`) VALUES
(-13, 'targetUrl', NULL, '/hot-sauces'),
(-12, 'imageUrl', NULL, '/img/rhs-ad.jpg'),
(-11, 'messageText', NULL, 'The Heat Clinic\'s Top Selling Sauces'),
(-9, 'htmlContent', NULL, '<h2>HOT SAUCE AFICIONADO?</h2> Click to join our Heat Clinic Frequent Care Program. The place to get all the deals on burn treatment.'),
(-6, 'targetUrl', NULL, '/register'),
(-5, 'imageUrl', NULL, '/cmsstatic/img/banners/member-special.jpg'),
(-4, 'targetUrl', NULL, '/merchandise'),
(-3, 'imageUrl', NULL, '/cmsstatic/img/banners/shirt-special.jpg'),
(-2, 'targetUrl', NULL, '/hot-sauces'),
(-1, 'imageUrl', NULL, '/cmsstatic/img/sauces/Sudden-Death-Sauce-Close.jpg'),
(51, 'imageUrl', NULL, '/cmsstatic/img/banners/promocion-camisas.jpg'),
(52, 'targetUrl', NULL, '/merchandise'),
(53, 'imageUrl', NULL, '/cmsstatic/img/banners/shirts-speciale.jpg'),
(54, 'targetUrl', NULL, '/merchandise'),
(55, 'htmlContent', NULL, '<h2>AFICIONADO DE SALSAS PICANTES?</h2> Haga click para unirse a nuerto programa de Cuidades Intensivos de Heat Clinic. El lugar para conseguir las mejores ofertas.'),
(56, 'htmlContent', NULL, '<h2>AFICIONADO SAUCE PIQUANTE?</h2> Cliquez ici pour vous joindre &agrave; notre clinique de chaleur du Programme de soins fr&eacute;quents. L&#39;endroit pour obtenir toutes les offres sur le traitement des br&ucirc;lures.'),
(57, 'messageText', NULL, 'Las Salsas M&aacute;s vendidas de Heat Clinic'),
(58, 'messageText', NULL, 'La Clinique Heat Sauces Meilleures Ventes'),
(1050, 'htmlContent', '<div class=\"col-md-4 col-sm-4 category-wrap\">\r\n	<div class=\"pink-border light-bg\">\r\n		<div class=\"category-content col-md-6 no-padding\">\r\n			<div class=\"title-wrap\">\r\n				<h2 class=\"section-title\">Çocuk Koleksiyonları</h2>\r\n				<h3 class=\"sub-title-small\">Up to 80% Off</h3>\r\n			</div>\r\n			<hr class=\"dash-divider\">\r\n			<h4 class=\"baby-years pink-color\">0 - 5 Years</h4>\r\n			<div class=\"category-shop\">\r\n				<a href=\"#\" class=\"pink-btn-small btn\"> İncele </a>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>', NULL),
(1051, 'htmlContent', '<div class=\"col-md-4 col-sm-4 category-wrap\">\r\n	<div class=\"green-border light-bg\">\r\n		<div class=\"category-content col-md-6 no-padding\">\r\n			<div class=\"category-new\">\r\n				<div class=\"green-new-tag new-tag\">\r\n					<a href=\"#\" class=\"funky-font\">Yeni!</a>\r\n				</div>\r\n			</div>\r\n			<div class=\"title-wrap\">\r\n				<h2 class=\"section-title\">\r\n				<span class=\"funky-font green-tag\">2-6 Yaş</span>\r\n				<span class=\"italic-font\">Oyuncaklar</span>\r\n				</h2>\r\n				<h3 class=\"sub-title-small\">%20\'ye varan indirimler!</h3>\r\n			</div>\r\n			<hr class=\"dash-divider\">\r\n			<h4 class=\"baby-years green-color\">4 - 6 Years</h4>\r\n			<div class=\"category-shop\">\r\n				<a href=\"#\" class=\"green-btn-small btn\"> İncele </a>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>', NULL),
(1052, 'htmlContent', '<div class=\"col-md-4 col-sm-4 category-wrap\">\r\n                            <div class=\"blue-border light-bg\">\r\n                                <div class=\"category-content col-md-6 no-padding\">\r\n                                    <div class=\"title-wrap\">\r\n                                        <h2 class=\"section-title\">\r\n                                            <span>\r\n                                                <span class=\"funky-font blue-tag\">Anne</span> \r\n                                                <span class=\"italic-font\">& Çocuk etkileşimi</span>\r\n                                            </span>\r\n                                        </h2>\r\n                                        <h3 class=\"sub-title-small\">%40 İndirim</h3>\r\n                                    </div>\r\n                                    <hr class=\"dash-divider\">\r\n                                    <h4 class=\"baby-years blue-color\">0 - 6 Yaş</h4>\r\n                                    <div class=\"category-shop\">\r\n                                        <a href=\"#\" class=\"blue-btn-small btn\"> İncele </a>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>', NULL),
(1100, 'imageUrl', NULL, '/cmsstatic/fashion1.jpg'),
(1101, 'targetUrl', NULL, NULL),
(1104, 'targetUrl', NULL, NULL),
(1105, 'imageUrl', NULL, '/cmsstatic/structured-content/1102/qweqwewewqeq.jpg'),
(1106, 'targetUrl', NULL, NULL),
(1107, 'imageUrl', NULL, '/cmsstatic/fashion4.jpg'),
(1108, 'imageUrl', NULL, '/cmsstatic/Fashion22.jpg'),
(1109, 'targetUrl', NULL, NULL),
(1150, 'targetUrl', NULL, NULL),
(1151, 'imageUrl', NULL, '/cmsstatic/1.jpg'),
(1152, 'targetUrl', NULL, NULL),
(1153, 'imageUrl', NULL, '/cmsstatic/2.jpg'),
(1154, 'targetUrl', NULL, NULL),
(1155, 'imageUrl', NULL, '/cmsstatic/3.jpg'),
(1202, 'targetUrl', NULL, NULL),
(1203, 'imageUrl', NULL, '/cmsstatic/structured-content/1301/slider banner.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `blc_sc_fldgrp_xref`
--

CREATE TABLE `blc_sc_fldgrp_xref` (
  `BLC_SC_FLDGRP_XREF_ID` bigint(20) NOT NULL,
  `GROUP_ORDER` int(11) DEFAULT NULL,
  `FLD_GROUP_ID` bigint(20) DEFAULT NULL,
  `SC_FLD_TMPLT_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sc_fldgrp_xref`
--

INSERT INTO `blc_sc_fldgrp_xref` (`BLC_SC_FLDGRP_XREF_ID`, `GROUP_ORDER`, `FLD_GROUP_ID`, `SC_FLD_TMPLT_ID`) VALUES
(-3, 0, 6, -3),
(-2, 0, 5, -2),
(-1, 0, 4, -1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_sc_fld_map`
--

CREATE TABLE `blc_sc_fld_map` (
  `BLC_SC_SC_FIELD_ID` bigint(20) NOT NULL,
  `MAP_KEY` varchar(255) NOT NULL,
  `SC_ID` bigint(20) NOT NULL,
  `SC_FLD_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sc_fld_map`
--

INSERT INTO `blc_sc_fld_map` (`BLC_SC_SC_FIELD_ID`, `MAP_KEY`, `SC_ID`, `SC_FLD_ID`) VALUES
(-173, 'targetUrl', -105, -6),
(-172, 'imageUrl', -105, -5),
(-171, 'targetUrl', -104, -4),
(-170, 'imageUrl', -104, -3),
(-169, 'targetUrl', -103, -2),
(-168, 'imageUrl', -103, -1),
(-167, 'targetUrl', -140, -13),
(-166, 'imageUrl', -140, -12),
(-165, 'messageText', -130, -11),
(-164, 'htmlContent', -110, -9),
(-163, 'targetUrl', -102, -6),
(-162, 'imageUrl', -102, -5),
(-161, 'targetUrl', -101, -4),
(-160, 'imageUrl', -101, -3),
(-159, 'targetUrl', -100, -2),
(-158, 'imageUrl', -100, -1),
(-157, 'messageText', 156, 58),
(-156, 'messageText', 155, 57),
(-155, 'htmlContent', 154, 56),
(-154, 'htmlContent', 153, 55),
(-153, 'targetUrl', 152, 54),
(-152, 'imageUrl', 152, 53),
(-151, 'targetUrl', 151, 52),
(-150, 'imageUrl', 151, 51),
(51, 'htmlContent', 1052, 1050),
(52, 'htmlContent', 1053, 1051),
(53, 'htmlContent', 1054, 1052),
(101, 'imageUrl', 1100, 1100),
(102, 'targetUrl', 1100, 1101),
(105, 'targetUrl', 1102, 1104),
(106, 'imageUrl', 1102, 1105),
(107, 'targetUrl', 1103, 1106),
(108, 'imageUrl', 1103, 1107),
(109, 'imageUrl', 1104, 1108),
(110, 'targetUrl', 1104, 1109),
(151, 'targetUrl', 1150, 1150),
(152, 'imageUrl', 1150, 1151),
(153, 'targetUrl', 1151, 1152),
(154, 'imageUrl', 1151, 1153),
(155, 'targetUrl', 1152, 1154),
(156, 'imageUrl', 1152, 1155),
(203, 'targetUrl', 1301, 1202),
(204, 'imageUrl', 1301, 1203);

-- --------------------------------------------------------

--
-- Table structure for table `blc_sc_fld_tmplt`
--

CREATE TABLE `blc_sc_fld_tmplt` (
  `SC_FLD_TMPLT_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sc_fld_tmplt`
--

INSERT INTO `blc_sc_fld_tmplt` (`SC_FLD_TMPLT_ID`, `NAME`) VALUES
(-3, 'Message Template'),
(-2, 'HTML Template'),
(-1, 'HTML Template'),
(1, 'BestSeller'),
(51, 'HTML Template'),
(52, 'HTML Template'),
(101, 'Banner Template'),
(102, 'Banner Template'),
(103, 'Banner Template'),
(104, 'Banner Template'),
(151, 'Banner Template'),
(152, 'Banner Template'),
(153, 'Banner Template'),
(451, 'HTML Template');

-- --------------------------------------------------------

--
-- Table structure for table `blc_sc_item_criteria`
--

CREATE TABLE `blc_sc_item_criteria` (
  `SC_ITEM_CRITERIA_ID` bigint(20) NOT NULL,
  `ORDER_ITEM_MATCH_RULE` longtext,
  `QUANTITY` int(11) NOT NULL,
  `SC_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_sc_rule`
--

CREATE TABLE `blc_sc_rule` (
  `SC_RULE_ID` bigint(20) NOT NULL,
  `MATCH_RULE` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_sc_rule_map`
--

CREATE TABLE `blc_sc_rule_map` (
  `BLC_SC_SC_ID` bigint(20) NOT NULL,
  `SC_RULE_ID` bigint(20) NOT NULL,
  `MAP_KEY` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_sc_type`
--

CREATE TABLE `blc_sc_type` (
  `SC_TYPE_ID` bigint(20) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SC_FLD_TMPLT_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sc_type`
--

INSERT INTO `blc_sc_type` (`SC_TYPE_ID`, `DESCRIPTION`, `NAME`, `SC_FLD_TMPLT_ID`) VALUES
(1, NULL, 'Homepage Banner Ad', -1),
(2, NULL, 'Homepage Middle Promo Snippet', -2),
(3, NULL, 'Homepage Featured Products Title', -3),
(4, NULL, 'Right Hand Side Banner Ad', -1),
(5, NULL, 'Homepage Slider Items', -1),
(1000, NULL, 'Category Banner 1', -2),
(1050, NULL, 'Category Banner 2', -2),
(1051, NULL, 'Category Banner 3', -2),
(1100, NULL, 'Fashion Banner 1', -1),
(1101, NULL, 'Fashion Banner 22', -1),
(1102, NULL, 'Fashion Banner 3', -1),
(1103, NULL, 'Fashion Banner 4', -1),
(1150, NULL, 'Slider Item 1', -1),
(1151, NULL, 'Slider Item 2', -1),
(1152, NULL, 'Slider Item 3', -1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_search_facet`
--

CREATE TABLE `blc_search_facet` (
  `SEARCH_FACET_ID` bigint(20) NOT NULL,
  `MULTISELECT` tinyint(1) DEFAULT NULL,
  `LABEL` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REQUIRES_ALL_DEPENDENT` tinyint(1) DEFAULT NULL,
  `SEARCH_DISPLAY_PRIORITY` int(11) DEFAULT NULL,
  `SHOW_ON_SEARCH` tinyint(1) DEFAULT NULL,
  `USE_FACET_RANGES` tinyint(1) DEFAULT NULL,
  `INDEX_FIELD_TYPE_ID` bigint(20) DEFAULT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_search_facet`
--

INSERT INTO `blc_search_facet` (`SEARCH_FACET_ID`, `MULTISELECT`, `LABEL`, `NAME`, `REQUIRES_ALL_DEPENDENT`, `SEARCH_DISPLAY_PRIORITY`, `SHOW_ON_SEARCH`, `USE_FACET_RANGES`, `INDEX_FIELD_TYPE_ID`, `ARCHIVED`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(1, 1, 'Manufacturer', 'Manufacturer Facet', NULL, 0, 0, 0, 2, NULL, NULL, NULL, NULL, NULL),
(2, 1, 'Heat Range', 'Heat Range Facet', NULL, 0, 0, 0, 3, NULL, NULL, NULL, NULL, NULL),
(3, 1, 'Price', 'Price Facet', NULL, 1, 1, 1, 4, NULL, NULL, NULL, NULL, NULL),
(4, 1, 'Color', 'Color Facet', NULL, 0, 1, 0, 9, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_search_facet_range`
--

CREATE TABLE `blc_search_facet_range` (
  `SEARCH_FACET_RANGE_ID` bigint(20) NOT NULL,
  `MAX_VALUE` decimal(19,5) DEFAULT NULL,
  `MIN_VALUE` decimal(19,5) NOT NULL,
  `SEARCH_FACET_ID` bigint(20) DEFAULT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_search_facet_range`
--

INSERT INTO `blc_search_facet_range` (`SEARCH_FACET_RANGE_ID`, `MAX_VALUE`, `MIN_VALUE`, `SEARCH_FACET_ID`, `ARCHIVED`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(1, '5.00000', '0.00000', 3, NULL, NULL, NULL, NULL, NULL),
(2, '10.00000', '5.00000', 3, NULL, NULL, NULL, NULL, NULL),
(3, '15.00000', '10.00000', 3, NULL, NULL, NULL, NULL, NULL),
(4, NULL, '15.00000', 3, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_search_facet_xref`
--

CREATE TABLE `blc_search_facet_xref` (
  `ID` bigint(20) NOT NULL,
  `REQUIRED_FACET_ID` bigint(20) NOT NULL,
  `SEARCH_FACET_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_search_intercept`
--

CREATE TABLE `blc_search_intercept` (
  `SEARCH_REDIRECT_ID` bigint(20) NOT NULL,
  `ACTIVE_END_DATE` datetime DEFAULT NULL,
  `ACTIVE_START_DATE` datetime DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `SEARCH_TERM` varchar(255) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_search_intercept`
--

INSERT INTO `blc_search_intercept` (`SEARCH_REDIRECT_ID`, `ACTIVE_END_DATE`, `ACTIVE_START_DATE`, `PRIORITY`, `SEARCH_TERM`, `URL`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(-2, NULL, '1992-10-15 14:28:36', -10, 'sale', '/clearance', NULL, NULL, NULL, NULL),
(-1, NULL, '1992-10-15 14:28:36', 1, 'insanity', '/hot-sauces/insanity_sauce', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blc_search_synonym`
--

CREATE TABLE `blc_search_synonym` (
  `SEARCH_SYNONYM_ID` bigint(20) NOT NULL,
  `SYNONYMS` varchar(255) DEFAULT NULL,
  `TERM` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_shipping_rate`
--

CREATE TABLE `blc_shipping_rate` (
  `ID` bigint(20) NOT NULL,
  `BAND_RESULT_PCT` int(11) NOT NULL,
  `BAND_RESULT_QTY` decimal(19,2) NOT NULL,
  `BAND_UNIT_QTY` decimal(19,2) NOT NULL,
  `FEE_BAND` int(11) NOT NULL,
  `FEE_SUB_TYPE` varchar(255) DEFAULT NULL,
  `FEE_TYPE` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_site`
--

CREATE TABLE `blc_site` (
  `SITE_ID` bigint(20) NOT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `DEACTIVATED` tinyint(1) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SITE_IDENTIFIER_TYPE` varchar(255) DEFAULT NULL,
  `SITE_IDENTIFIER_VALUE` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_site_catalog`
--

CREATE TABLE `blc_site_catalog` (
  `SITE_CATALOG_XREF_ID` bigint(20) NOT NULL,
  `CATALOG_ID` bigint(20) NOT NULL,
  `SITE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_site_map_cfg`
--

CREATE TABLE `blc_site_map_cfg` (
  `INDEXED_SITE_MAP_FILE_NAME` varchar(255) DEFAULT NULL,
  `INDEXED_SITE_MAP_FILE_PATTERN` varchar(255) DEFAULT NULL,
  `MAX_URL_ENTRIES_PER_FILE` int(11) DEFAULT NULL,
  `SITE_MAP_FILE_NAME` varchar(255) DEFAULT NULL,
  `MODULE_CONFIG_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_site_map_cfg`
--

INSERT INTO `blc_site_map_cfg` (`INDEXED_SITE_MAP_FILE_NAME`, `INDEXED_SITE_MAP_FILE_PATTERN`, `MAX_URL_ENTRIES_PER_FILE`, `SITE_MAP_FILE_NAME`, `MODULE_CONFIG_ID`) VALUES
(NULL, NULL, NULL, NULL, -1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_site_map_gen_cfg`
--

CREATE TABLE `blc_site_map_gen_cfg` (
  `GEN_CONFIG_ID` bigint(20) NOT NULL,
  `CHANGE_FREQ` varchar(255) NOT NULL,
  `DISABLED` tinyint(1) NOT NULL,
  `GENERATOR_TYPE` varchar(255) NOT NULL,
  `PRIORITY` varchar(255) DEFAULT NULL,
  `MODULE_CONFIG_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_site_map_gen_cfg`
--

INSERT INTO `blc_site_map_gen_cfg` (`GEN_CONFIG_ID`, `CHANGE_FREQ`, `DISABLED`, `GENERATOR_TYPE`, `PRIORITY`, `MODULE_CONFIG_ID`) VALUES
(-8, 'HOURLY', 1, 'SKU', '0.5', -1),
(-7, 'HOURLY', 0, 'CATEGORY', '0.5', -1),
(-6, 'HOURLY', 0, 'CATEGORY', '0.5', -1),
(-5, 'HOURLY', 0, 'CATEGORY', '0.5', -1),
(-4, 'HOURLY', 0, 'CATEGORY', '0.5', -1),
(-3, 'HOURLY', 0, 'PAGE', '0.5', -1),
(-2, 'HOURLY', 0, 'PRODUCT', '0.5', -1),
(-1, 'HOURLY', 0, 'CUSTOM', '0.5', -1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_site_map_url_entry`
--

CREATE TABLE `blc_site_map_url_entry` (
  `URL_ENTRY_ID` bigint(20) NOT NULL,
  `CHANGE_FREQ` varchar(255) NOT NULL,
  `LAST_MODIFIED` datetime NOT NULL,
  `LOCATION` varchar(255) NOT NULL,
  `PRIORITY` varchar(255) NOT NULL,
  `GEN_CONFIG_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_sku`
--

CREATE TABLE `blc_sku` (
  `SKU_ID` bigint(20) NOT NULL,
  `ACTIVE_END_DATE` datetime DEFAULT NULL,
  `ACTIVE_START_DATE` datetime DEFAULT NULL,
  `AVAILABLE_FLAG` char(1) DEFAULT NULL,
  `COST` decimal(19,5) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `CONTAINER_SHAPE` varchar(255) DEFAULT NULL,
  `DEPTH` decimal(19,2) DEFAULT NULL,
  `DIMENSION_UNIT_OF_MEASURE` varchar(255) DEFAULT NULL,
  `GIRTH` decimal(19,2) DEFAULT NULL,
  `HEIGHT` decimal(19,2) DEFAULT NULL,
  `CONTAINER_SIZE` varchar(255) DEFAULT NULL,
  `WIDTH` decimal(19,2) DEFAULT NULL,
  `DISCOUNTABLE_FLAG` char(1) DEFAULT NULL,
  `DISPLAY_TEMPLATE` varchar(255) DEFAULT NULL,
  `EXTERNAL_ID` varchar(255) DEFAULT NULL,
  `FULFILLMENT_TYPE` varchar(255) DEFAULT NULL,
  `INVENTORY_TYPE` varchar(255) DEFAULT NULL,
  `IS_MACHINE_SORTABLE` tinyint(1) DEFAULT NULL,
  `LONG_DESCRIPTION` longtext,
  `NAME` varchar(255) DEFAULT NULL,
  `QUANTITY_AVAILABLE` int(11) DEFAULT NULL,
  `RETAIL_PRICE` decimal(19,5) DEFAULT NULL,
  `SALE_PRICE` decimal(19,5) DEFAULT NULL,
  `TAX_CODE` varchar(255) DEFAULT NULL,
  `TAXABLE_FLAG` char(1) DEFAULT NULL,
  `UPC` varchar(255) DEFAULT NULL,
  `URL_KEY` varchar(255) DEFAULT NULL,
  `WEIGHT` decimal(19,2) DEFAULT NULL,
  `WEIGHT_UNIT_OF_MEASURE` varchar(255) DEFAULT NULL,
  `CURRENCY_CODE` varchar(255) DEFAULT NULL,
  `DEFAULT_PRODUCT_ID` bigint(20) DEFAULT NULL,
  `ADDL_PRODUCT_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sku`
--

INSERT INTO `blc_sku` (`SKU_ID`, `ACTIVE_END_DATE`, `ACTIVE_START_DATE`, `AVAILABLE_FLAG`, `COST`, `DESCRIPTION`, `CONTAINER_SHAPE`, `DEPTH`, `DIMENSION_UNIT_OF_MEASURE`, `GIRTH`, `HEIGHT`, `CONTAINER_SIZE`, `WIDTH`, `DISCOUNTABLE_FLAG`, `DISPLAY_TEMPLATE`, `EXTERNAL_ID`, `FULFILLMENT_TYPE`, `INVENTORY_TYPE`, `IS_MACHINE_SORTABLE`, `LONG_DESCRIPTION`, `NAME`, `QUANTITY_AVAILABLE`, `RETAIL_PRICE`, `SALE_PRICE`, `TAX_CODE`, `TAXABLE_FLAG`, `UPC`, `URL_KEY`, `WEIGHT`, `WEIGHT_UNIT_OF_MEASURE`, `CURRENCY_CODE`, `DEFAULT_PRODUCT_ID`, `ADDL_PRODUCT_ID`) VALUES
(1, NULL, '2017-09-16 16:40:54', NULL, '3.89000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, 'ALWAYS_AVAILABLE', NULL, 'As my Chilipals know, I am never one to be satisfied. Hence, the creation of Sudden Death. When you need to go beyond... Sudden Death will deliver!', 'Sudden Death Sauce', NULL, '10.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 1, NULL),
(2, NULL, '2017-09-16 16:40:54', NULL, '3.79000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, 'ALWAYS_AVAILABLE', NULL, 'The perfect topper for chicken, fish, burgers or pizza. A great blend of Habanero, Mango, Passion Fruit and more make this Death Sauce an amazing tropical treat.', 'Sweet Death Sauce', NULL, '10.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 2, NULL),
(3, NULL, '2017-09-16 16:40:54', NULL, '3.00000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Tangy, ripe cayenne peppes flow together with garlic, onion, tomato paste and a hint of cane sugar to make this a smooth sauce with a bite.  Wonderful on eggs, poultry, pork, or fish, this sauce blends to make rich marinades and soups.', 'Hoppin\' Hot Sauce', NULL, '4.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 3, NULL),
(4, NULL, '2017-09-16 16:40:54', NULL, '4.50000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'When any pepper is dried and smoked, it is referred to as a Chipotle. Usually with a wrinkled, drak brown appearance, the Chipotle delivers a smokey, sweet flavor which is generally used for adding a smokey, roasted flavor to salsas, stews and marinades.', 'Day of the Dead Chipotle Hot Sauce', NULL, '6.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 4, NULL),
(5, NULL, '2017-09-16 16:40:54', NULL, '5.50000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'If you want hot, this is the chile to choose. Native to the Carribean, Yucatan and Northern Coast of South America, the Habanero presents itself in a variety of colors ranging from light green to a bright red. The Habanero\'s bold heat, unique flavor and aroma has made it the favorite of chile lovers.', 'Day of the Dead Habanero Hot Sauce', NULL, '6.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 5, NULL),
(6, NULL, '2017-09-16 16:40:54', NULL, '5.40000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Often mistaken for the Habanero, the Scotch Bonnet has a deeply inverted tip as opposed to the pointed end of the Habanero. Ranging in many colors from green to yellow-orange, the Scotch Bonnet is a staple in West Indies and Barbados style pepper sauces.', 'Day of the Dead Scotch Bonnet Hot Sauce', NULL, '6.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 6, NULL),
(7, NULL, '2017-09-16 16:40:54', NULL, '8.10000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Made with Naga Bhut Jolokia, the World\'s Hottest pepper.', 'Green Ghost', NULL, '11.99000', '9.99000', NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 7, NULL),
(8, NULL, '2017-09-16 16:40:54', NULL, '3.00000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'You bet your boots, this hot sauce earned its name from folks that appreciate an outstanding hot sauce. What you\'ll find here is a truly original zesty flavor, not an overpowering pungency that is found in those ordinary Tabasco pepper sauces - even though the pepper used in this product was tested at 285,000 Scoville units. So, saddle up for a ride to remember. To make sure we brought you only the finest Habanero pepper sauce, we went to the foothills of the Mayan mountains in Belize, Central America. This product is prepared entirely by hand using only fresh vegetables and all natural ingredients.', 'Blazin\' Saddle XXX Hot Habanero Pepper Sauce', NULL, '4.99000', '3.99000', NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 8, NULL),
(9, NULL, '2017-09-16 16:40:54', NULL, '5.30000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'All Hell is breaking loose, fire &amp; brimstone rain down? prepare to meet your maker.', 'Armageddon The Hot Sauce To End All', NULL, '12.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 9, NULL),
(10, NULL, '2017-09-16 16:40:54', NULL, '6.89000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Here is the Prescription for those who enjoy intolerable heat. Dr. Chilemeister\'s sick and evil deadly brew should be used with caution. Pain can become addictive!', 'Dr. Chilemeister\'s Insane Hot Sauce', NULL, '12.99000', '10.99000', NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 10, NULL),
(11, NULL, '2017-09-16 16:40:54', NULL, '2.29000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Been there, roped that. Hotter than a buckin\' mare in heat! Sprinkle on meat entrees, seafood and vegetables. Use as additive in barbecue sauce or any food that needs a spicy flavor. Start with a few drops and work up to the desired flavor.', 'Bull Snort Cowboy Cayenne Pepper Hot Sauce', NULL, '3.99000', '2.99000', NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 11, NULL),
(12, NULL, '2017-09-16 16:40:54', NULL, '3.09000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'One of the more unusual sauces we sell. The original was an old style Cajun sauce and this is it\'s updated blackening version. It\'s sweet but you get a great hit of cinnamon and cloves with a nice kick of cayenne heat. Use on all foods to give that Cajun flair!', 'Cafe Louisiane Sweet Cajun Blackening Sauce', NULL, '4.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 12, NULL),
(13, NULL, '2017-09-16 16:40:54', NULL, '2.99000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Everything is bigger in Texas, even the burn of a Bull Snortin\' Hot Sauce! shower on that Texas sized steak they call the Ole 96er or your plane Jane vegetables. If you are a fan on making BBQ sauce from scratch like I am, you can use Bull Snort Smokin\' Tonsils Hot Sauce as an additive. Red hot habaneros and cayenne peppers give this tonsil tingler it\'s famous flavor and red hot heat. Bull Snort Smokin\' Tonsils Hot Sauce\'ll have your bowels buckin\' with just a drop!', 'Bull Snort Smokin\' Toncils Hot Sauce', NULL, '3.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 13, NULL),
(14, NULL, '2017-09-16 16:40:54', NULL, '3.99000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This sauce gets its great flavor from aged peppers and cane vinegar. It will enhance the flavor of most any meal.', 'Cool Cayenne Pepper Hot Sauce', NULL, '5.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 14, NULL),
(15, NULL, '2017-09-16 16:40:54', NULL, '4.29000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This sauce gets its great flavor from aged peppers and cane vinegar. It will enhance the flavor of most any meal.', 'Roasted Garlic Hot Sauce', NULL, '5.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 15, NULL),
(16, NULL, '2017-09-16 16:40:54', NULL, '2.89000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This sauce gets its great flavor from aged peppers and cane vinegar. It will enhance the flavor of most any meal.', 'Scotch Bonnet Hot Sauce', NULL, '5.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 16, NULL),
(17, NULL, '2017-09-16 16:40:54', NULL, '3.50000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This sauce gets its great flavor from aged peppers and cane vinegar. It will enhance the flavor of most any meal.', 'Insanity Sauce', NULL, '5.99000', '4.99000', NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 17, NULL),
(18, NULL, '2017-09-16 16:40:54', NULL, '3.25000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This sauce gets its great flavor from aged peppers and cane vinegar. It will enhance the flavor of most any meal.', 'Hurtin\' Jalapeno Hot Sauce', NULL, '5.99000', '4.49000', NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 18, NULL),
(19, NULL, '2017-09-16 16:40:54', NULL, '2.59000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This sauce gets its great flavor from aged peppers and cane vinegar. It will enhance the flavor of most any meal.', 'Roasted Red Pepper & Chipotle Hot Sauce', NULL, '5.99000', '4.09000', NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 19, NULL),
(100, NULL, '2017-09-16 16:40:54', NULL, '4.99000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 100, NULL),
(111, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/black_s', NULL, NULL, NULL, NULL, 100),
(112, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/black_m', NULL, NULL, NULL, NULL, 100),
(113, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/black_l', NULL, NULL, NULL, NULL, 100),
(114, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/black_xl', NULL, NULL, NULL, NULL, 100),
(121, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/red_s', NULL, NULL, NULL, NULL, 100),
(122, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/red_m', NULL, NULL, NULL, NULL, 100),
(123, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/red_l', NULL, NULL, NULL, NULL, 100),
(124, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/red_xl', NULL, NULL, NULL, NULL, 100),
(131, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/silver_s', NULL, NULL, NULL, NULL, 100),
(132, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/silver_m', NULL, NULL, NULL, NULL, 100),
(133, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/silver_l', NULL, NULL, NULL, NULL, 100),
(134, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Men\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Men\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/silver_xl', NULL, NULL, NULL, NULL, 100),
(200, NULL, '2017-09-16 16:40:54', NULL, '4.69000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 200, NULL),
(211, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/black_s', NULL, NULL, NULL, NULL, 200),
(212, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/black_m', NULL, NULL, NULL, NULL, 200),
(213, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/black_l', NULL, NULL, NULL, NULL, 200),
(214, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/black_xl', NULL, NULL, NULL, NULL, 200),
(221, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/red_s', NULL, NULL, NULL, NULL, 200),
(222, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/red_m', NULL, NULL, NULL, NULL, 200),
(223, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/red_l', NULL, NULL, NULL, NULL, 200),
(224, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/red_xl', NULL, NULL, NULL, NULL, 200),
(231, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/silver_s', NULL, NULL, NULL, NULL, 200),
(232, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/silver_m', NULL, NULL, NULL, NULL, 200),
(233, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '14.99000', NULL, NULL, 'Y', NULL, '/silver_l', NULL, NULL, NULL, NULL, 200),
(234, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Women\'s Habanero collection standard short sleeve screen-printed tee shirt in soft 30 singles cotton in regular fit.', 'Hawt Like a Habanero Shirt (Women\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/silver_xl', NULL, NULL, NULL, NULL, 200),
(300, NULL, '2017-09-16 16:40:54', NULL, '5.29000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 300, NULL),
(311, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_s', NULL, NULL, NULL, NULL, 300),
(312, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_m', NULL, NULL, NULL, NULL, 300),
(313, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_l', NULL, NULL, NULL, NULL, 300),
(314, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/black_xl', NULL, NULL, NULL, NULL, 300),
(321, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_s', NULL, NULL, NULL, NULL, 300),
(322, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_m', NULL, NULL, NULL, NULL, 300),
(323, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_l', NULL, NULL, NULL, NULL, 300),
(324, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/red_xl', NULL, NULL, NULL, NULL, 300),
(331, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_s', NULL, NULL, NULL, NULL, 300),
(332, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_m', NULL, NULL, NULL, NULL, 300),
(333, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_l', NULL, NULL, NULL, NULL, 300),
(334, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for men features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Men\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/silver_xl', NULL, NULL, NULL, NULL, 300),
(400, NULL, '2017-09-16 16:40:54', NULL, '5.49000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 400, NULL),
(411, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_s', NULL, NULL, NULL, NULL, 400),
(412, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_m', NULL, NULL, NULL, NULL, 400),
(413, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_l', NULL, NULL, NULL, NULL, 400),
(414, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/black_xl', NULL, NULL, NULL, NULL, 400),
(421, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_s', NULL, NULL, NULL, NULL, 400),
(422, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_m', NULL, NULL, NULL, NULL, 400),
(423, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_l', NULL, NULL, NULL, NULL, 400),
(424, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/red_xl', NULL, NULL, NULL, NULL, 400),
(431, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_s', NULL, NULL, NULL, NULL, 400),
(432, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_m', NULL, NULL, NULL, NULL, 400),
(433, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_l', NULL, NULL, NULL, NULL, 400),
(434, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'This hand-drawn logo shirt for women features a regular fit in three different colors', 'Heat Clinic Hand-Drawn (Women\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/silver_xl', NULL, NULL, NULL, NULL, 400),
(500, NULL, '2017-09-16 16:40:54', NULL, '4.89000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 500, NULL),
(511, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_s', NULL, NULL, NULL, NULL, 500),
(512, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_m', NULL, NULL, NULL, NULL, 500),
(513, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_l', NULL, NULL, NULL, NULL, 500),
(514, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/black_xl', NULL, NULL, NULL, NULL, 500),
(521, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_s', NULL, NULL, NULL, NULL, 500),
(522, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_m', NULL, NULL, NULL, NULL, 500),
(523, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_l', NULL, NULL, NULL, NULL, 500),
(524, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/red_xl', NULL, NULL, NULL, NULL, 500),
(531, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_s', NULL, NULL, NULL, NULL, 500),
(532, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_m', NULL, NULL, NULL, NULL, 500),
(533, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_l', NULL, NULL, NULL, NULL, 500),
(534, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Men\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/silver_xl', NULL, NULL, NULL, NULL, 500),
(600, NULL, '2017-09-16 16:40:54', NULL, '4.99000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 600, NULL),
(611, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_s', NULL, NULL, NULL, NULL, 600),
(612, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_m', NULL, NULL, NULL, NULL, 600),
(613, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/black_l', NULL, NULL, NULL, NULL, 600),
(614, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/black_xl', NULL, NULL, NULL, NULL, 600),
(621, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_s', NULL, NULL, NULL, NULL, 600),
(622, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_m', NULL, NULL, NULL, NULL, 600),
(623, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/red_l', NULL, NULL, NULL, NULL, 600),
(624, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/red_xl', NULL, NULL, NULL, NULL, 600),
(631, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_s', NULL, NULL, NULL, NULL, 600),
(632, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_m', NULL, NULL, NULL, NULL, 600),
(633, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '15.99000', NULL, NULL, 'Y', NULL, '/silver_l', NULL, NULL, NULL, NULL, 600),
(634, NULL, '2017-09-16 16:40:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, 'Don\'t you just love our mascot? Get your very own shirt today!', 'Heat Clinic Mascot (Women\'s)', NULL, '16.99000', NULL, NULL, 'Y', NULL, '/silver_xl', NULL, NULL, NULL, NULL, 600),
(10001, NULL, '2017-09-26 12:34:25', NULL, '60.00000', NULL, NULL, NULL, 'CENTIMETERS', NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, 'ALWAYS_AVAILABLE', 0, '<strong>Lorem Ipsum</strong>, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500\'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. Beşyüz yıl boyunca varlığını sürdürmekle kalmamış, aynı zamanda pek değişmeden elektronik dizgiye de sıçramıştır. 1960\'larda Lorem Ipsum pasajları da içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü yayıncılık yazılımları ile popüler olmuştur.', 'Kız Bebek', 10, '81.00000', '70.00000', NULL, 'Y', NULL, NULL, NULL, 'KILOGRAMS', NULL, 10000, NULL),
(10101, NULL, '2017-10-14 00:21:21', NULL, NULL, NULL, NULL, NULL, 'CENTIMETERS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CHECK_QUANTITY', 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'KILOGRAMS', NULL, NULL, 2),
(10118, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10000),
(10119, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CHECK_QUANTITY', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10000),
(10120, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10000),
(10121, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10000);

-- --------------------------------------------------------

--
-- Table structure for table `blc_sku_attribute`
--

CREATE TABLE `blc_sku_attribute` (
  `SKU_ATTR_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  `SKU_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sku_attribute`
--

INSERT INTO `blc_sku_attribute` (`SKU_ATTR_ID`, `NAME`, `VALUE`, `SKU_ID`) VALUES
(1, 'heatRange', '4', 1),
(2, 'heatRange', '1', 2),
(3, 'heatRange', '2', 3),
(4, 'heatRange', '2', 4),
(5, 'heatRange', '4', 5),
(6, 'heatRange', '4', 6),
(7, 'heatRange', '3', 7),
(8, 'heatRange', '4', 8),
(9, 'heatRange', '5', 9),
(10, 'heatRange', '5', 10),
(11, 'heatRange', '2', 11),
(12, 'heatRange', '1', 12),
(13, 'heatRange', '2', 13),
(14, 'heatRange', '2', 14),
(15, 'heatRange', '1', 15),
(16, 'heatRange', '3', 16),
(17, 'heatRange', '5', 17),
(18, 'heatRange', '3', 18),
(19, 'heatRange', '1', 19);

-- --------------------------------------------------------

--
-- Table structure for table `blc_sku_availability`
--

CREATE TABLE `blc_sku_availability` (
  `SKU_AVAILABILITY_ID` bigint(20) NOT NULL,
  `AVAILABILITY_DATE` datetime DEFAULT NULL,
  `AVAILABILITY_STATUS` varchar(255) DEFAULT NULL,
  `LOCATION_ID` bigint(20) DEFAULT NULL,
  `QTY_ON_HAND` int(11) DEFAULT NULL,
  `RESERVE_QTY` int(11) DEFAULT NULL,
  `SKU_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_sku_bundle_item`
--

CREATE TABLE `blc_sku_bundle_item` (
  `SKU_BUNDLE_ITEM_ID` bigint(20) NOT NULL,
  `ITEM_SALE_PRICE` decimal(19,5) DEFAULT NULL,
  `QUANTITY` int(11) NOT NULL,
  `SEQUENCE` decimal(10,6) DEFAULT NULL,
  `PRODUCT_BUNDLE_ID` bigint(20) NOT NULL,
  `SKU_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_sku_fee`
--

CREATE TABLE `blc_sku_fee` (
  `SKU_FEE_ID` bigint(20) NOT NULL,
  `AMOUNT` decimal(19,5) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `EXPRESSION` longtext,
  `FEE_TYPE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `TAXABLE` tinyint(1) DEFAULT NULL,
  `CURRENCY_CODE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_sku_fee_xref`
--

CREATE TABLE `blc_sku_fee_xref` (
  `SKU_FEE_ID` bigint(20) NOT NULL,
  `SKU_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_sku_fulfillment_excluded`
--

CREATE TABLE `blc_sku_fulfillment_excluded` (
  `SKU_ID` bigint(20) NOT NULL,
  `FULFILLMENT_OPTION_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_sku_fulfillment_flat_rates`
--

CREATE TABLE `blc_sku_fulfillment_flat_rates` (
  `SKU_ID` bigint(20) NOT NULL,
  `RATE` decimal(19,5) DEFAULT NULL,
  `FULFILLMENT_OPTION_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_sku_media_map`
--

CREATE TABLE `blc_sku_media_map` (
  `SKU_MEDIA_ID` bigint(20) NOT NULL,
  `MAP_KEY` varchar(255) NOT NULL,
  `MEDIA_ID` bigint(20) DEFAULT NULL,
  `BLC_SKU_SKU_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sku_media_map`
--

INSERT INTO `blc_sku_media_map` (`SKU_MEDIA_ID`, `MAP_KEY`, `MEDIA_ID`, `BLC_SKU_SKU_ID`) VALUES
(-100, 'primary', 101, 1),
(-99, 'primary', 201, 2),
(-98, 'primary', 301, 3),
(-97, 'primary', 401, 4),
(-96, 'primary', 501, 5),
(-95, 'primary', 601, 6),
(-94, 'primary', 701, 7),
(-93, 'primary', 801, 8),
(-92, 'primary', 901, 9),
(-91, 'primary', 1001, 10),
(-90, 'primary', 1101, 11),
(-89, 'primary', 1201, 12),
(-88, 'primary', 1301, 13),
(-87, 'primary', 1401, 14),
(-86, 'primary', 1501, 15),
(-85, 'primary', 1601, 16),
(-84, 'primary', 1701, 17),
(-83, 'primary', 1801, 18),
(-82, 'primary', 1901, 19),
(-81, 'alt1', 102, 1),
(-80, 'alt1', 202, 2),
(-79, 'alt1', 302, 3),
(-78, 'alt1', 402, 4),
(-77, 'alt1', 502, 5),
(-76, 'alt1', 602, 6),
(-75, 'alt1', 702, 7),
(-74, 'alt1', 802, 8),
(-73, 'alt1', 902, 9),
(-72, 'alt1', 1002, 10),
(-71, 'alt1', 1102, 11),
(-70, 'alt1', 1202, 12),
(-69, 'alt1', 1302, 13),
(-68, 'alt1', 1402, 14),
(-67, 'alt1', 1502, 15),
(-66, 'alt1', 1602, 16),
(-65, 'alt1', 1702, 17),
(-64, 'alt1', 1802, 18),
(-63, 'alt1', 1902, 19),
(-62, 'alt2', 203, 2),
(-61, 'alt3', 204, 2),
(-60, 'alt4', 205, 2),
(-59, 'alt5', 206, 2),
(-58, 'primary', 10001, 100),
(-57, 'primary', 20002, 200),
(-56, 'primary', 30003, 300),
(-55, 'primary', 40002, 400),
(-54, 'primary', 50003, 500),
(-53, 'primary', 60001, 600),
(-52, 'alt1', 10002, 100),
(-51, 'alt1', 20001, 200),
(-50, 'alt1', 30001, 300),
(-49, 'alt1', 40001, 400),
(-48, 'alt1', 50001, 500),
(-47, 'alt1', 60002, 600),
(-46, 'alt2', 10003, 100),
(-45, 'alt2', 20003, 200),
(-44, 'alt2', 30002, 300),
(-43, 'alt2', 40003, 400),
(-42, 'alt2', 50002, 500),
(-41, 'alt2', 60003, 600),
(2, 'primary', 100021, 10001),
(3, 'secondary1', 100022, 10001),
(4, 'secondary2', 100023, 10001),
(5, 'banner', 100024, 10001);

-- --------------------------------------------------------

--
-- Table structure for table `blc_sku_option_value_xref`
--

CREATE TABLE `blc_sku_option_value_xref` (
  `SKU_OPTION_VALUE_XREF_ID` bigint(20) NOT NULL,
  `PRODUCT_OPTION_VALUE_ID` bigint(20) NOT NULL,
  `SKU_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_sku_option_value_xref`
--

INSERT INTO `blc_sku_option_value_xref` (`SKU_OPTION_VALUE_XREF_ID`, `PRODUCT_OPTION_VALUE_ID`, `SKU_ID`) VALUES
(1, 1, 111),
(2, 11, 111),
(3, 1, 112),
(4, 12, 112),
(5, 1, 113),
(6, 13, 113),
(7, 1, 114),
(8, 14, 114),
(9, 2, 121),
(10, 11, 121),
(11, 2, 122),
(12, 12, 122),
(13, 2, 123),
(14, 13, 123),
(15, 2, 124),
(16, 14, 124),
(17, 3, 131),
(18, 11, 131),
(19, 3, 132),
(20, 12, 132),
(21, 3, 133),
(22, 13, 133),
(23, 3, 134),
(24, 14, 134),
(25, 1, 211),
(26, 11, 211),
(27, 1, 212),
(28, 12, 212),
(29, 1, 213),
(30, 13, 213),
(31, 1, 214),
(32, 14, 214),
(33, 2, 221),
(34, 11, 221),
(35, 2, 222),
(36, 12, 222),
(37, 2, 223),
(38, 13, 223),
(39, 2, 224),
(40, 14, 224),
(41, 3, 231),
(42, 11, 231),
(43, 3, 232),
(44, 12, 232),
(45, 3, 233),
(46, 13, 233),
(47, 3, 234),
(48, 14, 234),
(49, 1, 311),
(50, 11, 311),
(51, 1, 312),
(52, 12, 312),
(53, 1, 313),
(54, 13, 313),
(55, 1, 314),
(56, 14, 314),
(57, 2, 321),
(58, 11, 321),
(59, 2, 322),
(60, 12, 322),
(61, 2, 323),
(62, 13, 323),
(63, 2, 324),
(64, 14, 324),
(65, 3, 331),
(66, 11, 331),
(67, 3, 332),
(68, 12, 332),
(69, 3, 333),
(70, 13, 333),
(71, 3, 334),
(72, 14, 334),
(73, 1, 411),
(74, 11, 411),
(75, 1, 412),
(76, 12, 412),
(77, 1, 413),
(78, 13, 413),
(79, 1, 414),
(80, 14, 414),
(81, 2, 421),
(82, 11, 421),
(83, 2, 422),
(84, 12, 422),
(85, 2, 423),
(86, 13, 423),
(87, 2, 424),
(88, 14, 424),
(89, 3, 431),
(90, 11, 431),
(91, 3, 432),
(92, 12, 432),
(93, 3, 433),
(94, 13, 433),
(95, 3, 434),
(96, 14, 434),
(97, 1, 511),
(98, 11, 511),
(99, 1, 512),
(100, 12, 512),
(101, 1, 513),
(102, 13, 513),
(103, 1, 514),
(104, 14, 514),
(105, 2, 521),
(106, 11, 521),
(107, 2, 522),
(108, 12, 522),
(109, 2, 523),
(110, 13, 523),
(111, 2, 524),
(112, 14, 524),
(113, 3, 531),
(114, 11, 531),
(115, 3, 532),
(116, 12, 532),
(117, 3, 533),
(118, 13, 533),
(119, 3, 534),
(120, 14, 534),
(121, 1, 611),
(122, 11, 611),
(123, 1, 612),
(124, 12, 612),
(125, 1, 613),
(126, 13, 613),
(127, 1, 614),
(128, 14, 614),
(129, 2, 621),
(130, 11, 621),
(131, 2, 622),
(132, 12, 622),
(133, 2, 623),
(134, 13, 623),
(135, 2, 624),
(136, 14, 624),
(137, 3, 631),
(138, 11, 631),
(139, 3, 632),
(140, 12, 632),
(141, 3, 633),
(142, 13, 633),
(143, 3, 634),
(144, 14, 634),
(1100, 1000, 10101),
(1132, 11, 10118),
(1133, 12, 10119),
(1134, 13, 10120),
(1135, 14, 10121),
(1136, 12, 10119),
(1137, 12, 10119),
(1138, 12, 10119);

-- --------------------------------------------------------

--
-- Table structure for table `blc_state`
--

CREATE TABLE `blc_state` (
  `ABBREVIATION` varchar(255) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `COUNTRY` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_state`
--

INSERT INTO `blc_state` (`ABBREVIATION`, `NAME`, `COUNTRY`) VALUES
('AA', 'ARMED FORCES AMERICA', 'US'),
('AB', 'ALBERTA', 'CA'),
('AE', 'ARMED FORCES', 'US'),
('AK', 'ALASKA', 'US'),
('AL', 'ALABAMA', 'US'),
('AP', 'ARMED FORCES PACIFIC', 'US'),
('AR', 'ARKANSAS', 'US'),
('AS', 'AMERICAN SAMOA', 'US'),
('AZ', 'ARIZONA', 'US'),
('BC', 'BRITISH COLUMBIA', 'CA'),
('CA', 'CALIFORNIA', 'US'),
('CO', 'COLORADO', 'US'),
('CT', 'CONNECTICUT', 'US'),
('DC', 'DISTRICT OF COLUMBIA', 'US'),
('DE', 'DELAWARE', 'US'),
('FL', 'FLORIDA', 'US'),
('FM', 'FEDERATED STATES OF MICRONESIA', 'US'),
('GA', 'GEORGIA', 'US'),
('GU', 'GUAM', 'US'),
('HI', 'HAWAII', 'US'),
('IA', 'IOWA', 'US'),
('ID', 'IDAHO', 'US'),
('IL', 'ILLINOIS', 'US'),
('IN', 'INDIANA', 'US'),
('KS', 'KANSAS', 'US'),
('KY', 'KENTUCKY', 'US'),
('LA', 'LOUISIANA', 'US'),
('MA', 'MASSACHUSETTS', 'US'),
('MB', 'MANITOBA', 'CA'),
('MD', 'MARYLAND', 'US'),
('ME', 'MAINE', 'US'),
('MH', 'MARSHALL ISLANDS', 'US'),
('MI', 'MICHIGAN', 'US'),
('MN', 'MINNESOTA', 'US'),
('MO', 'MISSOURI', 'US'),
('MP', 'NORTHERN MARIANA ISLANDS', 'US'),
('MS', 'MISSISSIPPI', 'US'),
('MT', 'MONTANA', 'US'),
('NB', 'NEW BRUNSWICK', 'CA'),
('NC', 'NORTH CAROLINA', 'US'),
('ND', 'NORTH DAKOTA', 'US'),
('NE', 'NEBRASKA', 'US'),
('NH', 'NEW HAMPSHIRE', 'US'),
('NJ', 'NEW JERSEY', 'US'),
('NL', 'NEWFOUNDLAND', 'CA'),
('NM', 'NEW MEXICO', 'US'),
('NS', 'NOVA SCOTIA', 'CA'),
('NT', 'NORTHWEST TERRITORIES', 'CA'),
('NU', 'NUNAVUT', 'CA'),
('NV', 'NEVADA', 'US'),
('NY', 'NEW YORK', 'US'),
('OH', 'OHIO', 'US'),
('OK', 'OKLAHOMA', 'US'),
('ON', 'ONTARIO', 'CA'),
('OR', 'OREGON', 'US'),
('PA', 'PENNSYLVANIA', 'US'),
('PE', 'PRINCE EDWARD ISLAND', 'CA'),
('PR', 'PUERTO RICO', 'US'),
('PW', 'PALAU', 'US'),
('QC', 'QUEBEC', 'CA'),
('RI', 'RHODE ISLAND', 'US'),
('SC', 'SOUTH CAROLINA', 'US'),
('SD', 'SOUTH DAKOTA', 'US'),
('SK', 'SASKATCHEWAN', 'CA'),
('TN', 'TENNESSEE', 'US'),
('TX', 'TEXAS', 'US'),
('UT', 'UTAH', 'US'),
('VA', 'VIRGINIA', 'US'),
('VI', 'VIRGIN ISLANDS', 'US'),
('VT', 'VERMONT', 'US'),
('WA', 'WASHINGTON', 'US'),
('WI', 'WISCONSIN', 'US'),
('WV', 'WEST VIRGINIA', 'US'),
('WY', 'WYOMING', 'US'),
('YT', 'YUKON', 'CA');

-- --------------------------------------------------------

--
-- Table structure for table `blc_static_asset`
--

CREATE TABLE `blc_static_asset` (
  `STATIC_ASSET_ID` bigint(20) NOT NULL,
  `ALT_TEXT` varchar(255) DEFAULT NULL,
  `FILE_EXTENSION` varchar(255) DEFAULT NULL,
  `FILE_SIZE` bigint(20) DEFAULT NULL,
  `FULL_URL` varchar(255) NOT NULL,
  `MIME_TYPE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  `STORAGE_TYPE` varchar(255) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_static_asset`
--

INSERT INTO `blc_static_asset` (`STATIC_ASSET_ID`, `ALT_TEXT`, `FILE_EXTENSION`, `FILE_SIZE`, `FULL_URL`, `MIME_TYPE`, `NAME`, `STORAGE_TYPE`, `TITLE`, `CREATED_BY`, `DATE_CREATED`, `DATE_UPDATED`, `UPDATED_BY`) VALUES
(-2005, NULL, NULL, NULL, '/img/banners/shirts-speciale.jpg', 'image/jpg', 'Shirts Speciale', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(-2004, NULL, NULL, NULL, '/img/banners/shirt-special.jpg', 'image/jpg', 'Shirt Special', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(-2003, NULL, NULL, NULL, '/img/banners/promocion-camisas.jpg', 'image/jpg', 'Promocion Camisas', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(-2002, NULL, NULL, NULL, '/img/banners/member-special.jpg', 'image/jpg', 'Member Special', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(-2001, NULL, NULL, NULL, '/img/banners/buy-two-get-one.jpg', 'image/jpg', 'Buy Two Get One', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(-2000, NULL, NULL, NULL, '/img/banners/buy-one-get-one-home-banner.jpg', 'image/jpg', 'Buy One Get One', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(101, NULL, NULL, NULL, '/img/sauces/Sudden-Death-Sauce-Bottle.jpg', 'image/jpg', 'Sudden Death Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(102, NULL, NULL, NULL, '/img/sauces/Sudden-Death-Sauce-Close.jpg', 'image/jpg', 'Sudden Death Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(201, NULL, NULL, NULL, '/img/sauces/Sweet-Death-Sauce-Bottle.jpg', 'image/jpg', 'Sweet Death Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(202, NULL, NULL, NULL, '/img/sauces/Sweet-Death-Sauce-Close.jpg', 'image/jpg', 'Sweet Death Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(203, NULL, NULL, NULL, '/img/sauces/Sweet-Death-Sauce-Skull.jpg', 'image/jpg', 'Sweet Death Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(204, NULL, NULL, NULL, '/img/sauces/Sweet-Death-Sauce-Tile.jpg', 'image/jpg', 'Sweet Death Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(205, NULL, NULL, NULL, '/img/sauces/Sweet-Death-Sauce-Grass.jpg', 'image/jpg', 'Sweet Death Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(206, NULL, NULL, NULL, '/img/sauces/Sweet-Death-Sauce-Logo.jpg', 'image/jpg', 'Sweet Death Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(301, NULL, NULL, NULL, '/img/sauces/Hoppin-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Hoppin Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(302, NULL, NULL, NULL, '/img/sauces/Hoppin-Hot-Sauce-Close.jpg', 'image/jpg', 'Hoppin Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(401, NULL, NULL, NULL, '/img/sauces/Day-of-the-Dead-Chipotle-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Day of the Dead Chipotle Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(402, NULL, NULL, NULL, '/img/sauces/Day-of-the-Dead-Chipotle-Hot-Sauce-Close.jpg', 'image/jpg', 'Day of the Dead Chipotle Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(501, NULL, NULL, NULL, '/img/sauces/Day-of-the-Dead-Habanero-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Day of the Dead Habanero Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(502, NULL, NULL, NULL, '/img/sauces/Day-of-the-Dead-Habanero-Hot-Sauce-Close.jpg', 'image/jpg', 'Day of the Dead Habanero Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(601, NULL, NULL, NULL, '/img/sauces/Day-of-the-Dead-Scotch-Bonnet-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Day of the Dead Scotch Bonnet Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(602, NULL, NULL, NULL, '/img/sauces/Day-of-the-Dead-Scotch-Bonnet-Hot-Sauce-Close.jpg', 'image/jpg', 'Day of the Dead Scotch Bonnet Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(701, NULL, NULL, NULL, '/img/sauces/Green-Ghost-Bottle.jpg', 'image/jpg', 'Green Ghost Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(702, NULL, NULL, NULL, '/img/sauces/Green-Ghost-Close.jpg', 'image/jpg', 'Green Ghost Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(801, NULL, NULL, NULL, '/img/sauces/Blazin-Saddle-XXX-Hot-Habanero-Pepper-Sauce-Bottle.jpg', 'image/jpg', 'Blazin Saddle XXX Hot Habanero Pepper Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(802, NULL, NULL, NULL, '/img/sauces/Blazin-Saddle-XXX-Hot-Habanero-Pepper-Sauce-Close.jpg', 'image/jpg', 'Blazin Saddle XXX Hot Habanero Pepper Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(901, NULL, NULL, NULL, '/img/sauces/Armageddon-The-Hot-Sauce-To-End-All-Bottle.jpg', 'image/jpg', 'Armageddon The Hot Sauce To End All Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(902, NULL, NULL, NULL, '/img/sauces/Armageddon-The-Hot-Sauce-To-End-All-Close.jpg', 'image/jpg', 'Armageddon The Hot Sauce To End All Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1001, NULL, NULL, NULL, '/img/sauces/Dr.-Chilemeisters-Insane-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Dr. Chilemeisters Insane Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1002, NULL, NULL, NULL, '/img/sauces/Dr.-Chilemeisters-Insane-Hot-Sauce-Close.jpg', 'image/jpg', 'Dr. Chilemeisters Insane Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1101, NULL, NULL, NULL, '/img/sauces/Bull-Snort-Cowboy-Cayenne-Pepper-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Bull Snort Cowboy Cayenne Pepper Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1102, NULL, NULL, NULL, '/img/sauces/Bull-Snort-Cowboy-Cayenne-Pepper-Hot-Sauce-Close.jpg', 'image/jpg', 'Bull Snort Cowboy Cayenne Pepper Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1201, NULL, NULL, NULL, '/img/sauces/Cafe-Louisiane-Sweet-Cajun-Blackening-Sauce-Bottle.jpg', 'image/jpg', 'Cafe Louisiane Sweet Cajun Blackening Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1202, NULL, NULL, NULL, '/img/sauces/Cafe-Louisiane-Sweet-Cajun-Blackening-Sauce-Close.jpg', 'image/jpg', 'Cafe Louisiane Sweet Cajun Blackening Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1301, NULL, NULL, NULL, '/img/sauces/Bull-Snort-Smokin-Toncils-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Bull Snort Smokin Toncils Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1302, NULL, NULL, NULL, '/img/sauces/Bull-Snort-Smokin-Toncils-Hot-Sauce-Close.jpg', 'image/jpg', 'Bull Snort Smokin Toncils Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1401, NULL, NULL, NULL, '/img/sauces/Cool-Cayenne-Pepper-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Cool Cayenne Pepper Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1402, NULL, NULL, NULL, '/img/sauces/Cool-Cayenne-Pepper-Hot-Sauce-Close.jpg', 'image/jpg', 'Cool Cayenne Pepper Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1501, NULL, NULL, NULL, '/img/sauces/Roasted-Garlic-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Roasted Garlic Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1502, NULL, NULL, NULL, '/img/sauces/Roasted-Garlic-Hot-Sauce-Close.jpg', 'image/jpg', 'Roasted Garlic Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1601, NULL, NULL, NULL, '/img/sauces/Scotch-Bonnet-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Scotch Bonnet Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1602, NULL, NULL, NULL, '/img/sauces/Scotch-Bonnet-Hot-Sauce-Close.jpg', 'image/jpg', 'Scotch Bonnet Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1701, NULL, NULL, NULL, '/img/sauces/Insanity-Sauce-Bottle.jpg', 'image/jpg', 'Insanity Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1702, NULL, NULL, NULL, '/img/sauces/Insanity-Sauce-Close.jpg', 'image/jpg', 'Insanity Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1801, NULL, NULL, NULL, '/img/sauces/Hurtin-Jalepeno-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Hurtin Jalepeno Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1802, NULL, NULL, NULL, '/img/sauces/Hurtin-Jalepeno-Hot-Sauce-Close.jpg', 'image/jpg', 'Hurtin Jalepeno Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1901, NULL, NULL, NULL, '/img/sauces/Roasted-Red-Pepper-and-Chipotle-Hot-Sauce-Bottle.jpg', 'image/jpg', 'Roasted Red Pepper and Chipotle Hot Sauce Bottle', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(1902, NULL, NULL, NULL, '/img/sauces/Roasted-Red-Pepper-and-Chipotle-Hot-Sauce-Close.jpg', 'image/jpg', 'Roasted Red Pepper and Chipotle Hot Sauce Close-up', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(10001, NULL, NULL, NULL, '/img/merch/habanero_mens_black.jpg', 'image/jpg', 'Hawt Like a Habanero Men\'s Black', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(10002, NULL, NULL, NULL, '/img/merch/habanero_mens_red.jpg', 'image/jpg', 'Hawt Like a Habanero Men\'s Red', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(10003, NULL, NULL, NULL, '/img/merch/habanero_mens_silver.jpg', 'image/jpg', 'Hawt Like a Habanero Men\'s Silver', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(20001, NULL, NULL, NULL, '/img/merch/habanero_womens_black.jpg', 'image/jpg', 'Hawt Like a Habanero Women\'s Black', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(20002, NULL, NULL, NULL, '/img/merch/habanero_womens_red.jpg', 'image/jpg', 'Hawt Like a Habanero Women\'s Red', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(20003, NULL, NULL, NULL, '/img/merch/habanero_womens_silver.jpg', 'image/jpg', 'Hawt Like a Habanero Women\'s Silver', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(30001, NULL, NULL, NULL, '/img/merch/heat_clinic_handdrawn_mens_black.jpg', 'image/jpg', 'Heat Clinic Hand-Drawn Men\'s Black', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(30002, NULL, NULL, NULL, '/img/merch/heat_clinic_handdrawn_mens_red.jpg', 'image/jpg', 'Heat Clinic Hand-Drawn Men\'s Red', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(30003, NULL, NULL, NULL, '/img/merch/heat_clinic_handdrawn_mens_silver.jpg', 'image/jpg', 'Heat Clinic Hand-Drawn Men\'s Silver', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(40001, NULL, NULL, NULL, '/img/merch/heat_clinic_handdrawn_womens_black.jpg', 'image/jpg', 'Heat Clinic Hand-Drawn Women\'s Black', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(40002, NULL, NULL, NULL, '/img/merch/heat_clinic_handdrawn_womens_red.jpg', 'image/jpg', 'Heat Clinic Hand-Drawn Women\'s Red', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(40003, NULL, NULL, NULL, '/img/merch/heat_clinic_handdrawn_womens_silver.jpg', 'image/jpg', 'Heat Clinic Hand-Drawn Women\'s Silver', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(50001, NULL, NULL, NULL, '/img/merch/heat_clinic_mascot_mens_black.jpg', 'image/jpg', 'Heat Clinic Mascot Men\'s Black', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(50002, NULL, NULL, NULL, '/img/merch/heat_clinic_mascot_mens_red.jpg', 'image/jpg', 'Heat Clinic Mascot Men\'s Red', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(50003, NULL, NULL, NULL, '/img/merch/heat_clinic_mascot_mens_silver.jpg', 'image/jpg', 'Heat Clinic Mascot Men\'s Silver', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(60001, NULL, NULL, NULL, '/img/merch/heat_clinic_mascot_womens_black.jpg', 'image/jpg', 'Heat Clinic Mascot Women\'s Black', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(60002, NULL, NULL, NULL, '/img/merch/heat_clinic_mascot_womens_red.jpg', 'image/jpg', 'Heat Clinic Mascot Women\'s Red', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(60003, NULL, NULL, NULL, '/img/merch/heat_clinic_mascot_womens_silver.jpg', 'image/jpg', 'Heat Clinic Mascot Women\'s Silver', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100000, NULL, 'jpg', 27883, '/category/2002/Untitled-1.jpg', 'image/jpeg', 'Untitled-1.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100001, NULL, 'jpeg', 82819, '/product/kiz-oyuncaklari-canim-bebegim-1.jpeg', 'image/jpeg', 'kiz-oyuncaklari-canim-bebegim-1.jpeg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100002, NULL, 'jpeg', 82819, '/product/kiz-oyuncaklari-canim-bebegim-1-1.jpeg', 'image/jpeg', 'kiz-oyuncaklari-canim-bebegim-1.jpeg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100003, NULL, 'jpeg', 82819, '/product/10000/kiz-oyuncaklari-canim-bebegim-1.jpeg', 'image/jpeg', 'kiz-oyuncaklari-canim-bebegim-1.jpeg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100004, NULL, 'jpg', 76785, '/product/10000/vardem-kutulu-4-asorti-4pcs-mini-evcilik-seti-d_1.jpg', 'image/jpeg', 'vardem-kutulu-4-asorti-4pcs-mini-evcilik-seti-d_1.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100005, NULL, 'jpg', 56603, '/product/10000/63721_3.jpg', 'image/jpeg', '63721_3.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100006, NULL, 'jpg', 59031, '/product/10000/media_banner.jpg', 'image/jpeg', 'media_banner.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100056, NULL, 'jpg', 85626, '/fashion1.jpg', 'image/jpeg', 'fashion1.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100057, NULL, 'jpg', 195883, '/Fashion22.jpg', 'image/jpeg', 'Fashion22.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100058, NULL, 'jpg', 65509, '/fashion3.jpg', 'image/jpeg', 'fashion3.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100059, NULL, 'jpg', 41156, '/fashion4.jpg', 'image/jpeg', 'fashion4.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100060, NULL, 'jpg', 65509, '/structured-content/1102/qweqwewewqeq.jpg', 'image/jpeg', 'qweqwewewqeq.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100100, NULL, 'jpg', 950859, '/1.jpg', 'image/jpeg', '1.jpg', 'FILESYSTEM', 'Slider 1', NULL, NULL, NULL, NULL),
(100101, NULL, 'jpg', 752676, '/2.jpg', 'image/jpeg', '2.jpg', 'FILESYSTEM', 'Slider 2', NULL, NULL, NULL, NULL),
(100102, NULL, 'jpg', 761989, '/3.jpg', 'image/jpeg', '3.jpg', 'FILESYSTEM', 'Slider 3', NULL, NULL, NULL, NULL),
(100151, NULL, 'jpg', 147698, '/category/10055/woman-category-banner-123.jpg', 'image/jpeg', 'woman-category-banner-123.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100152, NULL, 'jpg', 156624, '/category/10058/children-category-banner-23.jpg', 'image/jpeg', 'children-category-banner-23.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100200, NULL, 'jpg', 474914, '/structured-content/1300/345345345435.jpg', 'image/jpeg', '345345345435.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100201, NULL, 'jpg', 474914, '/structured-content/1301/slider banner.jpg', 'image/jpeg', 'slider banner.jpg', 'FILESYSTEM', NULL, NULL, NULL, NULL, NULL),
(100250, NULL, 'jpg', 119535, '/category/10050/man-category-banner-asddsa.jpg', 'image/jpeg', 'man-category-banner-asddsa.jpg', 'FILESYSTEM', NULL, -1, '2018-02-28 15:57:04', '2018-02-28 15:57:04', -1),
(100251, NULL, 'jpg', 147698, '/category/10055/woman-category-banner-123-1.jpg', 'image/jpeg', 'woman-category-banner-123.jpg', 'FILESYSTEM', NULL, -1, '2018-02-28 15:57:27', '2018-02-28 15:57:27', -1),
(100252, NULL, 'jpg', 156624, '/category/10058/children-category-banner-23-1.jpg', 'image/jpeg', 'children-category-banner-23.jpg', 'FILESYSTEM', NULL, -1, '2018-02-28 15:57:52', '2018-02-28 15:57:52', -1);

-- --------------------------------------------------------

--
-- Table structure for table `blc_static_asset_desc`
--

CREATE TABLE `blc_static_asset_desc` (
  `STATIC_ASSET_DESC_ID` bigint(20) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LONG_DESCRIPTION` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DATE_CREATED` datetime DEFAULT NULL,
  `DATE_UPDATED` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_static_asset_strg`
--

CREATE TABLE `blc_static_asset_strg` (
  `STATIC_ASSET_STRG_ID` bigint(20) NOT NULL,
  `FILE_DATA` longblob,
  `STATIC_ASSET_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_store`
--

CREATE TABLE `blc_store` (
  `STORE_ID` bigint(20) NOT NULL,
  `ARCHIVED` char(1) DEFAULT NULL,
  `LATITUDE` double DEFAULT NULL,
  `LONGITUDE` double DEFAULT NULL,
  `STORE_NAME` varchar(255) NOT NULL,
  `STORE_OPEN` tinyint(1) DEFAULT NULL,
  `STORE_HOURS` varchar(255) DEFAULT NULL,
  `STORE_NUMBER` varchar(255) DEFAULT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_system_property`
--

CREATE TABLE `blc_system_property` (
  `BLC_SYSTEM_PROPERTY_ID` bigint(20) NOT NULL,
  `FRIENDLY_GROUP` varchar(255) DEFAULT NULL,
  `FRIENDLY_NAME` varchar(255) DEFAULT NULL,
  `FRIENDLY_TAB` varchar(255) DEFAULT NULL,
  `PROPERTY_NAME` varchar(255) NOT NULL,
  `OVERRIDE_GENERATED_PROP_NAME` tinyint(1) DEFAULT NULL,
  `PROPERTY_TYPE` varchar(255) DEFAULT NULL,
  `PROPERTY_VALUE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_tar_crit_offer_xref`
--

CREATE TABLE `blc_tar_crit_offer_xref` (
  `OFFER_TAR_CRIT_ID` bigint(20) NOT NULL,
  `OFFER_ID` bigint(20) NOT NULL,
  `OFFER_ITEM_CRITERIA_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_tar_crit_offer_xref`
--

INSERT INTO `blc_tar_crit_offer_xref` (`OFFER_TAR_CRIT_ID`, `OFFER_ID`, `OFFER_ITEM_CRITERIA_ID`) VALUES
(-100, 1, 1),
(51, 1101, 1100),
(105, 1150, 1154);

-- --------------------------------------------------------

--
-- Table structure for table `blc_tax_detail`
--

CREATE TABLE `blc_tax_detail` (
  `TAX_DETAIL_ID` bigint(20) NOT NULL,
  `AMOUNT` decimal(19,5) DEFAULT NULL,
  `TAX_COUNTRY` varchar(255) DEFAULT NULL,
  `JURISDICTION_NAME` varchar(255) DEFAULT NULL,
  `RATE` decimal(19,5) DEFAULT NULL,
  `TAX_REGION` varchar(255) DEFAULT NULL,
  `TAX_NAME` varchar(255) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `CURRENCY_CODE` varchar(255) DEFAULT NULL,
  `MODULE_CONFIG_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_translation`
--

CREATE TABLE `blc_translation` (
  `TRANSLATION_ID` bigint(20) NOT NULL,
  `ENTITY_ID` varchar(255) DEFAULT NULL,
  `ENTITY_TYPE` varchar(255) DEFAULT NULL,
  `FIELD_NAME` varchar(255) DEFAULT NULL,
  `LOCALE_CODE` varchar(255) DEFAULT NULL,
  `TRANSLATED_VALUE` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_translation`
--

INSERT INTO `blc_translation` (`TRANSLATION_ID`, `ENTITY_ID`, `ENTITY_TYPE`, `FIELD_NAME`, `LOCALE_CODE`, `TRANSLATED_VALUE`) VALUES
(-1003, '3', 'Page', 'pageTemplate|body', 'es', '<h2 style=\"text-align:center;\">Este es un ejemplo de una p&aacute;gina de contenido-manejado.</h2>'),
(-1002, '2', 'Page', 'pageTemplate|body', 'es', '<h2 style=\"text-align:center;\">Este es un ejemplo de una p&aacute;gina de contenido-manejado.</h2><h4 style=\"text-align:center;\">Haga <a href=\"http://www.broadleafcommerce.com/features/content\">click aqu&iacute;</a> para mas informaci&oacute;n.</h4>'),
(-1001, '1', 'Page', 'pageTemplate|body', 'es', 'prueba de contenido'),
(-1000, '1', 'Page', 'pageTemplate|title', 'es', 'Espa&ntilde;ol G&eacute;nerico'),
(-398, '6', 'MenuItem', 'label', 'fr', 'FAQ'),
(-397, '5', 'MenuItem', 'label', 'fr', 'Nouveau Hot Sauce?'),
(-396, '3', 'FulfillmentOption', 'longDescription', 'fr', '1 - 2 Journ&eacute;es'),
(-395, '3', 'FulfillmentOption', 'name', 'fr', 'Express'),
(-394, '2', 'FulfillmentOption', 'longDescription', 'fr', '3 - 5 Journ&eacute;es'),
(-393, '2', 'FulfillmentOption', 'name', 'fr', 'Priorit&eacute;'),
(-392, '1', 'FulfillmentOption', 'longDescription', 'fr', '5 - 7 Journ&eacute;es'),
(-391, '1', 'FulfillmentOption', 'name', 'fr', 'Norme'),
(-382, '3', 'SearchFacet', 'label', 'fr', 'Prix'),
(-381, '2', 'SearchFacet', 'label', 'fr', 'Degr&eacute; de chaleur'),
(-380, '1', 'SearchFacet', 'label', 'fr', 'Fabricant'),
(-300, '600', 'Sku', 'longDescription', 'fr', 'Avez-vous pas juste notre mascotte? Obtenez votre chemise propre aujourd\'hui!'),
(-299, '600', 'Sku', 'name', 'fr', 'Mascot Clinique chaleur (Femmes)'),
(-298, '500', 'Sku', 'longDescription', 'fr', 'Avez-vous pas juste notre mascotte? Obtenez votre chemise propre aujourd\'hui!'),
(-297, '500', 'Sku', 'name', 'fr', 'Mascot Clinique chaleur (Hommes)'),
(-296, '400', 'Sku', 'longDescription', 'fr', 'Ce t-shirt logo dessin&eacute; &agrave; la main pour les femmes dispose d\'une coupe r&eacute;guli&egrave;re en trois couleurs diff&eacute;rentes.'),
(-295, '400', 'Sku', 'name', 'fr', 'Clinique de chaleur tir&eacute; par la main (Femmes)'),
(-294, '300', 'Sku', 'longDescription', 'fr', 'Ce t-shirt logo dessin&eacute; &agrave; la main pour les hommes dispose d\'une coupe r&eacute;guli&egrave;re en trois couleurs diff&eacute;rentes.'),
(-293, '300', 'Sku', 'name', 'fr', 'Clinique de chaleur tir&eacute; par la main (Hommes)'),
(-292, '200', 'Sku', 'longDescription', 'fr', 'Collecte de femmes Habanero standards chemise &agrave; manches courtes shirt s&eacute;rigraphi&eacute; &agrave; 30 coton doux singles en coupe regular.'),
(-291, '200', 'Sku', 'name', 'fr', 'Hawt comme une chemise Habanero (Femmes)'),
(-290, '100', 'Sku', 'longDescription', 'fr', 'Collecte Hommes Habanero standards chemise &agrave; manches courtes t s&eacute;rigraphi&eacute;es en 30 coton doux singles en coupe regular.'),
(-289, '100', 'Sku', 'name', 'fr', 'Hawt comme une chemise Habanero (Hommes)'),
(-288, '19', 'Sku', 'longDescription', 'fr', 'Cette sauce tire sa saveur des poivrons grand &acirc;ge et le vinaigre de canne. Il permettra d\'am&eacute;liorer la saveur de la plupart de n\'importe quel repas.'),
(-287, '19', 'Sku', 'name', 'fr', 'Sauce chaudes Chipotle'),
(-286, '18', 'Sku', 'longDescription', 'fr', 'Cette sauce tire sa saveur des poivrons grand &acirc;ge et le vinaigre de canne. Il permettra d\'am&eacute;liorer la saveur de la plupart de n\'importe quel repas.'),
(-285, '18', 'Sku', 'name', 'fr', 'Sauces chaudes Jalapeno'),
(-284, '17', 'Sku', 'longDescription', 'fr', 'Cette sauce tire sa saveur des poivrons grand &acirc;ge et le vinaigre de canne. Il permettra d\'am&eacute;liorer la saveur de la plupart de n\'importe quel repas.'),
(-283, '17', 'Sku', 'name', 'fr', 'Sauce Scotch Bonnet chaud'),
(-282, '16', 'Sku', 'longDescription', 'fr', 'Cette sauce tire sa saveur des poivrons grand &acirc;ge et le vinaigre de canne. Il permettra d\'am&eacute;liorer la saveur de la plupart de n\'importe quel repas.'),
(-281, '16', 'Sku', 'name', 'fr', 'Sauce Scotch Bonnet chaud'),
(-280, '15', 'Sku', 'longDescription', 'fr', 'Cette sauce tire sa saveur des poivrons grand &acirc;ge et le vinaigre de canne. Il permettra d\'am&eacute;liorer la saveur de la plupart de n\'importe quel repas.'),
(-279, '15', 'Sku', 'name', 'fr', 'Sauce &agrave; l\'ail rôti chaud'),
(-278, '14', 'Sku', 'longDescription', 'fr', 'Cette sauce tire sa saveur des poivrons grand &acirc;ge et le vinaigre de canne. Il permettra d\'am&eacute;liorer la saveur de la plupart de n\'importe quel repas.'),
(-277, '14', 'Sku', 'name', 'fr', 'Frais Poivre de Cayenne Hot Sauce'),
(-276, '13', 'Sku', 'longDescription', 'fr', 'Todo es m&aacute;s grande en Texas, incluso lo picante de la Salsa de Snortin Bull! Tout est plus grand au Texas, m&ecirc;me la brûlure de Hot Sauce une Snortin Bull! douche sur le Texas Steak taille qu\'ils appellent le 96er Ole ou vos l&eacute;gumes Jane avion. Si vous &ecirc;tes un fan sur faire de la sauce barbecue &agrave; partir de z&eacute;ro comme je suis, vous pouvez utiliser la sauce Bull amygdales Snort Smokin \'Hot tant qu\'additif. Red hot habaneros et piments donner &agrave; cette tingler amygdales sa saveur c&eacute;l&egrave;bre et rouge de chaleur chaud. Bull Snort Smokin \'Hot amygdales Sauce\'ll avoir vos entrailles buckin »avec une goutte d\'eau.'),
(-275, '13', 'Sku', 'name', 'fr', 'Bull Snort Smokin \'Hot Sauce Toncils'),
(-274, '12', 'Sku', 'longDescription', 'fr', 'L\'une des sauces les plus insolites que nous vendons. L\'original &eacute;tait un vieux style sauce cajun et c\'est ça le noircissement &agrave; jour de version. C\'est gentil, mais vous obtenez un grand succ&egrave;s de cannelle et de clou de girofle avec un coup de chaleur agr&eacute;able de Cayenne. Utilisez-le sur tous les aliments &agrave; donner cette ambiance cajun.'),
(-273, '12', 'Sku', 'name', 'fr', 'Caf&eacute; Cajun Louisiane Douce Sauce Blackening'),
(-272, '11', 'Sku', 'longDescription', 'fr', 'Been there, encord&eacute;s cela. Hotter than jument buckin \'en chaleur! Saupoudrez de plats de viande, de fruits de mer et l&eacute;gumes. Utilisation comme additif dans une sauce barbecue ou tout aliment qui a besoin d\'une saveur &eacute;pic&eacute;e. Commencez avec quelques gouttes et travailler jusqu\'&agrave; la saveur d&eacute;sir&eacute;e.'),
(-271, '11', 'Sku', 'name', 'fr', 'Bull Snort Cowboy poivre de Cayenne Hot Sauce'),
(-270, '10', 'Sku', 'longDescription', 'fr', 'Voici la prescription pour ceux qui aiment la chaleur intol&eacute;rable. Dr Chilemeister potion de malades et mal mortel doit &ecirc;tre utilis&eacute; avec prudence. La douleur peut devenir une d&eacute;pendance!'),
(-269, '10', 'Sku', 'name', 'fr', 'Dr Chilemeister Sauce Hot Insane'),
(-268, '9', 'Sku', 'longDescription', 'fr', 'Tout l\'enfer se d&eacute;chaîne, le feu et le soufre pleuvoir? se pr&eacute;parer &agrave; rencontrer votre machine?'),
(-267, '9', 'Sku', 'name', 'fr', 'Armageddon Le Hot Sauce To End All'),
(-266, '8', 'Sku', 'longDescription', 'fr', 'Vous misez vos bottes, cette sauce chaude valu son nom de gens qui appr&eacute;cient une sauce chaude exceptionnel. Ce que vous trouverez ici est une saveur piquante vraiment original, pas un piquant irr&eacute;sistible que l\'on retrouve dans les sauces au poivre Tabasco ordinaires - m&ecirc;me si le piment utilis&eacute; dans ce produit a &eacute;t&eacute; test&eacute; &agrave; 285.000 unit&eacute;s Scoville. Alors, en selle pour une balade inoubliable. Pour vous assurer que nous vous avons apport&eacute; la plus belle sauce au poivre de Habanero, nous sommes all&eacute;s aux contreforts des montagnes mayas au Belize, en Am&eacute;rique centrale. Ce produit est pr&eacute;par&eacute; enti&egrave;rement &agrave; la main en utilisant uniquement des l&eacute;gumes frais et de tous les ingr&eacute;dients naturels.'),
(-265, '8', 'Sku', 'name', 'fr', 'Blazin \'Selle XXX Hot Habanero sauce au poivre'),
(-264, '7', 'Sku', 'longDescription', 'fr', 'Fabriqu&eacute; avec Naga Bhut Jolokia, plus chaud poivre dans le monde.'),
(-263, '7', 'Sku', 'name', 'fr', 'Green Ghost'),
(-262, '6', 'Sku', 'longDescription', 'fr', 'Souvent confondu avec le Habanero, le Scotch Bonnet a une pointe profond&eacute;ment invers&eacute;e par rapport &agrave; l\'extr&eacute;mit&eacute; pointue de l\'Habanero. Allant dans de nombreuses couleurs allant du vert au jaune-orange, le Scotch Bonnet est un aliment de base dans les Antilles et sauces au poivre de style Barbade.'),
(-261, '6', 'Sku', 'name', 'fr', 'Jour de la sauce Scotch Bonnet Hot Morte'),
(-260, '5', 'Sku', 'longDescription', 'fr', 'Si vous voulez chaud, c\'est le piment de choisir. Originaire de la Caraïbe, du Yucatan et du Nord Côte de l\'Am&eacute;rique du Sud, le Habanero se pr&eacute;sente dans une vari&eacute;t&eacute; de couleurs allant du vert p&acirc;le au rouge vif. La chaleur gras Habanero, la saveur et l\'arôme unique, en a fait le favori des amateurs de chili.'),
(-259, '5', 'Sku', 'name', 'fr', 'Jour de la sauce Habanero Hot Morte'),
(-258, '4', 'Sku', 'longDescription', 'fr', 'Lorsque tout le poivre est s&eacute;ch&eacute; et fum&eacute;, il est consid&eacute;r&eacute; comme un Chipotle. Normalement, avec un aspect froiss&eacute;, drak brun, le chipotle fum&eacute; offre une saveur douce qui est g&eacute;n&eacute;ralement utilis&eacute; pour ajouter un smokey, saveur rôtie aux salsas, les ragoûts et marinades.'),
(-257, '4', 'Sku', 'name', 'fr', 'Jour de la sauce chaude Morte Chipotle'),
(-256, '3', 'Sku', 'longDescription', 'fr', 'Tangy, venu de Cayenne poivron flux avec l\'ail, l\'oignon p&acirc;te de tomate, et un soupçon de sucre de canne pour en faire une sauce onctueuse avec une morsure. Magnifique sur les œufs, la volaille, le porc ou le poisson, cette sauce marie pour faire des marinades et des soupes riches.'),
(-255, '3', 'Sku', 'name', 'fr', 'Hot Sauce Hoppin'),
(-254, '2', 'Sku', 'longDescription', 'fr', 'Le parfait topper pour le poulet, le poisson, des hamburgers ou une pizza. Un grand m&eacute;lange de Habanero, mangue, fruits de la passion et de plus faire cette sauce Mort d\'un festin incroyable tropicale'),
(-253, '2', 'Sku', 'name', 'fr', 'Sauce Sweet Death'),
(-252, '1', 'Sku', 'longDescription', 'fr', 'Comme mes Chilipals sais, je suis pas du genre &agrave; &ecirc;tre satisfaite. Par cons&eacute;quent, la cr&eacute;ation de la mort subite. Lorsque vous avez besoin d\'aller au-del&agrave; ... Mort subite livrera!'),
(-251, '1', 'Sku', 'name', 'fr', 'Sauce mort subite'),
(-247, '14', 'ProdOptionVal', 'attributeValue', 'fr', 'XG'),
(-246, '13', 'ProdOptionVal', 'attributeValue', 'fr', 'G'),
(-245, '12', 'ProdOptionVal', 'attributeValue', 'fr', 'M'),
(-244, '11', 'ProdOptionVal', 'attributeValue', 'fr', 'P'),
(-243, '3', 'ProdOptionVal', 'attributeValue', 'fr', 'Argent'),
(-242, '2', 'ProdOptionVal', 'attributeValue', 'fr', 'Rouge'),
(-241, '1', 'ProdOptionVal', 'attributeValue', 'fr', 'Noir'),
(-232, '2', 'ProdOption', 'label', 'fr', 'Shirt Taille'),
(-231, '1', 'ProdOption', 'label', 'fr', 'Shirt Couleur'),
(-215, '2005', 'Category', 'description', 'fr', 'Cartes Cadeaux'),
(-214, '2004', 'Category', 'description', 'fr', 'D&eacute;gagement'),
(-213, '2003', 'Category', 'description', 'fr', 'Marchandisfr'),
(-212, '2002', 'Category', 'description', 'fr', 'Sauces chaudfr'),
(-211, '2001', 'Category', 'description', 'fr', 'Page d\'accueil'),
(-205, '2005', 'Category', 'name', 'fr', 'Cartes Cadeaux'),
(-204, '2004', 'Category', 'name', 'fr', 'D&eacute;gagement'),
(-203, '2003', 'Category', 'name', 'fr', 'Marchandisfr'),
(-202, '2002', 'Category', 'name', 'fr', 'Sauces chaudfr'),
(-201, '2001', 'Category', 'name', 'fr', 'Page d\'accueil'),
(-198, '6', 'MenuItem', 'label', 'es', 'FAQ'),
(-197, '5', 'MenuItem', 'label', 'aes', 'Nuevo a la Salsa?'),
(-196, '3', 'FulfillmentOption', 'longDescription', 'es', '1 - 2 D&iacute;as'),
(-195, '3', 'FulfillmentOption', 'name', 'es', 'Express'),
(-194, '2', 'FulfillmentOption', 'longDescription', 'es', '3 - 5 D&iacute;as'),
(-193, '2', 'FulfillmentOption', 'name', 'es', 'Ejecutiva'),
(-192, '1', 'FulfillmentOption', 'longDescription', 'es', '5 - 7 D&iacute;as'),
(-191, '1', 'FulfillmentOption', 'name', 'es', 'Estándar'),
(-182, '3', 'SearchFacet', 'label', 'es', 'Precio'),
(-181, '2', 'SearchFacet', 'label', 'es', 'Rango de Calor'),
(-180, '1', 'SearchFacet', 'label', 'es', 'Fabricante'),
(-100, '600', 'Sku', 'longDescription', 'es', '&iquest;No te encanta nuestra mascota? Compre su propia camiseta hoy!'),
(-99, '600', 'Sku', 'name', 'es', 'Mascota de Heat Clinic (Mujeres)'),
(-98, '500', 'Sku', 'longDescription', 'es', '&iquest;No te encanta nuestra mascota? Compre su propia camiseta hoy!'),
(-97, '500', 'Sku', 'name', 'es', 'Mascota de Heat Clinic (Hombres)'),
(-96, '400', 'Sku', 'longDescription', 'es', 'Esta camiseta tiene el logo dibujado a mano para hombres, ofrece un ajuste regular en tres colores diferentes.'),
(-95, '400', 'Sku', 'name', 'es', 'Heat Clinic dibujado a mano (Mujeres)'),
(-94, '300', 'Sku', 'longDescription', 'es', 'Esta camiseta tiene el logo dibujado a mano para hombres, ofrece un ajuste regular en tres colores diferentes.'),
(-93, '300', 'Sku', 'name', 'es', 'Heat Clinic dibujado a mano (Hombres)'),
(-92, '200', 'Sku', 'longDescription', 'es', 'Colecci&oacute;n de Mujeres Habanero est&aacute;ndar de manga corta serigrafiadas, camiseta de algod&oacute;n suave en ajuste normal.'),
(-91, '200', 'Sku', 'name', 'es', 'Camisa de Habanero Hawt (Mujeres)'),
(-90, '100', 'Sku', 'longDescription', 'es', 'Colecci&oacute;n de Hombres Habanero est&aacute;ndar de manga corta serigrafiadas, camiseta de algod&oacute;n suave en ajuste normal.'),
(-89, '100', 'Sku', 'name', 'es', 'Camisa de Habanero Hawt (Hombres)'),
(-88, '19', 'Sku', 'longDescription', 'es', 'Esta salsa debe su gran sabor a los pimientos de edad y vinagre de ca&ntilde;a. Mejorar&aacute; el sabor de cualquier comida.'),
(-87, '19', 'Sku', 'name', 'es', 'Salsa de Pimienta Roja y Chipotle'),
(-86, '18', 'Sku', 'longDescription', 'es', 'Esta salsa debe su gran sabor a los pimientos de edad y vinagre de ca&ntilde;a. Mejorar&aacute; el sabor de cualquier comida.'),
(-85, '18', 'Sku', 'name', 'es', 'Salsa de Jalape&ntilde;o Ardiente'),
(-84, '17', 'Sku', 'longDescription', 'es', 'Esta salsa debe su gran sabor a los pimientos de edad y vinagre de ca&ntilde;a. Mejorar&aacute; el sabor de cualquier comida.'),
(-83, '17', 'Sku', 'name', 'es', 'Salsa de Locura'),
(-82, '16', 'Sku', 'longDescription', 'es', 'Esta salsa debe su gran sabor a los pimientos de edad y vinagre de ca&ntilde;a. Mejorar&aacute; el sabor de cualquier comida.'),
(-81, '16', 'Sku', 'name', 'es', 'Salsa de Locura'),
(-80, '15', 'Sku', 'longDescription', 'es', 'Esta salsa debe su gran sabor a los pimientos de edad y vinagre de ca&ntilde;a. Mejorar&aacute; el sabor de cualquier comida.'),
(-79, '15', 'Sku', 'name', 'es', 'Salsa de Ajo Tostado'),
(-78, '14', 'Sku', 'longDescription', 'es', 'Esta salsa debe su gran sabor a los pimientos de edad y vinagre de ca&ntilde;a. Mejorar&aacute; el sabor de cualquier comida.'),
(-77, '14', 'Sku', 'name', 'es', 'Salsa de Cayene Fresco'),
(-76, '13', 'Sku', 'longDescription', 'es', 'Todo es m&aacute;s grande en Texas, incluso lo picante de la Salsa de Snortin Bull! Si usted es un fan de hacer la salsa de barbacoa a partir de cero, como yo, puede utilizar la Salsa de Snortin Bull como aditivo. Habaneros ardientes y los pimientos de cayena dan a la garganta un sabor y calor al rojo vivo.'),
(-75, '13', 'Sku', 'name', 'es', 'Salsa Mata-Gargantas de Bull Snort'),
(-74, '12', 'Sku', 'longDescription', 'es', 'Una de las salsas m&aacute;s inusuales que vendemos. La original era un viejo estilo salsa caj&uacute;n y esto versi&oacute;n actualizada ennegrecimiento. Es dulce, pero se obtiene un gran sabor de canela y clavo de olor con un tiro agradable de piacnte de Cayena. Use en todos los alimentos para dar ese toque caj&uacute;n.'),
(-73, '12', 'Sku', 'name', 'es', 'Salsa Dulce de Cajun de Lousiane'),
(-72, '11', 'Sku', 'longDescription', 'es', 'Espolvorear con platos de carne, pescado y verduras. Use como aditivo en la salsa de barbacoa o cualquier alimento que necesita un sabor picante. Comience con unas gotas y aumente hasta llegar al sabor deseado.'),
(-71, '11', 'Sku', 'name', 'es', 'Salsa Picante del Vaquero'),
(-70, '10', 'Sku', 'longDescription', 'es', 'Aquí est&aacute; la receta para aquellos que disfrutan de picante intolerable. Esta salsa macabra y mortal del Dr. Chilemeister se debe utilizar con precauci&oacute;n. El dolor puede llegar a ser adictivo!'),
(-69, '10', 'Sku', 'name', 'es', 'Salsa Loca del Dr. Chilemeister'),
(-68, '9', 'Sku', 'longDescription', 'es', 'Todo el infierno se ha desatado, fuego y azufre. Est&aacute; listo para el fin?'),
(-67, '9', 'Sku', 'name', 'es', 'Fin del Mundo Salsa'),
(-66, '8', 'Sku', 'longDescription', 'es', 'Esta salsa caliente recibe su nombre por la gente que aprecian una salsa picante. Lo que vas a encontrar aquí es un sabor picante realmente original, no una acritud abrumador que se encuentra en las salsas de chile Tabasco ordinarios - a pesar de la pimienta usado en este producto ha sido probado en 285.000 unidades Scoville. Por lo tanto, ensillar a dar un paseo para recordar. Para asegurarnos de que usted trajo s&oacute;lo la mejor salsa de pimiento habanero, nos fuimos a las faldas de las monta&ntilde;as mayas en Belice, Am&eacute;rica Central. Este producto se prepara totalmente a mano utilizando s&oacute;lo las verduras frescas y solo ingredientes naturales.'),
(-65, '8', 'Sku', 'name', 'es', 'Salsa de Habanero de la Silla Ardiente'),
(-64, '7', 'Sku', 'longDescription', 'es', 'Hecho con Naga Jolokia Bhut, el chile m&aacute;s picante del mundo.'),
(-63, '7', 'Sku', 'name', 'es', 'Fantasma Verde'),
(-62, '6', 'Sku', 'longDescription', 'es', 'Parecido al Habanero, el Bonnet Escoc&eacute;s tiene una punta profundamente invertidas en comparaci&oacute;n con el extremo puntiagudo del Habanero. Van en colores de verde a amarillo-naranja, el Bonnet Escoc&eacute;s es un alimento b&aacute;sico en West Indies, en Barbados y salsas estilo pimienta.'),
(-61, '6', 'Sku', 'name', 'es', 'Salsa del D&iacute;a de los Muertos de Bonnet Escoc&eacute;s'),
(-60, '5', 'Sku', 'longDescription', 'es', 'Si quieres picante, este es el Chile a elegir. Originario del Caribe, Yucat&aacute;n y la Costa norte de Am&eacute;rica del Sur, el habanero se presenta en una variedad de colores que van desde el verde claro a un rojo brillante. El calor, sabor y aroma &uacute;nicos del Habanero ha convertido el chile en el favorito de los amantes del picante.'),
(-59, '5', 'Sku', 'name', 'es', 'Salsa del D&iacute;a de los Muertos de Habanero'),
(-58, '4', 'Sku', 'longDescription', 'es', 'Cuando cualquier pimienta se seca y se fuma, se refiere como un Chipotle. Por lo general, con una apariencia arrugada, caf&eacute; oscuro, el Chipotle ofrece un sabor ahumado y dulce que se utiliza generalmente para agregar un sabor asado a las salsas, guisos y adobos.'),
(-57, '4', 'Sku', 'name', 'es', 'Salsa del D&iacute;a de los Muertos de Chipotle'),
(-56, '3', 'Sku', 'longDescription', 'es', 'Picante, maduro pimienta que se mezcla junto con el ajo, la cebolla, pasta de tomate y una pizca de az&uacute;car de ca&ntilde;a para hacer de esto una salsa suave. Maravilloso en huevos, aves de corral, carne de cerdo o pescado, esta salsa se ​​mezcla para hacer los adobos y sopas ricas.'),
(-55, '3', 'Sku', 'name', 'es', 'Salsa de la Muerte Saltante'),
(-54, '2', 'Sku', 'longDescription', 'es', 'El perfecto acompa&ntilde;ante para el pollo, el pescado, hamburguesas o pizza. Una gran mezcla de habanero, mango, fruta de la pasi&oacute;n y mucho m&aacute;s hacen de esta salsa de la Muerte una delicia tropical incre&iacute;ble.'),
(-53, '2', 'Sku', 'name', 'es', 'Salsa de la Muerte Dulce'),
(-52, '1', 'Sku', 'longDescription', 'es', 'Como mis amigos salseros saben, nunca soy f&aacute;cil de satisfacer. Por lo tanto, naci&oacute; la creaci&oacute;n de la Muerte S&uacute;bita. Cuando este listo para saborear al m&aacute;s all&aacute; ... Muerte s&uacute;bita entregar&aacute;!'),
(-51, '1', 'Sku', 'name', 'es', 'Salsa de la Muerte S&uacute;bita'),
(-47, '14', 'ProdOptionVal', 'attributeValue', 'es', 'XG'),
(-46, '13', 'ProdOptionVal', 'attributeValue', 'es', 'G'),
(-45, '12', 'ProdOptionVal', 'attributeValue', 'es', 'M'),
(-44, '11', 'ProdOptionVal', 'attributeValue', 'es', 'CH'),
(-43, '3', 'ProdOptionVal', 'attributeValue', 'es', 'Plateado'),
(-42, '2', 'ProdOptionVal', 'attributeValue', 'es', 'Rojo'),
(-41, '1', 'ProdOptionVal', 'attributeValue', 'es', 'Negro'),
(-32, '2', 'ProdOption', 'label', 'es', 'Tama&ntilde;o de Camisa'),
(-31, '1', 'ProdOption', 'label', 'es', 'Color de Camisa'),
(-15, '2005', 'Category', 'description', 'es', 'Tarjetas de Regalo'),
(-14, '2004', 'Category', 'description', 'es', 'Descuento'),
(-13, '2003', 'Category', 'description', 'es', 'Mercanc&iacute;a'),
(-12, '2002', 'Category', 'description', 'es', 'Salsas Picantes'),
(-11, '2001', 'Category', 'description', 'es', 'Inicio'),
(-5, '2005', 'Category', 'name', 'es', 'Tarjetas de Regalo'),
(-4, '2004', 'Category', 'name', 'es', 'Descuento'),
(-3, '2003', 'Category', 'name', 'es', 'Mercanc&iacute;a'),
(-2, '2002', 'Category', 'name', 'es', 'Salsas'),
(-1, '2001', 'Category', 'name', 'es', 'Inicio'),
(1, '1003', 'Offer', 'marketingMessage', 'en_GB', 'Adamsın'),
(2, '1050', 'Offer', 'marketingMessage', 'en_GB', 'fsfasfdfasdf'),
(3, '1050', 'Offer', 'marketingMessage', 'en', 'asdfafasfdsfasfa'),
(4, '1050', 'Offer', 'marketingMessage', 'en_US', 'afasfdsfasfafss');

-- --------------------------------------------------------

--
-- Table structure for table `blc_trans_additnl_fields`
--

CREATE TABLE `blc_trans_additnl_fields` (
  `PAYMENT_TRANSACTION_ID` bigint(20) NOT NULL,
  `FIELD_VALUE` longtext,
  `FIELD_NAME` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_trans_additnl_fields`
--

INSERT INTO `blc_trans_additnl_fields` (`PAYMENT_TRANSACTION_ID`, `FIELD_VALUE`, `FIELD_NAME`) VALUES
(1, 'COD', 'PASSTHROUGH_PAYMENT_TYPE');

-- --------------------------------------------------------

--
-- Table structure for table `blc_url_handler`
--

CREATE TABLE `blc_url_handler` (
  `URL_HANDLER_ID` bigint(20) NOT NULL,
  `INCOMING_URL` varchar(255) NOT NULL,
  `IS_REGEX` tinyint(1) DEFAULT NULL,
  `NEW_URL` varchar(255) NOT NULL,
  `URL_REDIRECT_TYPE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blc_url_handler`
--

INSERT INTO `blc_url_handler` (`URL_HANDLER_ID`, `INCOMING_URL`, `IS_REGEX`, `NEW_URL`, `URL_REDIRECT_TYPE`) VALUES
(1, '/googlePerm', NULL, 'http://www.google.com', 'REDIRECT_PERM'),
(2, '/googleTemp', NULL, 'http://www.google.com', 'REDIRECT_TEMP'),
(3, '/insanity', NULL, '/hot-sauces/insanity_sauce', 'FORWARD'),
(4, '/jalepeno', NULL, '/hot-sauces/hurtin_jalepeno_hot_sauce', 'REDIRECT_TEMP');

-- --------------------------------------------------------

--
-- Table structure for table `blc_userconnection`
--

CREATE TABLE `blc_userconnection` (
  `providerId` varchar(255) NOT NULL,
  `providerUserId` varchar(255) NOT NULL,
  `userId` varchar(255) NOT NULL,
  `accessToken` varchar(255) NOT NULL,
  `displayName` varchar(255) DEFAULT NULL,
  `expireTime` bigint(20) DEFAULT NULL,
  `imageUrl` varchar(255) DEFAULT NULL,
  `profileUrl` varchar(255) DEFAULT NULL,
  `rank` int(11) NOT NULL,
  `refreshToken` varchar(255) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blc_zip_code`
--

CREATE TABLE `blc_zip_code` (
  `ZIP_CODE_ID` varchar(255) NOT NULL,
  `ZIP_CITY` varchar(255) DEFAULT NULL,
  `ZIP_LATITUDE` double DEFAULT NULL,
  `ZIP_LONGITUDE` double DEFAULT NULL,
  `ZIP_STATE` varchar(255) DEFAULT NULL,
  `ZIPCODE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sequence_generator`
--

CREATE TABLE `sequence_generator` (
  `ID_NAME` varchar(255) NOT NULL,
  `ID_VAL` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sequence_generator`
--

INSERT INTO `sequence_generator` (`ID_NAME`, `ID_VAL`) VALUES
('AddressImpl', 51),
('CategoryExcludedSearchFacetImpl', 1050),
('CategoryImpl', 10150),
('CategoryMediaXrefImpl', 201),
('CategoryProductImpl', 1000),
('CategoryProductXrefImpl', 1200),
('CategorySearchFacetImpl', 1000),
('CategoryXrefImpl', 1150),
('ChallengeQuestionImpl', 1000),
('CountrySubdivisionCategoryImpl', 1000),
('CrossSaleProductImpl', 101),
('FeaturedProductImpl', 1050),
('FieldDefinitionImpl', 1000),
('FieldEnumerationImpl', 1000),
('FieldEnumerationItemImpl', 1000),
('FieldGroupImpl', 1000),
('FieldImpl', 1000),
('FulfillmentGroupImpl', 351),
('FulfillmentGroupItemImpl', 351),
('FulfillmentOptionImpl', 1000),
('IndexFieldImpl', 1000),
('IndexFieldTypeImpl', 1000),
('MediaImpl', 100200),
('MenuImpl', 1100),
('MenuItemImpl', 1100),
('OfferAuditImpl', 51),
('OfferCodeImpl', 1050),
('OfferImpl', 1200),
('OfferItemCriteriaImpl', 1200),
('OfferQualifyingCriteriaXrefImpl', 151),
('OfferTargetCriteriaXrefImpl', 151),
('OrderAdjustmentImpl', 101),
('OrderImpl', 401),
('OrderItemImpl', 351),
('OrderItemPriceDetailAdjustmentImpl', 51),
('OrderItemPriceDetailImpl', 351),
('OrderItemQualifierImpl', 51),
('OrderPaymentImpl', 51),
('PageFieldImpl', 1050),
('PageImpl', 1100),
('PageTemplateImpl', 1000),
('PaymentTransactionImpl', 51),
('PersonalMessageImpl', 51),
('PhoneImpl', 51),
('ProductAttributeImpl', 1000),
('ProductImpl', 10050),
('ProductOptionImpl', 1050),
('ProductOptionValueImpl', 1050),
('ProductOptionXrefImpl', 1100),
('RoleImpl', 1000),
('SandBoxImpl', 51),
('SandBoxManagementImpl', 51),
('SearchFacetImpl', 1000),
('SearchFacetRangeImpl', 1000),
('SearchInterceptImpl', 1000),
('SkuAttributeImpl', 1000),
('SkuImpl', 10150),
('SkuMediaXrefImpl', 51),
('SkuProductOptionValueXrefImpl', 1150),
('StaticAssetImpl', 100300),
('StructuredContentFieldImpl', 1250),
('StructuredContentFieldTemplateImpl', 501),
('StructuredContentFieldXrefImpl', 251),
('StructuredContentImpl', 1400),
('StructuredContentRuleImpl', 1000),
('StructuredContentTypeImpl', 1500),
('TranslationImpl', 51),
('UpSaleProductImpl', 101),
('URLHandlerImpl', 1000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blc_additional_offer_info`
--
ALTER TABLE `blc_additional_offer_info`
  ADD PRIMARY KEY (`BLC_ORDER_ORDER_ID`,`OFFER_ID`),
  ADD KEY `FK3BFDBD63B5D9C34D` (`OFFER_INFO_ID`),
  ADD KEY `FK3BFDBD63D5F3FAF4` (`OFFER_ID`),
  ADD KEY `FK3BFDBD631891FF79` (`BLC_ORDER_ORDER_ID`);

--
-- Indexes for table `blc_address`
--
ALTER TABLE `blc_address`
  ADD PRIMARY KEY (`ADDRESS_ID`),
  ADD KEY `ADDRESS_COUNTRY_INDEX` (`COUNTRY`),
  ADD KEY `ADDRESS_ISO_COUNTRY_IDX` (`ISO_COUNTRY_ALPHA2`),
  ADD KEY `ADDRESS_PHONE_FAX_IDX` (`PHONE_FAX_ID`),
  ADD KEY `ADDRESS_PHONE_PRI_IDX` (`PHONE_PRIMARY_ID`),
  ADD KEY `ADDRESS_PHONE_SEC_IDX` (`PHONE_SECONDARY_ID`),
  ADD KEY `ADDRESS_STATE_INDEX` (`STATE_PROV_REGION`),
  ADD KEY `FK299F86CEA46E16CF` (`COUNTRY`),
  ADD KEY `FK299F86CE3A39A488` (`ISO_COUNTRY_ALPHA2`),
  ADD KEY `FK299F86CEF1A6533F` (`PHONE_FAX_ID`),
  ADD KEY `FK299F86CEBF4449BA` (`PHONE_PRIMARY_ID`),
  ADD KEY `FK299F86CEE12DC0C8` (`PHONE_SECONDARY_ID`),
  ADD KEY `FK299F86CE337C4D50` (`STATE_PROV_REGION`);

--
-- Indexes for table `blc_admin_module`
--
ALTER TABLE `blc_admin_module`
  ADD PRIMARY KEY (`ADMIN_MODULE_ID`),
  ADD KEY `ADMINMODULE_NAME_INDEX` (`NAME`);

--
-- Indexes for table `blc_admin_password_token`
--
ALTER TABLE `blc_admin_password_token`
  ADD PRIMARY KEY (`PASSWORD_TOKEN`);

--
-- Indexes for table `blc_admin_permission`
--
ALTER TABLE `blc_admin_permission`
  ADD PRIMARY KEY (`ADMIN_PERMISSION_ID`);

--
-- Indexes for table `blc_admin_permission_entity`
--
ALTER TABLE `blc_admin_permission_entity`
  ADD PRIMARY KEY (`ADMIN_PERMISSION_ENTITY_ID`),
  ADD KEY `FK23C09E3DE88B7D38` (`ADMIN_PERMISSION_ID`);

--
-- Indexes for table `blc_admin_permission_xref`
--
ALTER TABLE `blc_admin_permission_xref`
  ADD KEY `FKBCAD1F5E88B7D38` (`ADMIN_PERMISSION_ID`),
  ADD KEY `FKBCAD1F575A3C445` (`CHILD_PERMISSION_ID`);

--
-- Indexes for table `blc_admin_role`
--
ALTER TABLE `blc_admin_role`
  ADD PRIMARY KEY (`ADMIN_ROLE_ID`);

--
-- Indexes for table `blc_admin_role_permission_xref`
--
ALTER TABLE `blc_admin_role_permission_xref`
  ADD PRIMARY KEY (`ADMIN_PERMISSION_ID`,`ADMIN_ROLE_ID`),
  ADD KEY `FK4A819D98E88B7D38` (`ADMIN_PERMISSION_ID`),
  ADD KEY `FK4A819D985F43AAD8` (`ADMIN_ROLE_ID`);

--
-- Indexes for table `blc_admin_section`
--
ALTER TABLE `blc_admin_section`
  ADD PRIMARY KEY (`ADMIN_SECTION_ID`),
  ADD UNIQUE KEY `uc_BLC_ADMIN_SECTION_1` (`SECTION_KEY`),
  ADD KEY `ADMINSECTION_MODULE_INDEX` (`ADMIN_MODULE_ID`),
  ADD KEY `ADMINSECTION_NAME_INDEX` (`NAME`),
  ADD KEY `FK7EA7D92FB1A18498` (`ADMIN_MODULE_ID`);

--
-- Indexes for table `blc_admin_sec_perm_xref`
--
ALTER TABLE `blc_admin_sec_perm_xref`
  ADD KEY `FK5E832966E88B7D38` (`ADMIN_PERMISSION_ID`),
  ADD KEY `FK5E8329663AF7F0FC` (`ADMIN_SECTION_ID`);

--
-- Indexes for table `blc_admin_user`
--
ALTER TABLE `blc_admin_user`
  ADD PRIMARY KEY (`ADMIN_USER_ID`),
  ADD KEY `ADMINPERM_EMAIL_INDEX` (`EMAIL`),
  ADD KEY `ADMINUSER_NAME_INDEX` (`NAME`);

--
-- Indexes for table `blc_admin_user_addtl_fields`
--
ALTER TABLE `blc_admin_user_addtl_fields`
  ADD PRIMARY KEY (`ATTRIBUTE_ID`),
  ADD KEY `ADMINUSERATTRIBUTE_INDEX` (`ADMIN_USER_ID`),
  ADD KEY `ADMINUSERATTRIBUTE_NAME_INDEX` (`FIELD_NAME`),
  ADD KEY `FK73274CDD46EBC38` (`ADMIN_USER_ID`);

--
-- Indexes for table `blc_admin_user_permission_xref`
--
ALTER TABLE `blc_admin_user_permission_xref`
  ADD PRIMARY KEY (`ADMIN_PERMISSION_ID`,`ADMIN_USER_ID`),
  ADD KEY `FKF0B3BEEDE88B7D38` (`ADMIN_PERMISSION_ID`),
  ADD KEY `FKF0B3BEED46EBC38` (`ADMIN_USER_ID`);

--
-- Indexes for table `blc_admin_user_role_xref`
--
ALTER TABLE `blc_admin_user_role_xref`
  ADD PRIMARY KEY (`ADMIN_ROLE_ID`,`ADMIN_USER_ID`),
  ADD KEY `FKFFD33A265F43AAD8` (`ADMIN_ROLE_ID`),
  ADD KEY `FKFFD33A2646EBC38` (`ADMIN_USER_ID`);

--
-- Indexes for table `blc_admin_user_sandbox`
--
ALTER TABLE `blc_admin_user_sandbox`
  ADD PRIMARY KEY (`ADMIN_USER_ID`),
  ADD KEY `FKD0A97E09579FE59D` (`SANDBOX_ID`),
  ADD KEY `FKD0A97E0946EBC38` (`ADMIN_USER_ID`);

--
-- Indexes for table `blc_asset_desc_map`
--
ALTER TABLE `blc_asset_desc_map`
  ADD PRIMARY KEY (`STATIC_ASSET_ID`,`MAP_KEY`),
  ADD KEY `FKE886BAE3E2BA0C9D` (`STATIC_ASSET_DESC_ID`),
  ADD KEY `FKE886BAE367F70B63` (`STATIC_ASSET_ID`);

--
-- Indexes for table `blc_bank_account_payment`
--
ALTER TABLE `blc_bank_account_payment`
  ADD PRIMARY KEY (`PAYMENT_ID`),
  ADD KEY `BANKACCOUNT_INDEX` (`REFERENCE_NUMBER`);

--
-- Indexes for table `blc_bundle_order_item`
--
ALTER TABLE `blc_bundle_order_item`
  ADD PRIMARY KEY (`ORDER_ITEM_ID`),
  ADD KEY `FK489703DBCCF29B96` (`PRODUCT_BUNDLE_ID`),
  ADD KEY `FK489703DBB78C9977` (`SKU_ID`),
  ADD KEY `FK489703DB9AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_bund_item_fee_price`
--
ALTER TABLE `blc_bund_item_fee_price`
  ADD PRIMARY KEY (`BUND_ITEM_FEE_PRICE_ID`),
  ADD KEY `FK14267A943FC68307` (`BUND_ORDER_ITEM_ID`);

--
-- Indexes for table `blc_candidate_fg_offer`
--
ALTER TABLE `blc_candidate_fg_offer`
  ADD PRIMARY KEY (`CANDIDATE_FG_OFFER_ID`),
  ADD KEY `CANDIDATE_FG_INDEX` (`FULFILLMENT_GROUP_ID`),
  ADD KEY `CANDIDATE_FGOFFER_INDEX` (`OFFER_ID`),
  ADD KEY `FKCE785605028DC55` (`FULFILLMENT_GROUP_ID`),
  ADD KEY `FKCE78560D5F3FAF4` (`OFFER_ID`);

--
-- Indexes for table `blc_candidate_item_offer`
--
ALTER TABLE `blc_candidate_item_offer`
  ADD PRIMARY KEY (`CANDIDATE_ITEM_OFFER_ID`),
  ADD KEY `CANDIDATE_ITEMOFFER_INDEX` (`OFFER_ID`),
  ADD KEY `CANDIDATE_ITEM_INDEX` (`ORDER_ITEM_ID`),
  ADD KEY `FK9EEE9B2D5F3FAF4` (`OFFER_ID`),
  ADD KEY `FK9EEE9B29AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_candidate_order_offer`
--
ALTER TABLE `blc_candidate_order_offer`
  ADD PRIMARY KEY (`CANDIDATE_ORDER_OFFER_ID`),
  ADD KEY `CANDIDATE_ORDEROFFER_INDEX` (`OFFER_ID`),
  ADD KEY `CANDIDATE_ORDER_INDEX` (`ORDER_ID`),
  ADD KEY `FK61852289D5F3FAF4` (`OFFER_ID`),
  ADD KEY `FK6185228989FE8A02` (`ORDER_ID`);

--
-- Indexes for table `blc_catalog`
--
ALTER TABLE `blc_catalog`
  ADD PRIMARY KEY (`CATALOG_ID`);

--
-- Indexes for table `blc_category`
--
ALTER TABLE `blc_category`
  ADD PRIMARY KEY (`CATEGORY_ID`),
  ADD KEY `CATEGORY_PARENT_INDEX` (`DEFAULT_PARENT_CATEGORY_ID`),
  ADD KEY `CATEGORY_E_ID_INDEX` (`EXTERNAL_ID`),
  ADD KEY `CATEGORY_NAME_INDEX` (`NAME`),
  ADD KEY `CATEGORY_URL_INDEX` (`URL`),
  ADD KEY `CATEGORY_URLKEY_INDEX` (`URL_KEY`),
  ADD KEY `FK55F82D44B177E6` (`DEFAULT_PARENT_CATEGORY_ID`);

--
-- Indexes for table `blc_category_attribute`
--
ALTER TABLE `blc_category_attribute`
  ADD PRIMARY KEY (`CATEGORY_ATTRIBUTE_ID`),
  ADD KEY `CATEGORYATTRIBUTE_INDEX` (`CATEGORY_ID`),
  ADD KEY `CATEGORYATTRIBUTE_NAME_INDEX` (`NAME`),
  ADD KEY `FK4E441D4115D1A13D` (`CATEGORY_ID`);

--
-- Indexes for table `blc_category_media_map`
--
ALTER TABLE `blc_category_media_map`
  ADD PRIMARY KEY (`CATEGORY_MEDIA_ID`),
  ADD KEY `FKCD24B106D786CEA2` (`BLC_CATEGORY_CATEGORY_ID`),
  ADD KEY `FKCD24B1066E4720E0` (`MEDIA_ID`);

--
-- Indexes for table `blc_category_product_xref`
--
ALTER TABLE `blc_category_product_xref`
  ADD PRIMARY KEY (`CATEGORY_PRODUCT_ID`),
  ADD KEY `FK635EB1A615D1A13D` (`CATEGORY_ID`),
  ADD KEY `FK635EB1A65F11A0B7` (`PRODUCT_ID`);

--
-- Indexes for table `blc_category_xref`
--
ALTER TABLE `blc_category_xref`
  ADD PRIMARY KEY (`CATEGORY_XREF_ID`),
  ADD KEY `FKE889733615D1A13D` (`CATEGORY_ID`),
  ADD KEY `FKE8897336D6D45DBE` (`SUB_CATEGORY_ID`);

--
-- Indexes for table `blc_cat_search_facet_excl_xref`
--
ALTER TABLE `blc_cat_search_facet_excl_xref`
  ADD PRIMARY KEY (`CAT_EXCL_SEARCH_FACET_ID`),
  ADD KEY `FK8361EF4E15D1A13D` (`CATEGORY_ID`),
  ADD KEY `FK8361EF4EB96B1C93` (`SEARCH_FACET_ID`);

--
-- Indexes for table `blc_cat_search_facet_xref`
--
ALTER TABLE `blc_cat_search_facet_xref`
  ADD PRIMARY KEY (`CATEGORY_SEARCH_FACET_ID`),
  ADD KEY `FK32210EEB15D1A13D` (`CATEGORY_ID`),
  ADD KEY `FK32210EEBB96B1C93` (`SEARCH_FACET_ID`);

--
-- Indexes for table `blc_cat_site_map_gen_cfg`
--
ALTER TABLE `blc_cat_site_map_gen_cfg`
  ADD PRIMARY KEY (`GEN_CONFIG_ID`),
  ADD KEY `FK1BA4E695C5F3D60` (`ROOT_CATEGORY_ID`),
  ADD KEY `FK1BA4E69BCAB9F56` (`GEN_CONFIG_ID`);

--
-- Indexes for table `blc_challenge_question`
--
ALTER TABLE `blc_challenge_question`
  ADD PRIMARY KEY (`QUESTION_ID`);

--
-- Indexes for table `blc_cms_menu`
--
ALTER TABLE `blc_cms_menu`
  ADD PRIMARY KEY (`MENU_ID`);

--
-- Indexes for table `blc_cms_menu_item`
--
ALTER TABLE `blc_cms_menu_item`
  ADD PRIMARY KEY (`MENU_ITEM_ID`),
  ADD KEY `FKFC9BDD76E4720E0` (`MEDIA_ID`),
  ADD KEY `FKFC9BDD7A8D0E60C` (`LINKED_MENU_ID`),
  ADD KEY `FKFC9BDD77BB4A41` (`LINKED_PAGE_ID`),
  ADD KEY `FKFC9BDD73876279D` (`PARENT_MENU_ID`);

--
-- Indexes for table `blc_code_types`
--
ALTER TABLE `blc_code_types`
  ADD PRIMARY KEY (`CODE_ID`);

--
-- Indexes for table `blc_country`
--
ALTER TABLE `blc_country`
  ADD PRIMARY KEY (`ABBREVIATION`);

--
-- Indexes for table `blc_country_sub`
--
ALTER TABLE `blc_country_sub`
  ADD PRIMARY KEY (`ABBREVIATION`),
  ADD KEY `COUNTRY_SUB_ALT_ABRV_IDX` (`ALT_ABBREVIATION`),
  ADD KEY `COUNTRY_SUB_NAME_IDX` (`NAME`),
  ADD KEY `FKA804FBD172AAB3C0` (`COUNTRY_SUB_CAT`),
  ADD KEY `FKA804FBD1A46E16CF` (`COUNTRY`);

--
-- Indexes for table `blc_country_sub_cat`
--
ALTER TABLE `blc_country_sub_cat`
  ADD PRIMARY KEY (`COUNTRY_SUB_CAT_ID`);

--
-- Indexes for table `blc_credit_card_payment`
--
ALTER TABLE `blc_credit_card_payment`
  ADD PRIMARY KEY (`PAYMENT_ID`),
  ADD KEY `CREDITCARD_INDEX` (`REFERENCE_NUMBER`);

--
-- Indexes for table `blc_currency`
--
ALTER TABLE `blc_currency`
  ADD PRIMARY KEY (`CURRENCY_CODE`);

--
-- Indexes for table `blc_customer`
--
ALTER TABLE `blc_customer`
  ADD PRIMARY KEY (`CUSTOMER_ID`),
  ADD KEY `CUSTOMER_CHALLENGE_INDEX` (`CHALLENGE_QUESTION_ID`),
  ADD KEY `CUSTOMER_EMAIL_INDEX` (`EMAIL_ADDRESS`),
  ADD KEY `FK7716F0241422B204` (`CHALLENGE_QUESTION_ID`),
  ADD KEY `FK7716F024A1E1C128` (`LOCALE_CODE`);

--
-- Indexes for table `blc_customer_address`
--
ALTER TABLE `blc_customer_address`
  ADD PRIMARY KEY (`CUSTOMER_ADDRESS_ID`),
  ADD KEY `CUSTOMERADDRESS_ADDRESS_INDEX` (`ADDRESS_ID`),
  ADD KEY `FK75B95AB9C13085DD` (`ADDRESS_ID`),
  ADD KEY `FK75B95AB97470F437` (`CUSTOMER_ID`);

--
-- Indexes for table `blc_customer_attribute`
--
ALTER TABLE `blc_customer_attribute`
  ADD PRIMARY KEY (`CUSTOMER_ATTR_ID`),
  ADD KEY `FKB974C8217470F437` (`CUSTOMER_ID`);

--
-- Indexes for table `blc_customer_offer_xref`
--
ALTER TABLE `blc_customer_offer_xref`
  ADD PRIMARY KEY (`CUSTOMER_OFFER_ID`),
  ADD KEY `CUSTOFFER_CUSTOMER_INDEX` (`CUSTOMER_ID`),
  ADD KEY `CUSTOFFER_OFFER_INDEX` (`OFFER_ID`),
  ADD KEY `FK685E80397470F437` (`CUSTOMER_ID`),
  ADD KEY `FK685E8039D5F3FAF4` (`OFFER_ID`);

--
-- Indexes for table `blc_customer_password_token`
--
ALTER TABLE `blc_customer_password_token`
  ADD PRIMARY KEY (`PASSWORD_TOKEN`);

--
-- Indexes for table `blc_customer_payment`
--
ALTER TABLE `blc_customer_payment`
  ADD PRIMARY KEY (`CUSTOMER_PAYMENT_ID`),
  ADD UNIQUE KEY `CSTMR_PAY_UNIQUE_CNSTRNT` (`CUSTOMER_ID`,`PAYMENT_TOKEN`),
  ADD KEY `CUSTOMERPAYMENT_TYPE_INDEX` (`PAYMENT_TYPE`),
  ADD KEY `FK8B3DF0CBC13085DD` (`ADDRESS_ID`),
  ADD KEY `FK8B3DF0CB7470F437` (`CUSTOMER_ID`);

--
-- Indexes for table `blc_customer_payment_fields`
--
ALTER TABLE `blc_customer_payment_fields`
  ADD PRIMARY KEY (`CUSTOMER_PAYMENT_ID`,`FIELD_NAME`),
  ADD KEY `FK5CCB14ADCA0B98E0` (`CUSTOMER_PAYMENT_ID`);

--
-- Indexes for table `blc_customer_phone`
--
ALTER TABLE `blc_customer_phone`
  ADD PRIMARY KEY (`CUSTOMER_PHONE_ID`),
  ADD UNIQUE KEY `CSTMR_PHONE_UNIQUE_CNSTRNT` (`CUSTOMER_ID`,`PHONE_NAME`),
  ADD KEY `CUSTPHONE_PHONE_INDEX` (`PHONE_ID`),
  ADD KEY `FK3D28ED737470F437` (`CUSTOMER_ID`),
  ADD KEY `FK3D28ED73D894CB5D` (`PHONE_ID`);

--
-- Indexes for table `blc_customer_role`
--
ALTER TABLE `blc_customer_role`
  ADD PRIMARY KEY (`CUSTOMER_ROLE_ID`),
  ADD KEY `CUSTROLE_CUSTOMER_INDEX` (`CUSTOMER_ID`),
  ADD KEY `CUSTROLE_ROLE_INDEX` (`ROLE_ID`),
  ADD KEY `FK548EB7B17470F437` (`CUSTOMER_ID`),
  ADD KEY `FK548EB7B1B8587B7` (`ROLE_ID`);

--
-- Indexes for table `blc_cust_site_map_gen_cfg`
--
ALTER TABLE `blc_cust_site_map_gen_cfg`
  ADD PRIMARY KEY (`GEN_CONFIG_ID`),
  ADD KEY `FK67C0DBA0BCAB9F56` (`GEN_CONFIG_ID`);

--
-- Indexes for table `blc_data_drvn_enum`
--
ALTER TABLE `blc_data_drvn_enum`
  ADD PRIMARY KEY (`ENUM_ID`),
  ADD KEY `ENUM_KEY_INDEX` (`ENUM_KEY`);

--
-- Indexes for table `blc_data_drvn_enum_val`
--
ALTER TABLE `blc_data_drvn_enum_val`
  ADD PRIMARY KEY (`ENUM_VAL_ID`),
  ADD KEY `HIDDEN_INDEX` (`HIDDEN`),
  ADD KEY `ENUM_VAL_KEY_INDEX` (`ENUM_KEY`),
  ADD KEY `FKB2D5700DA60E0554` (`ENUM_TYPE`);

--
-- Indexes for table `blc_discrete_order_item`
--
ALTER TABLE `blc_discrete_order_item`
  ADD PRIMARY KEY (`ORDER_ITEM_ID`),
  ADD KEY `DISCRETE_PRODUCT_INDEX` (`PRODUCT_ID`),
  ADD KEY `DISCRETE_SKU_INDEX` (`SKU_ID`),
  ADD KEY `FKBC3A8A845CDFCA80` (`BUNDLE_ORDER_ITEM_ID`),
  ADD KEY `FKBC3A8A845F11A0B7` (`PRODUCT_ID`),
  ADD KEY `FKBC3A8A84B78C9977` (`SKU_ID`),
  ADD KEY `FKBC3A8A841285903B` (`SKU_BUNDLE_ITEM_ID`),
  ADD KEY `FKBC3A8A849AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_disc_item_fee_price`
--
ALTER TABLE `blc_disc_item_fee_price`
  ADD PRIMARY KEY (`DISC_ITEM_FEE_PRICE_ID`),
  ADD KEY `FK2A641CC8B76B9466` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_dyn_discrete_order_item`
--
ALTER TABLE `blc_dyn_discrete_order_item`
  ADD PRIMARY KEY (`ORDER_ITEM_ID`),
  ADD KEY `FK209DEE9EB76B9466` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_email_tracking`
--
ALTER TABLE `blc_email_tracking`
  ADD PRIMARY KEY (`EMAIL_TRACKING_ID`),
  ADD KEY `EMAILTRACKING_INDEX` (`EMAIL_ADDRESS`);

--
-- Indexes for table `blc_email_tracking_clicks`
--
ALTER TABLE `blc_email_tracking_clicks`
  ADD PRIMARY KEY (`CLICK_ID`),
  ADD KEY `TRACKINGCLICKS_CUSTOMER_INDEX` (`CUSTOMER_ID`),
  ADD KEY `TRACKINGCLICKS_TRACKING_INDEX` (`EMAIL_TRACKING_ID`),
  ADD KEY `FKFDF9F52AFA1E5D61` (`EMAIL_TRACKING_ID`);

--
-- Indexes for table `blc_email_tracking_opens`
--
ALTER TABLE `blc_email_tracking_opens`
  ADD PRIMARY KEY (`OPEN_ID`),
  ADD KEY `TRACKINGOPEN_TRACKING` (`EMAIL_TRACKING_ID`),
  ADD KEY `FKA5C3722AFA1E5D61` (`EMAIL_TRACKING_ID`);

--
-- Indexes for table `blc_fg_adjustment`
--
ALTER TABLE `blc_fg_adjustment`
  ADD PRIMARY KEY (`FG_ADJUSTMENT_ID`),
  ADD KEY `FGADJUSTMENT_INDEX` (`FULFILLMENT_GROUP_ID`),
  ADD KEY `FGADJUSTMENT_OFFER_INDEX` (`OFFER_ID`),
  ADD KEY `FK468C8F255028DC55` (`FULFILLMENT_GROUP_ID`),
  ADD KEY `FK468C8F25D5F3FAF4` (`OFFER_ID`);

--
-- Indexes for table `blc_fg_fee_tax_xref`
--
ALTER TABLE `blc_fg_fee_tax_xref`
  ADD UNIQUE KEY `UK_25426DC0FA888C35` (`TAX_DETAIL_ID`),
  ADD KEY `FK25426DC071448C19` (`TAX_DETAIL_ID`),
  ADD KEY `FK25426DC0598F6D02` (`FULFILLMENT_GROUP_FEE_ID`);

--
-- Indexes for table `blc_fg_fg_tax_xref`
--
ALTER TABLE `blc_fg_fg_tax_xref`
  ADD UNIQUE KEY `UK_61BEA455FA888C35` (`TAX_DETAIL_ID`),
  ADD KEY `FK61BEA45571448C19` (`TAX_DETAIL_ID`),
  ADD KEY `FK61BEA4555028DC55` (`FULFILLMENT_GROUP_ID`);

--
-- Indexes for table `blc_fg_item_tax_xref`
--
ALTER TABLE `blc_fg_item_tax_xref`
  ADD UNIQUE KEY `UK_DD3E8443FA888C35` (`TAX_DETAIL_ID`),
  ADD KEY `FKDD3E844371448C19` (`TAX_DETAIL_ID`),
  ADD KEY `FKDD3E8443E3BBB4D2` (`FULFILLMENT_GROUP_ITEM_ID`);

--
-- Indexes for table `blc_field`
--
ALTER TABLE `blc_field`
  ADD PRIMARY KEY (`FIELD_ID`),
  ADD KEY `ENTITY_TYPE_INDEX` (`ENTITY_TYPE`);

--
-- Indexes for table `blc_fld_def`
--
ALTER TABLE `blc_fld_def`
  ADD PRIMARY KEY (`FLD_DEF_ID`),
  ADD KEY `FK3FCB575E38D08AB5` (`ENUM_ID`),
  ADD KEY `FK3FCB575E6A79BDB5` (`FLD_GROUP_ID`);

--
-- Indexes for table `blc_fld_enum`
--
ALTER TABLE `blc_fld_enum`
  ADD PRIMARY KEY (`FLD_ENUM_ID`);

--
-- Indexes for table `blc_fld_enum_item`
--
ALTER TABLE `blc_fld_enum_item`
  ADD PRIMARY KEY (`FLD_ENUM_ITEM_ID`),
  ADD KEY `FK83A6A84AFD2EA299` (`FLD_ENUM_ID`);

--
-- Indexes for table `blc_fld_group`
--
ALTER TABLE `blc_fld_group`
  ADD PRIMARY KEY (`FLD_GROUP_ID`);

--
-- Indexes for table `blc_fulfillment_group`
--
ALTER TABLE `blc_fulfillment_group`
  ADD PRIMARY KEY (`FULFILLMENT_GROUP_ID`),
  ADD KEY `FG_ADDRESS_INDEX` (`ADDRESS_ID`),
  ADD KEY `FG_METHOD_INDEX` (`METHOD`),
  ADD KEY `FG_ORDER_INDEX` (`ORDER_ID`),
  ADD KEY `FG_MESSAGE_INDEX` (`PERSONAL_MESSAGE_ID`),
  ADD KEY `FG_PHONE_INDEX` (`PHONE_ID`),
  ADD KEY `FG_PRIMARY_INDEX` (`IS_PRIMARY`),
  ADD KEY `FG_REFERENCE_INDEX` (`REFERENCE_NUMBER`),
  ADD KEY `FG_SERVICE_INDEX` (`SERVICE`),
  ADD KEY `FG_STATUS_INDEX` (`STATUS`),
  ADD KEY `FKC5B9EF18C13085DD` (`ADDRESS_ID`),
  ADD KEY `FKC5B9EF1881F34C7F` (`FULFILLMENT_OPTION_ID`),
  ADD KEY `FKC5B9EF1889FE8A02` (`ORDER_ID`),
  ADD KEY `FKC5B9EF1877F565E1` (`PERSONAL_MESSAGE_ID`),
  ADD KEY `FKC5B9EF18D894CB5D` (`PHONE_ID`);

--
-- Indexes for table `blc_fulfillment_group_fee`
--
ALTER TABLE `blc_fulfillment_group_fee`
  ADD PRIMARY KEY (`FULFILLMENT_GROUP_FEE_ID`),
  ADD KEY `FK6AA8E1BF5028DC55` (`FULFILLMENT_GROUP_ID`);

--
-- Indexes for table `blc_fulfillment_group_item`
--
ALTER TABLE `blc_fulfillment_group_item`
  ADD PRIMARY KEY (`FULFILLMENT_GROUP_ITEM_ID`),
  ADD KEY `FGITEM_FG_INDEX` (`FULFILLMENT_GROUP_ID`),
  ADD KEY `FGITEM_STATUS_INDEX` (`STATUS`),
  ADD KEY `FKEA74EBDA5028DC55` (`FULFILLMENT_GROUP_ID`),
  ADD KEY `FKEA74EBDA9AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_fulfillment_option`
--
ALTER TABLE `blc_fulfillment_option`
  ADD PRIMARY KEY (`FULFILLMENT_OPTION_ID`);

--
-- Indexes for table `blc_fulfillment_option_fixed`
--
ALTER TABLE `blc_fulfillment_option_fixed`
  ADD PRIMARY KEY (`FULFILLMENT_OPTION_ID`),
  ADD KEY `FK408360313E2FC4F9` (`CURRENCY_CODE`),
  ADD KEY `FK4083603181F34C7F` (`FULFILLMENT_OPTION_ID`);

--
-- Indexes for table `blc_fulfillment_opt_banded_prc`
--
ALTER TABLE `blc_fulfillment_opt_banded_prc`
  ADD PRIMARY KEY (`FULFILLMENT_OPTION_ID`),
  ADD KEY `FKB1FD71E981F34C7F` (`FULFILLMENT_OPTION_ID`);

--
-- Indexes for table `blc_fulfillment_opt_banded_wgt`
--
ALTER TABLE `blc_fulfillment_opt_banded_wgt`
  ADD PRIMARY KEY (`FULFILLMENT_OPTION_ID`),
  ADD KEY `FKB1FD8AEC81F34C7F` (`FULFILLMENT_OPTION_ID`);

--
-- Indexes for table `blc_fulfillment_price_band`
--
ALTER TABLE `blc_fulfillment_price_band`
  ADD PRIMARY KEY (`FULFILLMENT_PRICE_BAND_ID`),
  ADD KEY `FK46C9EA726CDF59CA` (`FULFILLMENT_OPTION_ID`);

--
-- Indexes for table `blc_fulfillment_weight_band`
--
ALTER TABLE `blc_fulfillment_weight_band`
  ADD PRIMARY KEY (`FULFILLMENT_WEIGHT_BAND_ID`),
  ADD KEY `FK6A048D95A0B429C3` (`FULFILLMENT_OPTION_ID`);

--
-- Indexes for table `blc_giftwrap_order_item`
--
ALTER TABLE `blc_giftwrap_order_item`
  ADD PRIMARY KEY (`ORDER_ITEM_ID`),
  ADD KEY `FKE1BE1563B76B9466` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_gift_card_payment`
--
ALTER TABLE `blc_gift_card_payment`
  ADD PRIMARY KEY (`PAYMENT_ID`),
  ADD KEY `GIFTCARD_INDEX` (`REFERENCE_NUMBER`);

--
-- Indexes for table `blc_id_generation`
--
ALTER TABLE `blc_id_generation`
  ADD PRIMARY KEY (`ID_TYPE`);

--
-- Indexes for table `blc_img_static_asset`
--
ALTER TABLE `blc_img_static_asset`
  ADD PRIMARY KEY (`STATIC_ASSET_ID`),
  ADD KEY `FKCC4B772167F70B63` (`STATIC_ASSET_ID`);

--
-- Indexes for table `blc_index_field`
--
ALTER TABLE `blc_index_field`
  ADD PRIMARY KEY (`INDEX_FIELD_ID`),
  ADD KEY `FK9A53C1073C3907C4` (`FIELD_ID`),
  ADD KEY `INDEX_FIELD_SEARCHABLE_INDEX` (`SEARCHABLE`);

--
-- Indexes for table `blc_index_field_type`
--
ALTER TABLE `blc_index_field_type`
  ADD PRIMARY KEY (`INDEX_FIELD_TYPE_ID`),
  ADD KEY `FK4A310B72CBDA280B` (`INDEX_FIELD_ID`);

--
-- Indexes for table `blc_iso_country`
--
ALTER TABLE `blc_iso_country`
  ADD PRIMARY KEY (`ALPHA_2`);

--
-- Indexes for table `blc_item_offer_qualifier`
--
ALTER TABLE `blc_item_offer_qualifier`
  ADD PRIMARY KEY (`ITEM_OFFER_QUALIFIER_ID`),
  ADD KEY `FKD9C50C61D5F3FAF4` (`OFFER_ID`),
  ADD KEY `FKD9C50C619AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_locale`
--
ALTER TABLE `blc_locale`
  ADD PRIMARY KEY (`LOCALE_CODE`),
  ADD KEY `FK56C7DC203E2FC4F9` (`CURRENCY_CODE`);

--
-- Indexes for table `blc_media`
--
ALTER TABLE `blc_media`
  ADD PRIMARY KEY (`MEDIA_ID`),
  ADD KEY `MEDIA_TITLE_INDEX` (`TITLE`),
  ADD KEY `MEDIA_URL_INDEX` (`URL`);

--
-- Indexes for table `blc_module_configuration`
--
ALTER TABLE `blc_module_configuration`
  ADD PRIMARY KEY (`MODULE_CONFIG_ID`);

--
-- Indexes for table `blc_offer`
--
ALTER TABLE `blc_offer`
  ADD PRIMARY KEY (`OFFER_ID`),
  ADD KEY `OFFER_DISCOUNT_INDEX` (`OFFER_DISCOUNT_TYPE`),
  ADD KEY `OFFER_MARKETING_MESSAGE_INDEX` (`MARKETING_MESSASGE`),
  ADD KEY `OFFER_NAME_INDEX` (`OFFER_NAME`),
  ADD KEY `OFFER_TYPE_INDEX` (`OFFER_TYPE`);

--
-- Indexes for table `blc_offer_audit`
--
ALTER TABLE `blc_offer_audit`
  ADD PRIMARY KEY (`OFFER_AUDIT_ID`),
  ADD KEY `OFFERAUDIT_CUSTOMER_INDEX` (`CUSTOMER_ID`),
  ADD KEY `OFFERAUDIT_OFFER_CODE_INDEX` (`OFFER_CODE_ID`),
  ADD KEY `OFFERAUDIT_OFFER_INDEX` (`OFFER_ID`),
  ADD KEY `OFFERAUDIT_ORDER_INDEX` (`ORDER_ID`);

--
-- Indexes for table `blc_offer_code`
--
ALTER TABLE `blc_offer_code`
  ADD PRIMARY KEY (`OFFER_CODE_ID`),
  ADD KEY `OFFER_CODE_EMAIL_INDEX` (`EMAIL_ADDRESS`),
  ADD KEY `OFFERCODE_OFFER_INDEX` (`OFFER_ID`),
  ADD KEY `OFFERCODE_CODE_INDEX` (`OFFER_CODE`),
  ADD KEY `FK76B8C8D6D5F3FAF4` (`OFFER_ID`);

--
-- Indexes for table `blc_offer_info`
--
ALTER TABLE `blc_offer_info`
  ADD PRIMARY KEY (`OFFER_INFO_ID`);

--
-- Indexes for table `blc_offer_info_fields`
--
ALTER TABLE `blc_offer_info_fields`
  ADD PRIMARY KEY (`OFFER_INFO_FIELDS_ID`,`FIELD_NAME`),
  ADD KEY `FKA901886183AE7237` (`OFFER_INFO_FIELDS_ID`);

--
-- Indexes for table `blc_offer_item_criteria`
--
ALTER TABLE `blc_offer_item_criteria`
  ADD PRIMARY KEY (`OFFER_ITEM_CRITERIA_ID`);

--
-- Indexes for table `blc_offer_rule`
--
ALTER TABLE `blc_offer_rule`
  ADD PRIMARY KEY (`OFFER_RULE_ID`);

--
-- Indexes for table `blc_offer_rule_map`
--
ALTER TABLE `blc_offer_rule_map`
  ADD PRIMARY KEY (`OFFER_OFFER_RULE_ID`),
  ADD KEY `FKCA468FE245C66D1D` (`BLC_OFFER_OFFER_ID`),
  ADD KEY `FKCA468FE2C11A218D` (`OFFER_RULE_ID`);

--
-- Indexes for table `blc_order`
--
ALTER TABLE `blc_order`
  ADD PRIMARY KEY (`ORDER_ID`),
  ADD KEY `ORDER_CUSTOMER_INDEX` (`CUSTOMER_ID`),
  ADD KEY `ORDER_EMAIL_INDEX` (`EMAIL_ADDRESS`),
  ADD KEY `ORDER_NAME_INDEX` (`NAME`),
  ADD KEY `ORDER_NUMBER_INDEX` (`ORDER_NUMBER`),
  ADD KEY `ORDER_STATUS_INDEX` (`ORDER_STATUS`),
  ADD KEY `FK8F5B64A83E2FC4F9` (`CURRENCY_CODE`),
  ADD KEY `FK8F5B64A87470F437` (`CUSTOMER_ID`),
  ADD KEY `FK8F5B64A8A1E1C128` (`LOCALE_CODE`);

--
-- Indexes for table `blc_order_adjustment`
--
ALTER TABLE `blc_order_adjustment`
  ADD PRIMARY KEY (`ORDER_ADJUSTMENT_ID`),
  ADD KEY `ORDERADJUST_OFFER_INDEX` (`OFFER_ID`),
  ADD KEY `ORDERADJUST_ORDER_INDEX` (`ORDER_ID`),
  ADD KEY `FK1E92D164D5F3FAF4` (`OFFER_ID`),
  ADD KEY `FK1E92D16489FE8A02` (`ORDER_ID`);

--
-- Indexes for table `blc_order_attribute`
--
ALTER TABLE `blc_order_attribute`
  ADD PRIMARY KEY (`ORDER_ATTRIBUTE_ID`),
  ADD KEY `FKB3A467A589FE8A02` (`ORDER_ID`);

--
-- Indexes for table `blc_order_item`
--
ALTER TABLE `blc_order_item`
  ADD PRIMARY KEY (`ORDER_ITEM_ID`),
  ADD KEY `ORDERITEM_CATEGORY_INDEX` (`CATEGORY_ID`),
  ADD KEY `ORDERITEM_GIFT_INDEX` (`GIFT_WRAP_ITEM_ID`),
  ADD KEY `ORDERITEM_ORDER_INDEX` (`ORDER_ID`),
  ADD KEY `ORDERITEM_TYPE_INDEX` (`ORDER_ITEM_TYPE`),
  ADD KEY `ORDERITEM_PARENT_INDEX` (`PARENT_ORDER_ITEM_ID`),
  ADD KEY `ORDERITEM_MESSAGE_INDEX` (`PERSONAL_MESSAGE_ID`),
  ADD KEY `FK9A2E704A15D1A13D` (`CATEGORY_ID`),
  ADD KEY `FK9A2E704AFD2F1F10` (`GIFT_WRAP_ITEM_ID`),
  ADD KEY `FK9A2E704A89FE8A02` (`ORDER_ID`),
  ADD KEY `FK9A2E704AB0B0D00A` (`PARENT_ORDER_ITEM_ID`),
  ADD KEY `FK9A2E704A77F565E1` (`PERSONAL_MESSAGE_ID`);

--
-- Indexes for table `blc_order_item_add_attr`
--
ALTER TABLE `blc_order_item_add_attr`
  ADD PRIMARY KEY (`ORDER_ITEM_ID`,`NAME`),
  ADD KEY `FKA466AB44B76B9466` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_order_item_adjustment`
--
ALTER TABLE `blc_order_item_adjustment`
  ADD PRIMARY KEY (`ORDER_ITEM_ADJUSTMENT_ID`),
  ADD KEY `OIADJUST_ITEM_INDEX` (`ORDER_ITEM_ID`),
  ADD KEY `FKA2658C82D5F3FAF4` (`OFFER_ID`),
  ADD KEY `FKA2658C829AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_order_item_attribute`
--
ALTER TABLE `blc_order_item_attribute`
  ADD PRIMARY KEY (`ORDER_ITEM_ATTRIBUTE_ID`),
  ADD KEY `FK9F1ED0C79AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_order_item_cart_message`
--
ALTER TABLE `blc_order_item_cart_message`
  ADD KEY `FK5D53C79D9AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_order_item_dtl_adj`
--
ALTER TABLE `blc_order_item_dtl_adj`
  ADD PRIMARY KEY (`ORDER_ITEM_DTL_ADJ_ID`),
  ADD KEY `FK85F0248FD5F3FAF4` (`OFFER_ID`),
  ADD KEY `FK85F0248FD4AEA2C0` (`ORDER_ITEM_PRICE_DTL_ID`);

--
-- Indexes for table `blc_order_item_price_dtl`
--
ALTER TABLE `blc_order_item_price_dtl`
  ADD PRIMARY KEY (`ORDER_ITEM_PRICE_DTL_ID`),
  ADD KEY `FK1FB64BF19AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_order_lock`
--
ALTER TABLE `blc_order_lock`
  ADD PRIMARY KEY (`LOCK_KEY`,`ORDER_ID`);

--
-- Indexes for table `blc_order_multiship_option`
--
ALTER TABLE `blc_order_multiship_option`
  ADD PRIMARY KEY (`ORDER_MULTISHIP_OPTION_ID`),
  ADD KEY `MULTISHIP_OPTION_ORDER_INDEX` (`ORDER_ID`),
  ADD KEY `FKB3D3F7D6C13085DD` (`ADDRESS_ID`),
  ADD KEY `FKB3D3F7D681F34C7F` (`FULFILLMENT_OPTION_ID`),
  ADD KEY `FKB3D3F7D689FE8A02` (`ORDER_ID`),
  ADD KEY `FKB3D3F7D69AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_order_offer_code_xref`
--
ALTER TABLE `blc_order_offer_code_xref`
  ADD KEY `FKFDF0E8533BB10F6D` (`OFFER_CODE_ID`),
  ADD KEY `FKFDF0E85389FE8A02` (`ORDER_ID`);

--
-- Indexes for table `blc_order_payment`
--
ALTER TABLE `blc_order_payment`
  ADD PRIMARY KEY (`ORDER_PAYMENT_ID`),
  ADD KEY `ORDERPAYMENT_ADDRESS_INDEX` (`ADDRESS_ID`),
  ADD KEY `ORDERPAYMENT_ORDER_INDEX` (`ORDER_ID`),
  ADD KEY `ORDERPAYMENT_REFERENCE_INDEX` (`REFERENCE_NUMBER`),
  ADD KEY `ORDERPAYMENT_TYPE_INDEX` (`PAYMENT_TYPE`),
  ADD KEY `FK9517A14FC13085DD` (`ADDRESS_ID`),
  ADD KEY `FK9517A14F89FE8A02` (`ORDER_ID`);

--
-- Indexes for table `blc_order_payment_transaction`
--
ALTER TABLE `blc_order_payment_transaction`
  ADD PRIMARY KEY (`PAYMENT_TRANSACTION_ID`),
  ADD KEY `FK86FDE7CE6A69DD9D` (`ORDER_PAYMENT`),
  ADD KEY `FK86FDE7CEE1B66C71` (`PARENT_TRANSACTION`);

--
-- Indexes for table `blc_page`
--
ALTER TABLE `blc_page`
  ADD PRIMARY KEY (`PAGE_ID`),
  ADD KEY `PAGE_FULL_URL_INDEX` (`FULL_URL`),
  ADD KEY `FKF41BEDD5D49D3961` (`PAGE_TMPLT_ID`);

--
-- Indexes for table `blc_page_attributes`
--
ALTER TABLE `blc_page_attributes`
  ADD PRIMARY KEY (`ATTRIBUTE_ID`),
  ADD KEY `PAGEATTRIBUTE_NAME_INDEX` (`FIELD_NAME`),
  ADD KEY `PAGEATTRIBUTE_INDEX` (`PAGE_ID`),
  ADD KEY `FK4FE27601883C2667` (`PAGE_ID`);

--
-- Indexes for table `blc_page_fld`
--
ALTER TABLE `blc_page_fld`
  ADD PRIMARY KEY (`PAGE_FLD_ID`),
  ADD KEY `FK86433AD4883C2667` (`PAGE_ID`);

--
-- Indexes for table `blc_page_item_criteria`
--
ALTER TABLE `blc_page_item_criteria`
  ADD PRIMARY KEY (`PAGE_ITEM_CRITERIA_ID`);

--
-- Indexes for table `blc_page_rule`
--
ALTER TABLE `blc_page_rule`
  ADD PRIMARY KEY (`PAGE_RULE_ID`);

--
-- Indexes for table `blc_page_rule_map`
--
ALTER TABLE `blc_page_rule_map`
  ADD PRIMARY KEY (`BLC_PAGE_PAGE_ID`,`MAP_KEY`),
  ADD KEY `FK1ABA0CA336D91846` (`PAGE_RULE_ID`),
  ADD KEY `FK1ABA0CA3C38455DD` (`BLC_PAGE_PAGE_ID`);

--
-- Indexes for table `blc_page_tmplt`
--
ALTER TABLE `blc_page_tmplt`
  ADD PRIMARY KEY (`PAGE_TMPLT_ID`),
  ADD KEY `FK325C9D5A1E1C128` (`LOCALE_CODE`);

--
-- Indexes for table `blc_payment_log`
--
ALTER TABLE `blc_payment_log`
  ADD PRIMARY KEY (`PAYMENT_LOG_ID`),
  ADD KEY `PAYMENTLOG_CUSTOMER_INDEX` (`CUSTOMER_ID`),
  ADD KEY `PAYMENTLOG_LOGTYPE_INDEX` (`LOG_TYPE`),
  ADD KEY `PAYMENTLOG_ORDERPAYMENT_INDEX` (`ORDER_PAYMENT_ID`),
  ADD KEY `PAYMENTLOG_REFERENCE_INDEX` (`ORDER_PAYMENT_REF_NUM`),
  ADD KEY `PAYMENTLOG_TRANTYPE_INDEX` (`TRANSACTION_TYPE`),
  ADD KEY `PAYMENTLOG_USER_INDEX` (`USER_NAME`),
  ADD KEY `FKA43703453E2FC4F9` (`CURRENCY_CODE`),
  ADD KEY `FKA43703457470F437` (`CUSTOMER_ID`);

--
-- Indexes for table `blc_personal_message`
--
ALTER TABLE `blc_personal_message`
  ADD PRIMARY KEY (`PERSONAL_MESSAGE_ID`);

--
-- Indexes for table `blc_pgtmplt_fldgrp_xref`
--
ALTER TABLE `blc_pgtmplt_fldgrp_xref`
  ADD PRIMARY KEY (`PG_TMPLT_FLD_GRP_ID`),
  ADD KEY `FK99D625F66A79BDB5` (`FLD_GROUP_ID`),
  ADD KEY `FK99D625F6D49D3961` (`PAGE_TMPLT_ID`);

--
-- Indexes for table `blc_phone`
--
ALTER TABLE `blc_phone`
  ADD PRIMARY KEY (`PHONE_ID`);

--
-- Indexes for table `blc_product`
--
ALTER TABLE `blc_product`
  ADD PRIMARY KEY (`PRODUCT_ID`),
  ADD KEY `PRODUCT_CATEGORY_INDEX` (`DEFAULT_CATEGORY_ID`),
  ADD KEY `PRODUCT_URL_INDEX` (`URL`,`URL_KEY`),
  ADD KEY `FK5B95B7C9DF057C3F` (`DEFAULT_CATEGORY_ID`),
  ADD KEY `FK5B95B7C96D386535` (`DEFAULT_SKU_ID`);

--
-- Indexes for table `blc_product_attribute`
--
ALTER TABLE `blc_product_attribute`
  ADD PRIMARY KEY (`PRODUCT_ATTRIBUTE_ID`),
  ADD KEY `PRODUCTATTRIBUTE_NAME_INDEX` (`NAME`),
  ADD KEY `PRODUCTATTRIBUTE_INDEX` (`PRODUCT_ID`),
  ADD KEY `FK56CE05865F11A0B7` (`PRODUCT_ID`);

--
-- Indexes for table `blc_product_bundle`
--
ALTER TABLE `blc_product_bundle`
  ADD PRIMARY KEY (`PRODUCT_ID`),
  ADD KEY `FK8CC5B85F11A0B7` (`PRODUCT_ID`);

--
-- Indexes for table `blc_product_cross_sale`
--
ALTER TABLE `blc_product_cross_sale`
  ADD PRIMARY KEY (`CROSS_SALE_PRODUCT_ID`),
  ADD KEY `CROSSSALE_CATEGORY_INDEX` (`CATEGORY_ID`),
  ADD KEY `CROSSSALE_INDEX` (`PRODUCT_ID`),
  ADD KEY `CROSSSALE_RELATED_INDEX` (`RELATED_SALE_PRODUCT_ID`),
  ADD KEY `FK8324FB3C15D1A13D` (`CATEGORY_ID`),
  ADD KEY `FK8324FB3C5F11A0B7` (`PRODUCT_ID`),
  ADD KEY `FK8324FB3C62D84F9B` (`RELATED_SALE_PRODUCT_ID`);

--
-- Indexes for table `blc_product_featured`
--
ALTER TABLE `blc_product_featured`
  ADD PRIMARY KEY (`FEATURED_PRODUCT_ID`),
  ADD KEY `PRODFEATURED_CATEGORY_INDEX` (`CATEGORY_ID`),
  ADD KEY `PRODFEATURED_PRODUCT_INDEX` (`PRODUCT_ID`),
  ADD KEY `FK4C49FFE415D1A13D` (`CATEGORY_ID`),
  ADD KEY `FK4C49FFE45F11A0B7` (`PRODUCT_ID`);

--
-- Indexes for table `blc_product_option`
--
ALTER TABLE `blc_product_option`
  ADD PRIMARY KEY (`PRODUCT_OPTION_ID`),
  ADD KEY `PRODUCT_OPTION_NAME_INDEX` (`NAME`);

--
-- Indexes for table `blc_product_option_value`
--
ALTER TABLE `blc_product_option_value`
  ADD PRIMARY KEY (`PRODUCT_OPTION_VALUE_ID`),
  ADD KEY `FK6DEEEDBD92EA8136` (`PRODUCT_OPTION_ID`);

--
-- Indexes for table `blc_product_option_xref`
--
ALTER TABLE `blc_product_option_xref`
  ADD PRIMARY KEY (`PRODUCT_OPTION_XREF_ID`),
  ADD KEY `FKDA42AB2F5F11A0B7` (`PRODUCT_ID`),
  ADD KEY `FKDA42AB2F92EA8136` (`PRODUCT_OPTION_ID`);

--
-- Indexes for table `blc_product_up_sale`
--
ALTER TABLE `blc_product_up_sale`
  ADD PRIMARY KEY (`UP_SALE_PRODUCT_ID`),
  ADD KEY `UPSALE_CATEGORY_INDEX` (`CATEGORY_ID`),
  ADD KEY `UPSALE_PRODUCT_INDEX` (`PRODUCT_ID`),
  ADD KEY `UPSALE_RELATED_INDEX` (`RELATED_SALE_PRODUCT_ID`),
  ADD KEY `FKF69054F515D1A13D` (`CATEGORY_ID`),
  ADD KEY `FKF69054F55F11A0B7` (`PRODUCT_ID`),
  ADD KEY `FKF69054F562D84F9B` (`RELATED_SALE_PRODUCT_ID`);

--
-- Indexes for table `blc_promotion_message`
--
ALTER TABLE `blc_promotion_message`
  ADD PRIMARY KEY (`PROMOTION_MESSAGE_ID`),
  ADD KEY `PROMOTION_MESSAGE_NAME_INDEX` (`NAME`),
  ADD KEY `FKFF297AA5A1E1C128` (`LOCALE_CODE`),
  ADD KEY `FKFF297AA56E4720E0` (`MEDIA_ID`);

--
-- Indexes for table `blc_prorated_order_item_adjust`
--
ALTER TABLE `blc_prorated_order_item_adjust`
  ADD PRIMARY KEY (`PRORATED_ORDER_ITEM_ADJUST_ID`),
  ADD KEY `POIADJUST_ITEM_INDEX` (`ORDER_ITEM_ID`),
  ADD KEY `FK87C0E828D5F3FAF4` (`OFFER_ID`),
  ADD KEY `FK87C0E8289AF166DF` (`ORDER_ITEM_ID`);

--
-- Indexes for table `blc_qual_crit_offer_xref`
--
ALTER TABLE `blc_qual_crit_offer_xref`
  ADD PRIMARY KEY (`OFFER_QUAL_CRIT_ID`),
  ADD KEY `FKD592E919D5F3FAF4` (`OFFER_ID`),
  ADD KEY `FKD592E9193615A91A` (`OFFER_ITEM_CRITERIA_ID`);

--
-- Indexes for table `blc_qual_crit_page_xref`
--
ALTER TABLE `blc_qual_crit_page_xref`
  ADD PRIMARY KEY (`PAGE_ID`,`PAGE_ITEM_CRITERIA_ID`),
  ADD UNIQUE KEY `UK_874BE5902B6BC67F` (`PAGE_ITEM_CRITERIA_ID`),
  ADD KEY `FK874BE590883C2667` (`PAGE_ID`),
  ADD KEY `FK874BE590378418CD` (`PAGE_ITEM_CRITERIA_ID`);

--
-- Indexes for table `blc_qual_crit_sc_xref`
--
ALTER TABLE `blc_qual_crit_sc_xref`
  ADD PRIMARY KEY (`SC_ID`,`SC_ITEM_CRITERIA_ID`),
  ADD UNIQUE KEY `UK_C4A353AFFF06F4DE` (`SC_ITEM_CRITERIA_ID`),
  ADD KEY `FKC4A353AF85C77F2B` (`SC_ITEM_CRITERIA_ID`),
  ADD KEY `FKC4A353AF13D95585` (`SC_ID`);

--
-- Indexes for table `blc_rating_detail`
--
ALTER TABLE `blc_rating_detail`
  ADD PRIMARY KEY (`RATING_DETAIL_ID`),
  ADD KEY `RATING_CUSTOMER_INDEX` (`CUSTOMER_ID`),
  ADD KEY `FKC9D04AD7470F437` (`CUSTOMER_ID`),
  ADD KEY `FKC9D04ADD4E76BF4` (`RATING_SUMMARY_ID`);

--
-- Indexes for table `blc_rating_summary`
--
ALTER TABLE `blc_rating_summary`
  ADD PRIMARY KEY (`RATING_SUMMARY_ID`),
  ADD KEY `RATINGSUMM_ITEM_INDEX` (`ITEM_ID`),
  ADD KEY `RATINGSUMM_TYPE_INDEX` (`RATING_TYPE`);

--
-- Indexes for table `blc_review_detail`
--
ALTER TABLE `blc_review_detail`
  ADD PRIMARY KEY (`REVIEW_DETAIL_ID`),
  ADD KEY `REVIEWDETAIL_CUSTOMER_INDEX` (`CUSTOMER_ID`),
  ADD KEY `REVIEWDETAIL_RATING_INDEX` (`RATING_DETAIL_ID`),
  ADD KEY `REVIEWDETAIL_SUMMARY_INDEX` (`RATING_SUMMARY_ID`),
  ADD KEY `REVIEWDETAIL_STATUS_INDEX` (`REVIEW_STATUS`),
  ADD KEY `FK9CD7E6927470F437` (`CUSTOMER_ID`),
  ADD KEY `FK9CD7E69245DC39E0` (`RATING_DETAIL_ID`),
  ADD KEY `FK9CD7E692D4E76BF4` (`RATING_SUMMARY_ID`);

--
-- Indexes for table `blc_review_feedback`
--
ALTER TABLE `blc_review_feedback`
  ADD PRIMARY KEY (`REVIEW_FEEDBACK_ID`),
  ADD KEY `REVIEWFEED_CUSTOMER_INDEX` (`CUSTOMER_ID`),
  ADD KEY `REVIEWFEED_DETAIL_INDEX` (`REVIEW_DETAIL_ID`),
  ADD KEY `FK7CC929867470F437` (`CUSTOMER_ID`),
  ADD KEY `FK7CC92986AE4769D6` (`REVIEW_DETAIL_ID`);

--
-- Indexes for table `blc_role`
--
ALTER TABLE `blc_role`
  ADD PRIMARY KEY (`ROLE_ID`),
  ADD KEY `ROLE_NAME_INDEX` (`ROLE_NAME`);

--
-- Indexes for table `blc_sandbox`
--
ALTER TABLE `blc_sandbox`
  ADD PRIMARY KEY (`SANDBOX_ID`),
  ADD KEY `SANDBOX_NAME_INDEX` (`SANDBOX_NAME`),
  ADD KEY `FKDD37A9A174160452` (`PARENT_SANDBOX_ID`);

--
-- Indexes for table `blc_sandbox_mgmt`
--
ALTER TABLE `blc_sandbox_mgmt`
  ADD PRIMARY KEY (`SANDBOX_MGMT_ID`),
  ADD UNIQUE KEY `UK_4845009FE52B6993` (`SANDBOX_ID`),
  ADD KEY `FK4845009F579FE59D` (`SANDBOX_ID`);

--
-- Indexes for table `blc_sc`
--
ALTER TABLE `blc_sc`
  ADD PRIMARY KEY (`SC_ID`),
  ADD KEY `CONTENT_NAME_INDEX` (`CONTENT_NAME`),
  ADD KEY `SC_OFFLN_FLG_INDX` (`OFFLINE_FLAG`),
  ADD KEY `CONTENT_PRIORITY_INDEX` (`PRIORITY`),
  ADD KEY `FK74EEB716A1E1C128` (`LOCALE_CODE`),
  ADD KEY `FK74EEB71671EBFA46` (`SC_TYPE_ID`);

--
-- Indexes for table `blc_sc_fld`
--
ALTER TABLE `blc_sc_fld`
  ADD PRIMARY KEY (`SC_FLD_ID`);

--
-- Indexes for table `blc_sc_fldgrp_xref`
--
ALTER TABLE `blc_sc_fldgrp_xref`
  ADD PRIMARY KEY (`BLC_SC_FLDGRP_XREF_ID`),
  ADD KEY `FK71612AEA6A79BDB5` (`FLD_GROUP_ID`),
  ADD KEY `FK71612AEAF6B0BA84` (`SC_FLD_TMPLT_ID`);

--
-- Indexes for table `blc_sc_fld_map`
--
ALTER TABLE `blc_sc_fld_map`
  ADD PRIMARY KEY (`BLC_SC_SC_FIELD_ID`),
  ADD KEY `FKD948019213D95585` (`SC_ID`),
  ADD KEY `FKD9480192DD6FD28A` (`SC_FLD_ID`);

--
-- Indexes for table `blc_sc_fld_tmplt`
--
ALTER TABLE `blc_sc_fld_tmplt`
  ADD PRIMARY KEY (`SC_FLD_TMPLT_ID`);

--
-- Indexes for table `blc_sc_item_criteria`
--
ALTER TABLE `blc_sc_item_criteria`
  ADD PRIMARY KEY (`SC_ITEM_CRITERIA_ID`),
  ADD KEY `FK6D52BDA213D95585` (`SC_ID`);

--
-- Indexes for table `blc_sc_rule`
--
ALTER TABLE `blc_sc_rule`
  ADD PRIMARY KEY (`SC_RULE_ID`);

--
-- Indexes for table `blc_sc_rule_map`
--
ALTER TABLE `blc_sc_rule_map`
  ADD PRIMARY KEY (`BLC_SC_SC_ID`,`MAP_KEY`),
  ADD KEY `FK169F1C8256E51A06` (`SC_RULE_ID`),
  ADD KEY `FK169F1C82156E72FC` (`BLC_SC_SC_ID`);

--
-- Indexes for table `blc_sc_type`
--
ALTER TABLE `blc_sc_type`
  ADD PRIMARY KEY (`SC_TYPE_ID`),
  ADD KEY `SC_TYPE_NAME_INDEX` (`NAME`),
  ADD KEY `FKE19886C3F6B0BA84` (`SC_FLD_TMPLT_ID`);

--
-- Indexes for table `blc_search_facet`
--
ALTER TABLE `blc_search_facet`
  ADD PRIMARY KEY (`SEARCH_FACET_ID`),
  ADD KEY `FK4FFCC986BE3CE566` (`INDEX_FIELD_TYPE_ID`);

--
-- Indexes for table `blc_search_facet_range`
--
ALTER TABLE `blc_search_facet_range`
  ADD PRIMARY KEY (`SEARCH_FACET_RANGE_ID`),
  ADD KEY `FK7EC3B124B96B1C93` (`SEARCH_FACET_ID`),
  ADD KEY `SEARCH_FACET_INDEX` (`SEARCH_FACET_ID`);

--
-- Indexes for table `blc_search_facet_xref`
--
ALTER TABLE `blc_search_facet_xref`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK35A63034DA7E1C7C` (`REQUIRED_FACET_ID`),
  ADD KEY `FK35A63034B96B1C93` (`SEARCH_FACET_ID`);

--
-- Indexes for table `blc_search_intercept`
--
ALTER TABLE `blc_search_intercept`
  ADD PRIMARY KEY (`SEARCH_REDIRECT_ID`),
  ADD KEY `SEARCH_ACTIVE_INDEX` (`ACTIVE_END_DATE`);

--
-- Indexes for table `blc_search_synonym`
--
ALTER TABLE `blc_search_synonym`
  ADD PRIMARY KEY (`SEARCH_SYNONYM_ID`),
  ADD KEY `SEARCHSYNONYM_TERM_INDEX` (`TERM`);

--
-- Indexes for table `blc_shipping_rate`
--
ALTER TABLE `blc_shipping_rate`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `SHIPPINGRATE_FEESUB_INDEX` (`FEE_SUB_TYPE`),
  ADD KEY `SHIPPINGRATE_FEE_INDEX` (`FEE_TYPE`);

--
-- Indexes for table `blc_site`
--
ALTER TABLE `blc_site`
  ADD PRIMARY KEY (`SITE_ID`),
  ADD KEY `BLC_SITE_ID_VAL_INDEX` (`SITE_IDENTIFIER_VALUE`);

--
-- Indexes for table `blc_site_catalog`
--
ALTER TABLE `blc_site_catalog`
  ADD PRIMARY KEY (`SITE_CATALOG_XREF_ID`),
  ADD KEY `FK5F3F2047A350C7F1` (`CATALOG_ID`),
  ADD KEY `FK5F3F2047843A8B63` (`SITE_ID`);

--
-- Indexes for table `blc_site_map_cfg`
--
ALTER TABLE `blc_site_map_cfg`
  ADD PRIMARY KEY (`MODULE_CONFIG_ID`),
  ADD KEY `FK7012930FC50D449` (`MODULE_CONFIG_ID`);

--
-- Indexes for table `blc_site_map_gen_cfg`
--
ALTER TABLE `blc_site_map_gen_cfg`
  ADD PRIMARY KEY (`GEN_CONFIG_ID`),
  ADD KEY `FK1D76000A340ED71` (`MODULE_CONFIG_ID`);

--
-- Indexes for table `blc_site_map_url_entry`
--
ALTER TABLE `blc_site_map_url_entry`
  ADD PRIMARY KEY (`URL_ENTRY_ID`),
  ADD KEY `FKE2004FED36AFE1EE` (`GEN_CONFIG_ID`);

--
-- Indexes for table `blc_sku`
--
ALTER TABLE `blc_sku`
  ADD PRIMARY KEY (`SKU_ID`),
  ADD KEY `FK28E82CF73E2FC4F9` (`CURRENCY_CODE`),
  ADD KEY `SKU_ACTIVE_END_INDEX` (`ACTIVE_END_DATE`),
  ADD KEY `FK28E82CF77E555D75` (`DEFAULT_PRODUCT_ID`),
  ADD KEY `SKU_ACTIVE_START_INDEX` (`ACTIVE_START_DATE`),
  ADD KEY `FK28E82CF750D6498B` (`ADDL_PRODUCT_ID`),
  ADD KEY `SKU_AVAILABLE_INDEX` (`AVAILABLE_FLAG`),
  ADD KEY `SKU_DISCOUNTABLE_INDEX` (`DISCOUNTABLE_FLAG`),
  ADD KEY `SKU_EXTERNAL_ID_INDEX` (`EXTERNAL_ID`),
  ADD KEY `SKU_NAME_INDEX` (`NAME`),
  ADD KEY `SKU_TAXABLE_INDEX` (`TAXABLE_FLAG`),
  ADD KEY `SKU_UPC_INDEX` (`UPC`),
  ADD KEY `SKU_URL_KEY_INDEX` (`URL_KEY`);

--
-- Indexes for table `blc_sku_attribute`
--
ALTER TABLE `blc_sku_attribute`
  ADD PRIMARY KEY (`SKU_ATTR_ID`),
  ADD KEY `FK6C6A5934B78C9977` (`SKU_ID`),
  ADD KEY `SKUATTR_NAME_INDEX` (`NAME`),
  ADD KEY `SKUATTR_SKU_INDEX` (`SKU_ID`);

--
-- Indexes for table `blc_sku_availability`
--
ALTER TABLE `blc_sku_availability`
  ADD PRIMARY KEY (`SKU_AVAILABILITY_ID`),
  ADD KEY `SKUAVAIL_STATUS_INDEX` (`AVAILABILITY_STATUS`),
  ADD KEY `SKUAVAIL_LOCATION_INDEX` (`LOCATION_ID`),
  ADD KEY `SKUAVAIL_SKU_INDEX` (`SKU_ID`);

--
-- Indexes for table `blc_sku_bundle_item`
--
ALTER TABLE `blc_sku_bundle_item`
  ADD PRIMARY KEY (`SKU_BUNDLE_ITEM_ID`),
  ADD KEY `FKD55968CCF29B96` (`PRODUCT_BUNDLE_ID`),
  ADD KEY `FKD55968B78C9977` (`SKU_ID`);

--
-- Indexes for table `blc_sku_fee`
--
ALTER TABLE `blc_sku_fee`
  ADD PRIMARY KEY (`SKU_FEE_ID`),
  ADD KEY `FKEEB7181E3E2FC4F9` (`CURRENCY_CODE`);

--
-- Indexes for table `blc_sku_fee_xref`
--
ALTER TABLE `blc_sku_fee_xref`
  ADD KEY `FKD88D409CB78C9977` (`SKU_ID`),
  ADD KEY `FKD88D409CCF4C9A82` (`SKU_FEE_ID`);

--
-- Indexes for table `blc_sku_fulfillment_excluded`
--
ALTER TABLE `blc_sku_fulfillment_excluded`
  ADD KEY `FK84162D7381F34C7F` (`FULFILLMENT_OPTION_ID`),
  ADD KEY `FK84162D73B78C9977` (`SKU_ID`);

--
-- Indexes for table `blc_sku_fulfillment_flat_rates`
--
ALTER TABLE `blc_sku_fulfillment_flat_rates`
  ADD PRIMARY KEY (`SKU_ID`,`FULFILLMENT_OPTION_ID`),
  ADD KEY `FKC1988C9681F34C7F` (`FULFILLMENT_OPTION_ID`),
  ADD KEY `FKC1988C96B78C9977` (`SKU_ID`);

--
-- Indexes for table `blc_sku_media_map`
--
ALTER TABLE `blc_sku_media_map`
  ADD PRIMARY KEY (`SKU_MEDIA_ID`),
  ADD KEY `FKEB4AECF96E4720E0` (`MEDIA_ID`),
  ADD KEY `FKEB4AECF9D93D857F` (`BLC_SKU_SKU_ID`);

--
-- Indexes for table `blc_sku_option_value_xref`
--
ALTER TABLE `blc_sku_option_value_xref`
  ADD PRIMARY KEY (`SKU_OPTION_VALUE_XREF_ID`),
  ADD KEY `FK7B61DC0BB0C16A73` (`PRODUCT_OPTION_VALUE_ID`),
  ADD KEY `FK7B61DC0BB78C9977` (`SKU_ID`);

--
-- Indexes for table `blc_state`
--
ALTER TABLE `blc_state`
  ADD PRIMARY KEY (`ABBREVIATION`),
  ADD KEY `FK8F94A1EBA46E16CF` (`COUNTRY`),
  ADD KEY `STATE_NAME_INDEX` (`NAME`);

--
-- Indexes for table `blc_static_asset`
--
ALTER TABLE `blc_static_asset`
  ADD PRIMARY KEY (`STATIC_ASSET_ID`),
  ADD KEY `ASST_FULL_URL_INDX` (`FULL_URL`);

--
-- Indexes for table `blc_static_asset_desc`
--
ALTER TABLE `blc_static_asset_desc`
  ADD PRIMARY KEY (`STATIC_ASSET_DESC_ID`);

--
-- Indexes for table `blc_static_asset_strg`
--
ALTER TABLE `blc_static_asset_strg`
  ADD PRIMARY KEY (`STATIC_ASSET_STRG_ID`),
  ADD KEY `STATIC_ASSET_ID_INDEX` (`STATIC_ASSET_ID`);

--
-- Indexes for table `blc_store`
--
ALTER TABLE `blc_store`
  ADD PRIMARY KEY (`STORE_ID`),
  ADD KEY `FK8F94D63BC13085DD` (`ADDRESS_ID`);

--
-- Indexes for table `blc_system_property`
--
ALTER TABLE `blc_system_property`
  ADD PRIMARY KEY (`BLC_SYSTEM_PROPERTY_ID`);

--
-- Indexes for table `blc_tar_crit_offer_xref`
--
ALTER TABLE `blc_tar_crit_offer_xref`
  ADD PRIMARY KEY (`OFFER_TAR_CRIT_ID`),
  ADD KEY `FK125F5803D5F3FAF4` (`OFFER_ID`),
  ADD KEY `FK125F58033615A91A` (`OFFER_ITEM_CRITERIA_ID`);

--
-- Indexes for table `blc_tax_detail`
--
ALTER TABLE `blc_tax_detail`
  ADD PRIMARY KEY (`TAX_DETAIL_ID`),
  ADD KEY `FKEABE4A4B3E2FC4F9` (`CURRENCY_CODE`),
  ADD KEY `FKEABE4A4BC50D449` (`MODULE_CONFIG_ID`);

--
-- Indexes for table `blc_translation`
--
ALTER TABLE `blc_translation`
  ADD PRIMARY KEY (`TRANSLATION_ID`),
  ADD KEY `TRANSLATION_INDEX` (`ENTITY_TYPE`,`ENTITY_ID`,`FIELD_NAME`,`LOCALE_CODE`);

--
-- Indexes for table `blc_trans_additnl_fields`
--
ALTER TABLE `blc_trans_additnl_fields`
  ADD PRIMARY KEY (`PAYMENT_TRANSACTION_ID`,`FIELD_NAME`),
  ADD KEY `FK376DDE4B9E955B1D` (`PAYMENT_TRANSACTION_ID`);

--
-- Indexes for table `blc_url_handler`
--
ALTER TABLE `blc_url_handler`
  ADD PRIMARY KEY (`URL_HANDLER_ID`),
  ADD KEY `INCOMING_URL_INDEX` (`INCOMING_URL`),
  ADD KEY `IS_REGEX_HANDLER_INDEX` (`IS_REGEX`);

--
-- Indexes for table `blc_userconnection`
--
ALTER TABLE `blc_userconnection`
  ADD PRIMARY KEY (`providerId`,`providerUserId`,`userId`);

--
-- Indexes for table `blc_zip_code`
--
ALTER TABLE `blc_zip_code`
  ADD PRIMARY KEY (`ZIP_CODE_ID`),
  ADD KEY `ZIPCODE_CITY_INDEX` (`ZIP_CITY`),
  ADD KEY `ZIPCODE_LATITUDE_INDEX` (`ZIP_LATITUDE`),
  ADD KEY `ZIPCODE_LONGITUDE_INDEX` (`ZIP_LONGITUDE`),
  ADD KEY `ZIPCODE_STATE_INDEX` (`ZIP_STATE`),
  ADD KEY `ZIPCODE_ZIP_INDEX` (`ZIPCODE`);

--
-- Indexes for table `sequence_generator`
--
ALTER TABLE `sequence_generator`
  ADD PRIMARY KEY (`ID_NAME`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blc_additional_offer_info`
--
ALTER TABLE `blc_additional_offer_info`
  ADD CONSTRAINT `FK3BFDBD631891FF79` FOREIGN KEY (`BLC_ORDER_ORDER_ID`) REFERENCES `blc_order` (`ORDER_ID`),
  ADD CONSTRAINT `FK3BFDBD63B5D9C34D` FOREIGN KEY (`OFFER_INFO_ID`) REFERENCES `blc_offer_info` (`OFFER_INFO_ID`),
  ADD CONSTRAINT `FK3BFDBD63D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_address`
--
ALTER TABLE `blc_address`
  ADD CONSTRAINT `FK299F86CE337C4D50` FOREIGN KEY (`STATE_PROV_REGION`) REFERENCES `blc_state` (`ABBREVIATION`),
  ADD CONSTRAINT `FK299F86CE3A39A488` FOREIGN KEY (`ISO_COUNTRY_ALPHA2`) REFERENCES `blc_iso_country` (`ALPHA_2`),
  ADD CONSTRAINT `FK299F86CEA46E16CF` FOREIGN KEY (`COUNTRY`) REFERENCES `blc_country` (`ABBREVIATION`),
  ADD CONSTRAINT `FK299F86CEBF4449BA` FOREIGN KEY (`PHONE_PRIMARY_ID`) REFERENCES `blc_phone` (`PHONE_ID`),
  ADD CONSTRAINT `FK299F86CEE12DC0C8` FOREIGN KEY (`PHONE_SECONDARY_ID`) REFERENCES `blc_phone` (`PHONE_ID`),
  ADD CONSTRAINT `FK299F86CEF1A6533F` FOREIGN KEY (`PHONE_FAX_ID`) REFERENCES `blc_phone` (`PHONE_ID`);

--
-- Constraints for table `blc_admin_permission_entity`
--
ALTER TABLE `blc_admin_permission_entity`
  ADD CONSTRAINT `FK23C09E3DE88B7D38` FOREIGN KEY (`ADMIN_PERMISSION_ID`) REFERENCES `blc_admin_permission` (`ADMIN_PERMISSION_ID`);

--
-- Constraints for table `blc_admin_permission_xref`
--
ALTER TABLE `blc_admin_permission_xref`
  ADD CONSTRAINT `FKBCAD1F575A3C445` FOREIGN KEY (`CHILD_PERMISSION_ID`) REFERENCES `blc_admin_permission` (`ADMIN_PERMISSION_ID`),
  ADD CONSTRAINT `FKBCAD1F5E88B7D38` FOREIGN KEY (`ADMIN_PERMISSION_ID`) REFERENCES `blc_admin_permission` (`ADMIN_PERMISSION_ID`);

--
-- Constraints for table `blc_admin_role_permission_xref`
--
ALTER TABLE `blc_admin_role_permission_xref`
  ADD CONSTRAINT `FK4A819D985F43AAD8` FOREIGN KEY (`ADMIN_ROLE_ID`) REFERENCES `blc_admin_role` (`ADMIN_ROLE_ID`),
  ADD CONSTRAINT `FK4A819D98E88B7D38` FOREIGN KEY (`ADMIN_PERMISSION_ID`) REFERENCES `blc_admin_permission` (`ADMIN_PERMISSION_ID`);

--
-- Constraints for table `blc_admin_section`
--
ALTER TABLE `blc_admin_section`
  ADD CONSTRAINT `FK7EA7D92FB1A18498` FOREIGN KEY (`ADMIN_MODULE_ID`) REFERENCES `blc_admin_module` (`ADMIN_MODULE_ID`);

--
-- Constraints for table `blc_admin_sec_perm_xref`
--
ALTER TABLE `blc_admin_sec_perm_xref`
  ADD CONSTRAINT `FK5E8329663AF7F0FC` FOREIGN KEY (`ADMIN_SECTION_ID`) REFERENCES `blc_admin_section` (`ADMIN_SECTION_ID`),
  ADD CONSTRAINT `FK5E832966E88B7D38` FOREIGN KEY (`ADMIN_PERMISSION_ID`) REFERENCES `blc_admin_permission` (`ADMIN_PERMISSION_ID`);

--
-- Constraints for table `blc_admin_user_addtl_fields`
--
ALTER TABLE `blc_admin_user_addtl_fields`
  ADD CONSTRAINT `FK73274CDD46EBC38` FOREIGN KEY (`ADMIN_USER_ID`) REFERENCES `blc_admin_user` (`ADMIN_USER_ID`);

--
-- Constraints for table `blc_admin_user_permission_xref`
--
ALTER TABLE `blc_admin_user_permission_xref`
  ADD CONSTRAINT `FKF0B3BEED46EBC38` FOREIGN KEY (`ADMIN_USER_ID`) REFERENCES `blc_admin_user` (`ADMIN_USER_ID`),
  ADD CONSTRAINT `FKF0B3BEEDE88B7D38` FOREIGN KEY (`ADMIN_PERMISSION_ID`) REFERENCES `blc_admin_permission` (`ADMIN_PERMISSION_ID`);

--
-- Constraints for table `blc_admin_user_role_xref`
--
ALTER TABLE `blc_admin_user_role_xref`
  ADD CONSTRAINT `FKFFD33A2646EBC38` FOREIGN KEY (`ADMIN_USER_ID`) REFERENCES `blc_admin_user` (`ADMIN_USER_ID`),
  ADD CONSTRAINT `FKFFD33A265F43AAD8` FOREIGN KEY (`ADMIN_ROLE_ID`) REFERENCES `blc_admin_role` (`ADMIN_ROLE_ID`);

--
-- Constraints for table `blc_admin_user_sandbox`
--
ALTER TABLE `blc_admin_user_sandbox`
  ADD CONSTRAINT `FKD0A97E0946EBC38` FOREIGN KEY (`ADMIN_USER_ID`) REFERENCES `blc_admin_user` (`ADMIN_USER_ID`),
  ADD CONSTRAINT `FKD0A97E09579FE59D` FOREIGN KEY (`SANDBOX_ID`) REFERENCES `blc_sandbox` (`SANDBOX_ID`);

--
-- Constraints for table `blc_asset_desc_map`
--
ALTER TABLE `blc_asset_desc_map`
  ADD CONSTRAINT `FKE886BAE367F70B63` FOREIGN KEY (`STATIC_ASSET_ID`) REFERENCES `blc_static_asset` (`STATIC_ASSET_ID`),
  ADD CONSTRAINT `FKE886BAE3E2BA0C9D` FOREIGN KEY (`STATIC_ASSET_DESC_ID`) REFERENCES `blc_static_asset_desc` (`STATIC_ASSET_DESC_ID`);

--
-- Constraints for table `blc_bundle_order_item`
--
ALTER TABLE `blc_bundle_order_item`
  ADD CONSTRAINT `FK489703DB9AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`),
  ADD CONSTRAINT `FK489703DBB78C9977` FOREIGN KEY (`SKU_ID`) REFERENCES `blc_sku` (`SKU_ID`),
  ADD CONSTRAINT `FK489703DBCCF29B96` FOREIGN KEY (`PRODUCT_BUNDLE_ID`) REFERENCES `blc_product_bundle` (`PRODUCT_ID`);

--
-- Constraints for table `blc_bund_item_fee_price`
--
ALTER TABLE `blc_bund_item_fee_price`
  ADD CONSTRAINT `FK14267A943FC68307` FOREIGN KEY (`BUND_ORDER_ITEM_ID`) REFERENCES `blc_bundle_order_item` (`ORDER_ITEM_ID`);

--
-- Constraints for table `blc_candidate_fg_offer`
--
ALTER TABLE `blc_candidate_fg_offer`
  ADD CONSTRAINT `FKCE785605028DC55` FOREIGN KEY (`FULFILLMENT_GROUP_ID`) REFERENCES `blc_fulfillment_group` (`FULFILLMENT_GROUP_ID`),
  ADD CONSTRAINT `FKCE78560D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_candidate_item_offer`
--
ALTER TABLE `blc_candidate_item_offer`
  ADD CONSTRAINT `FK9EEE9B29AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`),
  ADD CONSTRAINT `FK9EEE9B2D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_candidate_order_offer`
--
ALTER TABLE `blc_candidate_order_offer`
  ADD CONSTRAINT `FK6185228989FE8A02` FOREIGN KEY (`ORDER_ID`) REFERENCES `blc_order` (`ORDER_ID`),
  ADD CONSTRAINT `FK61852289D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_category`
--
ALTER TABLE `blc_category`
  ADD CONSTRAINT `FK55F82D44B177E6` FOREIGN KEY (`DEFAULT_PARENT_CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`);

--
-- Constraints for table `blc_category_attribute`
--
ALTER TABLE `blc_category_attribute`
  ADD CONSTRAINT `FK4E441D4115D1A13D` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`);

--
-- Constraints for table `blc_category_media_map`
--
ALTER TABLE `blc_category_media_map`
  ADD CONSTRAINT `FKCD24B1066E4720E0` FOREIGN KEY (`MEDIA_ID`) REFERENCES `blc_media` (`MEDIA_ID`),
  ADD CONSTRAINT `FKCD24B106D786CEA2` FOREIGN KEY (`BLC_CATEGORY_CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`);

--
-- Constraints for table `blc_category_product_xref`
--
ALTER TABLE `blc_category_product_xref`
  ADD CONSTRAINT `FK635EB1A615D1A13D` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`),
  ADD CONSTRAINT `FK635EB1A65F11A0B7` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`);

--
-- Constraints for table `blc_category_xref`
--
ALTER TABLE `blc_category_xref`
  ADD CONSTRAINT `FKE889733615D1A13D` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`),
  ADD CONSTRAINT `FKE8897336D6D45DBE` FOREIGN KEY (`SUB_CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`);

--
-- Constraints for table `blc_cat_search_facet_excl_xref`
--
ALTER TABLE `blc_cat_search_facet_excl_xref`
  ADD CONSTRAINT `FK8361EF4E15D1A13D` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`),
  ADD CONSTRAINT `FK8361EF4EB96B1C93` FOREIGN KEY (`SEARCH_FACET_ID`) REFERENCES `blc_search_facet` (`SEARCH_FACET_ID`);

--
-- Constraints for table `blc_cat_search_facet_xref`
--
ALTER TABLE `blc_cat_search_facet_xref`
  ADD CONSTRAINT `FK32210EEB15D1A13D` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`),
  ADD CONSTRAINT `FK32210EEBB96B1C93` FOREIGN KEY (`SEARCH_FACET_ID`) REFERENCES `blc_search_facet` (`SEARCH_FACET_ID`);

--
-- Constraints for table `blc_cat_site_map_gen_cfg`
--
ALTER TABLE `blc_cat_site_map_gen_cfg`
  ADD CONSTRAINT `FK1BA4E695C5F3D60` FOREIGN KEY (`ROOT_CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`),
  ADD CONSTRAINT `FK1BA4E69BCAB9F56` FOREIGN KEY (`GEN_CONFIG_ID`) REFERENCES `blc_site_map_gen_cfg` (`GEN_CONFIG_ID`);

--
-- Constraints for table `blc_cms_menu_item`
--
ALTER TABLE `blc_cms_menu_item`
  ADD CONSTRAINT `FKFC9BDD73876279D` FOREIGN KEY (`PARENT_MENU_ID`) REFERENCES `blc_cms_menu` (`MENU_ID`),
  ADD CONSTRAINT `FKFC9BDD76E4720E0` FOREIGN KEY (`MEDIA_ID`) REFERENCES `blc_media` (`MEDIA_ID`),
  ADD CONSTRAINT `FKFC9BDD77BB4A41` FOREIGN KEY (`LINKED_PAGE_ID`) REFERENCES `blc_page` (`PAGE_ID`),
  ADD CONSTRAINT `FKFC9BDD7A8D0E60C` FOREIGN KEY (`LINKED_MENU_ID`) REFERENCES `blc_cms_menu` (`MENU_ID`);

--
-- Constraints for table `blc_country_sub`
--
ALTER TABLE `blc_country_sub`
  ADD CONSTRAINT `FKA804FBD172AAB3C0` FOREIGN KEY (`COUNTRY_SUB_CAT`) REFERENCES `blc_country_sub_cat` (`COUNTRY_SUB_CAT_ID`),
  ADD CONSTRAINT `FKA804FBD1A46E16CF` FOREIGN KEY (`COUNTRY`) REFERENCES `blc_country` (`ABBREVIATION`);

--
-- Constraints for table `blc_customer`
--
ALTER TABLE `blc_customer`
  ADD CONSTRAINT `FK7716F0241422B204` FOREIGN KEY (`CHALLENGE_QUESTION_ID`) REFERENCES `blc_challenge_question` (`QUESTION_ID`),
  ADD CONSTRAINT `FK7716F024A1E1C128` FOREIGN KEY (`LOCALE_CODE`) REFERENCES `blc_locale` (`LOCALE_CODE`);

--
-- Constraints for table `blc_customer_address`
--
ALTER TABLE `blc_customer_address`
  ADD CONSTRAINT `FK75B95AB97470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`),
  ADD CONSTRAINT `FK75B95AB9C13085DD` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `blc_address` (`ADDRESS_ID`);

--
-- Constraints for table `blc_customer_attribute`
--
ALTER TABLE `blc_customer_attribute`
  ADD CONSTRAINT `FKB974C8217470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`);

--
-- Constraints for table `blc_customer_offer_xref`
--
ALTER TABLE `blc_customer_offer_xref`
  ADD CONSTRAINT `FK685E80397470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`),
  ADD CONSTRAINT `FK685E8039D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_customer_payment`
--
ALTER TABLE `blc_customer_payment`
  ADD CONSTRAINT `FK8B3DF0CB7470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`),
  ADD CONSTRAINT `FK8B3DF0CBC13085DD` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `blc_address` (`ADDRESS_ID`);

--
-- Constraints for table `blc_customer_payment_fields`
--
ALTER TABLE `blc_customer_payment_fields`
  ADD CONSTRAINT `FK5CCB14ADCA0B98E0` FOREIGN KEY (`CUSTOMER_PAYMENT_ID`) REFERENCES `blc_customer_payment` (`CUSTOMER_PAYMENT_ID`);

--
-- Constraints for table `blc_customer_phone`
--
ALTER TABLE `blc_customer_phone`
  ADD CONSTRAINT `FK3D28ED737470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`),
  ADD CONSTRAINT `FK3D28ED73D894CB5D` FOREIGN KEY (`PHONE_ID`) REFERENCES `blc_phone` (`PHONE_ID`);

--
-- Constraints for table `blc_customer_role`
--
ALTER TABLE `blc_customer_role`
  ADD CONSTRAINT `FK548EB7B17470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`),
  ADD CONSTRAINT `FK548EB7B1B8587B7` FOREIGN KEY (`ROLE_ID`) REFERENCES `blc_role` (`ROLE_ID`);

--
-- Constraints for table `blc_cust_site_map_gen_cfg`
--
ALTER TABLE `blc_cust_site_map_gen_cfg`
  ADD CONSTRAINT `FK67C0DBA0BCAB9F56` FOREIGN KEY (`GEN_CONFIG_ID`) REFERENCES `blc_site_map_gen_cfg` (`GEN_CONFIG_ID`);

--
-- Constraints for table `blc_data_drvn_enum_val`
--
ALTER TABLE `blc_data_drvn_enum_val`
  ADD CONSTRAINT `FKB2D5700DA60E0554` FOREIGN KEY (`ENUM_TYPE`) REFERENCES `blc_data_drvn_enum` (`ENUM_ID`);

--
-- Constraints for table `blc_discrete_order_item`
--
ALTER TABLE `blc_discrete_order_item`
  ADD CONSTRAINT `FKBC3A8A841285903B` FOREIGN KEY (`SKU_BUNDLE_ITEM_ID`) REFERENCES `blc_sku_bundle_item` (`SKU_BUNDLE_ITEM_ID`),
  ADD CONSTRAINT `FKBC3A8A845CDFCA80` FOREIGN KEY (`BUNDLE_ORDER_ITEM_ID`) REFERENCES `blc_bundle_order_item` (`ORDER_ITEM_ID`),
  ADD CONSTRAINT `FKBC3A8A845F11A0B7` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`),
  ADD CONSTRAINT `FKBC3A8A849AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`),
  ADD CONSTRAINT `FKBC3A8A84B78C9977` FOREIGN KEY (`SKU_ID`) REFERENCES `blc_sku` (`SKU_ID`);

--
-- Constraints for table `blc_disc_item_fee_price`
--
ALTER TABLE `blc_disc_item_fee_price`
  ADD CONSTRAINT `FK2A641CC8B76B9466` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_discrete_order_item` (`ORDER_ITEM_ID`);

--
-- Constraints for table `blc_dyn_discrete_order_item`
--
ALTER TABLE `blc_dyn_discrete_order_item`
  ADD CONSTRAINT `FK209DEE9EB76B9466` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_discrete_order_item` (`ORDER_ITEM_ID`);

--
-- Constraints for table `blc_email_tracking_clicks`
--
ALTER TABLE `blc_email_tracking_clicks`
  ADD CONSTRAINT `FKFDF9F52AFA1E5D61` FOREIGN KEY (`EMAIL_TRACKING_ID`) REFERENCES `blc_email_tracking` (`EMAIL_TRACKING_ID`);

--
-- Constraints for table `blc_email_tracking_opens`
--
ALTER TABLE `blc_email_tracking_opens`
  ADD CONSTRAINT `FKA5C3722AFA1E5D61` FOREIGN KEY (`EMAIL_TRACKING_ID`) REFERENCES `blc_email_tracking` (`EMAIL_TRACKING_ID`);

--
-- Constraints for table `blc_fg_adjustment`
--
ALTER TABLE `blc_fg_adjustment`
  ADD CONSTRAINT `FK468C8F255028DC55` FOREIGN KEY (`FULFILLMENT_GROUP_ID`) REFERENCES `blc_fulfillment_group` (`FULFILLMENT_GROUP_ID`),
  ADD CONSTRAINT `FK468C8F25D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_fg_fee_tax_xref`
--
ALTER TABLE `blc_fg_fee_tax_xref`
  ADD CONSTRAINT `FK25426DC0598F6D02` FOREIGN KEY (`FULFILLMENT_GROUP_FEE_ID`) REFERENCES `blc_fulfillment_group_fee` (`FULFILLMENT_GROUP_FEE_ID`),
  ADD CONSTRAINT `FK25426DC071448C19` FOREIGN KEY (`TAX_DETAIL_ID`) REFERENCES `blc_tax_detail` (`TAX_DETAIL_ID`);

--
-- Constraints for table `blc_fg_fg_tax_xref`
--
ALTER TABLE `blc_fg_fg_tax_xref`
  ADD CONSTRAINT `FK61BEA4555028DC55` FOREIGN KEY (`FULFILLMENT_GROUP_ID`) REFERENCES `blc_fulfillment_group` (`FULFILLMENT_GROUP_ID`),
  ADD CONSTRAINT `FK61BEA45571448C19` FOREIGN KEY (`TAX_DETAIL_ID`) REFERENCES `blc_tax_detail` (`TAX_DETAIL_ID`);

--
-- Constraints for table `blc_fg_item_tax_xref`
--
ALTER TABLE `blc_fg_item_tax_xref`
  ADD CONSTRAINT `FKDD3E844371448C19` FOREIGN KEY (`TAX_DETAIL_ID`) REFERENCES `blc_tax_detail` (`TAX_DETAIL_ID`),
  ADD CONSTRAINT `FKDD3E8443E3BBB4D2` FOREIGN KEY (`FULFILLMENT_GROUP_ITEM_ID`) REFERENCES `blc_fulfillment_group_item` (`FULFILLMENT_GROUP_ITEM_ID`);

--
-- Constraints for table `blc_fld_def`
--
ALTER TABLE `blc_fld_def`
  ADD CONSTRAINT `FK3FCB575E38D08AB5` FOREIGN KEY (`ENUM_ID`) REFERENCES `blc_data_drvn_enum` (`ENUM_ID`),
  ADD CONSTRAINT `FK3FCB575E6A79BDB5` FOREIGN KEY (`FLD_GROUP_ID`) REFERENCES `blc_fld_group` (`FLD_GROUP_ID`);

--
-- Constraints for table `blc_fld_enum_item`
--
ALTER TABLE `blc_fld_enum_item`
  ADD CONSTRAINT `FK83A6A84AFD2EA299` FOREIGN KEY (`FLD_ENUM_ID`) REFERENCES `blc_fld_enum` (`FLD_ENUM_ID`);

--
-- Constraints for table `blc_fulfillment_group`
--
ALTER TABLE `blc_fulfillment_group`
  ADD CONSTRAINT `FKC5B9EF1877F565E1` FOREIGN KEY (`PERSONAL_MESSAGE_ID`) REFERENCES `blc_personal_message` (`PERSONAL_MESSAGE_ID`),
  ADD CONSTRAINT `FKC5B9EF1881F34C7F` FOREIGN KEY (`FULFILLMENT_OPTION_ID`) REFERENCES `blc_fulfillment_option` (`FULFILLMENT_OPTION_ID`),
  ADD CONSTRAINT `FKC5B9EF1889FE8A02` FOREIGN KEY (`ORDER_ID`) REFERENCES `blc_order` (`ORDER_ID`),
  ADD CONSTRAINT `FKC5B9EF18C13085DD` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `blc_address` (`ADDRESS_ID`),
  ADD CONSTRAINT `FKC5B9EF18D894CB5D` FOREIGN KEY (`PHONE_ID`) REFERENCES `blc_phone` (`PHONE_ID`);

--
-- Constraints for table `blc_fulfillment_group_fee`
--
ALTER TABLE `blc_fulfillment_group_fee`
  ADD CONSTRAINT `FK6AA8E1BF5028DC55` FOREIGN KEY (`FULFILLMENT_GROUP_ID`) REFERENCES `blc_fulfillment_group` (`FULFILLMENT_GROUP_ID`);

--
-- Constraints for table `blc_fulfillment_group_item`
--
ALTER TABLE `blc_fulfillment_group_item`
  ADD CONSTRAINT `FKEA74EBDA5028DC55` FOREIGN KEY (`FULFILLMENT_GROUP_ID`) REFERENCES `blc_fulfillment_group` (`FULFILLMENT_GROUP_ID`),
  ADD CONSTRAINT `FKEA74EBDA9AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`);

--
-- Constraints for table `blc_fulfillment_option_fixed`
--
ALTER TABLE `blc_fulfillment_option_fixed`
  ADD CONSTRAINT `FK408360313E2FC4F9` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `blc_currency` (`CURRENCY_CODE`),
  ADD CONSTRAINT `FK4083603181F34C7F` FOREIGN KEY (`FULFILLMENT_OPTION_ID`) REFERENCES `blc_fulfillment_option` (`FULFILLMENT_OPTION_ID`);

--
-- Constraints for table `blc_fulfillment_opt_banded_prc`
--
ALTER TABLE `blc_fulfillment_opt_banded_prc`
  ADD CONSTRAINT `FKB1FD71E981F34C7F` FOREIGN KEY (`FULFILLMENT_OPTION_ID`) REFERENCES `blc_fulfillment_option` (`FULFILLMENT_OPTION_ID`);

--
-- Constraints for table `blc_fulfillment_opt_banded_wgt`
--
ALTER TABLE `blc_fulfillment_opt_banded_wgt`
  ADD CONSTRAINT `FKB1FD8AEC81F34C7F` FOREIGN KEY (`FULFILLMENT_OPTION_ID`) REFERENCES `blc_fulfillment_option` (`FULFILLMENT_OPTION_ID`);

--
-- Constraints for table `blc_fulfillment_price_band`
--
ALTER TABLE `blc_fulfillment_price_band`
  ADD CONSTRAINT `FK46C9EA726CDF59CA` FOREIGN KEY (`FULFILLMENT_OPTION_ID`) REFERENCES `blc_fulfillment_opt_banded_prc` (`FULFILLMENT_OPTION_ID`);

--
-- Constraints for table `blc_fulfillment_weight_band`
--
ALTER TABLE `blc_fulfillment_weight_band`
  ADD CONSTRAINT `FK6A048D95A0B429C3` FOREIGN KEY (`FULFILLMENT_OPTION_ID`) REFERENCES `blc_fulfillment_opt_banded_wgt` (`FULFILLMENT_OPTION_ID`);

--
-- Constraints for table `blc_giftwrap_order_item`
--
ALTER TABLE `blc_giftwrap_order_item`
  ADD CONSTRAINT `FKE1BE1563B76B9466` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_discrete_order_item` (`ORDER_ITEM_ID`);

--
-- Constraints for table `blc_img_static_asset`
--
ALTER TABLE `blc_img_static_asset`
  ADD CONSTRAINT `FKCC4B772167F70B63` FOREIGN KEY (`STATIC_ASSET_ID`) REFERENCES `blc_static_asset` (`STATIC_ASSET_ID`);

--
-- Constraints for table `blc_index_field`
--
ALTER TABLE `blc_index_field`
  ADD CONSTRAINT `FK9A53C1073C3907C4` FOREIGN KEY (`FIELD_ID`) REFERENCES `blc_field` (`FIELD_ID`);

--
-- Constraints for table `blc_index_field_type`
--
ALTER TABLE `blc_index_field_type`
  ADD CONSTRAINT `FK4A310B72CBDA280B` FOREIGN KEY (`INDEX_FIELD_ID`) REFERENCES `blc_index_field` (`INDEX_FIELD_ID`);

--
-- Constraints for table `blc_item_offer_qualifier`
--
ALTER TABLE `blc_item_offer_qualifier`
  ADD CONSTRAINT `FKD9C50C619AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`),
  ADD CONSTRAINT `FKD9C50C61D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_locale`
--
ALTER TABLE `blc_locale`
  ADD CONSTRAINT `FK56C7DC203E2FC4F9` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `blc_currency` (`CURRENCY_CODE`);

--
-- Constraints for table `blc_offer_code`
--
ALTER TABLE `blc_offer_code`
  ADD CONSTRAINT `FK76B8C8D6D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_offer_info_fields`
--
ALTER TABLE `blc_offer_info_fields`
  ADD CONSTRAINT `FKA901886183AE7237` FOREIGN KEY (`OFFER_INFO_FIELDS_ID`) REFERENCES `blc_offer_info` (`OFFER_INFO_ID`);

--
-- Constraints for table `blc_offer_rule_map`
--
ALTER TABLE `blc_offer_rule_map`
  ADD CONSTRAINT `FKCA468FE245C66D1D` FOREIGN KEY (`BLC_OFFER_OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`),
  ADD CONSTRAINT `FKCA468FE2C11A218D` FOREIGN KEY (`OFFER_RULE_ID`) REFERENCES `blc_offer_rule` (`OFFER_RULE_ID`);

--
-- Constraints for table `blc_order`
--
ALTER TABLE `blc_order`
  ADD CONSTRAINT `FK8F5B64A83E2FC4F9` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `blc_currency` (`CURRENCY_CODE`),
  ADD CONSTRAINT `FK8F5B64A87470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`),
  ADD CONSTRAINT `FK8F5B64A8A1E1C128` FOREIGN KEY (`LOCALE_CODE`) REFERENCES `blc_locale` (`LOCALE_CODE`);

--
-- Constraints for table `blc_order_adjustment`
--
ALTER TABLE `blc_order_adjustment`
  ADD CONSTRAINT `FK1E92D16489FE8A02` FOREIGN KEY (`ORDER_ID`) REFERENCES `blc_order` (`ORDER_ID`),
  ADD CONSTRAINT `FK1E92D164D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_order_attribute`
--
ALTER TABLE `blc_order_attribute`
  ADD CONSTRAINT `FKB3A467A589FE8A02` FOREIGN KEY (`ORDER_ID`) REFERENCES `blc_order` (`ORDER_ID`);

--
-- Constraints for table `blc_order_item`
--
ALTER TABLE `blc_order_item`
  ADD CONSTRAINT `FK9A2E704A15D1A13D` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`),
  ADD CONSTRAINT `FK9A2E704A77F565E1` FOREIGN KEY (`PERSONAL_MESSAGE_ID`) REFERENCES `blc_personal_message` (`PERSONAL_MESSAGE_ID`),
  ADD CONSTRAINT `FK9A2E704A89FE8A02` FOREIGN KEY (`ORDER_ID`) REFERENCES `blc_order` (`ORDER_ID`),
  ADD CONSTRAINT `FK9A2E704AB0B0D00A` FOREIGN KEY (`PARENT_ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`),
  ADD CONSTRAINT `FK9A2E704AFD2F1F10` FOREIGN KEY (`GIFT_WRAP_ITEM_ID`) REFERENCES `blc_giftwrap_order_item` (`ORDER_ITEM_ID`);

--
-- Constraints for table `blc_order_item_add_attr`
--
ALTER TABLE `blc_order_item_add_attr`
  ADD CONSTRAINT `FKA466AB44B76B9466` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_discrete_order_item` (`ORDER_ITEM_ID`);

--
-- Constraints for table `blc_order_item_adjustment`
--
ALTER TABLE `blc_order_item_adjustment`
  ADD CONSTRAINT `FKA2658C829AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`),
  ADD CONSTRAINT `FKA2658C82D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_order_item_attribute`
--
ALTER TABLE `blc_order_item_attribute`
  ADD CONSTRAINT `FK9F1ED0C79AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`);

--
-- Constraints for table `blc_order_item_cart_message`
--
ALTER TABLE `blc_order_item_cart_message`
  ADD CONSTRAINT `FK5D53C79D9AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`);

--
-- Constraints for table `blc_order_item_dtl_adj`
--
ALTER TABLE `blc_order_item_dtl_adj`
  ADD CONSTRAINT `FK85F0248FD4AEA2C0` FOREIGN KEY (`ORDER_ITEM_PRICE_DTL_ID`) REFERENCES `blc_order_item_price_dtl` (`ORDER_ITEM_PRICE_DTL_ID`),
  ADD CONSTRAINT `FK85F0248FD5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_order_item_price_dtl`
--
ALTER TABLE `blc_order_item_price_dtl`
  ADD CONSTRAINT `FK1FB64BF19AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`);

--
-- Constraints for table `blc_order_multiship_option`
--
ALTER TABLE `blc_order_multiship_option`
  ADD CONSTRAINT `FKB3D3F7D681F34C7F` FOREIGN KEY (`FULFILLMENT_OPTION_ID`) REFERENCES `blc_fulfillment_option` (`FULFILLMENT_OPTION_ID`),
  ADD CONSTRAINT `FKB3D3F7D689FE8A02` FOREIGN KEY (`ORDER_ID`) REFERENCES `blc_order` (`ORDER_ID`),
  ADD CONSTRAINT `FKB3D3F7D69AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`),
  ADD CONSTRAINT `FKB3D3F7D6C13085DD` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `blc_address` (`ADDRESS_ID`);

--
-- Constraints for table `blc_order_offer_code_xref`
--
ALTER TABLE `blc_order_offer_code_xref`
  ADD CONSTRAINT `FKFDF0E8533BB10F6D` FOREIGN KEY (`OFFER_CODE_ID`) REFERENCES `blc_offer_code` (`OFFER_CODE_ID`),
  ADD CONSTRAINT `FKFDF0E85389FE8A02` FOREIGN KEY (`ORDER_ID`) REFERENCES `blc_order` (`ORDER_ID`);

--
-- Constraints for table `blc_order_payment`
--
ALTER TABLE `blc_order_payment`
  ADD CONSTRAINT `FK9517A14F89FE8A02` FOREIGN KEY (`ORDER_ID`) REFERENCES `blc_order` (`ORDER_ID`),
  ADD CONSTRAINT `FK9517A14FC13085DD` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `blc_address` (`ADDRESS_ID`);

--
-- Constraints for table `blc_order_payment_transaction`
--
ALTER TABLE `blc_order_payment_transaction`
  ADD CONSTRAINT `FK86FDE7CE6A69DD9D` FOREIGN KEY (`ORDER_PAYMENT`) REFERENCES `blc_order_payment` (`ORDER_PAYMENT_ID`),
  ADD CONSTRAINT `FK86FDE7CEE1B66C71` FOREIGN KEY (`PARENT_TRANSACTION`) REFERENCES `blc_order_payment_transaction` (`PAYMENT_TRANSACTION_ID`);

--
-- Constraints for table `blc_page`
--
ALTER TABLE `blc_page`
  ADD CONSTRAINT `FKF41BEDD5D49D3961` FOREIGN KEY (`PAGE_TMPLT_ID`) REFERENCES `blc_page_tmplt` (`PAGE_TMPLT_ID`);

--
-- Constraints for table `blc_page_attributes`
--
ALTER TABLE `blc_page_attributes`
  ADD CONSTRAINT `FK4FE27601883C2667` FOREIGN KEY (`PAGE_ID`) REFERENCES `blc_page` (`PAGE_ID`);

--
-- Constraints for table `blc_page_fld`
--
ALTER TABLE `blc_page_fld`
  ADD CONSTRAINT `FK86433AD4883C2667` FOREIGN KEY (`PAGE_ID`) REFERENCES `blc_page` (`PAGE_ID`);

--
-- Constraints for table `blc_page_rule_map`
--
ALTER TABLE `blc_page_rule_map`
  ADD CONSTRAINT `FK1ABA0CA336D91846` FOREIGN KEY (`PAGE_RULE_ID`) REFERENCES `blc_page_rule` (`PAGE_RULE_ID`),
  ADD CONSTRAINT `FK1ABA0CA3C38455DD` FOREIGN KEY (`BLC_PAGE_PAGE_ID`) REFERENCES `blc_page` (`PAGE_ID`);

--
-- Constraints for table `blc_page_tmplt`
--
ALTER TABLE `blc_page_tmplt`
  ADD CONSTRAINT `FK325C9D5A1E1C128` FOREIGN KEY (`LOCALE_CODE`) REFERENCES `blc_locale` (`LOCALE_CODE`);

--
-- Constraints for table `blc_payment_log`
--
ALTER TABLE `blc_payment_log`
  ADD CONSTRAINT `FKA43703453E2FC4F9` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `blc_currency` (`CURRENCY_CODE`),
  ADD CONSTRAINT `FKA43703457470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`);

--
-- Constraints for table `blc_pgtmplt_fldgrp_xref`
--
ALTER TABLE `blc_pgtmplt_fldgrp_xref`
  ADD CONSTRAINT `FK99D625F66A79BDB5` FOREIGN KEY (`FLD_GROUP_ID`) REFERENCES `blc_fld_group` (`FLD_GROUP_ID`),
  ADD CONSTRAINT `FK99D625F6D49D3961` FOREIGN KEY (`PAGE_TMPLT_ID`) REFERENCES `blc_page_tmplt` (`PAGE_TMPLT_ID`);

--
-- Constraints for table `blc_product`
--
ALTER TABLE `blc_product`
  ADD CONSTRAINT `FK5B95B7C96D386535` FOREIGN KEY (`DEFAULT_SKU_ID`) REFERENCES `blc_sku` (`SKU_ID`),
  ADD CONSTRAINT `FK5B95B7C9DF057C3F` FOREIGN KEY (`DEFAULT_CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`);

--
-- Constraints for table `blc_product_attribute`
--
ALTER TABLE `blc_product_attribute`
  ADD CONSTRAINT `FK56CE05865F11A0B7` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`);

--
-- Constraints for table `blc_product_bundle`
--
ALTER TABLE `blc_product_bundle`
  ADD CONSTRAINT `FK8CC5B85F11A0B7` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`);

--
-- Constraints for table `blc_product_cross_sale`
--
ALTER TABLE `blc_product_cross_sale`
  ADD CONSTRAINT `FK8324FB3C15D1A13D` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`),
  ADD CONSTRAINT `FK8324FB3C5F11A0B7` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`),
  ADD CONSTRAINT `FK8324FB3C62D84F9B` FOREIGN KEY (`RELATED_SALE_PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`);

--
-- Constraints for table `blc_product_featured`
--
ALTER TABLE `blc_product_featured`
  ADD CONSTRAINT `FK4C49FFE415D1A13D` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`),
  ADD CONSTRAINT `FK4C49FFE45F11A0B7` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`);

--
-- Constraints for table `blc_product_option_value`
--
ALTER TABLE `blc_product_option_value`
  ADD CONSTRAINT `FK6DEEEDBD92EA8136` FOREIGN KEY (`PRODUCT_OPTION_ID`) REFERENCES `blc_product_option` (`PRODUCT_OPTION_ID`);

--
-- Constraints for table `blc_product_option_xref`
--
ALTER TABLE `blc_product_option_xref`
  ADD CONSTRAINT `FKDA42AB2F5F11A0B7` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`),
  ADD CONSTRAINT `FKDA42AB2F92EA8136` FOREIGN KEY (`PRODUCT_OPTION_ID`) REFERENCES `blc_product_option` (`PRODUCT_OPTION_ID`);

--
-- Constraints for table `blc_product_up_sale`
--
ALTER TABLE `blc_product_up_sale`
  ADD CONSTRAINT `FKF69054F515D1A13D` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `blc_category` (`CATEGORY_ID`),
  ADD CONSTRAINT `FKF69054F55F11A0B7` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`),
  ADD CONSTRAINT `FKF69054F562D84F9B` FOREIGN KEY (`RELATED_SALE_PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`);

--
-- Constraints for table `blc_promotion_message`
--
ALTER TABLE `blc_promotion_message`
  ADD CONSTRAINT `FKFF297AA56E4720E0` FOREIGN KEY (`MEDIA_ID`) REFERENCES `blc_media` (`MEDIA_ID`),
  ADD CONSTRAINT `FKFF297AA5A1E1C128` FOREIGN KEY (`LOCALE_CODE`) REFERENCES `blc_locale` (`LOCALE_CODE`);

--
-- Constraints for table `blc_prorated_order_item_adjust`
--
ALTER TABLE `blc_prorated_order_item_adjust`
  ADD CONSTRAINT `FK87C0E8289AF166DF` FOREIGN KEY (`ORDER_ITEM_ID`) REFERENCES `blc_order_item` (`ORDER_ITEM_ID`),
  ADD CONSTRAINT `FK87C0E828D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_qual_crit_offer_xref`
--
ALTER TABLE `blc_qual_crit_offer_xref`
  ADD CONSTRAINT `FKD592E9193615A91A` FOREIGN KEY (`OFFER_ITEM_CRITERIA_ID`) REFERENCES `blc_offer_item_criteria` (`OFFER_ITEM_CRITERIA_ID`),
  ADD CONSTRAINT `FKD592E919D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_qual_crit_page_xref`
--
ALTER TABLE `blc_qual_crit_page_xref`
  ADD CONSTRAINT `FK874BE590378418CD` FOREIGN KEY (`PAGE_ITEM_CRITERIA_ID`) REFERENCES `blc_page_item_criteria` (`PAGE_ITEM_CRITERIA_ID`),
  ADD CONSTRAINT `FK874BE590883C2667` FOREIGN KEY (`PAGE_ID`) REFERENCES `blc_page` (`PAGE_ID`);

--
-- Constraints for table `blc_qual_crit_sc_xref`
--
ALTER TABLE `blc_qual_crit_sc_xref`
  ADD CONSTRAINT `FKC4A353AF13D95585` FOREIGN KEY (`SC_ID`) REFERENCES `blc_sc` (`SC_ID`),
  ADD CONSTRAINT `FKC4A353AF85C77F2B` FOREIGN KEY (`SC_ITEM_CRITERIA_ID`) REFERENCES `blc_sc_item_criteria` (`SC_ITEM_CRITERIA_ID`);

--
-- Constraints for table `blc_rating_detail`
--
ALTER TABLE `blc_rating_detail`
  ADD CONSTRAINT `FKC9D04AD7470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`),
  ADD CONSTRAINT `FKC9D04ADD4E76BF4` FOREIGN KEY (`RATING_SUMMARY_ID`) REFERENCES `blc_rating_summary` (`RATING_SUMMARY_ID`);

--
-- Constraints for table `blc_review_detail`
--
ALTER TABLE `blc_review_detail`
  ADD CONSTRAINT `FK9CD7E69245DC39E0` FOREIGN KEY (`RATING_DETAIL_ID`) REFERENCES `blc_rating_detail` (`RATING_DETAIL_ID`),
  ADD CONSTRAINT `FK9CD7E6927470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`),
  ADD CONSTRAINT `FK9CD7E692D4E76BF4` FOREIGN KEY (`RATING_SUMMARY_ID`) REFERENCES `blc_rating_summary` (`RATING_SUMMARY_ID`);

--
-- Constraints for table `blc_review_feedback`
--
ALTER TABLE `blc_review_feedback`
  ADD CONSTRAINT `FK7CC929867470F437` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `blc_customer` (`CUSTOMER_ID`),
  ADD CONSTRAINT `FK7CC92986AE4769D6` FOREIGN KEY (`REVIEW_DETAIL_ID`) REFERENCES `blc_review_detail` (`REVIEW_DETAIL_ID`);

--
-- Constraints for table `blc_sandbox`
--
ALTER TABLE `blc_sandbox`
  ADD CONSTRAINT `FKDD37A9A174160452` FOREIGN KEY (`PARENT_SANDBOX_ID`) REFERENCES `blc_sandbox` (`SANDBOX_ID`);

--
-- Constraints for table `blc_sandbox_mgmt`
--
ALTER TABLE `blc_sandbox_mgmt`
  ADD CONSTRAINT `FK4845009F579FE59D` FOREIGN KEY (`SANDBOX_ID`) REFERENCES `blc_sandbox` (`SANDBOX_ID`);

--
-- Constraints for table `blc_sc`
--
ALTER TABLE `blc_sc`
  ADD CONSTRAINT `FK74EEB71671EBFA46` FOREIGN KEY (`SC_TYPE_ID`) REFERENCES `blc_sc_type` (`SC_TYPE_ID`),
  ADD CONSTRAINT `FK74EEB716A1E1C128` FOREIGN KEY (`LOCALE_CODE`) REFERENCES `blc_locale` (`LOCALE_CODE`);

--
-- Constraints for table `blc_sc_fldgrp_xref`
--
ALTER TABLE `blc_sc_fldgrp_xref`
  ADD CONSTRAINT `FK71612AEA6A79BDB5` FOREIGN KEY (`FLD_GROUP_ID`) REFERENCES `blc_fld_group` (`FLD_GROUP_ID`),
  ADD CONSTRAINT `FK71612AEAF6B0BA84` FOREIGN KEY (`SC_FLD_TMPLT_ID`) REFERENCES `blc_sc_fld_tmplt` (`SC_FLD_TMPLT_ID`);

--
-- Constraints for table `blc_sc_fld_map`
--
ALTER TABLE `blc_sc_fld_map`
  ADD CONSTRAINT `FKD948019213D95585` FOREIGN KEY (`SC_ID`) REFERENCES `blc_sc` (`SC_ID`),
  ADD CONSTRAINT `FKD9480192DD6FD28A` FOREIGN KEY (`SC_FLD_ID`) REFERENCES `blc_sc_fld` (`SC_FLD_ID`);

--
-- Constraints for table `blc_sc_item_criteria`
--
ALTER TABLE `blc_sc_item_criteria`
  ADD CONSTRAINT `FK6D52BDA213D95585` FOREIGN KEY (`SC_ID`) REFERENCES `blc_sc` (`SC_ID`);

--
-- Constraints for table `blc_sc_rule_map`
--
ALTER TABLE `blc_sc_rule_map`
  ADD CONSTRAINT `FK169F1C82156E72FC` FOREIGN KEY (`BLC_SC_SC_ID`) REFERENCES `blc_sc` (`SC_ID`),
  ADD CONSTRAINT `FK169F1C8256E51A06` FOREIGN KEY (`SC_RULE_ID`) REFERENCES `blc_sc_rule` (`SC_RULE_ID`);

--
-- Constraints for table `blc_sc_type`
--
ALTER TABLE `blc_sc_type`
  ADD CONSTRAINT `FKE19886C3F6B0BA84` FOREIGN KEY (`SC_FLD_TMPLT_ID`) REFERENCES `blc_sc_fld_tmplt` (`SC_FLD_TMPLT_ID`);

--
-- Constraints for table `blc_search_facet`
--
ALTER TABLE `blc_search_facet`
  ADD CONSTRAINT `FK4FFCC986BE3CE566` FOREIGN KEY (`INDEX_FIELD_TYPE_ID`) REFERENCES `blc_index_field_type` (`INDEX_FIELD_TYPE_ID`);

--
-- Constraints for table `blc_search_facet_range`
--
ALTER TABLE `blc_search_facet_range`
  ADD CONSTRAINT `FK7EC3B124B96B1C93` FOREIGN KEY (`SEARCH_FACET_ID`) REFERENCES `blc_search_facet` (`SEARCH_FACET_ID`);

--
-- Constraints for table `blc_search_facet_xref`
--
ALTER TABLE `blc_search_facet_xref`
  ADD CONSTRAINT `FK35A63034B96B1C93` FOREIGN KEY (`SEARCH_FACET_ID`) REFERENCES `blc_search_facet` (`SEARCH_FACET_ID`),
  ADD CONSTRAINT `FK35A63034DA7E1C7C` FOREIGN KEY (`REQUIRED_FACET_ID`) REFERENCES `blc_search_facet` (`SEARCH_FACET_ID`);

--
-- Constraints for table `blc_site_catalog`
--
ALTER TABLE `blc_site_catalog`
  ADD CONSTRAINT `FK5F3F2047843A8B63` FOREIGN KEY (`SITE_ID`) REFERENCES `blc_site` (`SITE_ID`),
  ADD CONSTRAINT `FK5F3F2047A350C7F1` FOREIGN KEY (`CATALOG_ID`) REFERENCES `blc_catalog` (`CATALOG_ID`);

--
-- Constraints for table `blc_site_map_cfg`
--
ALTER TABLE `blc_site_map_cfg`
  ADD CONSTRAINT `FK7012930FC50D449` FOREIGN KEY (`MODULE_CONFIG_ID`) REFERENCES `blc_module_configuration` (`MODULE_CONFIG_ID`);

--
-- Constraints for table `blc_site_map_gen_cfg`
--
ALTER TABLE `blc_site_map_gen_cfg`
  ADD CONSTRAINT `FK1D76000A340ED71` FOREIGN KEY (`MODULE_CONFIG_ID`) REFERENCES `blc_site_map_cfg` (`MODULE_CONFIG_ID`);

--
-- Constraints for table `blc_site_map_url_entry`
--
ALTER TABLE `blc_site_map_url_entry`
  ADD CONSTRAINT `FKE2004FED36AFE1EE` FOREIGN KEY (`GEN_CONFIG_ID`) REFERENCES `blc_cust_site_map_gen_cfg` (`GEN_CONFIG_ID`);

--
-- Constraints for table `blc_sku`
--
ALTER TABLE `blc_sku`
  ADD CONSTRAINT `FK28E82CF73E2FC4F9` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `blc_currency` (`CURRENCY_CODE`),
  ADD CONSTRAINT `FK28E82CF750D6498B` FOREIGN KEY (`ADDL_PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`),
  ADD CONSTRAINT `FK28E82CF77E555D75` FOREIGN KEY (`DEFAULT_PRODUCT_ID`) REFERENCES `blc_product` (`PRODUCT_ID`);

--
-- Constraints for table `blc_sku_attribute`
--
ALTER TABLE `blc_sku_attribute`
  ADD CONSTRAINT `FK6C6A5934B78C9977` FOREIGN KEY (`SKU_ID`) REFERENCES `blc_sku` (`SKU_ID`);

--
-- Constraints for table `blc_sku_bundle_item`
--
ALTER TABLE `blc_sku_bundle_item`
  ADD CONSTRAINT `FKD55968B78C9977` FOREIGN KEY (`SKU_ID`) REFERENCES `blc_sku` (`SKU_ID`),
  ADD CONSTRAINT `FKD55968CCF29B96` FOREIGN KEY (`PRODUCT_BUNDLE_ID`) REFERENCES `blc_product_bundle` (`PRODUCT_ID`);

--
-- Constraints for table `blc_sku_fee`
--
ALTER TABLE `blc_sku_fee`
  ADD CONSTRAINT `FKEEB7181E3E2FC4F9` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `blc_currency` (`CURRENCY_CODE`);

--
-- Constraints for table `blc_sku_fee_xref`
--
ALTER TABLE `blc_sku_fee_xref`
  ADD CONSTRAINT `FKD88D409CB78C9977` FOREIGN KEY (`SKU_ID`) REFERENCES `blc_sku` (`SKU_ID`),
  ADD CONSTRAINT `FKD88D409CCF4C9A82` FOREIGN KEY (`SKU_FEE_ID`) REFERENCES `blc_sku_fee` (`SKU_FEE_ID`);

--
-- Constraints for table `blc_sku_fulfillment_excluded`
--
ALTER TABLE `blc_sku_fulfillment_excluded`
  ADD CONSTRAINT `FK84162D7381F34C7F` FOREIGN KEY (`FULFILLMENT_OPTION_ID`) REFERENCES `blc_fulfillment_option` (`FULFILLMENT_OPTION_ID`),
  ADD CONSTRAINT `FK84162D73B78C9977` FOREIGN KEY (`SKU_ID`) REFERENCES `blc_sku` (`SKU_ID`);

--
-- Constraints for table `blc_sku_fulfillment_flat_rates`
--
ALTER TABLE `blc_sku_fulfillment_flat_rates`
  ADD CONSTRAINT `FKC1988C9681F34C7F` FOREIGN KEY (`FULFILLMENT_OPTION_ID`) REFERENCES `blc_fulfillment_option` (`FULFILLMENT_OPTION_ID`),
  ADD CONSTRAINT `FKC1988C96B78C9977` FOREIGN KEY (`SKU_ID`) REFERENCES `blc_sku` (`SKU_ID`);

--
-- Constraints for table `blc_sku_media_map`
--
ALTER TABLE `blc_sku_media_map`
  ADD CONSTRAINT `FKEB4AECF96E4720E0` FOREIGN KEY (`MEDIA_ID`) REFERENCES `blc_media` (`MEDIA_ID`),
  ADD CONSTRAINT `FKEB4AECF9D93D857F` FOREIGN KEY (`BLC_SKU_SKU_ID`) REFERENCES `blc_sku` (`SKU_ID`);

--
-- Constraints for table `blc_sku_option_value_xref`
--
ALTER TABLE `blc_sku_option_value_xref`
  ADD CONSTRAINT `FK7B61DC0BB0C16A73` FOREIGN KEY (`PRODUCT_OPTION_VALUE_ID`) REFERENCES `blc_product_option_value` (`PRODUCT_OPTION_VALUE_ID`),
  ADD CONSTRAINT `FK7B61DC0BB78C9977` FOREIGN KEY (`SKU_ID`) REFERENCES `blc_sku` (`SKU_ID`);

--
-- Constraints for table `blc_state`
--
ALTER TABLE `blc_state`
  ADD CONSTRAINT `FK8F94A1EBA46E16CF` FOREIGN KEY (`COUNTRY`) REFERENCES `blc_country` (`ABBREVIATION`);

--
-- Constraints for table `blc_store`
--
ALTER TABLE `blc_store`
  ADD CONSTRAINT `FK8F94D63BC13085DD` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `blc_address` (`ADDRESS_ID`);

--
-- Constraints for table `blc_tar_crit_offer_xref`
--
ALTER TABLE `blc_tar_crit_offer_xref`
  ADD CONSTRAINT `FK125F58033615A91A` FOREIGN KEY (`OFFER_ITEM_CRITERIA_ID`) REFERENCES `blc_offer_item_criteria` (`OFFER_ITEM_CRITERIA_ID`),
  ADD CONSTRAINT `FK125F5803D5F3FAF4` FOREIGN KEY (`OFFER_ID`) REFERENCES `blc_offer` (`OFFER_ID`);

--
-- Constraints for table `blc_tax_detail`
--
ALTER TABLE `blc_tax_detail`
  ADD CONSTRAINT `FKEABE4A4B3E2FC4F9` FOREIGN KEY (`CURRENCY_CODE`) REFERENCES `blc_currency` (`CURRENCY_CODE`),
  ADD CONSTRAINT `FKEABE4A4BC50D449` FOREIGN KEY (`MODULE_CONFIG_ID`) REFERENCES `blc_module_configuration` (`MODULE_CONFIG_ID`);

--
-- Constraints for table `blc_trans_additnl_fields`
--
ALTER TABLE `blc_trans_additnl_fields`
  ADD CONSTRAINT `FK376DDE4B9E955B1D` FOREIGN KEY (`PAYMENT_TRANSACTION_ID`) REFERENCES `blc_order_payment_transaction` (`PAYMENT_TRANSACTION_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
